// O(N^2) solution
// Author: Azizkhan Almakhan
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <string>
#include <cmath>
#include <cstring>
#include <queue>
#include <stack>
#include <algorithm>
#include <sstream>
#include <numeric>
using namespace std;

#define f first
#define s second
#define mp make_pair
#define sz(a) int((a).size())
#define pb push_back
#define all(c) (c).begin(),(c).end()
#define forit(it,S) for(__typeof(S.begin()) it = S.begin(); it != S.end(); ++it)
#ifdef WIN32
#define I64d "%I64d"
#else
#define I64d "%lld"
#endif

typedef pair <int, int> pi;
typedef vector <int> vi;
typedef long long ll;

int a[2222], n;
const int base = 2000000;
int cnt[4444444];

int main() {
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
    
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }
    long long res = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            int x = -(a[i] + a[j]);
            res += cnt[x + base];

        }
        for (int j = 0; j < i; ++j) {
            ++cnt[a[i] + a[j] + base];
        }
    }
    cout << res << endl;
    return 0;
}