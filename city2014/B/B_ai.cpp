#include <cstdio>
#include <iostream>

using namespace std;

typedef long long i64;

const int MAXN = 2000;

int a[MAXN][MAXN];
int h[MAXN][MAXN];
int v[MAXN][MAXN];
int d[MAXN][MAXN];

bool p[1000001];

int main() {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);

	p[0] = p[1] = true;
	for (int i = 2; i * i <= 1000000; ++i) {
		if (p[i]) continue;
		for (int j = i * i; j < 1000000; j += i) {
			p[j] = true;
		}
	}
	int n, m;
	cin >> n >> m;
	int c = 0;
	i64 s = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			cin >> a[i][j];
			h[i][j] = d[i][j] = v[i][j] = a[i][j];
			if (!p[a[i][j]]) {
				++c;
				s += a[i][j];
			}
		}
	}
	for (int k = 2; k <= 6; ++k) {
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) if (a[i][j]) {
				if (i + k <= n) {
					v[i][j] = v[i][j] * 10 + a[i + k - 1][j];
					if (!p[v[i][j]]) {
						++c;
						s += v[i][j];
					}
					if (j + k <= m) {
						d[i][j] = d[i][j] * 10 + a[i + k - 1][j + k - 1];
						if (!p[d[i][j]]) {
							++c;
							s += d[i][j];
						}
					}
				}
				if (j + k <= m) {
					h[i][j] = h[i][j] * 10 + a[i][j + k - 1];
					if (!p[h[i][j]]) {
						++c;
						s += h[i][j];
					}
				}
			}
		}
	}
	cout << c << " " << s << endl;
	return 0;
}
