#pragma comment(linker, "/STACK:64000000")
#include<iostream>
#include<cstdio>
#include<cassert>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<vector>
#include<stack>
#include<queue>
#include<deque>
#include<string>
#include<map>
#include<set>
#include<sstream>
#include<algorithm>
#define forit(it,S) for(__typeof(S) it = (S).begin(); it != (S).end(); it++)
#define sz(x) (int)(x).size()
#define all(x) (x).begin(),(x).end()
#define ll long long
#define bit __builtin_popcountll
#define sqr(x) (x) * (x)
using namespace std;
typedef pair<int, int> pii;
const double eps = 1e-9;
const double pi = acos(-1.0);
int main() {
	freopen("H.in", "r", stdin);
	freopen("H.out", "w", stdout);

	int T; cin >> T;
	while(T--) {
		int n,m; cin >> n >> m;
		puts(n % 2 == 1 ? "FIRST" : "SECOND");
	}
	return 0;
}
