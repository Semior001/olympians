#include <iostream>
#include <cstdio>

using namespace std;

typedef long long i64;

#define abs(x) ((x) < 0 ? (-(x)) : (x))
#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

const int MAXN = 500;

int a[MAXN][MAXN];
int b[MAXN * 3][MAXN * 3];
i64 c[MAXN * 3][MAXN * 3];
bool f[MAXN * 3][MAXN * 3];

int main() {
	freopen("G.in", "r", stdin);
	freopen("G.out", "w", stdout);
	int n, m;
	scanf("%d %d", &n, &m);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			scanf("%d", &a[i][j]);
			b[i + j][n - 1 + j - i] = a[i][j];
			f[i + j][n - 1 + j - i] = true;
		}
	}
	int k = n + m - 1;
/*	for (int i = 0; i < k; ++i) {
		for (int j = 0; j < k; ++j) {
			cout << b[i][j] << " ";
		}
		cout << endl;
	}
*/
	for (int i = 0; i < k; ++i) {
		c[i][0] = b[i][0];
		for (int j = 1; j < k; ++j) {
			c[i][j] = c[i][j - 1] + b[i][j];
		}
		for (int j = 0; j < k; ++j) {
			c[i][j] += c[i - 1][j];
		}
	}
	i64 result = -2000000000;
	for (int i = 0; i < k; ++i) {
		for (int j = 0; j < k; ++j) {
			if (f[i][j]) {
				int x = i, y = j;
				while (x >= 0 && y >= 0 && f[x][y] && f[x][j] && f[i][y]) {
					i64 s = c[i][j] - (x ? c[x - 1][j] : 0) - (y ? c[i][y - 1] : 0) + (x && y ? c[x - 1][y - 1] : 0);
					if (s > result) {
						result = s;
						/*if (result == 32) {
							cout << x << " " << y << " " << i << " " << j << endl;
						}*/
					}
					x -= 2, y -= 2;
				}
			}
		}
	}
	cout << result << endl;

	return 0;
}
