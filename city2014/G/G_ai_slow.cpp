#include <iostream>
#include <cstdio>

using namespace std;

typedef long long i64;

#define abs(x) ((x) < 0 ? (-(x)) : (x))
#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

const int MAXN = 500;

int a[MAXN][MAXN];

int main() {
	freopen("G.in", "r", stdin);
	freopen("G.out", "w", stdout);

	int n, m;
	scanf("%d %d", &n, &m);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			scanf("%d", &a[i][j]);
		}
	}

	bool ok = false;
	i64 result = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			int K = min(min(i + 1, j + 1), min(n - i, m - j));
			for (int k = 1; k <= K; ++k) {
				i64 r = 0;
				for (int x = -k + 1; x < k; ++x) {
					for (int y = -(k - abs(x)) + 1; y < k - abs(x); ++y) {
						r += a[i + x][j + y];
					}
				}
				if (!ok || r > result) {
					ok = true;
					result = r;
				}
			}
		}
	}
	cout << result << endl;
	return 0;
}
