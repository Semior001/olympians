#include <bits/stdc++.h>

using namespace std;
int main(){
	int n;
	cin >> n;
	int i;
	int a[n];
	for(i=0;i<n;i++){
		cin >> a[i];
	}
	bool isSum = true;
	while(n>1){
		if(isSum){
			for(i = 0;i<(n-1);i++){
				a[i] = a[i] + a[i+1];
			}
			n--;
			isSum = false;
		}else{
			for(i = 0;i<(n-1);i++){
				a[i] = a[i] * a[i+1];
			}
			n--;
			isSum = true;
		}
	}
	cout << a[0] << endl;
	return 0;
}