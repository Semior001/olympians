#include <iostream>
#include <cstdio>

using namespace std;

typedef long long i64;

const int P = 1000000007;

i64 a[1000];

int main() {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);

	int n;
	cin >> n;
	for (int i = 0; i < n; ++i) {
		cin >> a[i];
	}
	for (int i = 0; n > 1; ++i) {
		for (int j = 0; j < n - 1; ++j) {
			a[j] += a[j + 1];
			if (a[j] >= P) a[j] -= P;
		}
		--n;
		for (int j = 0; j < n - 1; ++j) {
			a[j] *= a[j + 1];
			a[j] %= P;
		}
		--n;
	}
	cout << a[0] << endl;
	return 0;
}
