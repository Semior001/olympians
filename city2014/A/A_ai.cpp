#include <iostream>
#include <cstdio>

using namespace std;

typedef long long i64;

int main() {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);

	i64 a, b, c, d;
	cin >> a >> b >> c >> d;
	i64 x1 = (i64)(d * a * 1.0 / b + 0.5); 
	if (x1 < 1) x1 = 1;
	if (x1 > c) x1 = c;
	i64 y1 = d;
	i64 x2 = c;
	i64 y2 = (i64)(c * b * 1.0 / a + 0.5); 
	if (y2 < 1) y2 = 1;
	if (y2 > d) y2 = d;
	double q1 = a * 1.0 / b - x1 * 1.0 / y1;
	double q2 = a * 1.0 / b - x2 * 1.0 / y2;
	if (q1 < 0) q1 = -q1;
	if (q2 < 0) q2 = -q2;
	if (q1 < q2) {
		cout << x1 << " " << y1 << endl;
	} else {
		cout << x2 << " " << y2 << endl;
	}
	return 0;
}
