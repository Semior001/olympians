#include <iostream>
#include <cstdio>
#include <set>
#include <utility>
#include <cassert>

using namespace std;

typedef long long i64;

#define max(x, y) ((x) > (y) ? (x) : (y))

const int MAXN = 1000000;

bool u[MAXN * 2];
int a[MAXN * 2];
int b[MAXN * 2];

int main() {
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);

	int n;
	scanf("%d", &n);
	multiset<pair<i64, int> > s;
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
		s.insert(make_pair(a[i], 0));
	}
	for (int i = 0; i < n - 1; ++i) {
		pair<i64, int> p1 = *s.begin();
		s.erase(s.begin());
		pair<i64, int> p2 = *s.begin();
		s.erase(s.begin());
		s.insert(make_pair(p1.first + p2.first, max(p1.second, p2.second) + 1));
	}
	assert(s.size() == 1);
	cout << s.begin()->second << endl;
	return 0;
}
