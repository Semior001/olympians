#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

bool isPalindrom(long long l){
    int i, j;
    vector<int> s;
    while(l>0){
        s.push_back(l%10);
        l/=10;
    }
    i = 0;
    j = s.size() - 1;
    while(i < j && s[i] == s[j]){
       i++;
       j--;
    }
    return i >= j;
}

int main(){
    long long x;
    cin >> x;
    x++;
    while(!isPalindrom(x)){
        x++;
    }
    cout << x << endl;
    return 0;
}