// Brute force
// Author: Azizkhan Almakhan

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

int a[1111], an;

bool palindrome(int x) {
    an = 0;
    while (x > 0) {
        a[an++] = x % 10;
        x /= 10;
    }
    for (int i = 0, j = an - 1; i < j; ++i, --j) {
        if (a[i] != a[j]) return false;
    }
    return true;
}

int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
    int n;
    scanf("%d", &n);
    do {
        ++n;
    } while (!palindrome(n));
    cout << n << endl;
	return 0;
}

