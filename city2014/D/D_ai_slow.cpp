#include <iostream>
#include <cstdio>

using namespace std;

typedef long long i64;

bool ok(i64 n) {
	int a[100];
	int k = 0;
	do {
		a[k++] = n % 10;
		n /= 10;
	} while (n);
	for (int i = 0; i < k >> 1; ++i) {
		if (a[i] != a[k - 1 - i]) return false;
	}
	return true;
}

int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	
	i64 n;
	cin >> n;
	++n;
	while (!ok(n)) {
		++n;
	}
	if (n < 0) {
		cout << 1 << endl;
		return 0;
	}
	cout << n << endl;
	return 0;
}
