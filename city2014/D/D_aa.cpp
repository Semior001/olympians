// Linear solution
// Author: Azizkhan Almakhan

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

char s[1000005];
int len;
bool ninee[1000005], palin[1000005];

void solve(int l, int r) {
	//cout << l << " " << r << endl;
	if (r < l) return;
	if (l == r) {
		++s[l];
		return;
	}
	if (palin[l + 1] && s[r] < s[l]) {
		s[r] = s[l];
		return;
	}
	if (ninee[l + 1]) {
		++s[l];
		s[r] = s[l];
		for (int i = l + 1; i < r; ++i)
			s[i] = '0';
		return;
	}
	s[r] = s[l];
	solve(l + 1, r - 1);
}

void precalc() {
	int i = (len - 1) / 2, j;
	ninee[i + 1] = palin[i + 1] = 1;
	bool nin = 1, pal = 1;
	for (j = 0; j <= i; ++j) {
		if (len & 1) {
			ninee[i - j] = ((s[i - j] == '9' && s[i + j] == '9') && nin);
			palin[i - j] = ((s[i - j] == s[i + j]) && pal);
		} else {
			ninee[i - j] = ((s[i - j] == '9' && s[i + j + 1] == '9') && nin);
			palin[i - j] = ((s[i - j] == s[i + j + 1]) && pal);
		}
		nin = ninee[i - j];
		pal = palin[i - j];
	}
}

int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);

	int t, i;

    gets(s);
    len = strlen(s);
    precalc();
	
    if (ninee[0]) {
        s[0] = '1';
        s[len] = '1';
        for (i = 1; i < len; i++)
            s[i] = '0';
        ++len;
    } else {
        solve(0, len - 1);
    }
    
    for (i = 0; i < len; ++i)
        putchar(s[i]);
    puts("");
	return 0;
}

