#include <bits/stdc++.h>
using namespace std;
int main(){
    int n,i;
    cin >> n;
    double v[n], p[n];
    for(i = 0; i < n; i++)
        cin >> v[i];
    for(i = 0; i < n; i++){
        cin >> p[i];
        v[i] = v[i] - v[i] * p[i];
    }
    int maximum = 0;
    for(i = 1; i < n; i++){
        if(v[i]>v[maximum]){
            maximum = i;
        }
    }
    cout << maximum + 1 << endl;
    return 0;
}