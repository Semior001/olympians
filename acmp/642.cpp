#include <bits/stdc++.h>
using namespace std;
int main(){
    int n,s,i;
    cin >> n >> s;
    int a[n];
    for(i = 0; i < n; i++){
        cin >> a[i];
    }
    sort(a,a+n);
    int count = 0;
    for(i = 0; i < n; i++){
        if(s-a[i]>=0){
            s-=a[i];
            count++;
        }else{
            break;
        }
    }
    cout << count << endl;
    return 0;
}