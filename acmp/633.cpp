#include <bits/stdc++.h>
using namespace std;
void bubbleSort(string* arr, int size){
    string tmp;
    int i, j;
    for(i=0; i<size-1; ++i){
        for (j=0; j<size-1; ++j){
            if(arr[j+1] < arr[j]){
                tmp = arr[j+1];
                arr[j+1] = arr[j];
                arr[j] = tmp;
            }
        }
    }
}
int main(){
    string n,s[3],temp;
    int i;
    getline(cin, n);
    for(int i = 0; i < 3; i++){
        getline(cin, s[i]);
    }
    bubbleSort(s, 3);
    /*int end, j;
    bool sorted = false;
    while(!sorted){
        sorted = true;
        for(i = 0; i < 2; i++){
            for(j = 0; j < min(s[i].length(), s[i+1].length()); j++){
                if(s[i][j]>s[i+1][j]){
                    temp = s[i];
                    s[i] = s[i+1];
                    s[i+1] = temp;
                    sorted = false;
                    break;
                }
            }
        }
    }*/
    cout << n << ": ";
    for(int i = 0; i < 2; i++){
        cout << s[i] << ", ";
    }
    cout << s[2] << endl;
    return 0;
}