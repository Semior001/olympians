#include <bits/stdc++.h>
using namespace std;
int convert(long long n){
    int sum = 0;
    while(n>0){
        sum += n%2;
        n/=2;
    }
    return sum;
}
int main(){
    long long n;
    cin >> n;
    cout << n + convert(n) << endl;
    return 0;
}