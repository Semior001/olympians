#include <bits/stdc++.h>
using namespace std;
int main(){
    int n,m,i,j;
    cin >> n >> m;
    char s[n][m];
    int count,minimum = INT_MAX;
    for(i = 0; i < n; i++){
        count = 0;
        for(j = 0; j < m; j++){
            cin >> s[i][j];
            if(s[i][j]=='.'){
                count++;
            }
        }
        minimum = min(count,minimum);
    }
    cout << minimum << endl;
    return 0;
}