#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <utility>
typedef long long ll;
using namespace std;

ll check ( ll number, ll k )
{
    ll x=1,flag=0, i = 0;
    do {
        x*=k;
        ++i;
        if ( x > number ) {
            break;
        }
        if ( x == number ) {
            return i;
            flag=1;
        }
    } while ( flag != 1 );
    return -1;
}

int main(){
    ll l,r,k;
    cin >> l >> r >> k;
    ll i;
    vector<ll> stepeni;
    ll power;
    if(l<=1){
        power = 0;
    }else{
        for(i = l; i <= r; ++i){
            power = check(i,k);
            if(power>-1){
                break;
            }
        }
    }

    if(power==-1){
        cout << "-1" << endl;
        return 0;
    }

    ll result;

    for(i = power; pow(k,i)<=r; ++i){
        result = pow(k,i);
        cout << result << " ";
    }

    return 0;
}
