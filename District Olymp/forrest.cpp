#include <iostream>

using namespace std;
int main() {
	long long n, k, len, mid;
	cin >> n >> k;
	n = 1;
	len = 2;
	while (len <= k) {
		len *= 2;
		n++;
	}
	len --;
	while (len > 0) {
		mid = (len+1)/2;
		if (k > mid) {
			k = k - mid;
		} else if (k == mid) {
			cout << n << endl;
			break;
		}
		n--;
		len = mid - 1;
	}
	return 0;
}