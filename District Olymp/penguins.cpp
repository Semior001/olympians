#include <iostream>
#include <string>
using namespace std;

int main(){
	string a;
	int n;
	cin >> n;
	int e=0,l=0,m=0;
	for(int i=0;i<=n;i++){
		getline(cin,a);
		if(a=="Emperor Penguin"){
			e++;
		}
		if(a=="Little Penguin"){
			l++;
		}
		if(a=="Macaroni Penguin"){
			m++;
		}
	}
	if(e>l && e>m){
		cout << "Emperor Penguin" << endl;
	}
	if(l>e && l>m){
		cout << "Little Penguin" << endl;
	}
	if(m>e && m>l){
		cout << "Macaroni Penguin" << endl;
	}
	return 0;
}