#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>

using namespace std;

int main(){
	int n;
	cin >> n;
	vector<string> ops;
	while(n>0){
		if(n%10==0){
			ops.push_back("KOBEITU");
			n/=10;
		}else{
			ops.push_back("KOSU");
			n--;
		}
	}
	for(int i = ops.size()-1;i>=0;i--){
		cout << ops[i] << endl;
	}
	return 0;
}