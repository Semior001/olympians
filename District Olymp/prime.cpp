#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

bool is_prime(int n) {
	for (int i=2;i<=sqrt(n);i++)
		if (n%i==0)
			return false;
	return true;
}

int main() {
	long long n, i;
	vector<int> divs;
	cin >> n;
	for (i=2;i<=sqrt(n);i++) {
		if (n%i==0) {
			if (is_prime(i))
				divs.push_back(i);
			if (i != n/i && is_prime(n/i))
				divs.push_back(n/i);
		}
	}
	if (is_prime(n))
		divs.push_back(n);

	sort(divs.begin(), divs.end());

	for (i=0;i<divs.size();i++)
		cout << divs[i] << endl;
	return 0;
}