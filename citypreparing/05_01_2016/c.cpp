#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
using namespace std;

struct compare_length {
    bool operator()(const string &l, const string &r) const {
        return l.size()>r.size();
    }
};

int main() {
	unsigned int start_time =  clock(); // начальное время
	int i, j, n;
	cin >> n;
	vector<string> com(n);
	string name, press;
	map<string, string> com_name;
	for(i=0;i<n;i++){
		cin >> com[i] >> name;
		com_name[com[i]] = name;
	}
	cin >> press;
	sort(com.begin(), com.end(), compare_length());
	// НАДО УСКОРИТЬ!
	i = 0;
	while(i<press.length()){
		for(j=0;j<n;j++){
			if(press.compare(i, com[j].length(), com[j]) == 0){
				cout << com_name[com[j]] << endl;
				i += com[j].length();
				break;
			}
		}
	}
	unsigned int search_time = clock() - start_time; // искомое время
	cout << "---------------------" << endl;
	cout<<(double)(search_time)/CLOCKS_PER_SEC<<endl;
	return 0;
}
