#include <iostream>
#include <ctime>
using namespace std;
int main() {
	unsigned int start_time =  clock(); // начальное время
	int i, j, n;
	cin >> n;
	int a[n], l[n], r[n];
	for (i=0;i<n;i++) {
		cin >> a[i];
		l[i] = 0;
		r[i] = 0;
	}
	int largest;
	for (i=0;i<n;i++) {
		largest = a[i];
		for (j=i-1;j>=0;j--){
			if (a[j] >= largest) {
				l[i]++;
				largest = a[j];
			}
		}
		largest = a[i];
		for (j=i+1;j<n;j++){
			if (a[j] >= largest){
				r[i]++;
				largest = a[j];
			}
		}
	}
	for (i=0;i<n;i++) {
		cout << l[i] << " " << r[i] << endl;
	}
	unsigned int search_time = clock() - start_time; // искомое время
	cout << "---------------------" << endl;
	cout<<(double)(search_time)/CLOCKS_PER_SEC<<endl;
	return 0;
}