#include <iostream>
#include <fstream>

using namespace std;
int main() {

	int a, b;
	cin >> a >> b;
	int count = 0;
	while (a != b) {
		if (b%3==0 && b/3>=a)
			b /= 3;
		else if (b%2==0 && b/2>=a)
			b /= 2;
		else
			b --;
		count ++;
	}
	cout << count << endl;

	return 0;
}