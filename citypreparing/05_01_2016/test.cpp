#include <iostream>
#include <string>
#include <cstring>
 
using namespace std;
 
int main()
{
    char str[15]; 
    strcpy(str, "Hello world!");
    char *c = str;
    c = c + 5; // на 6-й  символ в строке
    char buf[5];
    strncpy(buf, c, 5);
    cout << buf << endl;
    return 0;
}  