#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <queue>
#include <utility>
#include <map>
#include <fstream>
#include <vector>
using namespace std;

struct compare_length{
    bool operator(const string &l, const string &r) const{
            return l.size()>r.size();
    }
};

int count_of_substrings(string src, string sub){
    int start = 0;
    int count = 0;
    int pos = 0;
    for(;;){
        pos = src.find(sub.c_str(),start);
        if (pos != -1){
            start = pos + sub.size();
            count++;
        } else {
            break;
        }
    }
    return count;
    }

int main(){
    ifstream fin("c.in");
    ofstream fout("c.out");
    int n;
    fin >> n;
    string cmd[n];
    map<string,string> names;
    int i;
    string a,b;
    for(i = 0; i<n;i++){
        fin >> a >> b;
        names.insert(pair<string,string>(a,b));
        cmd[i]=a;
    }
    sort(store, store+n, compare_length());
    string commands;
    fin >> commands;
    
    return 0;
}