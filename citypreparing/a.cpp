#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    int a,b;
    cin >> a >> b;
    int count = 0;
    while(b>a){
        if(b%3==0 && b/3>=a){
            b/=3;
            count++;
        }else if(b%2==0 && b/2>=a){
            b/=2;
            count++;
        }else{
            b--;
            count++;
        }
    }
    cout << count << endl;
    return 0;
}
