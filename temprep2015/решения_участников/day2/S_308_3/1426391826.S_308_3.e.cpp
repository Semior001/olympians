 #include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <set>

using namespace std;

int n,x,a[200001],i,mn,kol,mx,kol2,f,diff,len;
bool was[200001];
struct Pair {
	double x;
	int pos;
}ans[200001];

bool cmp(Pair q, Pair w) {
	return q.x < w.x || (q.x == w.x && q.pos >w.pos);
}

void dfs(int v, int pr = 1) {
	was[v] = true;
if (pr == 2*n) {
		sort(ans+1,ans+len+1,cmp);
		for (i = 1; i <= len; ++i)
		if (ans[i].pos == 1) {
			mx = max(mx, ans[i].pos);
			mn = min(mn, ans[i].pos);
            break;
		}
		return;
	}
	for (int j = 1; j <= n*2; ++j) {
		if (!was[j]) {
                cerr << a[v] << " " << a[j] << endl;
			if (pr % 2 == 1)
			ans[++len].x = double(a[v] + a[j])/2;
			if (pr == 1)
			ans[len].pos = 1;
			else
			ans[len].pos = 0;
			dfs(j, pr+1);
            --len;
	}
	}
	was[v] = false;
}

int main() {
	freopen("E.in","r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
    mn = 10000000;
    mx = -1000000;
	ios_base::sync_with_stdio(false);
	for (i = 1; i <= n*2; ++i) {
		cin >> a[i];
	}
	dfs(1);
    cout << mn << " " << mx;
	return 0;
}
