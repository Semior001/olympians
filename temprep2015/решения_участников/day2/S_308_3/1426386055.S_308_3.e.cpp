#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <string>

using namespace std;

int n,a[200001], kol,kol2,i,j;
bool was[200001], was1[200001],ok;
long double x, mn,mx;
long double eps = 0.0000001;
int main() {
	freopen("E.in","r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	cin >> x;
	for(i =1; i< n*2; ++i)
	cin >> a[i];
	sort(a+1,a+n*2);
	mn = x+a[1];
	kol = 1;
	for (i = 2; i < n*2; ++i) {
		ok = false;
		if (was[i])
			continue;
		for (j = i+1; j < n*2; ++j) {
			if (was[j] == false && double(a[i] + a[j])/2 - double(mn)/2 > eps) {
				was[j] = true;
				kol++;
			ok = true;
				break;
			}
		}
	}
	mx = x+a[n];
	kol2 = n;
	for (i = n*2-2; i >= 1; --i) {
		ok = false;
		if(was1[i])
		continue;
		for (j = i-1; j  >= 1; --j) {
			if (was1[j] == false && double(a[i] + a[j])/2 - double(mx)/2 <= eps) {
				was1[j] = true;
				kol2--;
				ok = true;
				break;
			}
		}
	}
	cout << kol2 << " " <<kol;
}
