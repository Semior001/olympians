#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>
#include <cassert>

using namespace std;

const int MOD = (int) 1e9 + 7;

int n, l, r, a;

int dp [10][(int) 1e6 + 1];

int gcd (int a, int b) {
	return (b ? gcd (b, a % b) : a);
}

int pw [15][(int) 1e5], cnt [11][(int) 1e4];

int calc (int pos = 0, int cur = a, int last = 0) {
	if (pos == n) {
		if (cur == 1) {
			return 1;
		} else {
			return 0;
		}
	}
	if (dp [pos][cur] != -1) {
		return dp [pos][cur];
	}
	/*
	if (cur == 1) {
		dp [pos][cur] = pw [n - pos + 1][r - last + 1];
		return dp [pos][cur];
	}
	*/
	dp [pos][cur] = 0;
	for (int i = max (l, last + 1); i <= r; ++ i) {
		dp [pos][cur] = (dp [pos][cur] + calc (pos + 1, cur / gcd (cur, i), i)) % MOD;
	}
	return dp [pos][cur];
}

int lcm (int a, int b) {
	return a * b / gcd (a, b);
}

int tmp [15], ans;
void slow (int posN, int pos = 0, int last = 0) {
	if (pos == posN) {
		int cur = 1;
		for (int i = 0; i < pos; ++ i) {
			cur = lcm (cur, tmp [i]);			                
		}
		if (cur % a == 0) {
			++ ans;
			/*
			for (int i = 0; i < pos; ++ i) {
				cout << tmp [i] << " ";
			}
			cout << endl;
			*/
		}
		return;
	}	
	for (int i = max (l, last); i <= r; ++ i) {
		tmp [pos] = i;
		slow (posN, pos + 1, i);
	}
}

void make () {
	for (int i = 1; i <= (r - l + 1); ++ i) {
		cnt [1][i] = 1;
	}
	for (int i = 2; i <= n; ++ i) {
		for (int j = 1; j <= (r - l + 1); ++ j) {
			for (int k = 1; k <= j; ++ k) {
				cnt [i][j] += cnt [i - 1][k];
			}
			pw [i][j] = pw [i][j - 1] + cnt [i][j];
		}
	}
}

int main () {
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	//memset (dp, -1, sizeof dp);
	cin >> n >> l >> r >> a;
	slow (n);
	cout << ans << endl;
	return 0;
	make ();
	cout << calc () << endl;
	return 0;
}
