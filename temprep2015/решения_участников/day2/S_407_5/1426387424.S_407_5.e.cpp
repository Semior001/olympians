#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>
#include <cassert>

using namespace std;

const int MAX_N = 10000;
const int INF = (int) 1e5;

int mn = INF + 1, mnid, mx, mxid;

int n, a [MAX_N];

vector < int > g [MAX_N]; 

int mt [MAX_N];
bool used [MAX_N];
bool dfs (int v) {
	if (used [v]) {
		return 0;
	}	
	used [v] = 1;
	for (int i = 0; i < g [v].size (); ++ i) {
		if (mt [g [v][i]] == -1 || dfs (mt [g [v][i]])) {
			mt [g [v][i]] = v;
			return 1;
		}
	} 
	return 0;
}

int find_max () {
	for (int i = 2; i < 2 * n; ++ i) {
		for (int j = 2; j < 2 * n; ++ j) {
			if (i != j && a [i] + a [j] < a [0] + a [1]) {
				g [i].push_back (j + 2 * n);				
			} 
		}			
	}	
	memset (mt, -1, sizeof mt);
	for (int i = 2; i < 2 * n; ++ i) {
		memset (used, 0, sizeof used);
		dfs (i);		
	}
	int ans = 0;
	for (int i = 2 * n; i < 4 * n; ++ i) {
		if (mt [i] != -1) {
			++ ans;
		} 
	}
	ans /= 2;
	int tmp = 0;
	for (int i = 2; i < 2 * n; ++ i) {
		g [i].clear ();
		for (int j = 2; j < 2 * n; ++ j) {
			if (i != j && a [i] + a [j] == a [0] + a [1] && mt [j + 2 * n] == -1) {
				g [i].push_back (j + n + n);								
			}
		}
	}
	memset (mt, -1, sizeof mt);
	for (int i = 2; i < 2 * n; ++ i) {
		memset (used, 0, sizeof used);
		dfs (i);		
	}
	for (int i = 2 * n; i < 4 * n; ++ i) {
		if (mt [i] != -1) {
			++ tmp;
		} 
	}
	tmp /= 2;
	return n - ans - tmp;
}

int find_min () {
	for (int i = 2; i < 2 * n; ++ i) {
		g [i].clear ();
		for (int j = 2; j < 2 * n; ++ j) {
			if (i != j && a [i] + a [j] > a [0] + a [1]) {
				g [i].push_back (j + 2 * n);				
			} 
		}			
	}	
	memset (mt, -1, sizeof mt);
	for (int i = 2; i < 2 * n; ++ i) {
		memset (used, 0, sizeof used);
		dfs (i);		
	}
	int ans = 0;
	for (int i = 2 * n; i < 4 * n; ++ i) {
		if (mt [i] != -1) {
			++ ans;
		}
	}
	ans /= 2;
	return ans + 1;
}

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 0; i < 2 * n; ++ i) {
		scanf ("%d", &a [i]);
	}
	for (int i = 1; i < 2 * n; ++ i) {
		if (mn > a [i]) {
			mn = a [i];
			mnid = i;
		}			
		if (mx < a [i]) {
			mx = a [i];
			mxid = i;
		}
	}
	swap (a [1], a [mxid]);
	cout << find_max () << " ";
	swap (a [1], a [mxid]);
	swap (a [1], a [mnid]);
	cout << find_min () << endl;
	return 0;
}