#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>
#include <cassert>

using namespace std;

const int MAX_N = (int) 1e5;
const int MAX_LOG = 24;

int n, a [MAX_N];
bool added [(int) 5e6];
int id [(int) 5e6];

class Trie {
public:
	struct node {
		int nx [2];
		int end;
		node () {
			nx [0] = nx [1] = 0;
			end = 0;
		}
	} nodes [MAX_N * MAX_LOG];
	int uk;
	Trie () {
		uk = 1;
	}
	inline void add (const int & mask) {
		if (added [mask]) {
			++ nodes [id [mask]].end;
			return;
		}
		int cur = 0;
		for (int i = MAX_LOG - 1; i >= 0; -- i) {
			if (mask & (1 << i)) {
				if (!nodes [cur].nx [1]) {
					nodes [cur].nx [1] = uk;
					++ uk;									
				}
				cur = nodes [cur].nx [1];								
			} else {
				if (!nodes [cur].nx [0]) {
					nodes [cur].nx [0] = uk;
					++ uk;									
				}
				cur = nodes [cur].nx [0];											
			}
		}
		id [mask] = cur;
		added [mask] = 1;
		++ nodes [cur].end;
	}
	inline int get (const int & mask, const int & pos = MAX_LOG - 1, const int & cur = 0) {
		if (pos == -1) {
			return nodes [cur].end;
		}
		if (cur == 0 && pos != MAX_LOG - 1) {
			return 0;
		}
		if (mask & (1 << pos)) {
			return get (mask, pos - 1, nodes [cur].nx [0]);
		} else {
			return get (mask, pos - 1, nodes [cur].nx [0]) +
			get (mask, pos - 1, nodes [cur].nx [1]);					
		}					
	}
} tree;

void slow () {
	for (int i = 0; i < n; ++ i) {
		scanf ("%d", &a [i]);	
	}
	for (int i = 0; i < n; ++ i) {
		int ans = 0;
		for (int j = 0; j < n; ++ j) {
			if ((a [i] & a [j]) == 0) {
				++ ans;				
			}
		}
		printf ("%d ", ans);
	}
	exit (0);
}

void for_special_tests () {
	sort (a, a + n);
	reverse (a, a + n);
	for (int i = 0; i < 4; ++ i) {
		random_shuffle (a, a + n);
	}
	for (int i = 0; i < n; ++ i) {
		int ans = 0;
		if (__builtin_popcount (a [i]) > 4) {
			ans = tree.get (a [i]);
		} else {
			for (int j = max (0, i - 2000); j < min (i + 2000, n); ++ j) {
				if ((a [i] & a [j]) == 0) {
					++ ans;				
				}
			}
		}
		printf ("%d ", ans);
	}
	exit (0);
}

int calced [(int) 5e6];
bool used [(int) 5e6];
                 
int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	scanf ("%d", &n);
	if (n <= 10000) {
		slow ();
	}
	for (int i = 0; i < n; ++ i) {
		scanf ("%d", &a [i]);
		tree.add (a [i]);
	}
	if (tree.uk > (int) 2e5) {   	
		for_special_tests ();
		exit (0);
	}
	for (int i = 0; i < n; ++ i) {
		if (used [a [i]]) {
			printf ("%d ", calced [a [i]]);
		} else {
			int tmp = tree.get (a [i]);
			calced [a [i]] = tmp;
			used [a [i]] = 1;
			printf ("%d ", tmp);
		}
	}
	return 0;
}
