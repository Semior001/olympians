#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>
#include <cassert>

using namespace std;

const int MAX_N = 10000;
const int INF = (int) 1e5;

int mn = INF + 1, mnid, mx, mxid;

int n, a [MAX_N];
bool used [MAX_N];

int find_max () {
	sort (a + 2, a + 2 * n);
	int ans = 0;
	int j = 2;
	for (int i = 2; i < 2 * n; ++ i) {
		if (used [i]) {
			continue;
		}
		for (j = max (j, i + 1); j < 2 * n; ++ j) {
			if (!used [j] && a [i] + a [j] < a [0] + a [1]) {
				++ ans;
				used [i] = used [j] = 1;
				++ j;
				break;
			}
		}
	}
	for (int i = 2; i < 2 * n; ++ i) {
		if (!used [i]) {
			for (int j = i + 1; j < 2 * n; ++ j) {
				if (!used [j] && a [i] + a [j] == a [0] + a [1]) {
					++ ans;
					used [j] = 1;
					break;			                            
				}
			}
		}
	}
	return n - ans;
}

int find_min () {
	sort (a + 2, a + 2 * n);
	int ans = 0;
	int j = 2 * n - 1;
	for (int i = 2 * n - 1; i >= 0; -- i) {
		for (j = min (j, i - 1); j >= 2; -- j) {
			if (a [i] + a [j] > a [0] + a [1]) {
				++ ans;
				-- j;
				break;
			}
		}
	}
	return ans + 1;
}

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 0; i < 2 * n; ++ i) {
		scanf ("%d", &a [i]);
	}
	for (int i = 1; i < 2 * n; ++ i) {
		if (mn > a [i]) {
			mn = a [i];
			mnid = i;
		}			
		if (mx < a [i]) {
			mx = a [i];
			mxid = i;
		}
	}
	swap (a [1], a [mxid]);
	cout << find_max () << " ";
	swap (a [1], a [mxid]);
	swap (a [1], a [mnid]);
	cout << find_min () << endl;
	return 0;
}