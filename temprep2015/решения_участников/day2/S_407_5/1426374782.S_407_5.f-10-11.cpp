#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>
#include <cassert>

using namespace std;

const int MAX_N = (int) 1e5;

int n, a [MAX_N];

int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	cin >> n;
	for (int i = 0; i < n; ++ i) {
		cin >> a [i];	
	}
	for (int i = 0; i < n; ++ i) {
		int ans = 0;
		for (int j = 0; j < n; ++ j) {
			if ((a [i] & a [j]) == 0) {
				++ ans;				
			}
		}
		cout << ans << " ";
	}
	return 0;
}