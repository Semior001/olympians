#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>
#include <assert.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "F."
#define F first
#define S second
#define mk_pr make_pair
#define _bits __builtin_popcount

typedef long long ll;
typedef pair <ll, ll> PII;

const int maxn = 100100;
const ll INF = 1e18;

/////////////Fenwick for dp/////////////////

struct fenwick_tree {
	int n, t[maxn];
	void init(const int &_n) {
		n = _n;
	}
	void upd(int x, const int &delta) {
		for (; x <= n; x = (x | (x + 1)))
			t[x] = max(t[x], delta);
	}
	int get(int x) {
		int ans = 0;
		for (; x >= 0; x = (x & (x + 1)) - 1)
			ans = max(ans, t[x]);
		return ans;	
	}
} tree;
       
void compress(int n, int *a) {
	vector <int> temp(a, a + n + 1);
	sort(temp.begin(), temp.end());
	map <int, int> convert;
	int cl = 0;
	forn(i, 1, n) {
		if (temp[i] == temp[i - 1]) continue;
		convert[temp[i]] = ++cl;
	}	
	forn(i, 1, n)
		a[i] = convert[a[i]];
}


int n;     
int h[maxn], cost[maxn];
int d[maxn];
ll mx[maxn], mx1[maxn], cost_sum;

/////////////Treap for max ans/////////////////

typedef struct treap_node* node_ptr;

int cnt = 0;

struct treap_node {
	int y;
	PII key;
	ll val, max_val;
	node_ptr l, r;
	treap_node(const int &x, const ll &v) : key(PII(x, ++cnt)) {
		y = (rand() << 16) | rand();
		val = max_val = v;  
		l = r = 0;
	}
	ll mx() {
		return this ? max_val : 0;
	}
	void recalc() {
		if (!this) return;
		max_val = max(val, max(l -> mx(), r -> mx()));
	}
} *L, *R, *Mid;
                 /*
void less_k(node_ptr &t, int k) {
	if (!t) return;
	less_k(t -> l, k);
	assert(t -> key <= k);
	less_k(t -> r, k);
}              */

void Merge(node_ptr &w, node_ptr left, node_ptr right) {
	if (!left || !right) {
		w = (left ? left : right);
		return;
	}		
	if (left -> y > right -> y)
		Merge(left -> r, left -> r, right), w = left;
	else
		Merge(right -> l, left, right -> l), w = right;
	w -> recalc();	
}

void Split(node_ptr w, node_ptr &left, node_ptr &right, const int &sep) {
	if (!w) {
		left = right = 0;
		return;
	}
	if (w -> key <= PII(sep, INF))
		Split(w -> r, w -> r, right, sep), left = w;
	else                                                
		Split(w -> l, left, w -> l, sep), right = w;	
	left -> recalc(), right -> recalc();	
}

void Insert(node_ptr &root, const int &k, const int &v) {
	node_ptr present = new treap_node(k, v);
	Split(root, L, R, k);
	Merge(L, L, present);
	Merge(root, L, R);
}

ll Get(node_ptr &root, const int &k) {
	Split(root, L, R, k);
	ll ans = L -> mx();
	Merge(root, L, R);
	return ans;
}              

node_ptr roots[maxn];
set <PII> sets[maxn];

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
               
    scanf("%d", &n);
    forn(i, 1, n)
    	scanf("%d", &h[i]);
    forn(i, 1, n) {
    	scanf("%d", &cost[i]);
    	cost_sum += cost[i];
    }	

    tree.init(n);
    compress(n, h);

    int longest = 0;
    forn(i, 1, n) { 
        d[i] = tree.get(h[i]) + 1;
        tree.upd(h[i], d[i]);
     	longest = max(longest, d[i]);      
    }	                    

    roots[0] = new treap_node(0, 0);
    forn(i, 1, n) {
    	mx[i] = Get(roots[d[i] - 1], h[i]) + cost[i];
    	Insert(roots[d[i]], h[i], mx[i]);
    }

    sets[0].insert(PII(0, 0));
    forn(i, 1, n) {                                   
    	for (auto x : sets[d[i] - 1]) {
    		if (x.F > h[i]) break;
    		mx1[i] = max(mx1[i], x.S + cost[i]);
    	}
    	sets[d[i]].insert(PII(h[i], mx1[i]));
    	if (mx1[i] > mx[i])
    		assert(0);
    }                                 

    ll ans = 0;                        	
    forn(i, 1, n)
    	if (d[i] == longest)
    		ans = max(ans, mx[i]);

    cout << cost_sum - ans << "\n";
	                        
	return 0;	
}
                     