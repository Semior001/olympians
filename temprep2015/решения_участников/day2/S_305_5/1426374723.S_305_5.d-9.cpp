#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "D."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 1024;
const int INF = 1e9 + 9;
                  
int a, c, l, r;

int f(const int &x) {
	if (!x) return 0;
	int q_b = (a - c);  
	int ans = 0;      
	for (int i = 1; i * i <= q_b && i <= x; ++i) {
		if (q_b % i != 0) continue;
		if (a % i == c)
			++ans;
		if (i != q_b / i && q_b / i <= x) 
			if (a % (q_b / i) == c)
				++ans;
	}
	return ans;
}

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%d %d %d %d", &a, &c, &l, &r);
	printf("%d", f(r) - f(l - 1));

	return 0;
}
