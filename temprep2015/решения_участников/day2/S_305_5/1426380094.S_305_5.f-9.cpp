#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>
#include <assert.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "F."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 1024;
const ll INF = 1e18;

bool bit(const int &mask, const int &pos) {
	return mask & (1 << pos);
}

int n;     
int h[maxn], cost[maxn];

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
               
    scanf("%d", &n);
    forn(i, 1, n)
    	scanf("%d", &h[i]);
    forn(i, 1, n)
    	scanf("%d", &cost[i]);

    ll ans = INF;
    forn(mask, 0, (1 << n) - 1) {
    	ll mask_cost = 0;
    	int last = 0;
    	bool valid = 1;
    	forn(i, 1, n) {
    		if (bit(mask, i - 1)) {
    			mask_cost += cost[i];
    			continue;
    		}
    		if (last > 0) 
    			valid &= (h[i] >= h[last]);	
    		last = i;	
    	}
    	if (valid) 
    		ans = min(ans, mask_cost);
    }
       	
    cout << ans << "\n";

	return 0;
}
