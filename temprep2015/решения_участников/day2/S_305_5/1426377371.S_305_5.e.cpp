#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>  
#include <set>
#include <map>
#include <assert.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "E."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;
typedef set <int> :: iterator s_it;

const int maxn = 100100;
const int INF = 1e9 + 9;

int n;
int a[maxn];
bool used[maxn];
      
int solve_max() {
	///naivysshee mesto
	multiset <int> st;
	forn(i, 2, n - 1)
		st.insert(a[i]);
	int amans_pair = a[1] + a[n];
	int pos = 0;         
	for1(i, n - 1, 2) {       
		if (st.find(a[i]) == st.end()) continue;
		int limit = amans_pair - a[i];          
		st.erase(st.find(a[i]));
		s_it it = st.upper_bound(limit);
		/////////////check//////////////
		s_it check = st.upper_bound(limit);
		if (check != st.begin() && check != st.end()) {
			--check;
			assert(*it != *check);
		}	
		/////////////check//////////////
		if (it == st.begin()) {
			++pos;
			st.erase(st.find(a[i - 1]));
			--i;
			continue;
		}                               
		--it; 
		assert(*it + a[i] <= amans_pair);
		st.erase(it);                               
	}	           
	return pos + 1;
}

int solve_min() {
	///nainizshee mesto
	multiset <int> st;
	forn(i, 3, n)
		st.insert(a[i]);
	int amans_pair = a[1] + a[2];
	int pos = 0;        
	forn(i, 3, n) {       
		if (st.find(a[i]) == st.end()) continue;
		int limit = amans_pair - a[i];    
		st.erase(st.find(a[i]));
		s_it it = st.upper_bound(limit);  	
		if (it == st.end()) {
			st.erase(st.find(a[i + 1]));
			++i;
			continue;
		}     
		assert(*it + a[i] > amans_pair);
		++pos;
		st.erase(it);                               
	}	           
	return pos + 1;
	return 0;
}

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);

    scanf("%d", &n);
    n <<= 1;
    forn(i, 1, n)
    	scanf("%d", &a[i]);

    sort(a + 2, a + n + 1);

    printf("%d %d\n", solve_max(), solve_min());                  
	
	return 0;
}
