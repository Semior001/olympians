#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>
#include <assert.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "F."
#define F first
#define S second
#define mk_pr make_pair
#define _bits __builtin_popcount

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 1024;
const ll INF = 1e18;

int n;     
int h[maxn], cost[maxn];
int d[maxn];
ll mx[maxn], cost_sum;

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
               
    scanf("%d", &n);
    forn(i, 1, n)
    	scanf("%d", &h[i]);
    forn(i, 1, n) {
    	scanf("%d", &cost[i]);
    	cost_sum += cost[i];
    }	

    int longest = 0;
    forn(i, 1, n) { 
    	forn(j, 0, i - 1)
    		if (h[j] <= h[i])
    			d[i] = d[j] + 1;
    	longest = max(longest, d[i]);
    }	                    

    forn(i, 0, n)
    	forn(j, i + 1, n)
    		if (d[j] == d[i] + 1)
    			mx[j] = max(mx[j], mx[i] + cost[j]);

    ll ans = 0;
    forn(i, 1, n)
    	if (d[i] == longest)
    		ans = max(ans, mx[i]);

    cout << cost_sum - ans << "\n";
	                        
	return 0;	
}
