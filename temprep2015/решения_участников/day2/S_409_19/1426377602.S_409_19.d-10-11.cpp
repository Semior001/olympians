
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "D"

int n, L, R, A;
int ans;
int a[maxn];

int lcm(int a, int b)
{
	return a / __gcd(a, b) * b;
}

void rec(int v)
{
	if (v == n + 1)
	{
		int lc = a[1];
		for (int i = 2; i <= n; i++)
			lc = lcm(a[i], lc);
		if (lc % A == 0)
		{
			/*
			for (int i = 1; i <= n; i++)
				printf("%d ", a[i]);
			printf("\n%d\n", lc);
			*/
			ans++;
		}
		return;
	}
	for (int k = max(L, a[v - 1]); k <= R; k++)
	{
		a[v] = k;
		rec(v + 1);
	}
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d%d%d", &n, &L, &R, &A);
	rec(1);
	printf("%d", ans);
}
