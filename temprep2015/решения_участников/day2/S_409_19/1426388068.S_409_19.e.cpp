
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "E"

int n, a[maxn * 2];
multiset <int> A, B;

void solve_min()
{
	auto it = A.end();
	it--;
	int nap = *it;
	A.erase(it);
	int cnt = 0;
	//printf("%d %d\n", a[1], nap);
	while (!A.empty())
	{
		auto end = A.end();
		end--;
		auto st = A.begin();
		if (st == end || ((*st) + (*end) > a[1] + nap))
		{
			st = end;
			st--;
		}
		if ((*st) + (*end) > a[1] + nap)
			cnt++;
		//printf("%d %d\n", (*st), (*end));
		A.erase(st);
		A.erase(end);
	}
	printf("%d ", cnt + 1);
}

void solve_max()
{
	auto it = B.begin();
	int nap = *it;
	B.erase(it);
	int cnt = 0;
	//printf("%d %d\n", a[1], nap);
	while (!B.empty())
	{
		auto st = B.begin();
		auto end = B.upper_bound(a[1] + nap - (*st));
		if (end == B.end() || end == st)
		{
			end = B.begin();
			end++;
		}
		if ((*st) + (*end) > a[1] + nap)
			cnt++;
		//printf("%d %d\n", (*st), (*end));
		B.erase(st);
		B.erase(end);
	}
	printf("%d", cnt + 1);
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	for (int i = 2; i <= 2 * n; i++)
	{
		A.insert(a[i]);
		B.insert(a[i]);
	}
	solve_min();
	solve_max();
}
