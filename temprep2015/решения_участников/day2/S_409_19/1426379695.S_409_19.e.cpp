
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "E"

int n, a[3], ve[100], A[100];
int c[100][3];
int us[maxn];
int mn = inf, mx = -inf;

int calc()
{
	int b[100];
	for (int i = 1; i <= n; i++)
		b[i] = ve[i];
	/*
	for (int i = 1; i <= n; i++)
		printf("%d %d\n", c[i][1], c[i][2]);
	puts("");
	*/
	sort(b + 1, b + 1 + n);
	reverse(b + 1, b + 1 + n);
	int i = 1;
	while (b[i] > ve[1])
		i++;
	return i;
}

void rec(int k, int v)
{
	if (v == 3)
	{
		if (k == n)
		{
			int c = calc();
			mn = min(mn, c);
			mx = max(mx, c);
			return;
		}
		ve[k] = a[1] + a[2];
		rec(k + 1, 1);
		return;
	}
	for (int t = 1; t <= 2 * n; t++)
	{
		if (us[t])
			continue;
		a[v] = A[t];
		c[k][v] = a[v];
		us[v] = 1;
		rec(k, v + 1);
		us[v] = 0;
	}
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n * 2; i++)
		scanf("%d", A + i);
	us[1] = 1;
	c[1][1] = A[1];
	a[1] = A[1];
	rec(1, 2);
	printf("%d %d", mn, mx);
}
