
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "F"

struct item
{
	int end;
	int next[3];
	int sz, pr;
};

item t[2 * maxn];
int sz = 1;
int LG = 0;
bool ch[23], buf[30];
int k = 0;

void shift(int sh)
{
	for (int i = 0; i < 30; i++)
		buf[i] = 0;
	for (int i = sh; i < LG; i++)
		buf[i] = ch[i - sh];
	for (int i = 0; i < 23; i++)
		ch[i] = buf[i];
	k = LG;
}

void add(int x)
{
	int v = 0;
	k = 0;
	while (x > 0)
	{
		ch[k++] = (x & 1);
		x >>= 1;
	}
	reverse(ch, ch + k);
	shift(LG - k);
	for (int i = 0; i < k; i++)
	{
		int to = t[v].next[ch[i]];
		//printf("%d %d\n", v, ch[i]);
		if (!to)
		{
			to = sz;
			t[v].next[ch[i]] = sz;
			t[to].pr = v;
			sz++;
		}
		v = to;
	}
	//puts("");
	t[v].end++;
	do
	{
		t[v].sz = t[v].end;
		for (int i = 0; i < 2; i++)
		{
			int to = t[v].next[i];
			if (!to)
				continue;
			t[v].sz += t[to].sz;
		}
		//printf("%d %d\n", v, t[v].sz);
		v = t[v].pr;
	} while (v);
	t[v].sz = t[v].end;
	for (int i = 0; i < 2; i++)
	{
		int to = t[v].next[i];
		if (!to)
			continue;
		t[v].sz += t[to].sz;
	}
}

int cnt(int x)
{
	int v = 0;
	while (x > 0)
	{
		ch[k++] = (x & 1);
		x >>= 1;
	}
	reverse(ch, ch + k);
	for (int i = 0; i < k; i++)
	{
		int to = t[v].next[ch[i]];
		if (!to)
			return 0;
		v = to;
	}
	//puts("");
	return t[v].end;
}

int get(int pos, int v)
{
	if (!v && pos != 0)
		return 0;
	if (pos == k)
	{
		//printf("%d %d %d %d\n", v, pos, ch[pos], t[v].sz);
		return t[v].sz;
	}
	int ans = 0;
	int to = t[v].next[0];
	if (to)
		ans += get(pos + 1, to);
	if (!ch[pos])
	{
		to = t[v].next[1];
		if (to)
			ans += get(pos + 1, to);
	}
	//printf("%d %d %d  %d\n", v, pos, ch[pos], ans);
	return ans;
}

int calc(int x)
{
	k = 0;
	while (x > 0)
	{
		ch[k++] = (x & 1);
		x >>= 1;
	}
	reverse(ch, ch + k);
	shift(LG - k);
	return get(0, 0);
}

int lg(int x)
{
	int ans = 0;
	while (x)
	{
		ans++;
		x >>= 1;
	}
	return ans;
}

int n, a[maxn];

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", a + i);
		LG = max(LG, lg(a[i]));
	}
	for (int i = 1; i <= n; i++)
		add(a[i]);
	for (int i = 1; i <= n; i++)
		printf("%d ", calc(a[i]));
}
