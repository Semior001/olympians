
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int maxr = int(1e3) + 12;
const int inf = int(1e9) + 7;
const int base = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "D"

int n, L, R, A;
int ans;
int dp[20][maxr][maxr * 10];

int lcm(int a, int b)
{
	return a / __gcd(a, b) * b;
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d%d%d", &n, &L, &R, &A);
	for (int i = L; i <= R; i++)
		dp[1][i][i] = 1;
	for (int pos = 1; pos < n; pos++)
		for (int i = 1; i < maxr * 10; i++)
			for (int last = L; last <= R; last++)
				if (dp[pos][last][i])
				{
					for (int j = max(L, last); j <= R; j++)
					{
						int lc = lcm(i, j);
						dp[pos + 1][j][lc] = (dp[pos + 1][j][lc] + dp[pos][last][i]) % base;
					}
				}
	/*
	for (int pos = 1; pos <= n; pos++)
	{
		for (int i = 1; i < maxr * 10; i++)
			for (int last = L; last <= R; last++)
				if (dp[pos][last][i])
					printf("%d %d %d\n", pos, i, dp[pos][last][i]);
	}
	puts("");
	*/
	int ans = 0;
	for (int i = A; i <= maxr * 10; i += A)
		for (int last = L; last <= R; last++)
			ans = (ans + dp[n][last][i]) % base;
	printf("%d", ans);
}
