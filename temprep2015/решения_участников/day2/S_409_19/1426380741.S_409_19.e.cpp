
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "E"

int n, A[maxn];
int us[maxn], buf[maxn];
int teams[maxn][4];
int mx = -inf, mn = inf;

int calc()
{
    for (int i = 1; i <= n; i++)
        buf[i] = teams[i][1] + teams[i][2];
    sort(buf + 1, buf + 1 + n);
    reverse(buf + 1, buf + 1 + n);
    int i = 1;
    while (buf[i] > teams[1][1] + teams[1][2])
        i++;
    return i;
}

void rec(int team, int pos)
{
    if (pos == 3)
    {
        if (team == n)
        {
            int now = calc();
            mn = min(mn, now);
            mx = max(mx, now);
        }
        else
            rec(team + 1, 1);
        return;
    }
    for (int t = 2; t <= 2 * n; t++)
    {
        if (us[t])
            continue;
        us[t] = 1;
        teams[team][pos] = A[t];
        rec(team, pos + 1);
        us[t] = 0;
    }
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n * 2; i++)
		scanf("%d", A + i);
	us[1] = 1;
	teams[1][1] = A[1];
	rec(1, 2);
	printf("%d %d", mn, mx);
}
