
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "F"

int n, a[maxn];
int cnt[maxn];
int ans[200];
bool fl = 0;

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++)
		if (a[i] > 100)
			fl = 1;
	if (fl)
	{
		for (int i = 1; i <= n; i++)
		{
			int ans = 0;
			for (int j = 1; j <= n; j++)
				if (!(a[i] & a[j]))
					ans++;
			printf("%d ", ans);
		}
		return 0;
	}
	for (int i = 1; i <= n; i++)
		cnt[a[i]]++;
	for (int i = 1; i <= 100; i++)
		if (cnt[i])
			for (int j = 1; j <= 100; j++)
				if (cnt[j])
					if (!(i & j))
						ans[i] += cnt[j];
	for (int i = 1; i <= n; i++)
		printf("%d ", ans[a[i]]);
}
