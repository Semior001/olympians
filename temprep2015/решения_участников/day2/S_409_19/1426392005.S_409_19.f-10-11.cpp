
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "F"

struct item
{
	int end;
	int next[3];
	int sz, pr;
};

item t[2 * maxn];
int sz = 1;
int LG = 0;
bool ch[maxn][23], buf[30];
int cur;
int k[maxn];

void shift(int sh)
{
	for (int i = 0; i < 30; i++)
		buf[i] = 0;
	for (int i = sh; i < LG; i++)
		buf[i] = ch[cur][i - sh];
	for (int i = 0; i < 23; i++)
		ch[cur][i] = buf[i];
	k[cur] = LG;
}

void add(int x)
{
	int v = 0;
	for (int i = 0; i < k[cur]; i++)
	{
		int to = t[v].next[ch[cur][i]];
		//printf("%d %d\n", v, ch[cur][i]);
		if (!to)
		{
			to = sz;
			t[v].next[ch[cur][i]] = sz;
			t[to].pr = v;
			sz++;
		}
		v = to;
	}
	//puts("");
	t[v].end++;
	do
	{
		t[v].sz = t[v].end;
		for (int i = 0; i < 2; i++)
		{
			int to = t[v].next[i];
			if (!to)
				continue;
			t[v].sz += t[to].sz;
		}
		//printf("%d %d\n", v, t[v].sz);
		v = t[v].pr;
	} while (v);
	t[v].sz = t[v].end;
	for (int i = 0; i < 2; i++)
	{
		int to = t[v].next[i];
		if (!to)
			continue;
		t[v].sz += t[to].sz;
	}
}

int get(int pos, int v)
{
	if (!v && pos != 0)
		return 0;
	if (pos == k[cur])
	{
		//printf("%d %d %d %d\n", v, pos, ch[cur][pos], t[v].sz);
		return t[v].sz;
	}
	int ans = 0;
	int to = t[v].next[0];
	if (to)
		ans += get(pos + 1, to);
	if (!ch[cur][pos])
	{
		to = t[v].next[1];
		if (to)
			ans += get(pos + 1, to);
	}
	//printf("%d %d %d  %d\n", v, pos, ch[cur][pos], ans);
	return ans;
}

int calc(int x)
{
	return get(0, 0);
}

int lg(int x)
{
	int ans = 0;
	k[cur] = 0;
	while (x > 0)
	{
		ch[cur][k[cur]++] = (x & 1);
		x >>= 1;
		ans++;
	}
	reverse(ch[cur], ch[cur] + k[cur]);
	return ans;
}

int n, a[maxn];

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", a + i);
		cur = i;
		LG = max(LG, lg(a[i]));
	}
	for (int i = 1; i <= n; i++)
		if (k[i] != LG)
		{
			cur = i;
			shift(LG - k[cur]);
		}

	for (int i = 1; i <= n; i++)
	{
		cur = i;
		k[cur] = LG;
		add(a[i]);
	}
	for (int i = 1; i <= n; i++)
	{
		cur = i;
		printf("%d ", calc(a[i]));
	}
}
