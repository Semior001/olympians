
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "E"

int n, a[maxn];
multiset <int> se;

void solve_min()
{
	multiset <int> s = se;
	auto it = s.end();
	it--;
	int nap = *it;
	s.erase(it);
	vector <int> stand;
	//printf("%d %d\n", a[1], nap);
	while (!s.empty())
	{
		auto st = s.begin(), end = s.end();
		end--;
		stand.pb((*st) + (*end));
		//printf("%d %d\n", (*st), (*end));
		s.erase(st);
		s.erase(end);
	}
	stand.pb(a[1] + nap);
	sort(stand.begin(), stand.end());
	reverse(stand.begin(), stand.end());
	int i = 0;
	while (stand[i] > a[1] + nap)
		i++;
	printf("%d ", i + 1);
}

void solve_max()
{
	multiset <int> s = se;
	auto it = s.begin();
	int nap = *it;
	s.erase(it);
	vector <int> stand;
	//printf("%d %d\n", a[1], nap);
	while (!s.empty())
	{
		auto st = s.begin();
		auto end = s.upper_bound(a[1] + nap - (*st));
		if (end == s.end() || end == st)
		{
			end = s.begin();
			end++;
		}
		stand.pb((*st) + (*end));
		//printf("%d %d\n", (*st), (*end));
		s.erase(st);
		s.erase(end);
	}
	stand.pb(a[1] + nap);
	sort(stand.begin(), stand.end());
	reverse(stand.begin(), stand.end());
	int i = 0;
	while (stand[i] > a[1] + nap)
		i++;
	printf("%d", i + 1);
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	for (int i = 2; i <= 2 * n; i++)
		se.insert(a[i]);
	solve_min();
	solve_max();
}
