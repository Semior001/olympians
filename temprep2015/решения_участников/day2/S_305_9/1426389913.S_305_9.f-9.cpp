#include <fstream>
using namespace std;
ifstream fin("F.in");
ofstream fout("F.out");

int n;
int h[100000];
int c[100000];
int mx,f,s,k,mxl,mxc;
int x;

void palm(int y,int u)
{
    if(y==u)return;
    mxl=0;
    mxc=0;
    f=0;
    s=0;
    for(int i=y;i<=u;i++)
    {
        int l=i;
        int r=i;
        k=0;
        i--;
        do
        {
            r++;
            k+=c[i];
        }
        while(h[r-1]==h[r]-1 || h[r-1]==h[r]);
        r--;
        i=r;
        if(r-l+1>mxl)
        {
            mxl=r-l+1;
            mxc=k;
            f=l;
            s=r;
        }
        else if(r-l+1==mxl && k>mxc)
        {
            f=l;
            s=r;
            mxl=r-l+1;
            mxc=k;
        }
        palm(y,(y+u)/2);
        palm((y+u)/2+1,u);
    }
    s--;
    f--;
    for(int i=y;i<f;i++)
    {
        if(h[i]>h[f])x+=c[i];
    }
    for(int i=s+1;i<=u;i++)
    {
        if(h[i]<h[s])x+=c[i];
    }
}
int main()
{
    fin>>n;
    for(int i=0;i<n;i++)
        fin>>h[i];
    for(int i=0;i<n;i++)
        fin>>c[i];
    palm(0,n-1);
    fout<<x;
    fin.close();
    fout.close();
    return 0;
}
