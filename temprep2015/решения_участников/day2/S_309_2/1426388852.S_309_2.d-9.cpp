#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back
#define ll long long

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int l, r, a, c, sum;
vector <int> dv;

int main () {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> a >> c >> l >> r;
	if (a == 0 || c == 0 || l == 0 || r == 0) {
		cout << 11 / 0;
		return 0;
	}
	a -= c;   
	for (int i = 1; i * i <= a; ++i) {
		if (a % i == 0) {
			dv.pb(i);
		}
	}    
	int sz = dv.size();
	for (int i = sz - 1; i >= 0; --i) {
		dv.pb(a / dv[i]);		
	}                       
	for (int i = 0; i < dv.size(); ++i) {
		if (dv[i] >= c && dv[i] >= l && dv[i] <= r && (a + c) % dv[i] == c) {
			//cout << dv[i] << ' ';
			sum++;
		}
	}	
	cout << sum;
	return 0;
}