#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back
#define ll long long

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);
                       

int sol2 (int a, int c, int l, int r) {
	int sum = 0, res = a - c;
    vector <int> dv;
    for (int i = res; i <= r; i += res) {
    	dv.pb(i);
    }
	a -= c;
	for (int i = 1; i * i <= a; ++i) {
		if (a % i == 0) {
			dv.pb(i);
		}
	}
	int sz = dv.size();
	for (int i = sz - 1; i >= 0; --i) {
		dv.pb(a / dv[i]);		
	}                       
	for (int i = 0; i < dv.size(); ++i) {
		if (dv[i] > c && dv[i] >= l && dv[i] <= r && (a + c) % dv[i] == c) {
			sum++;
		}
	}	        
	return sum;
}
    
int main () {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	int a, c, l, r;
	cin >> a >> c >> l >> r;
	cout << sol2(a, c, l, r);
	return 0;
}