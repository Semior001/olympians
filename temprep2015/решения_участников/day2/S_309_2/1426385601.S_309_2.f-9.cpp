#include <bits/stdc++.h>
#define pb push_back

using namespace std;

int n, mx = 13333333, mn, s, q;
vector <int> a, b;

int main () {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		int x; cin >> x;
		a.pb(x);
	}
	for (int i = 1; i <= n; ++i) {
		int x; cin >> x;
		b.pb(x);
	}
	do {             
		int Aman = a[0] + b[0];
		for (int i = 1; i < b.size(); ++i) {
			if (b[i] + a[i] > Aman) {
				s++;
			}
		}
		mn = max(mn, s);
		mx = min(mx, s);
		s = 0;
	} while (next_permutation(b.begin(), b.end()));
	cout << mx + 1 << " " << mn + 1;	                                     
	return 0;
}