#include <bits/stdc++.h>
#define pb push_back

using namespace std;

int n, mx = 1000000000, mn, s, q = 40000000, Aman, z;
vector <int> a, b;

int main () {
    freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	cin >> Aman;
	for (int i = 1; i < n * 2; ++i) {
		int x; cin >> x;
		b.pb(x);	
	}     
	while (q--) {                  
		z = Aman + b[0];		
		for (int i = 2; i < b.size(); i += 2) {
			if (b[i] + b[i - 1] > z) {
				s++;
			}
		} 
		mx = min(s, mx);
		mn = max(mn, s);
		s = 0;
		next_permutation(b.begin(), b.end());
	}
	cout << mx + 1 << ' '<< mn + 1;
	return 0;
}