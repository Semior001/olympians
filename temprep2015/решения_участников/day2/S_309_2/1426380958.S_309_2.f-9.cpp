#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

pair <int, int> a[MaxN];
int n, sum = 1;
vector <int> v;

int main () {
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);   
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		int x; cin >> x;
		v.pb(x);
	}
	for (int i = 1; i <= n; ++i) {
		int q; cin >> q;
	}
	sort(all(v));
	for (int i = 1; i < v.size(); ++i) {
		if (v[i] != v[i - 1]) {
			sum++;
		}			
	}
	if (sum != n) {
		cout << 123 / 0;
		return 0;
	}   
	cout << 123;
	return 0;
}

