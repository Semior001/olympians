#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back
#define ll long long

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int l, r, a, c, sum;

int main () {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> a >> c >> l >> r;
	for (int i = l; i <= r; ++i) {
		if (a % i == c) {
			sum++;
		}	
	}
	cout << sum;
	return 0;
}