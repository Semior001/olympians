#include <bits/stdc++.h>
#define pb push_back

using namespace std;

int n, mx = 1000000000, mn, s, q = 120;
vector <double> a, b;

int main () {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		double x; cin >> x;
		a.pb(x);
	}
	for (int i = 1; i <= n; ++i) {
		double x; cin >> x;
		b.pb(x);
	}
	while (q) {	
		double Aman = (a[0] + b[0]) / 2;
		for (int i = 1; i < b.size(); ++i) {
			if ((b[i] + a[i]) / 2 > Aman) {
				s++;
			}
		}
		mn = max(mn, s);
		mx = min(mx, s);
		s = 0;
		q--;
		next_permutation(b.begin(), b.end());
	}                                 
	cout << mx + 1 << " " << mn + 1;	                                     
	return 0;
}