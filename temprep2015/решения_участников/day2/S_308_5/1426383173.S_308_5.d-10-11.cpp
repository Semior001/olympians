#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <math.h>

using namespace std;

#define name "D"
#define mod 1000000007

const int N = 105;

int n;
int l, r;
int a;
int res = 0;

int nok(int a, int b){
	return a * b / __gcd(a, b);
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d%d", &n, &l, &r, &a);
	for(int i = l;i <= r;++ i){
		for(int j = i;j <= r;++ j){
			if(nok(i, j) % a == 0)
				++ res;
		}
	}
	printf("%d\n", res);
	return 0;
}