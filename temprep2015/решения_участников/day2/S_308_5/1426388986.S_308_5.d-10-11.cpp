#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <math.h>

using namespace std;

#define name "D"
#define mod 1000000007

const int N = 105;

int n;
int l, r;
int a;
int d[N][(1 << 8) + 1];
int cur = 0;
int pr[25], len = 0;

void add(int &a, int b){
	if((a += b) >= mod)
		a -= mod;
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d%d", &n, &l, &r, &a);
	int sq = (int)sqrt(a);
	for(int i = 2;i <= sq;++ i){
		if(a % i == 0){
			pr[len] = 1;
			while(a % i == 0){
				pr[len] *= i;
				a /= i;											
			}
			++ len;
		}
	}
	if(a > 1)
		pr[len ++] = a;
	d[0][0] = 1;
	for(int num = l;num <= r;++ num){
	    int our_mask = 0;
		for(int it = 0;it < len;++ it)
			if(num % pr[it] == 0)
				our_mask |= (1 << it);
		for(int i = 1;i <= n;++ i)
			for(int prev_mask = 0;prev_mask < (1 << len);++ prev_mask)
				add(d[i][prev_mask | our_mask], d[i - 1][prev_mask]); 
	}
	printf("%d\n", d[n][(1 << len) - 1]);
	return 0;
}