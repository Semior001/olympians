#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>

using namespace std;

#define name "E"

const int N = 200005;

int n;
int our, a[N];
bool used[N];

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	n += n;
	scanf("%d", &our);
	for(int i = 1;i < n;++ i)
		scanf("%d", &a[i]);
	sort(a + 1,a + n);
	int cnt1 = 0;
	for(int i = 2;i < n;++ i){
		if(used[i])
			continue;
		bool finded = false;
		for(int j = i + 1;j < n;++ j){
			if(!used[j] && a[i] + a[j] > our + a[1]){
			    finded = true;
				used[j] = true;
				break;
			}
		}
		if(!finded)
			used[i + 1] = true;
		else
			++ cnt1;
	}
	memset(used, 0, sizeof used);
	int cnt2 = 0;
	for(int i = n - 2;i >= 1;-- i){
		if(used[i])
			continue;		
		bool finded = false;
		for(int j = i - 1;j >= 1;-- j){
			if(!used[j] && a[i] + a[j] <= our + a[n - 1]){
			    finded = true;
				used[j] = true;
				break;
			}
		}
		if(!finded){
		    ++ cnt2;
			used[i - 1] = true;
		}   
	}
	cout << cnt2 + 1 << " " << cnt1 + 1;
	return 0;
}
