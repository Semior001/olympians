#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <set>

using namespace std;

#define name "E"
#define F first 
#define S second
#define mp make_pair

const int N = 200005;

int n;
int our, a[N];
bool used[N];
set < pair < int , int > > s;
set < pair < int , int > > ::iterator it;

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	n += n;
	scanf("%d", &our);
	for(int i = 1;i < n;++ i)
		scanf("%d", &a[i]);
	sort(a + 1,a + n);
	for(int i = 2;i < n;++ i)
		s.insert(mp(a[i], i));
	int cnt1 = 0;
	for(int i = 2;i < n;++ i){                       
	    if(used[i])                                  
	    	continue;                                
	    s.erase(mp(a[i], i));                        
	    int Need = our + a[1] - a[i];                
		it = s.upper_bound(mp(Need, n));             
		if(it != s.end()){                           
			used[it -> S] = true;                        
			s.erase(it);                             
			++ cnt1;                                 
		}                                            
		else {                                       
			s.erase(mp(a[i + 1], i + 1));            
			used[i + 1] = true;                      
		}                                            
	}                                                
	int cnt2 = 0;
	s.clear();
	for(int i = 1;i <= n - 2;++ i)
		s.insert(mp(a[i], i));
	memset(used, 0, sizeof used);
	for(int i = n - 2;i >= 1;-- i){
		if(used[i])
			continue;		
		s.erase(mp(a[i], i));
		int Need = our + a[n - 1] - a[i];
		it = s.upper_bound(mp(Need, n));
		if(it != s.begin()){
			-- it;
			used[it -> S] = true;
			s.erase(it);
		}
		else {
			used[i - 1] = true;
			s.erase(mp(a[i - 1], i - 1));
			++ cnt2;
		}
	}
	cout << cnt2 + 1 << " " << cnt1 + 1;
	return 0;
}
