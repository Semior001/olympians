#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <set>

using namespace std;

#define name "E"

const int N = 200005;

int n;
int our, a[N];
bool used[N];
multiset < int > s;
multiset < int > ::iterator it;

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	n += n;
	scanf("%d", &our);
	for(int i = 1;i < n;++ i){
		scanf("%d", &a[i]);
		s.insert(a[i]);
	}
	sort(a + 1,a + n);
	s.erase(a[1]);
	int cnt1 = 0;
	for(int i = 2;i < n;++ i){
	    s.erase(a[i]);
	    int Need = our + a[1] - a[i];
		it = s.upper_bound(Need);
		if(it != s.end()){
			s.erase(it);
			++ cnt1;
		}
		else {
			s.erase(a[i + 1]);
			++ i;
		}
	}
	memset(used, 0, sizeof used);
	int cnt2 = 0;
	for(int i = n - 2;i >= 1;-- i){
		if(used[i])
			continue;		
		bool finded = false;
		for(int j = i - 1;j >= 1;-- j){
			if(!used[j] && a[i] + a[j] <= our + a[n - 1]){
			    finded = true;
				used[j] = true;
				break;
			}
		}
		if(!finded){
		    ++ cnt2;
			used[i - 1] = true;
		}   
	}
	cout << cnt2 + 1 << " " << cnt1 + 1;
	return 0;
}
