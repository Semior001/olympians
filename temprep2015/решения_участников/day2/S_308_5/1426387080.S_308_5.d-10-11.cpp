#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <math.h>

using namespace std;

#define name "D"
#define mod 1000000007

const int N = 1005;

int n;
int l, r;
int a;
int d[25][N][N];
int pr[25], len = 0;

void add(int &a, int b){
	if((a += b) >= mod)
		a -= mod;
}

int sum(int id, int mask, int r){
	int res = 0;
	for(int last = 1;last <= r;++ last)
		add(res, d[id][last][mask]);
	return res;
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d%d", &n, &l, &r, &a);
	int sq = (int)sqrt(a);
	for(int i = 2;i <= sq;++ i){
		if(a % i == 0){
			pr[len] = 1;
			while(a % i == 0){
				pr[len] *= i;
				a /= i;											
			}
			++ len;
		}
	}
	if(a > 1)
		pr[len ++] = a;
	d[0][1][0] = 1;
	for(int i = 1;i <= n;++ i){
		for(int cur = l;cur <= r;++ cur){
		    int our_mask = 0;
			for(int it = 0;it < len;++ it)
				if(cur % pr[it] == 0)
					our_mask |= (1 << it);
			for(int prev_mask = 0;prev_mask < (1 << len);++ prev_mask)
				add(d[i][cur][prev_mask | our_mask], sum(i - 1, prev_mask, cur)); 
		}
	}
	int res = 0;
	for(int i = l;i <= r;++ i)
		add(res, d[n][i][(1 << len) - 1]);
	printf("%d\n", res);
	return 0;
}