#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>

using namespace std;

#define name "F"

const int N = 100005;

int n;
int a[N];

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	for(int i = 1;i <= n;++ i)
		scanf("%d", &a[i]);
	for(int i = 1;i <= n;++ i){
	    int cnt = 0;
		for(int j = 1;j <= n;++ j){
			if(i == j)
				continue;
			if(!(a[i] & a[j]))
				++ cnt;
		}
		printf("%d ", cnt);
	}
	return 0;
}
