#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <math.h>

using namespace std;

#define name "D"
#define mod 1000000007

const int N = 1000005;

int n;
int l, r;
int a;
int d[2][(1 << 6) + 1][N];
int cur = 0;
int sum[(1 << 6) + 1][N];
int pr[25], len = 0;

void add(int &a, int b){
	if((a += b) >= mod)
		a -= mod;
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d%d", &n, &l, &r, &a);
	int sq = (int)sqrt(a);
	for(int i = 2;i <= sq;++ i){
		if(a % i == 0){
			pr[len] = 1;
			while(a % i == 0){
				pr[len] *= i;
				a /= i;											
			}
			++ len;
		}
	}
	if(a > 1)
		pr[len ++] = a;
	for(int i = 1;i <= r;++ i)
		sum[0][i] = 1;
	for(int i = 1;i <= n;++ i){
	    memset(d[cur], 0, sizeof d[cur]);
		for(int num = l;num <= r;++ num){
		    int our_mask = 0;
			for(int it = 0;it < len;++ it)
				if(num % pr[it] == 0)
					our_mask |= (1 << it);
			for(int prev_mask = 0;prev_mask < (1 << len);++ prev_mask)
				add(d[cur][prev_mask | our_mask][num], sum[prev_mask][num]); 
		}
		memset(sum, 0, sizeof sum);
		for(int mask = 0;mask < (1 << len);++ mask)
			for(int num = 1;num <= r;++ num){
				sum[mask][num] += sum[mask][num - 1];
				add(sum[mask][num], d[cur][mask][num]);
			}
		cur ^= 1;
	}
	int res = 0;
	for(int i = l;i <= r;++ i)
		add(res, d[cur ^ 1][(1 << len) - 1][i]);
	printf("%d\n", res);
	return 0;
}