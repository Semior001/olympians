#include <bits/stdc++.h>
using namespace std;

const int N=50001;
long long n,a[N],x,k=1,k1=1;
bool used[N];
int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin>>n>>x;
    for (int i=2;i<=2*n;i++)
        cin>>a[i];
    sort(a+2,a+2*n+1);
    long long maxp=x+a[2*n], last=2*n-1;
    for (int i=2;i<2*n;i++)
    {
        while (used[last])
                --last;
        if (!used[i])
        {
            bool t=false;
            for (int j=i+1;j<2*n;j++)
            {
                if ((a[j]+a[i])<=maxp && ((a[j+1]+a[i])>maxp ||  (j==last) )&& !used[j])
                {
                    //cout<<(a[j]+a[i])<<" "<<maxp<<endl;
                    used[i]=t=used[j]=true;
                    break;
                }
            }
            if (!t)
                k++,used[i]=used[last]=true;
        }
    }
    for (int i=3;i<=2*n;i++)
        used[i]=false;

    int minp=x+a[2];
    for (int i=3;i<2*n;i++)
    {
        if (!used[i])
        {
            for (int j=i+1;j<=2*n;j++)
            {
                if ((a[i]+a[j])>minp && !used[j])
                {
                    used[i]=true;
                    k1++;
                    used[j]=true;
                    break;
                }
            }
        }
    }
    cout<<k<<" "<<k1;
    return 0;
}
