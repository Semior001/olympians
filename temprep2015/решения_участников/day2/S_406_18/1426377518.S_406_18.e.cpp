#include <bits/stdc++.h>
using namespace std;

const int N=50001;

long long n,a[N],x,k=1,k1=1;
bool used[N],used1[N];

int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);

    cin>>n>>x;
    for (int i=2;i<=2*n;i++)
        cin>>a[i];
    sort(a+2,a+2*n+1);
    long long maxp=x+a[2*n], last=2*n-1;
    for (int i=2;i<2*n;i++)
    {
        while (used[last])
                --last;
        if (!used[i])
        {
            bool t=false;
            for (int j=last;j>i;j--)
            {
                if ((a[j]+a[i])<=maxp && !used[j])
                {
                    //cout<<(a[j]+a[i])<<" "<<maxp<<endl;
                    used[i]=t=used[j]=true;
                    break;
                }
            }
            if (!t)
                k++,used[i]=used[last]=true;
        }
    }
    long long minp=x+a[2];
    for (int i=3;i<2*n;i++)
    {
        if (!used1[i])
        {
            for (int j=i+1;j<=2*n;j++)
            {
                if ((a[i]+a[j])>minp && !used1[j])
                {
                    used1[i]=true;
                    k1++;
                    used1[j]=true;
                    break;
                }
            }
        }
    }
    cout<<k<<" "<<k1;
    return 0;
}
