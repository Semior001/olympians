#include <bits/stdc++.h>
using namespace std;

const int N=3;

int n,l,r,a,cnt;

long long lcd(long long x, long long y)
{
    return x/__gcd(x,y)*y;
}

int main()
{
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);

    cin>>n>>l>>r>>a;

    if (n==1)
    {
        for (int i=l;i<=r;i++)
            if (i%a==0)
                cnt++;
    }
    else
    {
        for (int i=l;i<=r;i++)
            for (int j=i;j<=r;j++)
                if (lcd(i,j)%a==0)
                    cnt++;
    }
    cout<<cnt;
    return 0;
}
