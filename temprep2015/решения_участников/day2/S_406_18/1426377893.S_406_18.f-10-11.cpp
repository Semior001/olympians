#include <bits/stdc++.h>
using namespace std;

const int N = 1e5+1;

long long n,a[N],cnt[N];

int main()
{
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);

    cin>>n;
    for (int i=1;i<=n;i++)
        cin>>a[i];

    for (int i=1;i<n;i++)
        for (int j=i+1;j<=n;j++)
            if (!(a[i] & a[j]))
                cnt[i]++,cnt[j]++;

    for (int i=1;i<=n;i++)
        cout<<cnt[i]<<" ";

    return 0;
}
