#include <bits/stdc++.h>
using namespace std;

const int N = 1e5+1;

long long n,a[N],cnt[N],sum[N];

bool used[N];
vector <int> v[N];

int main()
{
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);

    cin>>n;
    for (int i=1;i<=n;i++)
    {
        cin>>a[i];
        if (a[i]<=100)
        {
            used[a[i]]=true;
            sum[a[i]]++;
        }
    }
    if (n<=10000)
    {
        for (int i=1;i<n;i++)
            for (int j=i+1;j<=n;j++)
                if (!(a[i] & a[j]))
                    cnt[i]++,cnt[j]++;

        for (int i=1;i<=n;i++)
            cout<<cnt[i]<<" ";
    }
    else
    {
        for (int i=1;i<=100;i++)
        {
            for (int j=1;j<=100;j++)
                if (!(i&j))
                {
                    v[i].push_back(j);
                    v[j].push_back(i);
                }
        }
        for (int i=1;i<=n;i++)
        {
            for (int j=0;j<v[a[i]].size();j++)
            {
                int x=v[a[i]][j];
                if (used[x])
                    cnt[x]++;
            }
        }
        for (int i=1;i<=n;i++)
            cout<<cnt[a[i]]/2<<" ";
    }
    return 0;
}
