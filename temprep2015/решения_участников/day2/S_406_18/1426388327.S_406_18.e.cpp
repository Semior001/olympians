#include <bits/stdc++.h>
using namespace std;

const int N=200010;

long long n,a[N],x,k=1,k1=1;
bool used[N],used1[N];

int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);

    cin>>n;
    for (int i=1;i<=2*n;i++)
        cin>>a[i];

    sort(a+2,a+2*n+1);

    long long maxp=a[1]+a[2*n], minp=a[1]+a[2],last=2*n-1,first=3;

    for (int i=2;i<2*n;i++)
    {
        if (!used[i])
        {
            used[i]=true;
            while (used[last])
                last--;
            int l=i+1,r=2*n-1,m1;
            while (l<=r)
            {
                int m=(l+r)/2;
                if (a[m]+a[i]>maxp)
                    r=m-1;
                else
                {
                    l=m+1;
                    m1=m;
                }
            }
            while (used[m1])
                m1--;
            if (m1<2)
            {
                //cout<<i<<" "<<last<<endl;
                used[last--]=true;
                k++;
            }
            else
            {
                //cout<<i<<" "<<m1<<endl;
                used[m1]=true;
            }
        }
    }
    cout<<k<<" ";

    for (int i=3;i<=2*n;i++)
    {
        if (!used1[i])
        {
            used1[i]=true;
            while (used1[first])
                first++;
            int l=i+1,r=2*n,m1;
            while (l<=r)
            {
                int m=(l+r)/2;
                if (a[m]+a[i]>minp)
                {
                    r=m-1;
                    m1=m;
                }
                else
                {
                    l=m+1;
                }
                //cout<<l<<"-"<<r<<",";
            }
            //cout<<endl;

            if (m1==0)
            {
                //cout<<i<<"-"<<first<<endl;
                used1[first++]=true;
            }
            else
            {
                int m2=m1;
                while (used1[m1])
                    m1++;
                if (m1>(2*n))
                {
                    while (used1[m2])
                        m2--;
                    m1=m2;
                }
                cout<<i<<" "<<m1<<endl;
                if (a[i]+a[m1]>minp)
                    k1++;
                used1[m1]=true;
            }
        }
    }
    cout<<k1;
    return 0;
}
