#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll N=1111111;
ll n,h[N]={},c[N]{},l,r,ans,minn,maxx;
bool ch,b[N];
int main(){
    freopen("F.in","r",stdin);
    freopen("F.out","w",stdout);
    cin>>n;
    for(ll i=1;i<=n;i++)
        cin>>h[i];
    for(ll i=1;i<=n;i++)
        cin>>c[i];
    l=1,r=n;
    while(h[r]>=h[r-1])r--;
    while(h[l]<=h[l+1])l++;
    while(l<r){
        minn=maxx=l;
        for(ll i=l;i<=r;i++){
            if(h[i]>=h[maxx]&&c[i]<=c[maxx]&&!b[maxx])maxx=i;
            if(h[i]<=h[minn]&&c[i]<=c[minn]&&!b[minn])minn=i;
        }
        if(c[maxx]>c[minn])
            l++,ans+=c[minn],b[minn]=1;
        else
            r--,ans+=c[maxx],b[maxx]=1;
    }
    cout<<ans;
    return 0;
}
