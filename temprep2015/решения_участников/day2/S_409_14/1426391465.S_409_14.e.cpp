#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int a[222222], n, sz;
int ma, mi = 1111111;
int cur, pos, pos1, ans;
int w[5555];

int main () {
	
	freopen ("a.in", "r", stdin);
	freopen ("a.out", "w", stdout);
	
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++) {
		scanf("%d", &a[i]);                                  
	}           

	for (int i = 2; i <= 2 * n; i++) {
		cur = (a[1] + a[i]) / 2;
		if (cur > ma) ma = cur, pos = i;
		if (cur < mi) mi = cur, pos1 = i;	
	}
	w[pos] = 1;
	for (int i = 2; i <= 2 * n; i++) {
		if (w[i]) continue;
		for (int j = i + 1; j <= 2 * n; j++) {
			if (w[j]) continue;
			else
			if ((a[i] + a[j]) / 2 <= ma) {ans++; w[i] = 1; w[j] = 1; break;} 
		}
	}
	cout << n - ans << " ";
	for (int i = 1; i <= 2 * n; i++) w[i] = 0;
	w[pos1] = 1; ans = 0;
	for (int i = 2; i <= n * 2; i++) {
		if (w[i]) continue;
		for (int j = n * 2; j >= 1; j--) {
			if (w[j]) continue;
			else
			if ((a[i] + a[j]) / 2 > mi) {ans++; w[i] = 1; w[j] = 1; break;}
		}
	}   
	cout << ans + 1;
}
