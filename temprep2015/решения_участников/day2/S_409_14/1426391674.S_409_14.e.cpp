#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int a[222222], n, sz;
pair < int, int > mi[222222], ma[222222];
int cur;
int b[5555][5555];

int main () {
	
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++) {
		scanf("%d", &a[i]);
		mi[i].fi = 1111111;
	}
	for (int i = 1; i <= 2 * n; i++) {
		for (int j = 1; j <= 2 * n; j++) {
			b[i][j] = (a[i] + a[j]) / 2;
		}
		for (int j = 1; j <= 2 * n; j++) {
			mi[i].fi = min(mi[i].fi, b[i][j]);
			ma[i].fi = max(ma[i].fi, b[i][j]);
		}
		mi[i].se = i;
		ma[i].se = i;
	}                        
	sort(ma + 1, ma + (2 * n + 1));
	sort(mi + 1, mi + (2 * n + 1));
	for (int i = 1; i <= 2 * n; i++) 
		if (mi[i].se == 1) cur = i;
	while (cur < 2 * n && mi[cur].fi == mi[cur + 1].fi) cur++;
	if (cur < n) cout << cur; else cout << (cur % n) + 1; cout << " ";
	for (int i = 1; i <= 2 * n; i++)
		if (ma[i].se == 1) cur = i;
	while (cur > 1 && ma[cur].fi == ma[cur - 1].fi) cur--;
	if (cur < n) cout << cur; else cout << cur % n;
}