#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;
int mod = 1000000007, ans, n, l, r, lcm, anss = 1;

int main () {
	
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf("%d%d%d%d", &n, &l, &r, &lcm);
	if (n == 1) { 
		for (int i = l; i <= r; i++)
			if (i % lcm == 0) ans++, ans %= mod;
		cout << ans;
		return 0;
	}
	for (int i = 2; i <= r; i++) {
		if (lcm % i == 0) ans++, ans %= mod;
	} 
	for (int i = 1; i <= n; i++)
		anss *= ans, anss %= mod;
	cout << anss % mod;
	return 0;
}