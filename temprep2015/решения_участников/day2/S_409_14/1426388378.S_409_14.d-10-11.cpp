#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int ans, l, r, n, lcm, a[111], mod = 1000000007;

inline int nok(int x, int y) {
	return ((x * y) / __gcd(x, y));
}

void go(int p) {
	if (p > n) {
		int cur = 1;
		for (int i = 1; i <= n; i++) {
			cur = nok(cur, a[i]);
		}
		if (cur % lcm == 0) ans++;
		ans %= mod;
	}
	else {
		for (int i = l; i <= r; i++)
			a[p] = i, go(p + 1);
	}
}

int main () {
	
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	
	cin >> n >> l >> r >> lcm;
	go(1);
	if (n > 1) ans++, ans >>= 1;
	cout << ans % mod;
	return 0;
}