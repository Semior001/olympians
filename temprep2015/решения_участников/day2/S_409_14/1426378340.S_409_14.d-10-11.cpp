#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;
int mod = 1000000007, ans, n, l, r, lcm;

int nok(int x, int y) {
	return ((x * y) / __gcd(x, y));	
}

int main () {
	
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf("%d%d%d%d", &n, &l, &r, &lcm);
	if (n == 1) { 
		for (int i = l; i <= r; i++)
			if (i % lcm == 0) ans++;
		cout << ans;
		return 0;
	}
	for (int i = l; i <= r; i++) {
		for (int j = i; j <= r; j++) {
			if (nok(i, j) % lcm == 0) ans++, ans %= mod;      
		}
	}
	cout << ans;
	return 0;
}