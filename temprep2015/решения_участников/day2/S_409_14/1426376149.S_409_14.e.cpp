#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;
int ma, mi = 1111111, n;
pair < int, int > a[2111111];

int main () {
	
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i].fi), a[i].se = i;
	stable_sort(a + 1, a + (2 * n) + 1);          

	do {
		pair <int, int> b[111];
		int sz = 0;
		for (int i = 1; i <= 2 * n; i += 2) {
			b[++sz].fi = -(a[i].fi + a[i + 1].fi) / 2, b[sz].se = -sz;
			if (a[i].se == 1 || a[i + 1].se == 1) b[sz].se = -1111111;		
		}
		sort(b + 1, b + sz + 1);
		for (int i = 1; i <= sz; i++) 
			if (b[i].se == -1111111) {
				mi = min(mi, i);
				ma = max(ma, i);
			}
	}
	while (next_permutation(a + 1, a + (2 * n) + 1)); 
	cout << mi << " " << ma;
	return 0;
}