#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;
int ma, mi = 1111111, n, w;
int a[2111111];
int team[50] = {0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12};
pair < int, int > b[111111];

int main () {
	
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)                  
		scanf("%d", &a[i]);

	do {
		int pos = team[1], cur;
		for (int i = 1; i <= 2 * n; i++) 
			b[team[i]].fi += a[i];
	    for (int i = 1; i <= n; i++) b[i].fi /= 2, b[i].se = i;
	    sort(b + 1, b + n + 1);
	    reverse(b + 1, b + n + 1);
	    for (int i = 1; i <= n; i++)
	    	if (b[i].se == pos) 
	    		cur = i;                                                                  
	    while (b[cur].fi == b[cur - 1].fi && cur >= 2) cur--;
	    mi = min(mi, cur);
	    ma = max(ma, cur);   
	    for (int i = 1; i <= n; i++) b[i].fi = 0, b[i].se = 0;

	} while (next_permutation(team + 1, team + (2 * n) + 1));
	cout << mi << " " << ma;
	return 0;
}