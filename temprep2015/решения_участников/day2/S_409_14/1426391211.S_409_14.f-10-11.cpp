#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int n, a[4444444], w[111][111], ans[4111111], d[111], ma;
vector < int > v[111];

int main () {
	
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++)
		ma = max(ma, a[i]);
	if (ma <= 100) {

	for (int i = 1; i <= n; i++)
		v[a[i]].pb(i);
	for (int i = 1; i <= 100; i++) 
		for (int j = 1; j <= 100; j++)
			if (!(i & j)) w[i][j] = 1;
	
	for (int i = 1; i <= 100; i++) {
		if (v[i].size() >= 1) {
			for (int j = 1; j <= 100; j++) {
				if (v[j].size() >= 1 && w[i][j]) d[i] += v[j].size();
			}
		}	
	}
	for (int i = 1; i <= 100; i++) {
		if (v[i].size() >= 1) {
			for (int j = 0; j < v[i].size(); j++) {
				ans[v[i][j]] = d[i];
			}
		}
	}
	for (int i = 1; i <= n; i++)
		printf("%d ", ans[i]);
	}
	else {
		for (int i = 1; i <= n; i++) {
			int k = 0;
			for (int j = 1; j <= n; j++)
				if (!(a[i] & a[j])) k++;
			printf("%d ", k);
		}	
	}
	return 0;
}