//by Nursultan Nogai

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;

const int N = 1001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("F.in","r",stdin);
    freopen("F.out","w",stdout);
    ll n;
    ll a[N];
    ll w[N];
    ll ans=0;
    cin>>n;
    for (int i=1;i<=n;++i) cin>>a[i];
    for (int i=1;i<=n;++i) cin>>w[i];
    for (int i=1;i<n;++i) {
        ll c=0,m;
        for (int j=i+1;j<=n;++j) {
            if (a[i]>a[j]) c++,m=w[j];
        }
        if (c>1) ans+=w[i];
        else if (c==1) ans+=min(m,w[i]);
    }
    cout<<ans;
    return 0;
}
