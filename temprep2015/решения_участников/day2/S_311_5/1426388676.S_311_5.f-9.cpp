//by Nursultan Nogai

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;

const int N = 31, INF = 1e9;

int n,h[N],c[N],ans=0;
bool u[N]={false};

bool T() {
    for (int i=1;i<n;++i) {
        if (h[i]>h[i+1])
            return false;
    }
    return true;
}

void rep(int s,bool q,int z) {
    int f;
    if (q) {
        for (int i=s+1;z>0;++i) {
            if (h[s]>h[i]) --z, f=i;
        }
    }
    else {
        for (int i=s-1;z>0;--i) {
            if (h[s]<h[i]) --z, f=i;
        }
    }
    if (!u[s]) {
        u[s] = true;
        ans += c[s];
    }
    if (s<f) {
        for (int i=s;i<f;++i) {
            swap(h[i],h[i+1]);
            swap(c[i],c[i+1]);
            swap(u[i],u[i+1]);
        }
    }
    else {
        for (int i=s;i>f;--i) {
            swap(h[i],h[i-1]);
            swap(c[i],c[i-1]);
            swap(u[i],u[i-1]);
        }
    }
}

int main() {
    //ios_base::sync_with_stdio(0);
    //freopen("F.in","r",stdin);
    //freopen("F.out","w",stdout);
    cin>>n;
    for (int i=1;i<=n;++i) cin>>h[i];
    for (int i=1;i<=n;++i) cin>>c[i];
    while (!T()) {
        int l[N]={0},r[N]={0},mx=0;
        for (int i=1;i<=n;++i) {
            for (int j=1;j<i;++j)
                if (h[j]>h[i]) ++l[i];
            for (int j=i+1;j<=n;++j)
                if (h[i]>h[j]) ++r[i];
            int t1 = max(l[mx],r[mx]);
            int t2 = max(l[i],r[i]);
            if (t1<t2 || (t1==t2 && (u[i] || c[mx]>c[i]))) mx = i;
        }
        if (mx>0) {
            if (l[mx]<r[mx]) rep(mx,1,r[mx]);
            else rep(mx,0,l[mx]);
        }
    }
    cout<<ans;
    return 0;
}
