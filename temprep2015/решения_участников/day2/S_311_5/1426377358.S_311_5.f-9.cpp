//by Nursultan Nogai

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;

const int N = 1050001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    int n,a,b,w;
    cin>>n>>a;
    vector<int>v(2*n-1);
    for (int i=0;i<2*n-1;++i) cin>>v[i];
    sort(v.begin(),v.end());
    int x=a+v[2*n-2],j=2*n-3;
    b=n;
    for (int i=0;i<j;++i) {
        if (v[i]+v[j]<=x) --b,--j;
        else ++i;
    }
    w=1; x=a+v[0];
    int r=2*n-2;
    for (int i=1;i<r;++i) {
        for (int j=r;j>i;--j) {
            if (v[i]+v[j]>x) {
                ++w;
                swap(v[j],v[r]);
                --r;
                break;
            }
        }
    }
    cout<<b<<' '<<w;
    return 0;
}
