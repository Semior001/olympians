//by Nursultan Nogai

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;

const int N = 1050001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);
    ll a,c,l,r,res;
    res = 0;
    cin>>a>>c>>l>>r;
    a -= c;
    for (ll i=1;i*i<=a;++i) {
        if (a%i==0) {
            ll x,y;
            x = i;
            y = a/i;
            if (l<=x && x<=r && a%x==c) ++res;
            if (l<=y && y<=r && y!=x && a%y==c) ++res;
        }
    }
    cout<<res;
    return 0;
}
