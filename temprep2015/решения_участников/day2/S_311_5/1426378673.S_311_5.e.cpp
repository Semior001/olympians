//by Nursultan Nogai

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;

const int N = 1050001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    ll n,a,b,w;
    cin>>n>>a;
    vector<ll>v(2*n-1);
    for (ll i=0;i<2*n-1;++i) cin>>v[i];
    sort(v.begin(),v.end());
    ll x=a+v[2*n-2],r=2*n-3;
    b=n;
    for (ll i=0;i<r;++i) {
        for (ll j=r;j>i;--j) {
            if (v[i]+v[j]<=x) {
                --b;
                swap(v[j],v[r]);
                --r;
                break;
            }
        }
    }
    w=1; x=a+v[0];
    r=2*n-2;
    for (ll i=1;i<r;++i) {
        for (ll j=r;j>i;--j) {
            if (v[i]+v[j]>x) {
                ++w;
                swap(v[j],v[r]);
                --r;
                break;
            }
        }
    }
    cout<<b<<' '<<w;
    return 0;
}
