#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<algorithm>
#include<cmath>

using namespace std;

int N, a[200010];
int A, pA, sz, pmin[100010], pmax[100010], minn, maxx;
int f3()
{
	sz = 0;
	pA = A + a[1];		
	for(int i = 2; i < N * 2 - 1; i += 4)
	{
		if(i + 2 >= N * 2 - 1)
			pmax[++sz] = a[i] + a[i + 1];
		else
		{		
			pmax[++sz] = a[i] + a[i + 2];
			pmax[++sz] = a[i + 1] + a[i + 3];
		}                             
	} 
	sort(pmax + 1, pmax + sz + 1);
	/*cout<<"\n";
	for(int i = 1; i <= sz; i++)
		cout<<pmax[i]<<" ";
	cout<<"\n";	
	*/
	for(int i = 1; i <= sz; i++)	
		if(pmax[i] > pA)
			return N - (i - 1);
	return 1;	
}
int f2()
{
	sz = 0;
	pA = A + a[1];		
	for(int i = 2; i < N * 2 - 1; i += 2)
		pmax[++sz] = a[i] + a[i + 1]; 
	sort(pmax + 1, pmax + sz + 1);
	for(int i = 1; i <= sz; i++)	
		if(pmax[i] > pA)
			return N - (i - 1);
	return 1;
}
int f1()
{
	sz = 0;
	pA = A + a[N * 2 - 1];
	//cout<<"f1:\n"<<a[N * 2 - 1]<<"\n";
	for(int i = 1; i <= N - 1; i++)
		pmin[++sz] = a[i] + ((N - 1) * 2 - i + 1);
	sort(pmin + 1, pmin + sz + 1);
	for(int i = 1; i <= sz; i++)
		if(pmin[i] > pA)
			return N - (i - 1);
	return 1;
}
int main()
{
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin>>N;
	for(int i = 1; i <= N * 2; i++)
		cin>>a[i];
	A = a[1];
	//cout<<A<<"\n";
	a[1] = 1000000;	
	sort(a + 1, a + N * 2 + 1);
	//for(int i = 1; i <= N * 2; i++)
	//	cout<<a[i]<<" ";
	//cout<<"\n";	
	cout<<f1()<<" "<<max(f2(), f3());	

	return 0;
}