#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<algorithm>
#include<cmath>

using namespace std;

int N, a[200010];
int A, sz, minn, maxx;
double pA, pmin[100010], pmax[100010]; 
int f4()
{
	int l = -1, j, c;
	c = 0;
	sz = 0;
	pA = (1.0 * (A + a[1])) / 2;
	//cout<<pA<<"\n";
	for(int i = N * 2 - 1; i > 1; --i)
	{
		if(l == -1)
		{
			j = i - 1;
			if(((1.0 * (a[i] + a[j])) / 2) <= pA)
				return c;
			while(((1.0 * (a[i] + a[j])) / 2) > pA && j > 1)
				--j;
			l = j + 2;
			//cout<<l<<"\n";
			c++;
		}
		else
		{
			//cout<<l<<" "<<i<<"\n";
			if(l >= i || ((1.0 * (a[i] + a[l])) / 2) <= pA)
				return c;
			else	
			{
				l++;
				c++;
			}
		}	
	}
	return c;
}
int f3()
{
	sz = 0;
	pA = (1.0 * (A + a[1])) / 2;		
	for(int i = 2; i <= N * 2 - 1; i += 4)
	{
		if(i + 2 >= N * 2 - 1)
			pmax[++sz] = (1.0 * (a[i] + a[i + 1])) / 2;
		else
		{		
			pmax[++sz] = (1.0 * (a[i] + a[i + 2])) / 2;
			pmax[++sz] = (1.0 * (a[i + 1] + a[i + 3])) / 2;
		}                             
	} 
	sort(pmax + 1, pmax + sz + 1);
	/*cout<<"\n";
	for(int i = 1; i <= sz; i++)
		cout<<pmax[i]<<" ";
	cout<<"\n";	
	*/
	for(int i = 1; i <= sz; i++)	
		if(pmax[i] > pA)
			return N - (i - 1);
	return 1;	
}
int f2()
{
	sz = 0;
	pA = (1.0 * (A + a[1])) / 2;		
	for(int i = 2; i <= N * 2 - 1; i += 2)
		pmax[++sz] = (1.0 * (a[i] + a[i + 1])) / 2; 
	sort(pmax + 1, pmax + sz + 1);
	for(int i = 1; i <= sz; i++)	
		if(pmax[i] > pA)
			return N - (i - 1);
	return 1;
}
int f1()
{
	sz = 0;
	pA = (1.0 * (A + a[N * 2 - 1])) / 2;
	//cout<<"f1:\n"<<a[N * 2 - 1]<<"\n";
	for(int i = 1; i <= N - 1; i++)
		pmin[++sz] = (1.0 * (a[i] + ((N - 1) * 2 - i + 1))) / 2;
	sort(pmin + 1, pmin + sz + 1);
	for(int i = 1; i <= sz; i++)
		if(pmin[i] > pA)
			return N - (i - 1);
	return 1;
}
int main()
{
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin>>N;
	for(int i = 1; i <= N * 2; i++)
		cin>>a[i];
	A = a[1];
	//cout<<A<<"\n";
	a[1] = 1000000;	
	sort(a + 1, a + N * 2 + 1);
	//for(int i = 1; i <= N * 2; i++)
	//	cout<<a[i]<<" ";
	//cout<<"\n";	
	cout<<f1()<<" "<<max(N - (N - f4()) + 1, f3());	

	return 0;
}