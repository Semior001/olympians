#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#include <unordered_map>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "D"
using namespace std;
const int N = 1500;
const int mod = 1000000007;
int n, l, r, A, ans, x[N];
unordered_map<int, bool> ok;
int nod (int a, int b)
{
	if (!a)
		return b;
	return nod(b % a, a);
}
int nok (int a, int b)
{
	return (a * b) / nod(a, b);
}
void solve(int v, int now = 0)
{
	if (v == n + 1)
	{
		if (ok[now])	
		{
			++ ans;
			if (ans > mod)
			{
				ans -= mod;
			}
		}
		return;
	}
	if (v == 1)
	{
		for(int i = l; i <= r; i ++)
		{
			x[v] = i;
			solve(v + 1, i);
		}
	}
	else
	{
		for(int i = x[v - 1]; i <= r; i ++)
		{
			x[v] = i;
			solve(v + 1, nok(now, i));
		}
	}
}
int main()
{
	srand(time(0));
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> A;
	if (n == 2)
	{
		for (int i = l; i <= r; i ++)
		{
			for (int j = i; j <= r; j ++)
			{
				if (nok(i, j) % A == 0)
				{
					++ ans;
					if (ans >= mod)
					{
						ans -= mod;
					}
				}
			}
		}
		cout << ans;
		return 0;
	}
	ans = 0;
	if (n == 1)
	{
		for (int i = A; i <= r; i += A)
		{
			if (i >= l)
			{
				++ ans;
				if (ans >= mod)
				{
					ans -= mod;
				}
			}
		}
		cout << ans; 
		return 0;
	}
	ans = 0;
	for (int i = A; i <= 1000000; i += A)
	{
		ok[i] = 1;
	}
	assert(n <= 10);
//	assert((r - l + 1) * n <= 100000000);
	solve(1);
	cout << ans;
	return 0;
}