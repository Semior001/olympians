#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "F"
using namespace std;
const int N = 100500;
int n, a[N], ans[N], answ;
vector <int> c[400];
multiset<int> s;
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	if (n > 10000)
	{

		for (int i = 1; i <= 200; i ++)	
		{
			for (int j = i; j <= 200; j ++)
			{
				if (!(i & j))
				{
					c[i].pb(j);
					c[j].pb(i);
				}
			}
		}
		for (int i = 1; i <= n; i ++)
		{
			cin >> a[i];
			s.insert(a[i]);
		}
		for (int i = 1; i <= n; i ++)
		{
			answ = 0;
			for (int j = 0; j < c[a[i]].size(); j ++)
			{
				answ += s.count(c[a[i]][j]);
			}
			cout << answ << " ";
		}
		return 0;
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i + 1; j <= n; j ++)
		{
			if (!(a[i] & a[j]))
			{
				ans[i] ++;
				ans[j] ++;
			}
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cout << ans[i] << " ";
	}
	return 0;
}