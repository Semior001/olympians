#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "D"
using namespace std;
const int N = 100500;
int n, l, r, A, ans;
int nod (int a, int b)
{
	if (!a)
		return b;
	return nod(b % a, a);
}
int nok (int a, int b)
{
	return (a * b) / nod(a, b);
}
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> A;
	if (n == 2)
	{
		for (int i = l; i <= r; i ++)
		{
			for (int j = i; j <= r; j ++)
			{
				if (nok(i, j) % A == 0)
				{
					++ ans;
				}
			}
		}
		cout << ans;
		return 0;
	}
	return 0;
}