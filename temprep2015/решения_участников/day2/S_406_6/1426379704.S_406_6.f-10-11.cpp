#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "F"
using namespace std;
const int N = 100001;
int n, a[N], ans[N], answ, c[N];
int used[N];
multiset<int> s;
int main()
{
//	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	scanf("%d", &n);
	if (n > 10000)
	{
		for (int i = 1; i <= n; i ++)
		{
			scanf("%d", &a[i]);
			used[a[i]] ++;
		}
		for (int i = 1; i <= 200; i ++)	
		{
			for (int j = i; j <= 200; j ++)
			{
				if (!(i & j) && used[i] && used[j])
				{
					c[i] += used[j];
					c[j] += used[i];
				}
			}
		}
		for (int i = 1; i <= n; i ++)
		{
			printf("%d ", c[a[i]]);
//			cout << answ << " ";
		}
		return 0;
	}
	for (int i = 1; i <= n; i ++)
	{
		scanf("%d", &a[i]);
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i + 1; j <= n; j ++)
		{
			if (!(a[i] & a[j]))
			{
				ans[i] ++;
				ans[j] ++;
			}
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		printf("%d ",ans[i]);
//		cout << ans[i] << " ";
	}
	return 0;
}