#include <bits/stdc++.h>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define INF (int)1e9
#define fname "E"
using namespace std;
const int N = 100500;
int n, maxi, mini, power, my_pow, uk1, uk2, ans;
struct az
{
	int pos, x;
}a[N], b[N];
bool cmp(az a, az b)
{
	if (a.x == b.x)
	{
		return a.pos < b.pos;
	}
	return a.x > b.x;
}
void solve();
int main()
{
//	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n + n; i ++)
	{
		cin >> a[i].x;
		a[i].pos = i;
		b[i].x = a[i].x;
		b[i].pos = i;
	}
	my_pow = a[1].x;
	sort(a + 1, a + n + n + 1, &cmp);
	ans = 0;
	mini = INF;
	power = (a[1].pos != 1 ? a[1].x : a[2].x);
	my_pow += power;
	uk1 = (a[1].pos != 1 ? 2 : 3);
	uk2 = (a[n + n].pos != 1 ? n + n : n + n - 1);
	while (uk1 < uk2)
	{
//		cout << uk1 << " " << uk2 << "\n";
		if (a[uk1].pos == 1)
		{
			++ uk1;
			continue;
		}
		if (a[uk2].pos == 1)
		{
			-- uk2;
			continue;
		}
		if (a[uk1].x + a[uk2].x <= my_pow)
		{
			uk1 ++;
			uk2 --;
			continue;
		}
		++ ans;
		uk1 ++;
		uk2 --;
	}
	mini = min(mini, ans);
	ans = 0;
	for (int i = (a[1].pos != 1 ? 2 : 3); i <= n + n; i)
	{
		if (a[i].pos == 1)
		{
			++ i;
			continue;
		}
		if (a[i + 1].pos == 1)
		{
			if (a[i].x + a[i + 2].x <= my_pow)
			{
				i += 3;
				continue;
			}
			++ ans;
			i += 3;
		}
		if (a[i].x + a[i + 1].x <= my_pow)
		{
			i += 2;
			continue;
		}
		++ ans;
		i += 2;
	}
	mini = min(mini, ans);
	uk1 = (a[n].pos != 1 ? n : n + 1);
	uk2 = (a[n + n].pos != 1 ? n + n : n + n - 1);
	ans = 0;
	while (uk1 && uk2)
	{
//		cout << uk1 << " " << uk2 << "\n";
		if (a[uk1].pos == 1)
		{
			-- uk1;
			continue;
		}
		if (a[uk2].pos == 1)
		{
			-- uk2;
			continue;
		}
		if (a[uk1].x + a[uk2].x <= my_pow)
		{
			uk1 --;
			uk2 --;
			continue;
		}
		++ ans;
		uk1 --;
		uk2 --;
	}
	mini = min(mini, ans);
	solve();
//	printf("%d %d", mini + 1, maxi + 1);
	cout << mini + 1 << " " << maxi + 1;
	return 0;
}
void solve()
{
	my_pow -= power;
	ans = 0;
	power = (a[n + n].pos != 1 ? a[n + n].x : a[n + n - 1].x);
	my_pow += power;
//	cout << my_pow;
	uk1 = (a[1].pos != 1 ? 2 : 3);
	uk2 = (a[n + n].pos != 1 ? n + n : n + n - 1);
	while (uk1 < uk2)
	{
//		cout << uk1 << " " << uk2 << "\n";
		if (a[uk1].pos == 1)
		{
			++ uk1;
			continue;
		}
		if (a[uk2].pos == 1)
		{
			-- uk2;
			continue;
		}
		if (a[uk1].x + a[uk2].x <= my_pow)
		{
			uk1 ++;
			uk2 --;
			continue;
		}
		++ ans;
		uk1 ++;
		uk2 --;
	}
	maxi = max(maxi, ans);
	ans = 0;
	for (int i = (a[1].pos != 1 ? 2 : 3); i <= n + n; i)
	{
		if (a[i].pos == 1)
		{
			++ i;
			continue;
		}
		if (a[i + 1].pos == 1)
		{
			if (a[i].x + a[i + 2].x <= my_pow)
			{
				i += 3;
				continue;
			}
			++ ans;
			i += 3;
		}
		if (a[i].x + a[i + 1].x <= my_pow)
		{
			i += 2;
			continue;
		}
		++ ans;
		i += 2;
	}
	maxi = max(maxi, ans);
	uk1 = (a[n].pos != 1 ? n : n + 1);
	uk2 = (a[n + n].pos != 1 ? n + n : n + n - 1);
	ans = 0;
	while (uk1 && uk2)
	{
//		cout << uk1 << " " << uk2 << "\n";
		if (a[uk1].pos == 1)
		{
			-- uk1;
			continue;
		}
		if (a[uk2].pos == 1)
		{
			-- uk2;
			continue;
		}
		if (a[uk1].x + a[uk2].x <= my_pow)
		{
			uk1 --;
			uk2 --;
			continue;
		}
		++ ans;
		uk1 --;
		uk2 --;
	}
	maxi = max(maxi, ans);
}