#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "F"
using namespace std;
const int N = 100500;
int n, a[N], ans[N];
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i + 1; j <= n; j ++)
		{
			if (!(i & j))
			{
				ans[i] ++;
				ans[j] ++;
			}
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cout << ans[i] << " ";
	}
	return 0;
}