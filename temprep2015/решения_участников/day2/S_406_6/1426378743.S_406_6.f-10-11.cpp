#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "F"
using namespace std;
const int N = 100500;
int n, a[N], ans[N], answ;
vector <int> c[200];
multiset<int> s;
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	if (n > 10000)
	{
		for (int i = 1; i <= 100; i ++)	
		{
			for (int j = 1; j <= 100; j ++)
			{
				if (!(i & j))
				{
					c[i].pb(j);
				}
			}
		}
		for (int i = 1; i <= n; i ++)
		{
			cin >> a[i];
			s.insert(a[i]);
		}
		for (int i = 1; i <= n; i ++)
		{
			answ = 0;
			for (int j = 0; j < c[i].size(); j ++)
			{
				answ += s.count(c[i][j]);
			}
			cout << answ << " ";
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i + 1; j <= n; j ++)
		{
			if (!(a[i] & a[j]))
			{
				ans[i] ++;
				ans[j] ++;
			}
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cout << ans[i] << " ";
	}
	return 0;
}