#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define INF (int)1e9
#define fname "E"
using namespace std;
const int N = 100500;
int n, a[N], maxi, mini = INF, ans, power;
void solve()
{
	ans = 0;
	power = a[1] + a[2];
	for (int i = 3; i <= n;i += 2)
	{
		if (a[i] + a[i + 1] <= power)
		{
			continue;
		}
		++ ans;
	}
	mini = min(mini, ans + 1);
	maxi = max(maxi, ans + 1);
}
int main()
{
//	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	scanf("%d", &n);
	n += n;
	for (int i = 1; i <= n; i ++)
	{
		scanf("%d", &a[i]);
	}
	sort(a + 2, a + n + 1);
	do
	{
		solve();
	}while(next_permutation(a + 2, a + n + 1));
	cout << mini << " " << maxi;
	return 0;
}