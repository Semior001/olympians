#include <bits/stdc++.h>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "E"
#define INF (int)1e9
using namespace std;
const int N = 100500;
int n, a[N], maxi, mini = INF, sum;
void solve(int wj)
{
	for (int i = 2; i <= n; i ++)
	{
		for (int j = 2; j <= n; j ++)
		{
			if (j != wj && i != wj)
			{
				if (a[i] + a[j] > a[1] + a[wj])
				{
					maxi = max(maxi, 2);
					if (sum - a[wj] - a[1] - a[i] - a[j] > a[1] + a[wj])
					{
//						cout << sum << " " << a[wj] + a[1] << " " << a[wj] + a[1] + a[i] + a[j] << "\n";
						maxi = max(maxi, 3);
					}
				}
				if (sum - a[wj] - a[1] - a[i] - a[j] > a[1] + a[wj])
				{
					maxi = max(maxi, 2);
				}
			}
		}
	}
	maxi = max (maxi, 1);
	for (int i = 2; i <= n; i ++)
	{
		for (int j = 2; j <= n; j ++)
		{
			if (j != wj && i != wj)
			{
				if (a[i] + a[j] <= a[1] + a[wj])
				{
					mini = min(mini, 2);
					if (sum - a[wj] - a[1] - a[i] - a[j] <= a[1] + a[wj])
					{
						mini = min(mini, 1);
					}
				}
				if (sum - a[wj] - a[1] - a[i] - a[j] <= a[1] + a[wj])
				{
					mini = min(mini, 2);
				}
			}
		}
	}
	mini = min (mini, n);
}
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	n += n;
	for(int i = 1; i <= n; i ++)
	{
		cin >> a[i];
		sum += a[i];
	}
	for (int i = 2; i <= n; i ++)
	{
		solve(i);
	}
	cout << mini << " " << maxi;
	return 0;
}