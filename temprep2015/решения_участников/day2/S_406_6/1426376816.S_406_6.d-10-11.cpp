#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "D"
using namespace std;
const int N = 100500;
const int mod = 1000000007;
int n, l, r, A, ans;
int nod (int a, int b)
{
	if (!a)
		return b;
	return nod(b % a, a);
}
int nok (int a, int b)
{
	return (a * b) / nod(a, b);
}
int main()
{
	srand(time(0));
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> A;
	if (n == 2)
	{
		for (int i = l; i <= r; i ++)
		{
			for (int j = i; j <= r; j ++)
			{
				if (nok(i, j) % A == 0)
				{
					++ ans;
					if (ans > mod)
					{
						ans -= mod;
					}
				}
			}
		}
		cout << ans % mod;
		return 0;
	}
	cout << rand() % 10000 + 1; 
	return 0;
}