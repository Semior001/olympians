#include <bits/stdc++.h>
using namespace std;
#define MAXN 55555
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
#define MOD ll(1e9+7)
typedef long long ll;

ll res;
ll n, l, r, v, ans = 1;
void enter(ll k, ll j)
{
    for (ll i = j; i <= r; i++)
    {
        if (n > k)
        {
            ans *= j;
            enter(k+1, i);
            ans/=j;
        }
        else
        {
            if ((ans*i) % v == 0)
            {
                res++;
                res %= MOD;
         ///       cout<<ans<<" "<<i<<endl;
            }
        }
    }
}

int main ()
{
    freopen ("D.in", "r", stdin);
    freopen ("D.out", "w", stdout);
    scanf("%lld%lld%lld%lld", &n, &l, &r, &v);
    enter(1, l);
    printf("%lld", res);
}
