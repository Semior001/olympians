#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;

ll res, k, v1, v2, v3, ttotal, pos;
ll ans, d1, d2, d3, total, sum1, sum2;
ll n, l, r, v, a[MAXN], b[MAXN], c[MAXN];

void go(ll l, ll j)
{
    for (ll i = j; i < 2*n; i++)
    {
        if (n < l+4)
            go(l+1, i+1);
    }
}

int main()
{
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= 2*n; i++)
        scanf("%lld", &a[i]), sum1+=a[i];
    sum2 = sum1;
    sort(a+2, a+1+2*n);
    ll maxx = a[1]+a[2*n];
    ll minn = a[1]+a[2];
    sum1 -= maxx;
    sum2 -= minn;

    for (ll i = 2; i < 2*n; i++)
    {
        pos = i;
            if (!b[a[i]])
        for (ll j = i+1 ; j < 2*n; j++)
        {
            if (a[i]+a[j] > maxx && !b[a[j-1]])
            {
                pos =j-1;
                b[a[j-1]]=1;
                break;
            }
        }
        if (i != pos)
            k++;
    }
    cout<<k+1<<" ";
    k = 0;
    for (ll i = 3; i <= 2*n; i++)
    {
        pos = i;
        if (!c[a[i]])
        for (ll j = i+1; j <= 2*n; j++)
        {
            if (a[i]+a[j]>minn&&!c[a[j]])
            {
                pos = j;
                c[a[j]]=1;
                break;
            }
        }
        if (i!=pos)
            k++;
    }
    cout<<k+1;
}
