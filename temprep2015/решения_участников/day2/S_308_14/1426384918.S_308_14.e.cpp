#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;

ll res, k, v1, v2, v3, ttotal;
ll ans, d1, d2, d3, total;
ll n, l, r, v, a[MAXN], b[MAXN];
inline bool check()
{
    if((total - d1)||(total-d2)||(total-d3))
        return 1;
    return 0;
}

int main()
{
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= 2*n; i++)
        scanf("%lld", &a[i]);
    k = n;
    sort(a+2, a+1+2*n);
    ll maxx = (a[1]+a[2*n]);
    ll minn = a[1]+a[2];

    for (ll i = 2; i <= n; i++)
    {
        if (maxx >= (a[i]*a[2*n-i+1]))
            k--;
    }

    v1 = a[2]+a[3];
    v2 = a[2]+a[4];
    v3 = a[2]+a[5];
    total = a[2]+a[3]+a[4]+a[5];
    if (maxx >= v1)
    {
        l++;
        if (maxx >= total - v1)
            l++;
    }
    ans = max(ans, l);
    l = 0;
    if (maxx >= v2)
    {
        l++;
        if (maxx >= total - v2)
            l++;
    }
    ans = max(ans, l);
    l = 0;
    if (maxx >= v3)
    {
        l++;
        if (maxx >= total - v3)
            l++;
    }
    ans = max(ans, l);
    l=0;

    ans = 0;

    d1 = a[3]+a[4];
    d2 = a[3]+a[5];
    d3 = a[3]+a[6];
    total = a[3]+a[4]+a[5]+a[6];
    if (minn < d1)
    {
        l++;
        if (minn < total - d1)
            l++;
    }
    ans = max(ans, l);
    l = 0;
    if (minn < d2)
    {
        l++;
        if (minn < total - d2)
            l++;
    }
    ans = max(ans, l);
    l = 0;
    if (minn < d3)
    {
        l++;
        if (minn < total - d3)
            l++;
    }
    ans = max(ans, l);
    ll g = check();
    ans = max(ans, g);
    cout<<k<<" "<<n-ans;
}
