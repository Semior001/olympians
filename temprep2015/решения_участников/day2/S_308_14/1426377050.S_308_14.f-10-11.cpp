#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define fi first
#define se second
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;
ll n, ans, b[MAXN], pos;
pair <ll, ll>a[MAXN];
inline bool cmp(pair<ll, ll> a, pair<ll, ll> b)
{
    if (a.fi%2==0)
        return 1;
    return 0;
}

int main ()
{
    freopen ("F.in", "r", stdin);
    freopen ("F.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= n; i++)
        scanf("%lld", &a[i].fi), a[i].se=i;
    sort(a+1, a+n+1, cmp);
    for (ll i = 1; i <= n; i++)
        if (a[i].fi%2)
        {
            pos = i;
            break;
        }
    for (ll i = 1; i < pos; i++)
    {
        for (ll j = pos; j <= n; j++)
        {
            if (!(a[i].fi&a[j].fi))
                b[a[i].se]++, b[a[j].se]++;
        }
    }

    for (ll i = 1; i < pos; i++)
    {
        for (ll j = 1; j < i; j++)
        {
            if (!(a[i].fi&a[j].fi))
                b[a[i].se]++, b[a[j].se]++;
        }
    }


    for (ll i = 1; i <= n; i++)
        printf("%lld ", b[i]);
}
