#include <bits/stdc++.h>
using namespace std;
#define MAXN 101111
#define INF ll(1e9)
typedef long long ll;

ll pos, k;
ll ans;
ll maxx, minn;
ll n, a[2*MAXN];
bool b[2*MAXN], c[2*MAXN];

int main()
{
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    scanf("%lld", &n);
    for (ll i = 1; i <= 2*n; i++)
        scanf("%lld", &a[i]);

    sort(a+2, a+1+2*n);

    maxx = a[1]+a[2*n];
    minn = a[1]+a[2];
    b[n*2]=1;

    for (ll i = 2; i < 2*n; i++)
    {
        pos = -INF;
            if (!b[i])
            {
                b[i]=1;
                for (ll j = i+1 ; j < 2*n; j++)
                {
                    if (a[i]+a[j] > maxx && !b[j-1])
                    {
                        pos = j-1;
                        b[j-1]=1;
                        break;
                    }
                    if (b[j+1] && !b[j])
                    {
                        b[j]=1;
                        pos = j;
                        break;
                    }
                }
                if (pos != -INF)
                {
                    if (a[i]+a[pos] <= maxx)
                        k++;
                }
            }
    }
    printf("%lld ", n-k);
    k = 0;
    b[n*2]=0;
    for (ll i = 3; i <= 2*n; i++)
    {
        pos = i;
        if (!c[i])
        for (ll j = i+1; j <= 2*n; j++)
        {
            if (a[i]+a[j]>minn&&!c[j])
            {
                pos = j;
                c[j]=1;
                break;
            }
        }
        if (i!=pos)
            k++;
    }
    printf("%lld", k+1);
}
