#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;

ll res, k;
ll n, l, r, v, a[MAXN], b[MAXN];

int main()
{
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= 2*n; i++)
        scanf("%lld", &a[i]);
    k = n;
    sort(a+2, a+1+2*n);
    ll maxx = (a[1]+a[2*n])>>1;
    ll minn = a[1]+a[2];

    for (ll i = 2; i <= n; i++)
    {
        if (maxx >= (a[i]*a[2*n-i+1])>>1)
            k--;
    }
    for (ll i = 3; i <= 2*n; i++)
        for (ll j = i+1; j <= 2*n; j++)
        {

        if (b[a[j]])
            continue;
        if ((a[i]+a[j]) >= minn)
        {
            l++;
            b[a[j]]=1;
            break;
        }
        }
    cout<<k<<" "<<n-l;
}
