#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;
ll n, ans, a[MAXN], b[4*MAXN];
int main ()
{
    freopen ("F.in", "r", stdin);
    freopen ("F.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= n; i++)
    {
        scanf("%lld", &a[i]);
        for (ll j = 1; j < i; j++)
        {
            if (!(a[i]&a[j]))
                b[i]++,b[j]++;
        }
    }
    for (ll i = 1; i <= n; i++)
        printf("%lld ", b[i]);
}
