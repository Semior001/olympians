#include <bits/stdc++.h>
using namespace std;
#define MAXN 111111
#define first fi
#define second se
#define INF ll(1e9)
#define abs(x) ((x>0)?x:(-x))
typedef long long ll;

ll pos, k;
ll ans;
ll n, a[MAXN], b[MAXN], c[MAXN];

int main()
{
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    cin >>n;
    for (ll i = 1; i <= 2*n; i++)
        scanf("%lld", &a[i]);
    sort(a+2, a+1+2*n);
    ll maxx = a[1]+a[2*n];
    ll minn = a[1]+a[2];

    for (ll i = 2; i < 2*n; i++)
    {
        pos = i;
            if (!b[i])
        for (ll j = i+1 ; j < 2*n; j++)
        {
            if (a[i]+a[j] > maxx && !b[j-1])
            {
                pos =j-1;
                b[j-1]=1;
                break;
            }
        }
        if (i != pos)
            k++;
    }
    cout<<k+1<<" ";
    k = 0;
    for (ll i = 3; i <= 2*n; i++)
    {
        pos = i;
        if (!c[i])
        for (ll j = i+1; j <= 2*n; j++)
        {
            if (a[i]+a[j]>minn&&!c[j])
            {
                pos = j;
                c[j]=1;
                break;
            }
        }
        if (i!=pos)
            k++;
    }
    cout<<k+1;
}
