#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "D"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int mod = INF + 7;
int n, k, l, r;


pair <int, int> b[77];
int sz = 0;

int prod[1001000];

ll d[120][(1 << 12)];

void add(int &x, int y) {
	if ((x += y) >= mod) x -= mod;
}

int a[1000100], ss = 0;

ll calc() {
	d[0][0] = 1;
	for (int i = 1; i <= ss; ++ i) {
		int j = i;
		while (j + 1 <= ss && prod[a[i]] == prod[a[j + 1]]) ++j;
	
		for (int mask = 0; mask < (1 << sz); ++ mask) {
			if ((mask | prod[a[i]]) == mask) {
				for (int cc = 1; cc <= j - i + 1; ++ cc) {
					for (int take = 0; take <= n; ++ take) {
						d[take + 1][mask | prod[a[i]]] += d[take][mask];
						if(d[take + 1][mask | prod[a[i]]] >= mod) d[take + 1][mask | prod[a[i]]] -= mod;
					}
				}
			}
			else {
				for (int take = 0; take <= n; ++ take) {
					d[take + 1][mask | prod[a[i]]] += d[take][mask] * (j - i + 1);
					d[take + 1][mask | prod[a[i]]] %= mod;
				}
			}
		}
		i = j;
	}
	return d[n][(1 << sz) - 1];
}

bool cmp(int a, int b) {
	return prod[a] < prod[b];
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n >> l >> r >> k;

	int x = k;

	for (int i = 2; i * i <= x; ++ i) {
		if (x % i == 0) {
			b[sz++] = mp(i, 0);
			while (x % i == 0) x /= i, b[sz - 1].S++;
		}
	}
	if (x > 1) b[sz++] = mp(x, 1);

	for (int i = l; i <= r; ++ i) {
		for (int j = 0; j < sz; ++ j) {
			x = i;
			int deg = 0;
			while(x % b[j].F == 0) ++deg, x /= b[j].F;
			if (deg >= b[j].S) prod[i] |= (1 << j);
		}
		a[++ss] = i;
	}

	random_shuffle(a + 1, a + 1 + ss);

//	sort(a + 1, a + 1 + ss, cmp);

//	for (int i = 0; i < sz; ++ i) cout << b[i].F << " " << b[i].S << endl;

	cout << calc();

	return 0;
}