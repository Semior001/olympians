#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "D"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int mod = INF + 7;
int n, k, l, r;


pair <int, int> b[77];
int sz = 0;

int prod[1001000];

ll d[12][(1 << 7)];

void add(int &x, int y) {
	if ((x += y) >= mod) x -= mod;
}

int calc() {
	d[0][0] = 1;
	for (int i = l; i <= r; ++ i) {
		
		int j = i;
		while (j + 1 <= r && prod[i] == prod[j + 1]) ++j;

		for (int take = 0; take <= n; ++ take) {
			for (int mask = 0; mask < (1 << sz); ++ mask) {
				d[take + 1][mask | prod[i]] += d[take][mask] * (j - i + 1);
				d[take + 1][mask | prod[i]] %= mod;
			}
		}
		i = j;
	}
	return d[n][(1 << sz) - 1];
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n >> l >> r >> k;

	int x = k;

	for (int i = 2; i * i <= x; ++ i) {
		if (x % i == 0) {
			b[sz++] = mp(i, 0);
			while (x % i == 0) x /= i, b[sz - 1].S++;
		}
	}
	if (x > 1) b[sz++] = mp(x, 1);

	for (int i = l; i <= r; ++ i) {
		for (int j = 0; j < sz; ++ j) {
			x = i;
			int deg = 0;
			while(x % b[j].F == 0) ++deg, x /= b[j].F;
			if (deg >= b[j].S) prod[i] |= (1 << j);
		}
	}

//	for (int i = 0; i < sz; ++ i) cout << b[i].F << " " << b[i].S << endl;

	cout << calc();

	return 0;
}