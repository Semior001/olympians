#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "F"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 100500;
int n, a[N], ans[N];

void solve() {
	if (n <= 10000) {
		for (int i = 1; i <= n; ++ i) {
			for (int j = i + 1; j <= n; ++ j) {
				if ((a[i] & a[j]) == 0) ans[i]++, ans[j]++;
			}
		}
		for (int i = 1; i <= n; ++ i) printf("%d ", ans[i]);
	}
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
	}

	solve();

	return 0;
}