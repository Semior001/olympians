#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "E"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int N = 100500;

int was[N];

int n, a[N], rating, b[N], our, sz;

int s1 = 0, s2 = 0;

int nn = (1 << 18), t[(1 << 20)];

void upd(int v) {
	v += nn - 1;
	t[v] = 0;
	v >>= 1;
	while(v) {
		t[v] = t[v + v] + t[v + v + 1];
		v >>= 1;
	}
}

int get(int l, int r) {
	if (l > r) return 0;
	l += nn - 1, r += nn - 1;
	int res = 0;
	while(l < r) {
		if (l & 1) res += t[l++];
		if (!(r & 1)) res += t[r--];
		l >>= 1, r >>= 1;
	}
	if (l == r) res += t[l];
	return res;
}

void clear() {
	for (int i = 1; i < nn + nn; ++ i) t[i] = 0;
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &rating);
	
	n <<= 1;
	--n;

	for (int i = 1; i <= n; ++ i) scanf("%d", &a[i]);

	sort(a + 1, a + 1 + n);

	for (int i = 1; i <= n; ++ i) b[i] = a[i];

	int l, r, pos, id;

////////////////////////////////  iwem min mesto
	for (int i = 1; i <= n; ++ i) a[i] = b[i];
	our = rating + b[1];
	sz = n - 1;
	for (int i = 1; i <= sz; ++ i) a[i] = a[i + 1];
	assert(sz % 2 == 0);

	clear();

	for (int i = 1; i <= sz; ++ i) {
		t[i + nn - 1] = 1;
	}
	for (int i = nn - 1; i > 0; -- i) t[i] = t[i + i] + t[i + i + 1];

	for (int i = sz; i > 0; -- i) {
		if (!get(i, i) || i == 1 || (a[i] + a[i - 1] <= our)) continue;

		l = 1, r = i - 1, pos = i;
		
		while (l <= r) {
			int mid = (l + r) >> 1;
			if (a[mid] + a[i] > our) {
				pos = mid;
				r = mid - 1;
			}
			else l = mid + 1;
		}

		if (pos >= i || !get(pos, i - 1)) continue;

		//[pos, i - 1] nahodim first edini4ku

		l = pos, r = i - 1;
		id = i - 1;

		while(l <= r) {
			int mid = (l + r) >> 1;
			if (get(pos, mid)) {
				r = mid - 1;
				id = mid;
			}
			else l = mid + 1;
		}
		upd(id);
		upd(i);
		++s1;
	}
////////////////////////////////


////////////////////////////////  iwem max mesto
	for (int i = 1; i <= n; ++ i) a[i] = b[i], was[i] = 0;
	our = rating + b[n];
	sz = n - 1;
	assert(sz % 2 == 0);
	for (int i = 1; i <= sz; ++ i) {
		if (was[i]) continue;
		for (int j = sz; j > i; -- j) {
			if (!was[j] && (a[i] + a[j]) <= our) {
				was[i] = 1;
				was[j] = 1;
				++s2;
				break;
			}
		}
	}

////////////////////////////////

	if ((n + 1) / 2 - s2 > s1 + 1) assert(0);

	cout << (n + 1) / 2 - s2 << " " << s1 + 1;

	return 0;
}