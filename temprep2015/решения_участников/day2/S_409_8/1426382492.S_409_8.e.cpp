#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "E"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int N = 200500;

int n, a[N], rating, b[N], our, sz;

int s1 = 0, s2 = 0, was[N];
int t1 = 0, t2 = 0;
int u1 = 0, u2 = 0;
int v1 = 0, v2 = 0;

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &rating);
	
	n <<= 1;
	--n;

	for (int i = 1; i <= n; ++ i) scanf("%d", &a[i]);

	sort(a + 1, a + 1 + n);

	for (int i = 1; i <= n; ++ i) b[i] = a[i];

////////////////////////////////  iwem max mesto
	for (int i = 1; i <= n; ++ i) a[i] = b[i], was[i] = 0;
	our = rating + b[1];
	sz = n - 1;
	for (int i = 1; i <= sz; ++ i) a[i] = a[i + 1];
	assert(sz % 2 == 0);
	for (int i = sz; i > 0; -- i) {
		if (was[i]) continue;
		for (int j = 1; j <= sz; ++ j) {
			if (!was[j] && (a[i] + a[j]) > our) {
				was[i] = 1;
				was[j] = 1;
				++s1;
				break;
			}
		}
	}
	for (int i = 1; i <= sz; i += 2) {
		if (a[i] + a[i + 1] > our) ++t1;
	}
	for (int i = 1; i <= sz / 2; ++ i) {
		if (a[i] + a[i + sz / 2] > our) ++u1;
	}
	for (int i = 1; i <= sz / 2; ++ i) {
		if (a[i] + a[sz - i + 1] > our) ++v1;
	}
////////////////////////////////


////////////////////////////////  iwem min mesto
	for (int i = 1; i <= n; ++ i) a[i] = b[i], was[i] = 0;
	our = rating + b[n];
	sz = n - 1;
	assert(sz % 2 == 0);
	for (int i = 1; i <= sz; ++ i) {
		if (was[i]) continue;
		for (int j = sz; j > 0; -- j) {
			if (!was[j] && (a[i] + a[j]) <= our) {
				was[i] = 1;
				was[j] = 1;
				++s2;
				break;
			}
		}
	}
	for (int i = 1; i <= sz; i += 2) {
		if (a[i] + a[i + 1] <= our) ++t2;
	}
	for (int i = 1; i <= sz / 2; ++ i) {
		if (a[i] + a[i + sz / 2] <= our) ++u2;
	}
	for (int i = 1; i <= sz / 2; ++ i) {
		if (a[i] + a[sz - i + 1] <= our) ++v2;
	}

////////////////////////////////

	int mn = max(s2, max(t2, max(u2, v2)));
	int mx = max(s1, max(t1, max(u1, v1)));

	cout << (n + 1) / 2 - mn << " " << mx + 1;

	return 0;
}