#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "F"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 5000500;
int n, a[N], d[N];

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
		d[a[i]]++;
	}

	for (int bit = 22; bit >= 0; -- bit) {
		for (int mask = (1 << bit); mask < (1 << 22); ++ mask) {
			if (mask & (1 << bit)) d[mask] += d[mask ^ (1 << bit)];
		}
	}

	for (int i = 1; i <= n; ++ i) {
		int ss = (a[i] ^ ((1 << 7) - 1));
		printf("%d ", d[ss]);
	}

	return 0;
}