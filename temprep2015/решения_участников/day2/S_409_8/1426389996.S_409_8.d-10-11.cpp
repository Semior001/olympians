#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;

#define fname "D"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int mod = INF + 7;
int n, k, l, r;


pair <int, int> b[77];
int sz = 0;

int prod[1001000];

ll d[120][(1 << 12)];

void add(int &x, int y) {
	if ((x += y) >= mod) x -= mod;
}

int cnt[(1 << 12)];

ll calc() {
	d[0][0] = 1;
	for (int i = 0; i < (1 << sz); ++ i) {
		for (int mask = 0; mask < (1 << sz); ++ mask) {
			if ((mask | i) == mask) {
				for (int cc = 1; cc <= cnt[i]; ++ cc) {
					for (int take = 0; take <= n; ++ take) {
						d[take + 1][mask | i] += d[take][mask];
						if(d[take + 1][mask | i] >= mod) d[take + 1][mask | i] -= mod;
					}
				}
			}
			else {
				for (int take = 0; take <= n; ++ take) {
					d[take + 1][mask | i] += d[take][mask] * cnt[i];
					d[take + 1][mask | i] %= mod;
				}
			}
		}
	}
	return d[n][(1 << sz) - 1];
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n >> l >> r >> k;

	int x = k;

	for (int i = 2; i * i <= x; ++ i) {
		if (x % i == 0) {
			b[sz++] = mp(i, 0);
			while (x % i == 0) x /= i, b[sz - 1].S++;
		}
	}
	if (x > 1) b[sz++] = mp(x, 1);

	for (int i = l; i <= r; ++ i) {
		for (int j = 0; j < sz; ++ j) {
			x = i;
			int deg = 0;
			while(x % b[j].F == 0) ++deg, x /= b[j].F;
			if (deg >= b[j].S) prod[i] |= (1 << j);
		}
		cnt[prod[i]]++;
	}

//	for (int i = 0; i < sz; ++ i) cout << b[i].F << " " << b[i].S << endl;

	cout << calc();

	return 0;
}