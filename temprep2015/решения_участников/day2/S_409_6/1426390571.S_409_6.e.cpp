#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <deque>
#include <ctime>

#define pb push_back
#define ppb pop_back()
#define sz size()
#define f first
#define s second
#define mp make_pair
#define fname "E"
#define ll long long

using namespace std;
set < int > s;
int a[11], n, fp, mx, mn;
vector < int > ans;
	
int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n; 
	if (n == 1) {
		cout << 1 << ' ' << 1;
		return 0;
	}
	if (n == 2) {
		int a, b, c, d;
		cin >> a >> b >> c >> d;
		if (a == b == c == d) {
			cout << 1 << ' ' << 1;
			return 0;
		}
		if (a + b >= c + d && a + c >= b + d && a + d >= b + c) {
			cout << 1 << ' ' << 1;
			return 0;
		}
		if (a + b >= c + d) {
			fp = 1;
		}
		else {
			if (a + c >= b + d) {
				fp = 1;
			}
			else {
				if (a + d >= c + b) {
					fp = 1;
				}
				else 
					fp = 2;
			}
		}
		cout << fp << ' ' << 2;
		return 0;
	}
	if (n == 3) {
		fp = 1;
		for (int i = 1; i <= 6; i++) {
			cin >> a[i];
			s.insert(a[i]);
			ans.pb(a[i]);
		}
		if(s.sz == 1) {
			cout << 1 << ' ' << 1;
			return 0;
		}
		sort (ans.begin() + 1, ans.end());
		mx = ans[0] + ans[5];
		if (ans[1] + ans[2] > mx) 
			fp++;
		if (ans[4] + ans[3] > mx) 
			fp++;
		ans.pb(fp);
		fp = 1; 
		if (ans[1] + ans[3] > mx) 
			fp++;
		if (ans[2] + ans[4] > mx) 
			fp++;
		ans.pb(fp);
		fp = 1;
		if (ans[1] + ans[4] > mx) 
			fp++;
		if (ans[2] + ans[3] > mx) 
			fp++;
		ans.pb(fp);
		fp = 1;
		sort (ans.begin(), ans.end());
		fp = ans[0];
		ans.clear();
		int sp = n;
		mn = ans[0] + ans[1];
		if (ans[2] + ans[3] <= mn) 
			sp--;
		if (ans[4] + ans[5] <= mn) 
			sp--;
		ans.pb(sp);
		sp = n;
		if (ans[2] + ans[5] <= mn)
			sp--;
		if (ans[3] + ans[4] <= mn) 
			sp--;
		ans.pb(sp);
		sp = n;
		if (ans[2] + ans[4] <= mn) 
			sp--;
		if (ans[3] + ans[5] <= mn) 
			sp--;       
		ans.pb(sp);
		sort (ans.begin(), ans.end());
		cout << fp << ' ' << ans[2];

	}
	return 0;
}

