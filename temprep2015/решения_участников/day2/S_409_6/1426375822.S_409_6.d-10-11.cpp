#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <deque>
#include <ctime>

#define pb push_back
#define ppb pop_back()
#define sz size()
#define f first
#define s second
#define mp make_pair
#define fname "D"
#define ll long long

using namespace std;
const ll maxn = (ll)(1e3 + 111);
const ll mod = (ll)(1e9+7);

int ans[maxn], n, l, a, r, k;
int cnt;
int nok(int a, int b) {
	int c = a, d = b;
	while (c > 0 && d > 0) {
		if (c > d) 
			c %= d;
		else 
			d %= c;
	}
	return (b * a) / (d + c);
}

	
int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n >> l >> r >> a;
	for (int i = l; i <= r; i++) 
		ans[++k] = i;
	if (n == 1) { 
		for (int i = 1; i <= k; i++) {
			if (ans[i] % a == 0) 
				cnt++;
		}
		cout << cnt % mod;
		return 0;
	}
	for (int i = 1; i <= k; i++) {
		for (int j = 1; j <= k; j++) {
			if (i >= j) {
			int s = nok(ans[i] , ans[j]);
			if (s % a == 0)  {
				cnt++;
				cnt %= mod;
			}
			}
		}
	}	
	cout << cnt;


	return 0;
}




