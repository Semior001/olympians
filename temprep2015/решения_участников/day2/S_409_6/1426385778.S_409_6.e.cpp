#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <deque>
#include <ctime>

#define pb push_back
#define ppb pop_back()
#define sz size()
#define f first
#define s second
#define mp make_pair
#define fname "E"
#define ll long long

using namespace std;
set < int > s;
const ll maxn = (ll)(5e3 + 111);
int n, a[maxn], sum[5011][5011];
int mn = 100000000, mx = -1, fp, sp, mnn = 100000000, mxx = -1;
bool used[maxn], usedmn[maxn], usedmx[maxn], uused[maxn], flag, ok;
vector < pair < int , double > > g[maxn];

void dfs (int v) {
	used[v] = true;
	for (int i = 0; i < g[v].sz; i++) {
		int to = g[v][i].f;
		sum[v][to] = g[v][i].s;
		if (!used[to]) {
		        dfs(to);
		}
	}
}

	
int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n; 
	if (n == 1) {
		cout << 1 << ' ' << 1;
		return 0;
	}
	fp = 1;
	sp = n;
	for (int i = 1; i <= 2 * n; i++) {
		cin >> a[i];
		s.insert(a[i]);
	}
	for (int i = 1; i <= 2 * n; i++) {
		for (int j = 1; j <= 2 * n; j++) {
			if (i != j) {
				g[i].pb(mp(j, (a[i] + a[j])/2));
				g[j].pb(mp(i, (a[i] + a[j])/2));
			}
		}
	}

	dfs(1);
	for (int j = 2; j <= n * 2; j++) {
		mn = min(mn, sum[1][j]);
		mx = max(mx, sum[1][j]);
	}
	for (int j = 2; j <= n * 2; j++) {
		if (sum[1][j] == mn && !flag)  {
			usedmn[j] = true;
			flag = true;
		}
		if (sum[1][j] == mx && !ok ) { 
			usedmx[j] = true;
			ok = true;
		}
	}
	for (int i = 2; i <= 2 * n; i++) {
		for (int j = i; j <= 2 * n; j++) {
			if (i != j) {
				if (!usedmx[i] && !usedmx[j] && !usedmn[i] && !usedmn[j] && !uused[i] && !uused[j]) {
					if (sum[i][j] > mx) {
						fp++;
						uused[i] = uused[j] = true;
					}
					if (sum[i][j] <= mn) {
						sp--;
						uused[i] = uused[j] = true;	
					}
				}		

			}
		}
	}
	
	cout << fp << ' ' << sp;
	return 0;
}

