#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <deque>
#include <ctime>

#define pb push_back
#define ppb pop_back()
#define sz size()
#define f first
#define s second
#define mp make_pair
#define fname "F"
#define ll long long

using namespace std;
const ll maxn = (ll)(1e5 + 111);


int n, cnt[maxn], a[maxn];
	
int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; i++) 
		scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++) 
		for (int j = 1; j <= n; j++) 
			if (i != j) {
				if (!(a[i] & a[j])) 
					cnt[i]++;
			}	
	for (int i = 1; i <= n; i++) 
		printf ("%d " , cnt[i]);

	return 0;
}