#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define pb push_back
#define fname "E."

using namespace std;

int n, a[111111], cntmn = 1, cntmx = 1, b[111111], k, mn = 1111111, ind;

bool used[111111];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2*n; i++) {  
		scanf("%d", &a[i]);
	}
  	for (int i = 2; i <= 2*n; i++)
  		b[++k] = a[i];
  	sort(b+1, b+1+k);
  	reverse(b+1, b+1+k);
  	int cur = (a[1] + b[k]+1)/2;
  	for (int i = 1; i <= n-1; i++) {
  		mn = 11111111;
  		int ind1 =-1, ind2 =-1;
  	 	for (int j = 1; j < k; j++)
  	 		for (int f = j+1; f < k; f++)
  	 			if (b[j] + b[f] < mn && (b[j] + b[f] + 1)/2 > cur && !used[j] && !used[f]) {
  	 			 	mn = b[j] + b[f];
  	 			 	ind1 = j, ind2 = f;
  				}	
  	 	if (mn == 11111111) break;
  	 	used[ind1] = true;
  	 	used[ind2] = true;	
  	 	cntmn++;
  	}
  	memset(used,0 ,sizeof used);
  	cur = (a[1]+b[1]+1)/2;
  	for (int i = 1; i <= n-1; i++) {
  		mn = 11111111;
  		int ind1 =-1, ind2 =-1;
  	 	for (int j = 2; j <= k; j++)
  	 		for (int f = j+1; f <= k; f++)
  	 			if (b[j] + b[f] < mn && (b[j] + b[f] + 1)/2 > cur && !used[j] && !used[f]) {
  	 			 	mn = b[j] + b[f];
  	 			 	ind1 = j, ind2 = f;
  				}	
  	 	if (mn == 11111111) break;
  	 	used[ind1] = true;
  	 	used[ind2] = true;	
  	 	cntmx++;
  	}
  	cout << cntmx << ' ' << cntmn;

	return 0;       
}