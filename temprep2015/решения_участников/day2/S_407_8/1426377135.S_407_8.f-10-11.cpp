#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define pb push_back
#define fname "F."

using namespace std;

const int maxn = int(1e5)+7;
int n, a[maxn];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	cin >> n;
	for (int i= 1; i <= n; i++)
		cin >> a[i];
   	for (int i = 1; i <= n; i++) {
   		int cnt = 0;
   		for (int j = 1; j <= n; j++) {
   			if (!(a[i]&a[j])) cnt++;
   	  	}
   		cout << cnt << ' ';
    }
   			
	return 0;       
}