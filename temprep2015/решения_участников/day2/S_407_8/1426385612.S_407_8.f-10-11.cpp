#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define pb push_back
#define fname "F."

using namespace std;

const int maxn = int(1e5)+7;
int n, a[maxn], mx;
map <int, int> cnt, cnt1;

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		cnt[a[i]]++;
		mx = max(a[i], mx);
  	}
  	if (mx <= 1000) {
  	  	for (int i = 1; i <= 100; i++) {
  	  	 	if (cnt[i]) {
  	  	 	 	for (int j = 1; j <= n; j++)
  	  	 	 		if (!(i & a[j]))
  	  	 	 			cnt1[i]++;		
  	  	 	}
  	  	}
  	  	for (int i=1; i <= n; i++)
  	  		cout << cnt1[a[i]] << ' ';
  	}
  	else {
  		for (int i = 1; i <= n; i++) {
  			int cur = 0;
  			for (int j = 1; j <= n; j++)
  				if (!(a[i] & a[j]))
  					cur++;
  			cout << cur << ' ' ;
  		}
  	}
	return 0;       

}