#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define pb push_back
#define fname "E."

using namespace std;

int n, a[111111], cntmn = 1, cntmx = 1, b[111111], k, mn = 1111111, ind;


int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2*n; i++) {  
		scanf("%d", &a[i]);
	}
  	for (int i = 2; i <= 2*n; i++)
  		b[++k] = a[i];
  	sort(b+1, b+1+k);
  	reverse(b+1, b+1+k);
  	int cur = (a[1] + b[k]+1)/2;
  	for (int i = 2; i < k; i++)  {
  		if ((b[i-1] + b[i]+1)/2 > cur && b[i-1] + b[i] < mn) {
  		 	mn = b[i-1] + b[i];
  		 	ind = i-1;
  	  	}
  	}
	for (int i = 1; i < k; i++) {
	 	if ((b[ind+1-i] + b[ind+i]+1)/2 > cur)
	 		cntmn++;
	 	else
	 		break;
	}
	cur = (a[1] + b[1]+1)/2;
	for (int i = 2; i <= k; i++)
	 	if ((b[k+2-i] + b[i-1]+1)/2 > cur)
	 		cntmx++;  
  		                             
  	cout << cntmx << ' ' << cntmn;

	return 0;       
}