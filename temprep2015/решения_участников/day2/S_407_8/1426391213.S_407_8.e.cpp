#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define pb push_back
#define fname "E."

using namespace std;

int a[111111], n, b[111111], mx, mn, cntmn = 1, cntmx=1, k;
bool used[111111];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2*n; i++) {  
		scanf("%d", &a[i]);
		if (i > 1)
			b[++k] = a[i];
	}
	sort(b+1, b+1+k);
	mn = (a[1] + b[1]+1)/2;
	mx = (a[1] + b[k]+1)/2;
	int ind = lower_bound(b+1, b+1+k, a[1])-b, r = k-1;
	int l = ind;
	while (l < r) {
	 	if ((a[l] + a[r]+1)/2 > mx)
	 		cntmx++, l++, r--;
	   	else  
	   		l++;
	}
	l = 1, r = ind;
	while (true) {
		if (r == k) break; 
		if ((a[l] + a[r] + 1)/2 > mn)
			cntmn++, r++, l++;
	  	else {
	  	 	if (ind > k -ind)
	  	 		l++;
	  	 	else
	  	 		r++;
	 	}
	}
	for (int i = l+1; i <= k; i++)
		if (b[i] + b[i-1] > mn)
			cntmn++;
	cout << cntmx << ' ' << cntmn;
	return 0;       
}