#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "F"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, a[100001];
int cnt[100001];

int ans[100001];    

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n;
	bool ok = 0;

	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
		if(a[i] > 100) ok = 1;
	}


	if(ok) {

	for(int i = 1; i <= n; ++i) {
		for(int j = i + 1; j <= n; ++j) {
			if(!(a[i] & a[j])) {
				++cnt[i];
				++cnt[j];
			}
		}
	}
	
	for(int i = 1; i <= n; ++i) {
		cout << cnt[i] << " ";
	}
	return 0;
	
	}

	for(int i = 1; i <= n; ++i) {
		cnt[a[i]]++;
	}

	for(int i = 1; i <= n; ++i)
		for(int j = 0; j <= 100; ++j)
			if(!(a[i] & j)) ans[i] += cnt[j];
	for(int i = 1; i <= n; ++i)
		cout << ans[i] << " ";
	return 0;
}