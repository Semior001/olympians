#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "D"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, l, r, x[101], d[11][1000001];

LL lcm(LL a, LL b) {
	return a / __gcd(a, b) * b;
}
int a;
int ans;

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> a;
	int x = a;

	for(int i = l; i <= r; ++i) {
		d[1][i % a] = 1;
	}
	int maxlcm = r;

	for(int i = 2; i <= n; ++i) {
		for(int j = 1; j <= a; ++j) {
			for(int u = l; u <= r; ++u) {
				int newlcm = lcm(u, j);
				d[i][newlcm % a] += d[i - 1][j % a];
			}
		}	
	}

/*	for(int i = 2; i * i <= x; ++i) {
		if(x % i == 0) {
			del[++cnt] = i;
			while(x % i == 0) {
				len[cnt]++;
		   	x /= i;
			}
		}
	}
	if(x > 1) {
		del[++cnt] = x;
		len[cnt] = 1;
	}*/
	cout << d[n][0];
	return 0;
}