#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "E"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

multiset <int> s;
int n, Aman;

int a[N];

int cnt;;
pair <int, int> c[N];
int mx = -inf, mn = inf;
bool was[N];
vector <pair <int, pair <int, int> > > v;

void calc() {
	bool ok = 1;
	for(int i = 1; i <= n + n; ++i) {
		if(!was[i]) ok = 0;
	}
	if(ok) {
		sort(v.begin(), v.end());
		reverse(v.begin(), v.end());
		for(int i = 0; i < v.size(); ++i)
			if(v[i].second.F == 1) {
				int j = i;
				while(j > 0 && v[j].F == v[i].F) --j;
				if(v[j].F != v[i].F) ++j;
				mx = max(mx, j + 1);
				mn = min(mn, j + 1);
			}	
	}
	for(int i = 1; i <= n + n; ++i) {
		if(!was[i])  
		for(int j = 1; j <= n + n; ++j) {
			if(!was[j] && i != j) {
				was[i] = was[j] = 1;
				v.push_back(mp(a[i] + a[j], mp(i, j)));
				calc();
				was[i] = was[j] = 0;
				v.pop_back(); 
			}
		}
	}
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;

	for(int i = 1; i <= n + n; ++i) {
		cin >> a[i];
	}	

	was[1] = 1;
	for(int i = 1; i <= n + n; ++i) {
		if(!was[i]) {
		   was[i] = 1;
		   v.push_back(mp(a[i] + a[1], mp(1, i)));
			calc();
			was[i] = 0;
			v.pop_back();
		}
	}

	cout << mn <<  " " << mx;

	return 0;
}