#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "F"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

LL n, a[100001];
LL cnt[4000001];
LL ans[100001];
LL mx;
LL p1 = 1000000007, p2 = 251;
bool was[4000001];
map <LL, LL> d;

LL go(LL x, LL f, LL step) {
	if(d.count(x * p1 + step * p2 + f)) return d[x * p1+ step * p2 + f];
	if(step == mx) {
		return d[x * p1 + step * p2 + f] = cnt[f];
	}
	LL ret1 = 0;
	if(!(x & 1)) ret1 += go(x >> 1, f + (1 << step),step + 1);
	ret1 += go(x >> 1, f, step + 1);
	return d[x * p1 + step * p2 + f] = ret1;
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n;

	for(LL i = 1; i <= n; ++i) {
		cin >> a[i];
		LL x = a[i];
		cnt[a[i]]++;
		LL step = 0;
		while(x) {
	   	step++;
	   	x >>= 1;
		}	
		mx = max(mx, step);
	}

	for(LL i = 1; i <= n; ++i) {
		LL x = a[i];
		cout << go(x, 0, 0) << " ";
	}


	return 0;
}