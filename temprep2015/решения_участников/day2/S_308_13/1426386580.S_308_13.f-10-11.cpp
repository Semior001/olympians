#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "F"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, a[100001];
int cnt[4000001];
int ans[100001];
int mx;
map <pair<int, int>, int> d[21];

int go(int x, int f, int step) {
	if(d[step].count(mp(x, f))) return d[step][mp(x, f)];
	if(step == mx) {
		return d[step][mp(x, f)] = cnt[f];
	}
	int ret1 = 0;
	if(!(x & 1)) ret1 += go(x >> 1, f + (1 << step),step + 1);
	ret1 += go(x >> 1, f, step + 1);
	return d[step][mp(x, f)] = ret1;
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n;

	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
		int x = a[i];
		cnt[a[i]]++;
		int step = 0;
		while(x) {
	   	step++;
	   	x >>= 1;
		}	
		mx = max(mx, step);
	}

	for(int i = 1; i <= n; ++i) {
		int x = a[i];
		cout << go(x, 0, 0) << " ";
	}


	return 0;
}