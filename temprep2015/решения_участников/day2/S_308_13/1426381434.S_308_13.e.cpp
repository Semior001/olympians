#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "E"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

multiset <int> s;
int n, Aman;

int a[N];

int cnt;;
pair <int, int> c[N];
pair <int ,int > q[N];
int mx = -inf, mn = inf;


int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;

	for(int i = 1; i <= n + n; ++i) {
		cin >> a[i];
		c[i] = {a[i], i};
	}	
	sort(c + 1, c + n + n + 1);

	do {
		int s = 0, r =0;
		bool ok = 0;
		for(int i = 1; i <= n + n; ++i) {
			s += c[i].F;
			if(c[i].S == 1) ok = 1;
			if(i % 2 == 0) {
			   q[++r] = mp(s, ok? 0 : 1);
				s = 0;
				ok = 0;
			}
		}
		sort(q + 1, q + r + 1);
		reverse(q + 1, q + r + 1);
		for(int i = 1; i <= r; ++i) {
			if(q[i].S == 0) {
				mx = max(mx, i);
				mn = min(mn, i);
			}
		}
	} while(next_permutation(c + 1, c +n + n + 1));

	cout << mn <<  " " << mx;

	return 0;
}