#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "F"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, a[100001];
int cnt[4000001];

int ans[4000001];
int step[100001];
int mx;
int st[30];
int cur;

int go(int x, int f, int step) {
	if(step == mx) {
		return cnt[f];
	}
	int ret1 = 0;
	if(!(x & 1)) {
	//	cur += (1 << step);
		ret1 = go(x >> 1, f + st[step], step + 1);
	//	cur -= (1 << step);
		ans[cur] = ret1;
	}
	int ret2 = go(x >> 1, f, step + 1);
	return ret1 + ret2;
}

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n;

	st[0] = 1;

	for(int i = 1; i <= 22; ++i)
		st[i] = st[i - 1] * 2; 

	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
		int x = a[i];
		cnt[a[i]]++;
		while(x) {
	   	step[i]++;
	   	x >>= 1;
		}	
		mx = max(mx, step[i]);
	}

	for(int i = 1; i <= n; ++i) {
		int x = a[i];
		cur = a[i];
		if(!ans[x]) ans[x] = go(x, 0, 0);
		cout << ans[x] << " ";
	}


	return 0;
}