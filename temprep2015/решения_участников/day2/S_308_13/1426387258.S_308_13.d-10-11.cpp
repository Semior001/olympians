#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "D"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, l, r, x[101], d[11][1000001];

LL lcm(LL a, LL b) {
	return a / __gcd(a, b) * b;
}
int a;
int ans;

int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> a;
	if(n == 1) {
		for(int i = l; i <= r; ++i) {
			if(i % a ==0) ++ans;
		}
		cout << ans;
	} else if(n == 2){
		for(int i = l; i <= r; ++i) {
			for(int j = i; j <= r; ++j) {
				if(lcm(i, j) % a == 0) {
					++ans;
				}
			}
		}
		cout << ans;
	} else {
		return 0 ;
	}
	return 0;
}