#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second
#define fname "E"

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

multiset <int> s;
int n, Aman;

int a[2 * N];

int cnt;;
pair <int, int> c[2 * N];


int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	cin >> Aman;

	n *= 2;
	--n;
	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
		c[i] = {a[i], i};
	}	

	sort(c + 1, c +n + 1);

	int sum1 = Aman + c[n].F, sum2 = Aman + c[1].F;

	int m = n - 1;

	for(int i = 1; i <= m; ++i)
		s.insert(c[i].F);
	
	for(int i = 1; i <= (n + 1) / 2; ++i) {
		if(!s.size()) break;
		int x = *s.begin();

		auto itt = s.lower_bound(sum1 - x);
	
		if(itt == s.begin()) {
			break;
		}
		--itt;
		if(itt == s.begin()) break;
		++cnt;

		s.erase(itt);
		s.erase(s.begin());
	}              

	cout << (n + 1)/2 - cnt << " ";
	s.clear();

	cnt = 0;
	for(int i = 2; i <= m + 1; ++i)
		s.insert(c[i].F);

	for(int i = 1; i <= (n + 1)/ 2;++i) {
		
		if(!s.size()) break;
		auto it = s.end();
		--it;
		int x = *it;
	
		auto itt = s.upper_bound(sum2 - x);
		if(itt == s.end()) break;
	
		cnt++;

		s.erase(itt);
		it = s.end();
		--it;
		s.erase(it); 
	}

//	cerr << "OK\n";
	cout << min((n + 1)/2, cnt + 1);
	return 0;
}