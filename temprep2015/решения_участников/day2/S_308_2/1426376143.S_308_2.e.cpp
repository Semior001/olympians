#include <bits/stdc++.h>
#include <iostream>
#include <algorithm>

using namespace std;

int n;
double b[200000],a[200000],q[200000];

int main ()
{
	freopen ("E.in","r",stdin);
	freopen ("E.out","w",stdout);

	cin >> n;
	for (int i = 1; i <= 2*n; i++)
		cin >> a[i];

	int t = 0;

	for (int i = 2; i <= 2*n; i++)
	{
		t++;
		b[t] = a[i];
        }

        sort (b,b+t+1);

        double mx = (a[1] + b[t]) / 2;
        double mn = (a[1] + b[1]) / 2;

        int l = 1;
        int r = t-1;
        int tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]) / 2;
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt+1);

        int ans = 1;

        for (int i = tt; i >= 1; i--)
        	if (q[i] > mx) ans++; else break;

        cout << ans << ' ';

        l = 2;
        r = t;
        tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]) / 2;
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt+1);

        ans = 1;

        for (int i = tt; i >= 1; i--)
        	if (q[i] > mn) ans++; else break;

        cout << ans;
}