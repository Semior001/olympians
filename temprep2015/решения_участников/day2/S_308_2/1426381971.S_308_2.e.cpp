#include <bits/stdc++.h>
#include <iostream>
#include <algorithm>

using namespace std;

int n;
double b[200000],a[200000],q[200000];

int main ()
{
	freopen ("E.in","r",stdin);
	freopen ("E.out","w",stdout);

	cin >> n;
	for (int i = 1; i <= 2*n; i++)
		cin >> a[i];

	int t = 0;

	for (int i = 2; i <= 2*n; i++)
	{
		t++;
		b[t] = a[i];
        }

        sort (b,b+t+1);

        double mx = (a[1] + b[t]) / 2;
        double mn = (a[1] + b[1]) / 2;

        //cout << mx << ' ' << mn << endl;

        int tt = 1;
        int cnt = t - 1;

        while (cnt > 0)
        {
        	q[tt] = (b[cnt] + b[cnt-1]) / 2;
        	tt++;
        	cnt -= 2;
        }

        sort (q,q+tt);

        int ans1 = 1;

        for (int i = tt - 1; i >= 1; i--)	
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mx) ans1++; else break;
        }
//        cout << ans << ' ';
	//cout << endl;

	int l = 1;
        int r = t-1;
        tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]) / 2;
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt);

        int ans2 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mx) ans2++; else break;
        }

        //cout << endl;

        cout << min (ans1,ans2) << ' ';

        tt = 1;
        cnt = t;

        while (cnt > 1)
        {
        	q[tt] = (b[cnt] + b[cnt-1]) / 2;
        	tt++;
        	cnt -= 2;
        }

        sort (q,q+tt);

        ans1 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {	
        	//cout << q[i] << ' ';
        	if (q[i] > mn) ans1++; else break;
        }

        //cout << endl;

        l = 2;
        r = t;
        tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]) / 2;
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt);

        ans2 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mn) ans2++; else break;
        }

       cout << max(ans1,ans2);
}