#include <bits/stdc++.h>
#include <algorithm>

using namespace std;

int l,n,r,x,d[1000][1000];

int gcd (int a, int b)
{
	while (true)
	{
		int mn = a % b;
		int mx = b;
		a = mx;
		b = mn;

		if (b == 0) return a;
	}	
}

int main ()
{
	freopen ("D.in","r",stdin);
	freopen ("D.out","w",stdout);

	cin >> n >> l >> r >> x;
	
	for (int i = 1; i <= r; i++)
	{
		for (int j = 1; j <= r; j++)
		{
			int g = gcd(max(i,j),min(i,j));

			//cerr << "google";
			d[i][j] = i * j / g;
		}
	}

	int ans = 0;

	for (int i = 1; i <= r; i++)
	{
		for (int j = 1; j <= r; j++)
		{
			//cout << d[i][j] << ' ';
			if (d[i][j] % x == 0) ans ++;
		}
		//cout << endl;
	}

	if (ans % 2 == 1) cout << ans / 2 + 1; else cout << ans / 2;
}