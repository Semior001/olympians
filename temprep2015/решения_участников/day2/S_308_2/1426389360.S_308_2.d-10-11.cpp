#include <bits/stdc++.h>
#include <algorithm>

using namespace std;

int l,n,r,x,d[1000][1000],u[1000][1000];

int gcd (int a, int b)
{
	while (true)
	{
		int mn = a % b;
		int mx = b;
		a = mx;
		b = mn;

		if (b == 0) return a;
	}	
}

int main ()
{
	freopen ("D.in","r",stdin);
	freopen ("D.out","w",stdout);

	cin >> n >> l >> r >> x;
	
	int ans = 0;

	if (n == 1)
	{
		for (int i = l; i <= r; i++)
			if (i % x == 0) ans++;

		cout << ans;
		return 0;
	}

	for (int i = l; i <= r; i++)
	{
		for (int j = l; j <= r; j++)
		{
			if (u[i][j] == 0)
			{
				int g = gcd(max(i,j),min(i,j));

				//cerr << "google";
				d[i][j] = i * j / g;
				if (i != j) u[j][i] = 1;
			}
		}	
	}

	ans = 0;

	for (int i = l; i <= r; i++)
	{
		for (int j = l; j <= r; j++)
		{
			//cout << d[i][j] << ' ';
			if (d[i][j] % x == 0 && u[i][j] == 0) ans ++;
		}
		//cout << endl;
	}
	cout << ans;
//	if (ans % 2 == 1) cout << (ans / 2 + 1)% 1000000007; else cout << (ans / 2) % 1000000007;
}