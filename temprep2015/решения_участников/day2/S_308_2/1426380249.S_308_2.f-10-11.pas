var  n,i,j,ans : longint;
     a: array [0..20000] of longint;
begin
 assign (input,'F.in'); reset (input);
 assign (output,'F.out'); rewrite (output);

 read (n);
 for i := 1 to n do
 	read (a[i]);

 for i := 1 to n do begin
 	ans := 0;

 	for j := 1 to n do
 		if (a[i] and a[j] = 0) then inc (ans);

 	write (ans ,' ');
 end;

 close (input); close (output);
end.