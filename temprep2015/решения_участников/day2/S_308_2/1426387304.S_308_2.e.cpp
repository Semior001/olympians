#include <bits/stdc++.h>
#include <iostream>
#include <algorithm>

using namespace std;

int n;
int b[200000],a[200000],q[200000];

int main ()
{
	freopen ("E.in","r",stdin);
	freopen ("E.out","w",stdout);

	cin >> n;
	for (int i = 1; i <= 2*n; i++)
		cin >> a[i];

	int t = 0;

	for (int i = 2; i <= 2*n; i++)
	{
		t++;
		b[t] = a[i];
        }

        sort (b,b+t+1);

        int mx = (a[1] + b[t]);
        int mn = (a[1] + b[1]);

        //cout << mx << ' ' << mn << endl;
        int l = 1;
        int r = t-1;
        int ans = 1;

        while (l < r)
        {
        	if (b[l] + b[r] > mx)
        	{
        		ans ++;
        		r += 2;
        	} else 
        	{
        		l ++;
        		r--;
        	}
        }

        int tt = 1;
        int cnt = t - 1;

        while (cnt > 0)
        {
        	q[tt] = (b[cnt] + b[cnt-1]);
        	tt++;
        	cnt -= 2;
        }

        sort (q,q+tt);

        int ans1 = 1;

        for (int i = tt - 1; i >= 1; i--)	
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mx) ans1++; else break;
        }
//        cout << ans << ' ';
	//cout << endl;

	l = 1;
        r = t-1;
        tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]);
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt);

        int ans2 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mx) ans2++; else break;
        }

        //cout << endl;

        cout << min (min (ans1,ans2),ans) << ' ';

        tt = 1;
        cnt = t;

        while (cnt > 1)
        {
        	q[tt] = (b[cnt] + b[cnt-1]);
        	tt++;
        	cnt -= 2;
        }

        sort (q,q+tt);

        ans1 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {	
        	//cout << q[i] << ' ';
        	if (q[i] > mn) ans1++; else break;
        }

        //cout << endl;

        l = 2;
        r = t;
        tt = 1;

        while (l < r)
        {
        	q[tt] = (b[l] + b[r]);
        	tt++;
        	l++;
        	r--;
        }

        sort (q,q+tt);

        ans2 = 1;

        for (int i = tt - 1; i >= 1; i--)
        {
        	//cout << q[i] << ' ';
        	if (q[i] > mn) ans2++; else break;
        }

       cout << max(ans1,ans2);
}