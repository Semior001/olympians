 # include <iostream>
 # include <fstream>
 # include <cstdio>
 # include <cstdlib>
 # include <cmath>
 # include <cctype>
 # include <cstring>
 # include <vector>
 # include <algorithm>

 using namespace std;

 const int N = int (1e6)+1;
 const int INF = int (1e9)+1;

 vector <int> v;
 pair <int, int> d[N];
 int a[N], MIN = INF, MAX = -INF;

 int main () {

    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    int n;
    cin >> n;
    for (int i = 1; i <= 2*n; ++i) cin >> a[i];
    for (int i = 2; i <= 2*n; ++i) {
        int ans = 1;

        for (int j = 2; j <= 2*n; ++j)
            if (i != j)v.push_back (a[j]);
        sort (v.begin(), v.end());

            for (int k = 0; k < v.size()/2; ++k)
                if (v[k] + v[v.size()-k-1] > a[1] + a[i]) ans++;
        MIN = min (MIN, ans);
        v.clear ();
    }

    for (int i = 2; i <= 2*n; ++i) {
        int ans = 1;

        for (int j = 2; j <= 2*n; ++j)
            if (i != j)v.push_back (a[j]);
        sort (v.begin(), v.end());

            for (int k = 0; k < v.size()/2; k++)
                if (v[k] + v[k + v.size()/2] > a[1] + a[i]) ans++;
        MAX = max (MAX, ans);
        v.clear ();
    }

    cout << MIN << " " << MAX << endl;
    return 0;
 }
