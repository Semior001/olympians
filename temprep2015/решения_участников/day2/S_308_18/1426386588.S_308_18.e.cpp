 # include <iostream>
 # include <fstream>
 # include <cstdio>
 # include <cstdlib>
 # include <cmath>
 # include <cctype>
 # include <cstring>
 # include <vector>
 # include <algorithm>
 # include <time.h>
 # include <ctime>
 # include <cctype>
 # include <set>

 using namespace std;

 # define F first
 # define S second

 const int N = int (2e6)+1;
 const int INF = int (1e9)+1;
 const double eps = double (1e-6);

 int a[N], MIN = INF, MAX = -INF, n, L;
 pair <int, int> d[N];

 void randomise () {
    for (int i = 1; i <= n; ++i)
        swap (d[rand() % n + 1].F, d[rand() % n + 1].S);
 }

 void solve () {
     vector <double> x, y;
     for (int i = 1; i <= n; ++i)
        if (d[i].F == L || d[i].S == L)
            x.push_back ((d[i].F + d[i].S)*1.0 / 2.0);
    for (int i = 1; i <= n; ++i) y.push_back ((d[i].F + d[i].S)*1.0 / 2.0);

    for (int i = 0; i < x.size(); ++i) {
        int cnt = 0;
        for (int j = 0; j < y.size(); ++j)
            if (x[i] > x[j])
                cnt++;
        MIN = min (MIN, cnt);
        MAX = max (MAX, cnt);
    }
 }

 int main () {
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);

    scanf ("%d", &n);
    for (int i = 1; i <= 2*n; ++i) scanf ("%d", &a[i]);
    L = a[1];
    sort (a+1, a+n*2+1);
    int r = 2*n, l = 1;
    for (int i = 1; i <= n; ++i) d[i].F = a[r], d[i].S = a[l], r--, l++;

    while (double (clock ()) / CLOCKS_PER_SEC < 0.45) {
        solve ();
        randomise ();
    }

    printf ("%d %d\n", MIN, MAX);
    return 0;
 }
