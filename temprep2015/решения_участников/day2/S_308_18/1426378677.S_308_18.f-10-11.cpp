 # include <iostream>
 # include <fstream>
 # include <cstdio>
 # include <cstdlib>
 # include <cmath>
 # include <cctype>
 # include <cstring>
 # include <vector>
 # include <algorithm>

 using namespace std;

 const int N = int (1e6)+1;
 const int INF = int (1e9)+1;

 vector <int> d[N];
 int a[N], l[N], n, ans[N], MAX;

 bool ith_bit (int i, int j) {
    return i & (1 << j);
 }

 int main () {
     freopen ("F.in", "r", stdin);
     freopen ("F.out", "w", stdout);
     scanf ("%d", &n);
    for (int i = 1; i <= n; ++i) {
        scanf ("%d", &a[i]);
        l[i] = int (log2 (a[i]))+1;
    }

    for (int i = 1; i <= n; ++i)
     for (int j = 1; j <= n; ++j) {
        if (i == j && l[i] == l[j]) continue;
        bool ok = true;
        for (int k = 0; k < min (l[i], l[j]); ++k)
        if (ith_bit (a[i], k) && ith_bit (a[j], k)) { ok = false; break; }
        ans[i] += ok;
    }

    for (int i = 1; i <= n; ++i) printf ("%d ", ans[i]);
    return 0;
 }
