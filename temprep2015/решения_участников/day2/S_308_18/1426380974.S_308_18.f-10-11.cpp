 # include <iostream>
 # include <fstream>
 # include <cstdio>
 # include <cstdlib>
 # include <cmath>
 # include <cctype>
 # include <cstring>
 # include <vector>
 # include <algorithm>

 using namespace std;

 const int N = int (1e6)+1;
 const int INF = int (1e9)+1;

 vector <int> d[N];
 int a[N], l[N], n, ans[N], MAX, used_main[1005];

 bool ith_bit (int i, int j) {
    return i & (1 << j);
 }
 void square () {
    for (int i = 1; i <= n; ++i)
     for (int j = 1; j <= n; ++j) {
        int ok = a[i] & a[j];
        if (!ok) ans[i]++;
    }
 }

 void linear () {
    for (int i = 1; i <= 1000; ++i)
        for (int j = 1; j <= 1000; ++j) {
            int ok = i & j;
            if (!ok && used_main[i] && used_main[j]) ans[i]++;
        }
 }

 int main () {
     freopen ("F.in", "r", stdin);
     freopen ("F.out", "w", stdout);

     scanf ("%d", &n);
    for (int i = 1; i <= n; ++i) {
        scanf ("%d", &a[i]);
        l[i] = int (log2 (a[i]))+1;
    }

     if (n <= 10000) square ();
     else if (n >= 10000) linear ();
     for (int i = 1; i <= n; ++i) printf ("%d ", ans[i]);
    return 0;
 }
