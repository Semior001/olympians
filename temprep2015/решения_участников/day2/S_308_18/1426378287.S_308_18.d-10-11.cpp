 # include <iostream>
 # include <fstream>
 # include <cstdio>
 # include <cstdlib>
 # include <cmath>
 # include <cctype>
 # include <cstring>
 # include <vector>
 # include <algorithm>

 using namespace std;

 const int N = int (1e6)+1;
 const int INF = int (1e9)+1;

 int ans, used[1001][1001];

 int gcd (int a, int b) {
    while (b) {
        a %= b;
        swap (a, b);
    }
    return a;
 }

int main () {
    freopen ("D.in", "r", stdin);
    freopen ("D.out", "w", stdout);
    int n, l, r, a;
    cin >> n >> l >> r >> a;
    if (n == 1) {
        for (int i = l; i <= r; ++i)
            if (i % a == 0) ans++;
    }
    if (n == 2) {
    for (int i = l; i <= r; ++i)
        for (int j = l; j <= r; ++j)
            if ((i*j) % a == 0 && i <= j && used[i][j] == 0)
                ans++, used[i][j] = 1;
        }
    cout << ans << endl;
    return 0;
}
