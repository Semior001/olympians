#include <iostream>
#include <set>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, x, a[2 * MXN], p;
vector <long double> v;
long double cur;

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n *= 2;
    p = n - 1;
    cin >> x;
    for(int i = 1; i < n * 2; i ++){
        cin >> a[i];
    }
    sort(a + 1, a + n, greater <int> ());
    v.PB((x * 1.0 + a[1] * 1.0) / 2.0);
    cur = (x * 1.0 + a[1] * 1.0) / 2.0;
    for(int i = 2; i <= n / 2; i ++){
        v.PB((a[i] * 1.0 + a[p] * 1.0) / 2.0);
        p --;
    }
    sort(v.begin(), v.end());
    reverse(v.begin(), v.end());
    for(int i = 0; i < v.SZ; i ++){
        if(v[i] == cur){
            cout << i + 1 << ' ';
            break;
        }
    }
    v.clear();
    p = n - 1;
    sort(a + 1, a + n);
    v.PB((x * 1.0 + a[1] * 1.0) / 2.0);
    cur = (x * 1.0 + a[1] * 1.0) / 2.0;
    for(int i = 2; i <= n / 2; i ++){
        v.PB((a[i] * 1.0 + a[p] * 1.0) / 2.0);
        p --;
    }
    sort(v.begin(), v.end());
    reverse(v.begin(), v.end());
    for(int i = 0; i < v.SZ; i ++){
        if(v[i] == cur){
            cout << i + 1;
            return 0;
        }
    }
    return 0;
}


