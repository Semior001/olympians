#include <iostream>
#include <set>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, x, a[2 * MXN], p, ans = INF, ans2 = -INF, id = -INF, id2 = INF;
vector <int> v;
set <int> s;
llong cur;

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n *= 2;
    cin >> x;
    for(int i = 1; i < n; i ++){
        cin >> a[i];
    }
    if(n == 2)  id = id2 = 1;
    if(n == 4)
    for(int i = 1; i < n; i ++){
        for(int j = 1; j < n; j ++){
            for(int k = 1; k < n; k ++){
                        s.insert(i);
                        s.insert(j);
                        s.insert(k);
                        if(s.SZ == n - 1){
                            cur = (x + a[i]) / 2;
                            v.PB((x + a[i]) / 2);
                            v.PB((a[j] + a[k]) / 2);
                            sort(v.begin(), v.end());
                            reverse(v.begin(), v.end());
                            for(llong q = 0; q < v.SZ; q ++){
                                if(cur == v[q]){
                                    if(cur <= ans){
                                        ans = cur;
                                        id = max(id, q + 1);
                                    }
                                    if(cur >= ans2){
                                        ans2 = cur;
                                        id2 = min(id2, q + 1);
                                    }
                                }
                            }
                        }
                        v.clear();
                        s.clear();
            }
        }
    }
    if(n == 6)
    for(int i = 1; i < n; i ++){
        ans = INF;
        ans2 = -INF;
        for(int j = 1; j < n; j ++){
            for(int k = 1; k < n; k ++){
                for(int x = 1; x < n; x ++){
                    for(int y = 1; y < n; y ++){
                        s.insert(i);
                        s.insert(j);
                        s.insert(k);
                        s.insert(x);
                        s.insert(y);
                        if(s.SZ == n - 1){
                            cur = (x + a[i]) / 2;
                            v.PB((x + a[i]) / 2);
                            v.PB((a[j] + a[k]) /2);
                            v.PB((a[x] + a[y]) / 2);
                            sort(v.begin(), v.end());
                            reverse(v.begin(), v.end());
                            for(llong q = 0; q < v.SZ; q ++){
                                if(cur == v[q]){
                                    if(cur <= ans){
                                        ans = cur;
                                        id = max(id, q + 1);
                                    }
                                    if(cur >= ans2){
                                        ans2 = cur;
                                        id2 = min(id2, q + 1);
                                    }
                                }
                            }
                        }
                        v.clear();
                        s.clear();
                    }
                }
            }
        }
    }
    cout << id2 << ' ' << id;
    return 0;
}


