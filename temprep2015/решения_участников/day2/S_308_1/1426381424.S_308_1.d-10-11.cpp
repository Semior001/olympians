#include <iostream>
#include <set>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, l, r, a, cnt, p;
vector <int> ans, res;

llong nod(llong x, llong y){
    while(x * y != 0){
        if(x > y){
            x = x % y;
        }
        else
        {
            y = y % x;
        }
    }
    return x + y;
}

llong nok(llong x, llong y){
    return x * y / nod(x, y);
}

bool isprime(llong x){
    for(int i = 2; i * i <= x; i ++){
        if(x % i == 0){
            return false;
        }
    }
    return true;
}

void func(vector <int> v){
    set <int> s;
    for(int i = 0; i < v.SZ; i ++){
        s.insert(v[i]);
    }
    v.clear();
    while(!s.empty()){
        set <int> :: iterator it;
        it = s.end();
        v.PB(*it);
        s.erase(it);
        s.clear();
    }
}

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> n >> l >> r >> a;
    if(n == 1){
        cout << r / a - l / a;
        return 0;
    }
    for(int i = l; i <= r; i ++){
        for(int j = i; j <= r; j ++){
            if(nok(i, j) % a == 0){
                cnt ++;
            }
        }
    }
    cout << cnt % INF;
    return 0;
}


