#include <iostream>
#include <set>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, l, r, a, cnt;

llong nod(llong x, llong y){
    while(x * y != 0){
        if(x > y){
            x = x % y;
        }
        else
        {
            y = y % x;
        }
    }
    return x + y;
}

llong nok(llong x, llong y){
    return x * y / nod(x, y);
}

bool isprime(llong x){
    for(int i = 2; i * i <= x; i ++){
        if(x % i == 0){
            return false;
        }
    }
    return true;
}

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> n >> l >> r >> a;
    if(isprime(a)){
        for(int i = l; i <= r; i += a){
            cnt ++;
        }
        if(r % a == 0){
            cout << cnt % INF;
        }
        else
        {
            cout << (cnt - 1) % INF;
        }
        return 0;
    }
    cnt += r - l + 1;
    cout << cnt;
    return 0;
}


