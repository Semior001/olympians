#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll n,l,r,b,ans;
ll a[111][111111],cur,w[111][111111];
void solve(ll s)
{
    for(ll i=1;i<=r-l+1;i++)
    {
        ll x=cur;
        cur=cur*a[s][i]/(__gcd(cur,a[s][i]));
        if(s==n)
        {
            if(cur%b==0)
            {
                ans++;
                return;
            }
            else
            {
                cur=x;
                continue;
            }
        }
        else
            solve(s+1);
    }
}
int main() {
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);
    cin>>n>>l>>r>>b;
    for(ll i=1;i<=n;i++)
        for(ll j=l;j<=r;j++)
            a[i][j-l+1]=j;
    for(ll i=1;i<=r-l+1;i++)
    {
        cur=a[1][i];
        solve(2);
    }
    if(n==1)
        ans=r/b+l/b;
    cout<<ans;
}
