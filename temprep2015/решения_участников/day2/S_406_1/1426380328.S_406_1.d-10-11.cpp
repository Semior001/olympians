// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

int gcd (int a, int b) {
    if (!b) return a;
    else return gcd (b, a % b);
}

int main () {
freopen ("D.in", "r", stdin);
freopen ("D.out", "w", stdout);

    int n, l, r, a, ans = 0;
    cin >> n >> l >> r >> a;

    if (n == 1) {
        cout << (r - l + 1) / a << endl;
        return 0;
    }

    FOR (i, l, r)
        FOR (j, i, r)
            if ((i * j / gcd (i, j)) % a == 0) ans++;

    cout << ans << endl;

    return 0;
}
