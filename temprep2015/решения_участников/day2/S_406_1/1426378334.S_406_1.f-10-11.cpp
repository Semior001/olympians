// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = (int) 1e5 + 5;
int n, a[MAXN], ans[MAXN];

inline void solve () {
    FOR (i, 1, n)
        FOR (j, i + 1, n)
            if (!(a[i] & a[j])) ans[i]++, ans[j]++;
}

int main () {
freopen ("F.in", "r", stdin);
freopen ("F.out", "w", stdout);
    scanf ("%d", &n);
    FOR (i, 1, n) scanf ("%d", &a[i]);

    solve();

    FOR (i, 1, n) printf ("%d ", ans[i]);
    return 0;
}
