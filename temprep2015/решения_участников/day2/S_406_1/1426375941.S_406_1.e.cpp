// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <algorithm>
# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = (int) 2e5 + 5;
int n, mx_ans, mn_ans, a[MAXN];

bool cmp (int A, int B) {
    return (A > B);
}

inline void solve_for_max () {
    sort (a + 2, a + n + 1, cmp);
    double x = double (a[1] + a[2]) / 2.0;
    int up = 3, low = n, k = 0;
    while (up < low) {
        double avg = double (a[up] + a[low]) / 2.0;
        if (avg <= x) low--, k++;
        up++;
    }
    mx_ans += n / 2 - k;
}

inline void solve_for_min () {
    double x = double (a[1] + a[n]) / 2.0;
    int up = 2, low = n - 1, k = 0;
    while (up < low) {
        double avg = double (a[up] + a[low]) / 2.0;
        if (avg <= x) low--, k++;
        up++;
    }
    mn_ans = n / 2 - k;
}

int main () {
freopen ("E.in", "r", stdin);
freopen ("E.out", "w", stdout);
    scanf ("%d", &n);
    n *= 2;
    FOR (i, 1, n) scanf ("%d", &a[i]);

    solve_for_max ();
    solve_for_min ();

    printf ("%d %d\n", mx_ans, mn_ans);
    return 0;
}
