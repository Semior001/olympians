// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <algorithm>
# define FOR(x, y, z) for(int x = y; x <= z; x++)
# define FRO(x, y, z) for(int x = y; x >= z; x--)

using namespace std;

const int MAXN = (int) 2e5 + 5;
int n, mx_ans, mn_ans = 1, a[MAXN];


void solve_for_max () {
    sort (a + 2, a + n + 1);
    double x = double (a[1] + a[n]) / 2.0;
    int up = n, k = 0, up_;
    FOR (i, 2, n - 1) {
        FRO (j, up - 1, i + 1) {
            up_ = j;
            double avg = double (a[i] + a[j]) / 2.0;
            if (avg <= x) {
                k++;
                break;
            }
        }
        up = up_;
    }
    mx_ans = n / 2 - k;
}

void solve_for_min () {
    double x = double (a[1] + a[2]) / 2.0;
    int low = 1, k = 0, low_;
    FRO (i, n, 3) {
        FOR (j, low + 1, i - 1) {
            low_ = j;
            double avg = double (a[i] + a[j]) / 2.0;
            if (avg > x) {
                mn_ans++;
                break;
            }
        }
        low = low_;
    }
}

int main () {
freopen ("E.in", "r", stdin);
freopen ("E.out", "w", stdout);
    scanf ("%d", &n);
    n *= 2;
    FOR (i, 1, n) scanf ("%d", &a[i]);

    solve_for_max ();
    solve_for_min ();

    printf ("%d %d\n", mx_ans, mn_ans);
    return 0;
}
/*
3
999 3 1 2 1000 1
*/
