// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <algorithm>
# include <set>
# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXA = 100, MAXN = (int) 1e5 + 5, N = (int) 1e4;
int n, a[MAXN], ans[MAXN], xor_, ans_[MAXA + 1];
bool used[MAXA + 1];
multiset <int> a_;

inline void solve1 () {
    FOR (i, 1, n)
        FOR (j, i + 1, n)
            if (!(a[i] & a[j])) ans[i]++, ans[j]++;
}

inline void solve2 () {
    set <int> st[MAXA + 1];
    FOR (i, 1, MAXA)
        FOR (j, i + 1, MAXA)
            if (!(i & j)) {
                st[i].insert (j);
                st[j].insert (i);
            }

    FOR (i, 1, n) {
        if (used[a[i]]) {
            ans[i] = ans_[a[i]];
            continue;
        }

        for (set <int> :: iterator it = st[a[i]].begin(); it != st[a[i]].end(); it++) ans_[a[i]] += a_.count (*it);
        used[a[i]] = true;
        ans[i] = ans_[a[i]];
    }
}

int main () {
freopen ("F.in", "r", stdin);
freopen ("F.out", "w", stdout);
    scanf ("%d", &n);
    FOR (i, 1, n) {
        scanf ("%d", &a[i]);
        a_.insert (a[i]);
    }

    if (n <= N) solve1();
    else solve2();

    FOR (i, 1, n) printf ("%d ", ans[i]);
    return 0;
}
/*
7
2 7 8 2 6 10 1
*/
