#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<iomanip>
#include<set>
#include<stack>
#include<queue>
using namespace std;
long long a[1000011],b[100011],c[10000011],d[1000011];
bool used[1000011],used2[1000011];
int main(){
	//freopen("E.in","r",stdin);
	//freopen("E.out","w",stdout);
	long long n,mx=0,id=-1,mn=99999999,id1=-1;
	cin>>n;
	for(int i=1;i<=2*n;i++){
		cin>>a[i];
		d[i]=a[i];
		if(i>1){
			if(a[i]>mx){
				mx=a[i];
				id=i;
			}
		}
		if(i>1){
			if(a[i]<mn){
				mn=a[i];
				id1=i;
			}
		}
	} 
	b[1]=a[1]+mx;
	used2[1]=1;
	used2[id]=1;
	int ans1=0,ans2=0;
	for(int i=2;i<=2*n;i++){
		if(used2[i]){
			continue;
		}
		int x=-1,y=-1,p,pos;
		bool f=0;
		for(int j=2;j<=n*2;j++){
			if(used2[j]){
				continue;
			}
			if(i!=j&&a[i]+a[j]<=b[1]&&a[i]+a[j]>x){
				f=1;
				x=a[i]+a[j];		
				p=j;
			}
			if(a[i]+a[j]>y){
				y=a[i]+a[j];
				pos=j;
			}
		}      
		if(f==1){
			used2[p]=1;		
		}else{
			used2[pos]=1;
			ans1++;
		}
		used2[i]=1;	
	}            	
	c[1]=a[1]+mn;
	used[1]=1;
	used[id1]=1;
	int sz1=1;
	for(int i=2;i<=n*2;i++){
		if(used[i]){
			continue;
		}
		int m=99999999,m2=99999999,id2,id3;
		bool flag=0;
		for(int j=2;j<=n*2;j++){
			if(used[j]){
				//cout<<"YES";
				continue;
			}
			if(i!=j&&d[i]+d[j]>c[1]&&d[i]+d[j]<m2){
				flag=1;
				//cout<<"YES";
				m2=d[i]+d[j];
				id2=j;	
			} 
			if(m>d[i]+d[j]){
				m=d[i]+d[j];
				id3=j;
			}
		}   
		if(flag==1){
			c[++sz1]=m2;
			used[id2]=1;
			ans2++;
		}else{
			c[++sz1]=m;
			used[id3]=1;
		}    		
		used[i]=1;	
	}           
	ans1++;
	ans2++;
	cout<<ans1<<" "<<ans2;       
	return 0;
}