#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<iomanip>
#include<set>
#include<stack>
#include<queue>
using namespace std;
int gcd(int x, int y){
	while(x>0&&y>0){
		if(x>y){
			x%=y;
		}else y%=x;
	}
	return x+y;
}
map<pair<int, int>, bool> used;
int main(){
	freopen("D.in","r",stdin);
	freopen("D.out","w",stdout);
	int l,r,n,a,ans=0;
	cin>>n>>l>>r>>a;
	for(int i=l;i<=r;i++){
		for(int j=l;j<=r;j++){
			if(((i*j)/gcd(i,j))%a==0&&!used[make_pair<int,int>(i,j)]){
				//cout<<i<<" "<<j<<endl;
				used[make_pair<int,int>(i,j)]=1;
				used[make_pair<int,int>(j,i)]=1;
				ans++;
			}
		}		
	}
	cout<<ans;
	return 0;
}