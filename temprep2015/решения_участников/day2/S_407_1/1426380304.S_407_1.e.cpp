#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int n, i, ans, x, a[300001], l, r;
int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin>>n;
    for(i = 0; i < n * 2; i ++)
        cin>>a[i];
    sort(a + 1, a + n * 2);
    x = a[n * 2 - 1] + a[0];
    l = 1;
    r = n * 2 - 2;
    ans = 1;
    while(l < r){
        if(a[r] + a[l] > x){
            ans ++;
            r -= 2;
        }
        else{
            r --;
            l ++;
        }
    }
    cout<<ans<<" ";
    x = a[1] + a[0];
    l = 2;
    r = n * 2 - 1;
    ans = n;
    while(l < r){
        if(a[r] + a[l] <= x){
            ans --;
            l += 2;
        }
        else{
            r --;
            l ++;
        }
    }
    cout<<ans;
    fclose(stdin);
    fclose(stdout);
    return 0;
}
