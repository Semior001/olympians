#include<iostream>
#include<cstdio>
using namespace std;
int ob(int x, int y)
{
    if(x < y)
        swap(x, y);
    while(x != 0 && y != 0){
        if(x > y){
            x -= y;
        }
        else y -= x;
    }
    return max(x, y);
}
int main()
{
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    int n, l, r, a, i, j, x, ans = 0;
    cin>>n>>l>>r>>a;
    if(n == 1){
        for(i = l; i <= r; i ++)
            if(i % a == 0) ans ++;
        cout<<ans;
    }
    else if(n == 2){
        for(i = l; i <= r; i ++)
            for(j = i; j <= r; j ++)
                if((i / ob(i, j) * j) % a == 0)ans ++;
        cout<<ans;
    }
    fclose(stdin);
    fclose(stdout);
}
