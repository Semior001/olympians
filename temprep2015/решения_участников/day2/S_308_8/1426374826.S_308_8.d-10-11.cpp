//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "D"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 105;

int gcd(int a, int b)
{
	if (!b)
		return a;
	return gcd(b, a % b);
}	

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n, l, r, a;
	cin >> n >> l >> r >> a;
	if (n == 1)
	{
		int ans = 0;
		for (int i = l; i <= r; i++)
			if (i % a == 0)
				ans++;
		cout << ans;
		return 0;
	}
	if (n == 2)
	{
		int ans = 0;
		for (int x = l; x <= r; x++)
			for (int y = x; y <= r; y++)
				if (((x * y) / gcd(x, y)) % a == 0)
					ans++;	
		cout << ans;
		return 0;
	}
	cout << 0;	
	return 0;
}