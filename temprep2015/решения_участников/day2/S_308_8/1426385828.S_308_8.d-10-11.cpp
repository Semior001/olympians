//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "D"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 4000;

const int mod = 1000000007;

int p[maxn], deg[maxn];

int a[maxn];

int pos[maxn];

int n, L, R, A;

int sz = 0;

bool used[maxn];

int dp[12][20][1020];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n >> L >> R >> A;
	int x = A;
	int j = 2;
	while (j * j <= x)
	{
		if (x % j == 0)
		{
			p[sz] = 1;
			while (x % j == 0)
			{
				x /= j;
				p[sz] *= j;
			}
			sz++;
		}
		j++;
	}
	if (x > 1)
		p[sz++] = x;
	dp[0][0][L] = 1;
	for (int i = 0; i < n; i++)
	{
		for (int mask = 0; mask < (1 << sz); mask++)
		{
			for (int x = L; x <= R; x++)
			{
				if (!dp[i][mask][x])
					continue;
				for (int y = x; y <= R; y++)
				{
					int new_mask = mask;
					for (int j = 0; j < sz; j++)
						if (y % p[j] == 0)				
							new_mask |= (1 << j);
					dp[i + 1][new_mask][y] += dp[i][mask][x];
					if (dp[i + 1][new_mask][y] >= mod)
						dp[i + 1][new_mask][y] -= mod;
				}
			}
		}
	}
	int ans = 0;
	for (int i = L; i <= R; i++)	
		ans = (ans + dp[n][(1 << sz) - 1][i]) % mod;
	cout << ans;	
	return 0;
}