//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "E"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 301010;

int a[maxn], b[maxn];

int Min, Max;

int n;

bool used[maxn];

void rec_min(int x, int cur = 0)
{
	if (x == n + 1)
	{
		Min = min(Min, cur);
		return ;
	}
	for (int i = 2; i <= n; i++)
	{
		if (!used[i])
		{
			b[x] = a[i];
			if (x % 2 == 0)
			{
				if (b[1] + b[2] >= b[x] + b[x - 1])
				{
					used[i] = 1;
					rec_min(x + 1, cur);
					used[i] = 0;
				}
				else if (cur + 1 < Min)
				{
					used[i] = 1;
					rec_min(x + 1, cur + 1);
					used[i] = 0;
				}
			}
			else
			{
				used[i] = 1;
				rec_min(x + 1, cur);
				used[i] = 0;
			}								
		}
	}
}	

void rec_max(int x, int cur = 0)
{
	if (x == n + 1)
	{
/*		cout << cur << endl;
		for (int i = 1; i <= n; i++)
			cout << b[i] << " ";
		cout << endl;
*/		Max = max(Max, cur);
		return ;
	}
	for (int i = 2; i <= n; i++)
	{
		if (!used[i])
		{
			b[x] = a[i];
			if (x % 2 == 0)
			{
				if (b[1] + b[2] < b[x] + b[x - 1])
				{
					used[i] = 1;
					rec_max(x + 1, cur + 1);
					used[i] = 0;
				}
				else if (cur + (n - x) > Max)
				{
					used[i] = 1;
					rec_max(x + 1, cur);
					used[i] = 0;
				}
			}
			else
			{
				used[i] = 1;
				rec_max(x + 1, cur);
				used[i] = 0;
			}								
		}
	}
}	

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n;
	n += n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	b[1] = a[1];
	for (int i = 2; i <= n; i++)
	{
		used[i] = 1;
		b[2] = a[i];
		rec_min(3);
		rec_max(3);
		used[i] = 0;
	}
	cout << Min + 1 << " " << Max + 1;
	return 0;
}