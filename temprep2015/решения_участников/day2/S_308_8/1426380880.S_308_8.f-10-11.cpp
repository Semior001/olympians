//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "F"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 101010;

int a[maxn];

int cnt[maxn];

int Bits[maxn];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n;
	cin >> n;
	for (int i = 1; i < (1 << 7); i++)
		for (int j = 0; j < 7; j++)
			if (i & (1 << j))
				Bits[i]++;
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &a[i]);
		for (int mask = 1; mask < (1 << 7); mask++)	
			if ((a[i] & mask) == mask)
				cnt[mask]++;
   	}
   	if (n <= 10000)
   	{
		int cnt;												
		for (int i = 1; i <= n; i++)
		{
			cnt = 0;
			for (int j = 1; j <= n; j++)
				if (!(a[i] & a[j]))
					cnt++;
			printf("%d ", cnt);                             	
		}
		return 0;
	}	
   	for (int i = 1; i <= n; i++)
   	{	
		for (int mask = 1; mask < (1 << 7); mask++)	
			if ((a[i] & mask) == mask)
				cnt[mask]--;
		int ans = n - 1;
		for (int mask = 1; mask < (1 << 7); mask++)
		{
			if ((a[i] & mask) == mask)
			{
				if (Bits[mask] % 2 == 0)
					ans += cnt[mask];
				else
					ans -= cnt[mask];
			}
		}
		for (int mask = 1; mask < (1 << 7); mask++)	
			if ((a[i] & mask) == mask)
				cnt[mask]++;
		printf("%d ", ans);
	}	
	return 0;
}