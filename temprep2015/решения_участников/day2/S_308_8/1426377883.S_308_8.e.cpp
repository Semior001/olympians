//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "E"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 5555;

int a[maxn];

vector <int> g[maxn];

int mt[maxn];

bool used[maxn];

bool TryKuhn(int v)
{
	if (used[v])
		return 0;
	used[v] = 1;
	for (int i = 0; i < (int)g[v].size(); i++)
	{
		int to = g[v][i];
		if (mt[to] == -1 || TryKuhn(mt[to]))
		{
			mt[to] = v;
			return 1;
		}
	}
	return 0;
}

int n;

int Kuhn()
{
	memset(mt, -1, sizeof(mt));
	for (int i = 3; i <= n; i++)
	{
		if (a[i] <= (a[1] + a[2]) / 2)
		{
			memset(used, 0, sizeof(used));
			TryKuhn(i);
		}
	}
	int ans = 0;
	for (int i = 3; i <= n; i++) 	
		if (a[i] > (a[1] + a[2]) / 2 && mt[i] != -1)
			ans++;
	return ans;
}

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n;
	n += n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 3; i <= n; i++)
		if (a[i] < a[2])
			swap (a[i], a[2]);
	int Right = 0, Left = 0;
    for (int i = 3; i <= n; i++)
    {
		if (a[i] > (a[1] + a[2]) / 2)
			Right++;
		else
		{
			Left++;
			for (int j = 3; j <= n; j++)
				if (a[i] + a[j] > a[1] + a[2])    	
					g[i].pb(j);
		}
	}	
	int Max = Kuhn();
	Max += (Right - Max) / 2 + 1;
	for (int i = 3; i <= n; i++)
		g[i].clear();		
	for (int i = 3; i <= n; i++)
		if (a[i] > a[2])
			swap (a[i], a[2]);
	Left = Right = 0;
    for (int i = 3; i <= n; i++)
    {
		if (a[i] > (a[1] + a[2]) / 2)
			Right++;
		else
		{
			Left++;
			for (int j = 3; j <= n; j++)
				if (a[i] + a[j] < a[1] + a[2])    	
					g[i].pb(j);
		}
	}	
	int Min = Kuhn();
	Min += (Left - Min) / 2;
	Min = (n / 2) - Min;
	cout << Min << " " << Max;					
	return 0;
}