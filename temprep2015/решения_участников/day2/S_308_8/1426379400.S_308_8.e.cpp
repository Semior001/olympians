//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "E"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 202020;

int a[maxn];

int n;

bool check_min(int mid)
{
	int l = 3, r = n - mid * 2;
	while (l < r)
	{
		if (a[l] + a[r] > a[1] + a[2])
			return 0;
		l++, r--;
	}
	return 1;
}	

bool check_max(int mid)
{
	int l = n - mid * 2 + 1, r = n;
	while (l < r)
	{
		if (a[l] + a[r] <= a[1] + a[2])
			return 0;
		l++, r--;
	}
	return 1;
}	

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n;
	n += n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 3; i <= n; i++)
		if (a[i] > a[2])
			swap (a[i], a[2]);
	sort (a + 3, a + n + 1);
	int l = 0, r = n / 2 - 1;
	while (r - l > 1)
	{
		int mid = (l + r) >> 1;
		if (check_min(mid))
			r = mid;
		else
			l = mid;
	}
	int Min = r;
	if (check_min(l))		
		Min = l;
	Min++;
	for (int i = 3; i <= n; i++)
		if (a[i] < a[2])
			swap (a[i], a[2]);
	sort (a + 3, a + n + 1);   	
	l = 0, r = n / 2 - 1;
	while (r - l > 1)
	{
		int mid = (l + r) >> 1;
		if (check_max(mid))
			l = mid;
		else
			r = mid;
	}
	int Max = l;
	if (check_max(r))
		Max = r;
	Max++;
	cout << Min << " " << Max;
	return 0;
}