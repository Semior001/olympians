program E;
Uses SysUtils;
var
   f1, f2 : Text;
   a : array[1..100000] of longint;
   highst, lowst, i, N : longint;
   max, min : longint;
   aman, tmp : longint;
   nowMAX, nowMin, amanMAX, amanMIN : longint;
   isHighFound, isLowFound : boolean;
   FoundedMax, FoundedMin : integer;
   MaxCnt, MinCnt: longint;
begin
  assign(f1, 'E.in');
  reset(f1);
  assign(f2, 'E.out');
  rewrite(f2);
  read(f1, N, aman);
  max := aman; min := aman;
  if N = 1 then
  begin
       write(f2, '1 1');
       close(f2);
       exit;
  end;
  for i := 2 to 2*N do
  begin
       read(f1, tmp);
       inc(a[tmp]);
       if tmp > max then max := tmp;
       if tmp < min then min := tmp;
  end;
  isLowFound := false;
  isHighFound := false;
  FoundedMax := 0;
  MaxCnt := 0;
  if aman <> max then dec(a[max]);
  if aman <> min then dec(a[min]);
  for i := max downto min do
  begin
       if (a[i] <> 0) then
       begin
         //For max position
         if (not isHighFound) then
         begin
              if foundedMax = 0 then
              begin
                   if a[i] = 1 then foundedmax := i else
                   begin
                        inc(maxcnt);
                        if i <= (((max + aman) div 2) + ((max + aman) mod 2)) then
                        begin
                             highst := MaxCnt;
                             isHighFound := true;
                             break;
                        end else if (a[i] mod 2) = 1 then foundedmax := i;
                   end;
              end else
              begin
                   inc(MaxCnt);

                   if (foundedmax + i) <= (max + aman) then
                   begin
                        highst := MaxCnt;
                        isHighFound := true;
                        break;
                   end;
                   foundedMax := 0;
              end;
         end;
       end;
  end;
  if highst = 0 then highst := N;
  if aman <> max then inc(a[max]);
  for i := min to max do
  begin
       if a[i] <> 0 then
       begin
            if FoundedMin = 0 then
            begin
                 if a[i] = 1 then foundedmin := i else
                 begin
                      inc(mincnt);
                      if i >= (((min + aman) div 2) + ((min + aman) mod 2)) then
                      begin
                           lowst := N - minCnt + 1;
                           break;
                      end;
                 end;
            end else
            begin
                 inc(mincnt);

                 if (foundedmin + i) >= (aman+min) then
                 begin
                      lowst := N - MinCnt + 1;
                      break;
                 end;
                 foundedmin := 0;
            end;
       end;
  end;
  if lowst = 0 then lowst := 1;
  if max = min then write(f2, '1 1') else write(f2, highst, ' ', lowst);
  close(f2);
end.
