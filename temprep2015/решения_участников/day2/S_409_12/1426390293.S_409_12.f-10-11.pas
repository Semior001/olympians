program F;
var
   f1, f2 : Text;
   N, i, j : longint;
   a : array[1..100000] of longint;
   d : array[1..100000] of longint;
begin
  assign(f1, 'F.in');
  assign(f2, 'F.out');
  reset(f1);
  rewrite(f2);
  read(f1, N);
  for i := 1 to N do
  begin
       read(f1, a[i]);
  end;
  for i := 1 to N do
      for j := i + 1 to N do
          if (a[i] and a[j]) = 0 then
          begin
               inc(d[i]);
               inc(d[j]);
          end;
  for i := 1 to N do write(f2, d[i], ' ');
  close(f2);
end.

