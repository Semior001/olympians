program E;
Uses SysUtils, Math;
var
   f1, f2 : Text;
   a : array[1..100000] of longint;
   highst, lowst, i, N : longint;
   maxval, min : longint;
   aman, tmp : longint;
   nowmax, nowMin, amanmax, amanMIN : longint;
   isHighFound, isLowFound : boolean;
   Foundedmax, FoundedMin, prevsum : longint;
   maxvalCnt, MinCnt: longint;
begin
  assign(f1, 'E.in');
  reset(f1);
  assign(f2, 'E.out');
  rewrite(f2);
  read(f1, N, aman);
  maxval := aman; min := aman;
  if N = 1 then
  begin
       write(f2, '1 1');
       close(f2);
       exit;
  end;
  for i := 2 to 2*N do
  begin
       read(f1, tmp);
       inc(a[tmp]);
       if tmp > maxval then maxval := tmp;
       if tmp < min then min := tmp;
  end;
  isLowFound := false;
  isHighFound := false;
  Foundedmax := 0;
  maxvalCnt := 0;
  a[maxval] := max(a[maxval]-1, 0);

  prevsum := maxval + aman;
  maxvalcnt := 0;
  for i := maxval downto min do
  begin
       if (a[i] <> 0) then
       begin
         //For maxvalval position
         if (not isHighFound) then
         begin
              if foundedmax = 0 then
              begin
                   if a[i] = 1 then foundedmax := i else
                   begin
                        if (prevsum <> (2*i)) then inc(maxvalcnt);
                        if i <= (((maxval + aman) div 2) + ((maxval + aman) mod 2)) then
                        begin
                             if maxvalcnt <> 0 then highst := maxvalCnt else highst := 1;
                             isHighFound := true;
                             break;
                        end else if (a[i] mod 2) = 1 then foundedmax := i;
                   end;
              end else
              begin
                   if (prevsum <> (foundedmax + i)) then inc(maxvalCnt);

                   if (foundedmax + i) <= (maxval + aman) then
                   begin
                        if maxvalcnt <> 0 then highst := maxvalCnt  else highst := 1;
                        isHighFound := true;
                        break;
                   end;
                   prevsum := foundedmax + i;
                   foundedmax := 0;
              end;
         end;
       end;
  end;
  if highst = 0 then highst := N;
  inc(a[maxval]);
  a[min] := max(a[min]-1, 0);
  prevsum := min + aman;
  mincnt := 0;
  for i := min to maxval do
  begin
       if a[i] <> 0 then
       begin
            if FoundedMin = 0 then
            begin
                 if a[i] = 1 then foundedmin := i else
                 begin
                      if (prevsum <> i) then inc(mincnt);
                      if i >= (((min + aman) div 2) + ((min + aman) mod 2)) then
                      begin
                           if mincnt <> 0 then lowst := N - minCnt + 1 else lowst := 1;
                           break;
                      end;
                 end;
            end else
            begin
                 if (prevsum<>(foundedmin + i)) then inc(mincnt);

                 if (foundedmin + i) >= (aman+min) then
                 begin
                      if mincnt <> 0 then lowst := N - MinCnt+1 else lowst := 1;
                      break;
                 end;
                 prevsum := foundedmin + i;
                 foundedmin := 0;
            end;
       end;
  end;
  if lowst = 0 then lowst := 1;
  if maxval = min then write(f2, '1 1') else write(f2, highst, ' ', lowst);
  close(f2);
end.

