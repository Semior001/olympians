program E;
Uses SysUtils;
var
   f1, f2 : Text;
   a : array[1..100000] of longint;
   highst, lowst, i, N : longint;
   max, min : longint;
   aman, tmp : longint;
   nowMAX, nowMin, amanMAX, amanMIN : longint;
   isHighFound, isLowFound : boolean;
   FoundedMax, FoundedMin : integer;
   MaxCnt, MinCnt: longint;
begin
  assign(f1, 'E.in');
  reset(f1);
  assign(f2, 'E.out');
  rewrite(f2);
  read(f1, N, aman);
  max := aman; min := aman;
  if N = 1 then
  begin
       write(f2, '1 1');
       close(f2);
       exit;
  end;
  for i := 2 to 2*N do
  begin
       read(f1, tmp);
       inc(a[tmp]);
       if tmp > max then max := tmp;
       if tmp < min then min := tmp;
  end;
  isLowFound := false;
  isHighFound := false;
  amanMAX := (max + aman) div 2;
  amanMIN := (min + aman) div 2;
  FoundedMax := 0;
  MaxCnt := 0;
  dec(a[max]); dec(a[min]);
  for i := max downto min do
  begin
       if (a[i] <> 0) then
       begin
         //For max position
         if (not isHighFound) then
         begin
              if foundedMax = 0 then
              begin
                   if a[i] = 1 then foundedmax := i else
                   begin
                        inc(maxcnt);
                        if i <= amanMax then
                        begin
                             highst := MaxCnt;
                             isHighFound := true;
                             break;
                        end else if (a[i] mod 2) = 1 then foundedmax := i;
                   end;
              end else
              begin
                   nowMax := (foundedmax + i) div 2;
                   inc(MaxCnt);
                   foundedMax := 0;
                   if nowMax <= amanMax then
                   begin
                        highst := MaxCnt;
                        isHighFound := true;
                        break;
                   end;
              end;
         end;
       end;
  end;
  inc(a[max]);
  for i := min to max do
  begin
       if a[i] <> 0 then
       begin
            if FoundedMin= 0 then
            begin
                 if a[i] = 1 then foundedmin := i else
                 begin
                      inc(mincnt);
                      if i >= amanMin then
                      begin
                           lowst := N - minCnt + 1;
                           break;
                      end;
                 end;
            end else
            begin
                 nowMin := (foundedmin + i) div 2;
                 inc(mincnt);
                 foundedmin := 0;
                 if nowMin >= amanMin then
                 begin
                      lowst := N - MinCnt + 1;
                      break;
                 end;
            end;
       end;
  end;
  if max = min then write(f2, '1 1') else if highst = 0 then write(f2, '1 ', lowst) else write(f2, highst, ' ', lowst);
  close(f2);
end.

