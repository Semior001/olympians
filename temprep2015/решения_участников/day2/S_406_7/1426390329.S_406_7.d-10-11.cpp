#include <bits/stdc++.h>

using namespace std;

int ans, n, l, r, a, cnt;

inline int gcd (const int &x, const int &y) {
	return y ? gcd (y, x % y) : x;
}

int main () {
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf ("%d%d%d%d", &n, &l, &r, &a);
	if (n == 1) {
		for (int i = l; i <= r; ++i)
			if (i % a == 0)
				++cnt;
	}
	else if (n == 2) {
		for (int i = l; i <= r; ++i)
			for (int j = i; j <= r; ++j)
				if (((i * j) / gcd (i, j)) % a == 0)
					++cnt;
	}
	else
		assert (0);
	printf ("%d", cnt);
	return 0;
}