#include <bits/stdc++.h>

using namespace std;

const int N = 1e5 + 1, M = 130;

inline int bit (const int &mask, const int &pos) {
	return (mask >> pos) & 1;
}

bitset <N> b[M];
int n, a[N];

int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1; i <= n; ++i)
		scanf ("%d", &a[i]);
	if (n <= 10000) {
		for (int i = 1; i <= n; ++i) {
			int cnt = 0;
			for (int j = 1; j <= n; ++j) {
				if (!(a[i] & a[j]))
					++cnt;
			}
			printf ("%d ", cnt);
		}
		return 0;
	}
	for (int i = 1; i <= n; ++i)
		for (int j = 0; j <= 7; ++j)
			if (bit (a[i], j))
				b[1 << j][i] = 1;
	for (int i = 0; i <= 100; ++i)
		for (int j = 0; j <= 7; ++j)
			if ((i | (1 << j)) <= 100 && b[i | (1 << j)].none())
				b[i | (1 << j)] = b[i] | b[1 << j]; 
	for (int i = 1; i <= n; ++i) {
		printf ("%d ", n - b[a[i]].count());
	}
	return 0;
}