#include <bits/stdc++.h>

using namespace std;

const int N = 2e5 + 1;

int a[N];

#define mkp make_pair
#define F first
#define S second

inline bool operator < (pair <int, int> &lhs, pair <int, int> &rhs) {
	if (lhs.F == rhs.F)
		return lhs.S < rhs.S;
	return lhs.F < rhs.F;
}

set <pair <int, int> > Set;

int cnt;

int n, x, y, pos1, pos2;

int main () {
	
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf ("%d", &n);
	n = 2 * n - 1;
	scanf ("%d", &x);
	for (int i = 1; i <= n; ++i)
		scanf ("%d", &a[i]);
	sort (a + 1, a + n + 1);
	y = a[n];
	pos1 = 1;
	for (int i = n - 1; i; --i) {
		auto it = Set.lower_bound (mkp (x + y - a[i] + 1, 0));
		if (it == Set.begin()) {
			Set.insert (mkp (a[i], ++cnt));
		} else {
			--it;
			Set.erase (it);
		}
	}
	pos1 += Set.size() / 2;
	y = a[1];
	pos2 = 1;
	Set.clear();
	for (int i = n; i > 1; --i) {
		auto it = Set.lower_bound (mkp (x + y - a[i] + 1, 0));
		if (it == Set.end()) {
			Set.insert (mkp (a[i], ++cnt));
		} else {
			Set.erase (it);
			++pos2;
		}
	}
	printf ("%d %d", pos1, pos2);
	return 0;
}