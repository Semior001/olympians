#include <bits/stdc++.h>

using namespace std;

const int N = 2e5 + 1;

int a[N];

multiset <int> Set;

int n, x, y, pos1, pos2;

int main () {
	
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	scanf ("%d", &n);
	n = 2 * n - 1;
	scanf ("%d", &x);
	for (int i = 1; i <= n; ++i)
		scanf ("%d", &a[i]);
	sort (a + 1, a + n + 1);
	y = a[1];
	pos2 = 1;
	for (int i = 2; i <= n; ++i) {
		auto it = Set.upper_bound (x + y - a[i]);
		if (it == Set.end()) {
			Set.insert (a[i]);
		} else {
			Set.erase (it);
			++pos2;
		}
	}
	y = a[n];
	pos1 = 1;
	Set.clear();
	for (int i = 1; i < n; ++i) {
		auto it = Set.upper_bound (x + y - a[i]);
		if (it == Set.begin()) {
			Set.insert (a[i]);
		} else {
			--it;
			Set.erase (it);
		}
	}
	pos1 += Set.size() / 2;
	printf ("%d %d", pos1, pos2);
	return 0;
}