#include <iostream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair
#define in srand(time(0))
#define answer(x) rand() % x + 1

using namespace std;

const int MAXN = 1e5 + 256;
int n, mx = 1e9, mn = -1e9, arr[MAXN];
double a[MAXN];

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);

	cin >> n;

	n *= 2;

	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
		arr[i] = i;
	}
	if (n == 2) {
		cout << 1 << ' '<< 1;
		return 0;
	}
	if (n == 4) {
		double ans = 999999.999, ans2 = -99999.999, inx, sm = a[2] + a[3] + a[4];
		int fr = 1, t = 1;
		for (int i = 2; i <= 4; ++i) {
			if (a[i] < ans) {
				ans = a[i];
			}
			ans2 = max (ans2, a[i]);
		}
		sm -= ans;
		if (double (a[1] * 1.0 + ans * 1.0) / 2.0 < sm / 2.0)
			t = 2;
		sm += ans;
		sm -= ans2;
		if (double (a[1] * 1.0 + ans2 * 1.0) / 2.0 < sm / 2.0)
			fr = 2;
		cout << fr << ' '<< t;
		return 0;
	}
	assert (n != 6);

	return 0;
}