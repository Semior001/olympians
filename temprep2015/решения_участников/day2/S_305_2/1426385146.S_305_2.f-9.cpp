#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, h[MAXN], cost[MAXN], dp[MAXN], ans;

int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	cin >> n;

	for (int i = 1; i <= n; ++i) 
		cin >> h[i];
	for (int j = 1; j <= n; ++j)
		cin >> cost[j];
	fill (dp, dp + MAXN + 1, 1e9);

	dp[1] = 0;

	for (int i = 2; i <= n; ++i) {
		for (int j = 1; j < i; ++j) {
			if (h[j] > h[i])
				dp[i] = min (dp[i], dp[i - 1] + min (cost[j], cost[i]));
		}
		if (dp[i] == 1e9)
			dp[i] = dp[i - 1];
	}
	cout << dp[n];

	return 0;
}