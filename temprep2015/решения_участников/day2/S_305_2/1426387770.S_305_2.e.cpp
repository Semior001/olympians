#include <iostream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
int n, mx = 1e9, mn = -1e9;
double a[MAXN];

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);

	cin >> n;

	n *= 2;

	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}
	for (int i = 2; i <= n; ++i) {
		double rank = double ((a[1] * 1.0 + a[i] * 1.0) / 2.0);
		int tmx = 1;
		int u[MAXN] = {0};
		for (int j = 2; j <= n; ++j) {
			if (i == j || u[j])
				continue;
			for (int x = j + 1; x <= n; ++x) {
				if (u[x]) continue;
				u[x] = 1;
				double rr = (a[j] * 1.0 + a[x] * 1.0) / 2.0;
				if (rr > rank) {
					//cout << 1 << ' '<< i << " vs " << j << ' '<< x << " == " << rank << ' '<< rr << '\n';
					tmx++;
				}
			}
		}
		mx = min (mx, tmx);
		mn = max (mn, tmx);
	}
	cout << mx << ' '<< mn;

	return 0;
}