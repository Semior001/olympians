#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
int l, r, a, c, x, ans;
set < int > s;

int main () {
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	
	cin >> a >> c >> l >> r;

	if (a == 0 && c == 0) {
		cout << r - l;
		return 0;
	}
	if (l > r) {
		assert (0);
	}
	if (a == c) {
		cout << max (r - max (l, a), 0);
		return 0;
	}
	x = a - c;
	for (int i = 2; i * i <= x; ++i) {
		if (x % i == 0) {
			s.insert (i);
			s.insert (x / i);
		}
	}
	if (x > 0)
		s.insert (1);
	if (x > 0)
		s.insert (x);
	for (auto it : s) {
		if (a % it == c && it >= l && it <= r) {
			ans++;
		}
	}
	cout << ans;

	return 0;
}