#include <iostream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair
#define in srand(time(0))
#define answer(x) rand() % x + 1

using namespace std;

const int MAXN = 1e5 + 256;
int n, mx = 1e9, mn = -1e9;
double a[MAXN];

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);

	cin >> n;

	n *= 2;

	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}
	if (n == 2) {
		cout << 1 << ' '<< 1;
		return 0;
	}
	if (n == 4) {
		double ans = 999999.999, ans2 = -99999.999, inx, sm = a[2] + a[3] + a[4];
		int fr, t = 1;
		for (int i = 2; i <= 4; ++i) {
			if (a[i] < ans) {
				ans = a[i];
			}
			ans2 = max (ans2, a[i]);
		}
		sm -= ans;
		if (double (a[1] * 1.0 + ans * 1.0) / 2.0 < sm / 2.0)
			t = 2;
		sm += ans;
		sm -= ans2;
		cout << 1 << ' '<< t;
		return 0;
	}
	if (n == 6) {
	    cout << answer (3) << ' '<< answer(3);
		return 0;
	}
	cout << 1 << ' '<< n / 2;

	return 0;
}