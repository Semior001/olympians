#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, h[MAXN], cost[MAXN], ans;

int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	cin >> n;

	for (int i = 1; i <= n; ++i) 
		cin >> h[i];
	for (int j = 1; j <= n; ++j)
		cin >> cost[j];

	for (int i = 1; i < n; ++i) {
		for (int j = i + 1; j <= n; ++j) {
			if (h[i] > h[j]) {
				ans += min (cost[i], cost[j]);
				swap (cost[i], cost[j]);
				swap (h[i], h[j]);
			}
		}
	}
	cout << ans;

	return 0;
}