#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
int l, r, a, c, x, ans;
set < int > s;

int main () {
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	
	cin >> a >> c >> l >> r;

	/*for (int i = l; i <= r; ++i) {
		if (a / i * i == a - c) {
			cout << i << '\n';
			ans++;
		}
	}*/
	x = a - c;
	for (int i = 2; i * i <= x; ++i) {
		if (x % i == 0) {
			s.insert (i);
			s.insert (x / i);
		}
	}
	s.insert (1);
	s.insert (x);
	for (auto it : s) {
		if (a % it == c && it >= l && it <= r) {
			//cout << it << ' ';
			ans++;
		}
	}
	cout << ans;

	return 0;
}