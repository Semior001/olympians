#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, h[MAXN], cost[MAXN], ans;

int main () {
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	cin >> n;

	for (int i = 1; i <= n; ++i) 
		cin >> h[i];
	for (int j = 1; j <= n; ++j)
		cin >> cost[j];

	for (int i = 1; i < n; ++i) {
		int mn = cost[i], inx = 0;
		bool fl = 0;
		for (int j = n; j > i; --j) {
			if (h[i] > h[j]) {
				fl = 1;
				ans += min (cost[i], cost[j]);
			}
		}
	}
	cout << ans;

	return 0;
}