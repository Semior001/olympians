#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MAXN = 2 * 1e5 + 256;
int n, mn, mx, inm, inx;
pair < int, int > a[MAXN];
set < double > pl;

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	cin >> n;

	n *= 2;

	for (int i = 1; i <= n; ++i) {
		cin >> a[i].F;
		a[i].S = i;
	}
	sort (a + 1, a + 1 + n);

	inm = 1, inx = n;
	int fr;

	while (inm < inx) {
		if (a[inx].S == 1) {
		    fr = inx;
			inx--;
			continue;
		}
		if (a[inm].S == 1) {
			fr = inm;
			inm++;
			continue;
		}
		pl.insert (double (double(double (a[inx].F) + double (a[inm].F)) / 2.0));
		inx--;
		inm++;
	}
	double rank = double (a[inx].F * 1.0 + a[fr].F * 1.0) / 2.0;
	mx = 1;
	for (auto it : pl) {
		if (it > rank)
			mx++;
	}
	cout << mx << ' ';
	
	inm = n / 2, inx = n;

	pl.clear();

	while (inm > 0) {
		if (a[inx].S == 1) {
		    fr = inx;
			inx--;
			continue;
		}
		if (a[inm].S == 1) {
			fr = inm;
			inm--;
			continue;
		}
		pl.insert (double (double(double (a[inx].F) + double (a[inm].F)) / 2.0));
		inx--;
		inm--;
	}
	rank = double (a[inm + 1].F * 1.0 + a[fr].F * 1.0) / 2.0;

	mn = 1;

	for (auto it : pl) {
		if (it > rank)
			mn++;
	}

	cout << mn;

	return 0;
}