#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MAXN = 2 * 1e5 + 256;
int n, mn, mx, inm, inx;
pair < double, int > a[MAXN];
vector < double > pl;

int binsearch (double key, int mest) {
	int l = 1, r = n;
	while (r - l > 1) {
		int mid = (r + l) >> 1;
		if (a[mid].F > key)
			r = mid;
		else if (a[mid].F == key && a[mid].S > mest)
			r = mid;
		else l = mid;
	}
	return l;
}

int main () {
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	cin >> n;

	n *= 2;

	double kk;

	for (int i = 1; i <= n; ++i) {
		cin >> a[i].F;
		a[i].S = i;
	}
	kk = a[1].F;
	sort (a + 1, a + 1 + n);

	inm = 1, inx = n;
	int fr = binsearch (kk, 1);

	int sr;

	if (fr == n) {
		sr = fr - 1;
	}
	else
		sr = n;

	double rank = double (double (a[fr].F * 1.0 + a[sr].F * 1.0) / 2.0);

	inm = 1, inx = n;

	while (inm < inx) {
		if (inm == fr || inm == sr) {
			inm++;
			continue;
		}
		if (inx == fr || inx == sr) {
			inx--;
			continue;
		}
		pl.pb (double ( double (a[inm].F * 1.0 + a[inx].F * 1.0) / 2.0));
		inm++;
		inx--;
	}
	mx = 1;
	for (auto it : pl) {
		if (it > rank)
			mx++;
	}
	cout << mx << ' ';

	mn = 1;

	if (fr == 1)
		sr = 2;
	else
		sr = 1;
	rank = double (double (a[fr].F * 1.0 + a[sr].F * 1.0) / 2.0);
	inm = 1, inx = n;
	pl.clear();
	while (inm < inx) {
		if (inm == fr || inm == sr) {
			inm++;
			continue;
		}
		if (inx == fr || inm == sr) {
			inx--;
			continue;
		}
		pl.pb (double ( double (a[inm].F * 1.0 + a[inx].F * 1.0) / 2.0));
		//cout << '\n' << inm << ' '<< inx << '\n';
		inm++;
		inx--;
	}

	for (auto it : pl) {
		if (it > rank)
			mn++;
	}
	int mmn = 1;

	rank = double( double (a[fr].F * 1.0 + a[sr].F * 1.0) / 2.0);
	inm = 1, inx = 2;
	pl.clear();
	while (inm <= n && inx <= n) {
		if (inm == fr || inm == sr) {
			inm++;
			continue;
		}
		if (inx == fr || inx == sr) {
			inx++;
			continue;
		}
		if (inm == inx) {
			inx++;
			continue;
		}
		pl.pb (double ( double (a[inm].F * 1.0 + a[inx].F * 1.0) / 2.0));
		//cout << '\n' << inm << ' '<< inx << '\n';
		inm += 2;
		inx += 2;
	}
	for (auto it : pl) {
		if (it > rank)
			mmn++;
	}

	cout << max (mn, mmn);

	return 0;
}