#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <bits/stdc++.h>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
int l, r, a, c, x, ans;
vector < int > s;
map < int, bool > mm;

int main () {
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	
	cin >> a >> c >> l >> r;

	/*for (int i = l; i <= r; ++i) {
		if (a / i * i == a - c) {
			cout << i << '\n';
			ans++;
		}
	}*/
	x = a - c;
	for (int i = 2; i * i <= x; ++i) {
		if (x % i == 0) {
			s.pb (i);
			s.pb (x / i);
		}
	}
	s.pb (1);
	s.pb (x);
	for (auto it : s) {
		if (!mm[it] && a % it == c && it >= l && it <= r) {
			//cout << it << ' ';
			mm[it] = 1;
			ans++;
		}
	}
	cout << ans;

	return 0;
}