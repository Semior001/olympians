#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
int n, a[100200], u[4000009], ind, h[100200], ans, uu[4000009], d[100200], maxa=-inf;
void bin(int x)
{
    ind=0;
    while(x)
    {
        int y = x%2;
        h[ind] = y;
        ind++;
        x/=2;
    }
}
void check(int x)
{
    ans += u[x];
    //cout<<x<<" ";
}
void go(int v, int cur)
{
    //cout<<v<<" "<<h[v]<<" "<<cur<<endl;
    if(v == 9)
        check(cur);
    else
    {
        if(h[v] == 1)
            go(v+1, cur);
        else
        {
            go(v+1, cur);
            go(v+1, cur + pow(2, v));
        }
    }
}
int main()
{
    ifstream cin("F.in");
    ofstream cout("F.out");
    cin>>n;
    for(int i=1;i<=n;i++)
    {
        cin>>a[i];
        maxa = max(maxa, a[i]);
        u[a[i]]++;
    }
    if(n<=20000)
    {
        for(int i=1;i<=n;i++)
        {
            for(int j=i+1;j<=n;j++)
            {
                int x = a[i] & a[j];
                //cout<<i<<" "<<j<<" "<<a[i]<<" "<<a[j]<<" "<<x<<endl;
                if(x == 0)
                {
                    uu[i]++;
                    uu[j]++;
                }
            }
        }
        for(int i=1;i<=n;i++)
            cout<<uu[i]<<" ";
        return 0;
    }
    if(maxa <= 100)
    {
        for(int i=1;i<=n;i++)
        {
            for(int j=0;j<=7;j++)
                h[j] = 0;
            ans = 0;
            //cout<<i<<":"<<endl;
            bin(a[i]);
            //for(int j=0;j<ind;j++)
            //    cout<<h[j];
            //cout<<endl;
            go(0, 0);
            cout<<ans<<" ";
            //cout<<endl;
        }
        return 0;
    }
    for(int i=1;i<=n;i++)
    {
        for(int j=0;j<=22;j++)
            h[j] = 0;
        ans = 0;
        bin(a[i]);
        for(int j=0;j<22;j++)
        {
            if(h[j])
                continue;
            int beg = pow(2, j), add = pow(2, j+1);
            for(int k = beg; k <= maxa; k+=add)
            {
                int x = k & a[i];
                if(!x)
                    ans+=u[k];
            }
        }
        cout<<ans<<" ";
    }
}
/*
*/
