#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#include<vector>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
int n, l, r, a, ans;
vector<int> g;
void go(int x)
{
    for(int i = l; i<=r;i++)
    {
        g.push_back(i);
    }
}
long long lca(int x, int y)
{
    return x * y / __gcd(x, y);
}
int main()
{
    ifstream cin("A.in");
    ofstream cout("A.out");
    cin>>n>>l>>r>>a;
    if(n == 1)
    {
        for(int i=l;i<=r;i++)
        {
            if(i % a == 0)
                ans++;
        }
        cout<<ans;
        return 0;
    }
    go(a);
    for(int i=0;i<g.size();i++)
    {
        int x = g[i];
        for(int j=i;j<g.size();j++)
        {
            int y = g[j];
            //cout<<x<<" "<<y<<" "<<lca(x, y)<<" ";
            if(lca(x, y) % a == 0)
            {
                ans++;
            }
            //cout<<ans<<endl;
        }
    }
    if(n>5)
    {
        cout<<1/0;
    }
    cout<<ans;
}
