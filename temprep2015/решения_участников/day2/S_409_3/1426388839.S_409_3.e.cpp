//Elibay Nuptebek
#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
const int MaxN = 2e5 + 17, INF = 1e9 + 17;
long long int n, a[MaxN], x, y, Sum, S = 1, SS = 1, Sum2;
bool u[MaxN];
int main ()
{
    ios_base :: sync_with_stdio (0);
    freopen ("E.in", "r", stdin);
    freopen ("E.out", "w", stdout);
    cin >> n >> a[1];
    n = n + n;
    for (int i = 2; i <= n; ++ i)
        cin >> a[i];
    sort (a + 2, a + n + 1);
    Sum = a[1] + a[n];
    /*
    int L = 2;
    int R = n - 1;
    while (L < R)
    {
        int l, r;
        l = L;
        r = R - 1;
        while (l < r)
        {
            int m = (l + r) >> 1;
            if (a[m] + a[R] < Sum)
                l = m + 1;
            else
                r = m;
        }
        if (a[l] + a[R] > Sum)
        {
            R -= 2;
            S ++;
        }
        else
        {
            R --;
            if (R - l >= l - L)
            {
                for (int i = l; i > L; -- i)
                    swap (a[i], a[i - 1]);
            }
            else
            {
                for (int i = l; i < R; ++ i)
                    swap (a[i], a[i + 1]);
            }
            L ++;
        }
    }*/
    S = n / 2;
    for (int i = n - 1; i >= 2; -- i)
    {
        if (!u[i])
        {
            int b = 0;
            for (int j = i - 1; j >= 2; -- j)
            {
                if (!u[j])
                {
                    if (!b)
                        b = j;
                    if (a[i] + a[j] <= Sum)
                    {
                        S --;
                        u[i] = 1;
                        u[j] = 1;
                        break;
                    }
                }
            }
            if (!u[i])
            {
                u[i] = 1;
                u[b] = 1;
            }
            i = b + 1;
        }
    }
    cout << S << ' ';
    Sum2 = a[1] + a[2];
    memset (u, 0, sizeof (u));
    for (int i = 3; i <= n; ++ i)
    {
        if (!u[i])
        {
            int b = 0;
            for (int j = i + 1; j <= n; ++ j)
            {
                if (!u[j])
                {
                    if (!b)
                        b = j;
                    if (a[i] + a[j] > Sum2)
                    {
                        SS ++;
                        u[i] = 1;
                        u[j] = 1;
                        break;
                    }
                }
            }
            if (!u[i])
            {
                u[i] = 1;
                u[b] = 1;
            }
            i = b - 1;
        }
    }
    cout << SS;
    return 0;
}
//Will be Top 1
