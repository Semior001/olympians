//Elibay Nuptebek
#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
const int MaxN = 1e5 + 17, INF = 1e9 + 17;
int n, l, r, A, Sum;
int lca (int a, int b)
{
    return (a * b) / __gcd (a, b);
}
int main ()
{
    ios_base :: sync_with_stdio (0);
    freopen ("D.in", "r", stdin);
    freopen ("D.out", "w", stdout);
    cin >> n >> l >> r >> A;
    if (n == 1)
    {
        for (int i = l; i <= r; ++ i)
            if (i % A == 0)
                Sum ++;
        cout << Sum << endl;
    }
    else
    {
        for (int i = l; i <= r; ++ i)
            for (int j = i; j <= r; ++ j)
                if (lca (i, j) % A == 0)
                    Sum ++;
        cout << Sum;
    }
    return 0;
}
//Will be Top 1
