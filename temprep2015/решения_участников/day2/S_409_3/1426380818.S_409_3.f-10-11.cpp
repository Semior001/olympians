//Elibay Nuptebek
#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
const int MaxN = 2e5 + 17, INF = 1e9 + 17;
int n, a[MaxN], u[MaxN];
int main ()
{
    ios_base :: sync_with_stdio (0);
    freopen ("F.in", "r", stdin);
    freopen ("F.out", "w", stdout);
    cin >> n;
    for (int i = 1; i <= n; ++ i)
        cin >> a[i];
    for (int i = 1; i <= n; ++ i)
    {
        for (int j = i + 1; j <= n; ++ j)
            if ((a[i] & a[j]) == 0)
                u[j] ++, u[i] ++;
        cout << u[i] << ' ';
    }
    return 0;
}
//Will be Top 1
