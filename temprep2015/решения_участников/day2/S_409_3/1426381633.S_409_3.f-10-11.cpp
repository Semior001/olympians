//Elibay Nuptebek
#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
const int MaxN = 1e5 + 17, INF = 1e9 + 17;
int n, a[MaxN], u[MaxN], m = 1e3;
vector < int > v[MaxN];
int main ()
{
    ios_base :: sync_with_stdio (0);
    freopen ("F.in", "r", stdin);
    freopen ("F.out", "w", stdout);
    cin >> n;
    for (int i = 1; i <= n; ++ i)
        cin >> a[i];
    if (n <= int (1e4))
    {
        for (int i = 1; i <= n; ++ i)
        {
            for (int j = i + 1; j <= n; ++ j)
                if ((a[i] & a[j]) == 0)
                    u[j] ++, u[i] ++;
            cout << u[i] << ' ';
        }
    }
    else
    {
        for (int i = 1; i <= m; ++ i)
            for (int j = 1; j <= m; ++ j)
                if ((i & j) == 0)
                    v[i].push_back (j);
        for (int i = 1; i <= n; ++ i)
            u[a[i]] ++;
        for (int i = 1; i <= n; ++ i)
        {
            int x = a[i], Sum = 0;
            for (int j = 0; j < v[x].size(); ++j)
            {
                Sum += u[v[x][j]];
            }
            cout << Sum << ' ';
        }
    }
    return 0;
}
//Will be Top 1
