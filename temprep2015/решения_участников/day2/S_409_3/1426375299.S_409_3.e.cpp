//Elibay Nuptebek
#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
const int MaxN = 2e5 + 17, INF = 1e9 + 17;
int n, a[MaxN], X;
int main ()
{
    ios_base :: sync_with_stdio (0);
    freopen ("A.in", "r", stdin);
    freopen ("A.out", "w", stdout);
    cin >> n >> X;
    for (int i = 2; i <= n + n; ++ i)
        cin >> a[i];
    sort (a + 1, a + n + n + 1);
    int x = n + n - 1, ans = 1;
    double Q = (X + a[n + n]) / 2;
    while (x - 1 >= 2 && (a[x] + a[x - 1]) / 2 > Q)
    {
        x -= 2;
        ans ++;
    }
    cout << ans << ' ';
    Q = (X + a[2]) / 2;
    x = n + n;
    ans = 1;
    while (x - 1 >= 2 && (a[x] + a[x - 1]) / 2 > Q)
    {
        x -= 2;
        ans ++;
    }
    cout << ans;
    return 0;
}
//Will be Top 1
