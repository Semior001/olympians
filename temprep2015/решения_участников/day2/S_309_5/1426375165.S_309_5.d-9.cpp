#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;

int main() {
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    int cnt = 0;
    int a, c, l, r;
    scanf("%d %d %d %d", &a, &c, &l, &r);
    for (int i = l; i <= r; i++) {
        if (a % i == c) {
            cnt++;
        }
    }
    printf("%d\n", cnt);
    return 0;
}
