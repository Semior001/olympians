#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
int N;
int arr[800100];
int nums[800100];
bool used[800100];
bool answer[800100];
int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    scanf("%d", &N);
    for (int i = 0; i < 2 * N; i++) {
        scanf("%d", &arr[i]);
    }
    int maxs = 0;
    int maxsindd;
    for (int i = 1; i < 2 * N; i++) {
        if(maxs < arr[i]) {
            maxs = arr[i];
            maxsindd = i;
        }
    }
    used[0] = true;
    used[maxsindd] = true;
    answer[maxs + arr[0]] = true;
    int mas = maxs + arr[0];
    for (int i = 0; i < N - 1; i++) {
        int maxi = 0;
        int maxiindd;
        for (int i = 0; i < 2 * N; i++) {
            if (arr[i] > maxi && !used[i]) {
                maxi = arr[i];
                maxiindd = i;
            }
        }
        used[maxiindd] = true;
        int mixi = 1000000000;
        int mixiindd;
        for (int i = 0; i < 2 * N; i++) {
            if (arr[i] < mixi && !used[i]) {
                mixi = arr[i];
                mixiindd = i;
            }
        }
        used[mixiindd] = true;
        answer[maxi + mixi] = true;
    }
    int ans = 0;
    for (int i = 200000; i >= mas; i--) {
        if (answer[i]) {
            ans++;
        }
    }
    printf("%d ", ans);
    for (int i = 0; i < 2 * N; i++) {
        used[i] = false;
    }
    for (int i = 200100; i >= 0; i--) {
        answer[i] = false;
    }
    int mins = 1000000000;
    int minsindd;
    for (int i = 1; i < 2 * N; i++) {
        if(mins > arr[i]) {
            mins = arr[i];
            minsindd = i;
        }
    }
    used[0] = true;
    used[minsindd] = true;
    answer[minsindd + arr[0]] = true;
    int masi = minsindd + arr[0];
    for (int i = 0; i < N - 1; i++) {
        int maxi = 0;
        int maxiindd;
        for (int i = 0; i < 2 * N; i++) {
            if (arr[i] > maxi && !used[i]) {
                maxi = arr[i];
                maxiindd = i;
            }
        }
        used[maxiindd] = true;
        int mixi = 0;
        int mixiindd;
        for (int i = 0; i < 2 * N; i++) {
            if (arr[i] > mixi && !used[i]) {
                mixi = arr[i];
                mixiindd = i;
            }
        }
        used[mixiindd] = true;
        answer[maxi + mixi] = true;
    }
    int ans1 = 0;
    for (int i = 200000; i >= masi; i--) {
        if (answer[i]) {
            ans1++;
        }
    }
    printf("%d\n", ans1);
    return 0;
}
