#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
int N;
int arr[800100];
int nums[800100];
bool used[800100];
int track[800100];
int counter = 0;
int answer[800100];
void merr(int p, int mid, int r) {
    int u = p;
    int e = mid + 1;
    vector<int> c;
    vector<int> d;
    while (c.size() != r - p + 1) {
        if (e > r || (u <= mid && arr[u] < arr[e])) {
            c.push_back(arr[u]);
            d.push_back(nums[u]);
            u++;
        } else {
            c.push_back(arr[e]);
            d.push_back(nums[e]);
            e++;
        }
    }
    for (int i = p; i <= r; i++) {
        arr[i] = c[i - p];
        nums[i] = d[i - p];
    }
}
void merr1(int p, int mid, int r) {
    int u = p;
    int e = mid + 1;
    vector<int> c;
    vector<int> d;
    while (c.size() != r - p + 1) {
        if (e > r || (u <= mid && answer[u] > answer[e]) || (u <= mid && (answer[u] == answer[e] && track[u] < track[e]))) {
            c.push_back(answer[u]);
            d.push_back(track[u]);
            u++;
        } else {
            c.push_back(answer[e]);
            d.push_back(track[e]);
            e++;
        }
    }
    for (int i = p; i <= r; i++) {
        answer[i] = c[i - p];
        track[i] = d[i - p];
    }
}
void merge_sort(int p, int r) {
    int mid;
    if (p < r) {
        mid = (p + r) / 2;
        merge_sort(p, mid);
        merge_sort(mid + 1, r);
        merr(p, mid, r);
    }
}
void merge_sort1(int p, int r) {
    int mid;
    if (p < r) {
        mid = (p + r) / 2;
        merge_sort1(p, mid);
        merge_sort1(mid + 1, r);
        merr1(p, mid, r);
    }
}
int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    scanf("%d", &N);
    for (int i = 0; i < 2 * N; i++) {
        scanf("%d", &arr[i]);
        nums[i] = i;
    }
    merge_sort(0, 2 * N - 1);
    for (int i = 0; i < 2 * N; i++) {
        if (nums[i] == 0 && i != 2 * N - 1) {
            answer[counter] = (arr[i] + arr[2 * N - 1]); // might not work
            track[counter] = counter;
            counter++;
            used[i] = true;
            used[2 * N - 1] = true;
            break;
        } else if (nums[i] == 0 && i == 0) {
            answer[counter] = (arr[i] + arr[2 * N - 2]); // might not work
            track[counter] = counter;
            counter++;
            used[i] = true;
            used[2 * N - 2] = true;
            break;
        }
    }
    int l = 0;
    int r = 2 * N - 1;
    while (r - l >= 1) {
        while(used[r]) {
            r--;
        }
        while(used[l]) {
            l++;
        }
        used[l] = true;
        used[r] = true;
        answer[counter] = (arr[l] + arr[r]);
        track[counter] = counter;
        counter++;
        r--;
        l++;
    }
    merge_sort1(0, counter - 1);
    for (int i = 0; i < counter; i++) {
        if (track[i] == 0) {
            printf("%d ", i + 1);
            break;
        }
    }
    for (int i = 0; i < counter; i++) {
        track[i] = i;
        answer[i] = 0;
    }
    counter = 0;
    for (int i = 0; i < 2 * N; i++) {
        used[i] = false;
    }
    for (int i = 0; i < 2 * N; i++) {
        if (nums[i] == 0 && i != 0) {
            answer[counter] = (arr[i] + arr[0]); // might not work
            track[counter] = counter;
            counter++;
            used[i] = true;
            used[0] = true;
            break;
        } else if(nums[i] == 0 && i == 0) {
            answer[counter] = (arr[i] + arr[1]); // might not work
            track[counter] = counter;
            counter++;
            used[i] = true;
            used[1] = true;
        }
    }
    for (int i = 2 * N - 1; i >= 1; i--) {
        if (!used[i]) {
            int cursor = i - 1;
            while(used[cursor]) {
                cursor--;
            }
            used[i] = true;
            used[cursor] = true;
            answer[counter] = (arr[i] + arr[cursor]);
            track[counter] = counter;
            counter++;
        }
    }
    merge_sort1(0, counter - 1);
    for (int i = 0; i < counter; i++) {
        if (track[i] == 0) {
            printf("%d ", i + 1);
            break;
        }
    }
    printf("\n");
    return 0;
}
