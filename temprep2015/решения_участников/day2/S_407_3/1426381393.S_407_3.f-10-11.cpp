#include <algorithm>
#include <vector>
#include <map>
#include <cstdio>
#include <cmath>
#include <set>
#include <string>
#include <iostream>
#include <cstring>

using namespace std;

#define fname "F"
#define pb(x) push_back(x)
#define sc scanf
#define pr printf

int n, last[30], a[100001], ans[100001], k;
vector <int> g[30];
map <int, bool> w[100001];

int main ()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> a[i];
	for (int i = 1; i <= n; ++i)
		for (int j = i + 1; j <= n; ++j)
			if ((a[i] & a[j]) == 0)
				ans[i]++, ans[j]++;
    for (int i = 1; i <= n; ++i)
    	cout << ans[i] << ' ';
	return  0;
}
