#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <cmath>
#include <set>
#include <string>
#include <cstring>

using namespace std;

#define fname "E"
#define pb(x) push_back(x)
#define sc scanf
#define pr printf

int n, need, cnt, a[100001], pos,l, r, MIN,MAX,last;
bool ok;
map <int, bool> sum, sum1;
bool w[100001], w1[100001];

int main ()
{
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n * 2; ++i)
		cin >> a[i];
	sort (a + 2, a + n * 2 + 1);
	MAX = a[1] + a[n * 2];
	MIN = a[1] + a[2];
	for (int i = n * 2 - 1; i > 2; --i)
	{
		l = i;
		if (w[l]) continue;
		ok = 0;
		last = 0;
		for (r = l - 1; r >= 2; --r)
		{
			if (w[r]) continue;
			if (last == 0) last = r;
			if (a[l] + a[r] < MAX)
			{
				ok = 1;
				w[l] = w[r] = true, sum[a[l] + a[r]] = true;
				break;
			}
		}
		if (!ok)
		{
			w[l] = w[last] = true;
			if (!sum[a[l] + a[last]])
				cnt++;
			sum[a[l] + a[last]] = true;
		}
	}
	cout << cnt + 1 << ' ';
	cnt = 0;
	for (int i = 3; i <= n * 2 - 1; ++i)
	{
		l = i;
		if (w1[l]) continue;
		ok = 0;
		last = 0;
		for (r = l + 1; r <= n * 2; ++r)
		{
			if (w1[r]) continue;
			if (last == 0) last = r;
			if (a[l] + a[r] > MIN)
			{
				ok = 1;
				w1[l] = w1[r] = true, sum1[a[l] + a[r]] = true;
				break;
			}
		}
		if (!ok)
		{
			w1[l] = w1[last] = true;
			if (!sum1[a[l] + a[last]])
				cnt++;
			sum1[a[l] + a[last]] = true;
		}
	}
	cout << cnt + 1;
	return  0;
}

