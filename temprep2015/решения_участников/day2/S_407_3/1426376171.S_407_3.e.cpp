#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <cmath>
#include <set>
#include <string>
#include <cstring>

using namespace std;

#define fname "E"
#define pb(x) push_back(x)
#define sc scanf
#define pr printf

int n, need, cnt, a[100001];

int main ()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n * 2; ++i)
		cin >> a[i];
	sort (a + 2, a + n * 2 + 1);
	need = a[1] + a[2];
	for (int l = 3; l <= n * 2; l += 2)
		if (need < a[l] + a[l + 1])
			cnt++;
	cout << cnt + 1 << ' ';
	need = a[1] + a[n * 2];
	cnt = 0;
	for (int l = 2, r = n * 2 - 1; l < r; ++l, r--)
		if (a[l] + a[r] < need)
			cnt++;
	cout << cnt + 1;
	return  0;
}
