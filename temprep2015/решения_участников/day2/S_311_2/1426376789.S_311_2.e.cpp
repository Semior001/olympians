#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

struct asd{
       int x, p;
       bool s;
};

asd a[200500], c[200500], d[200500];
bool was[200500], was1[200500];
int kol, kol1;

bool cmp(asd p1, asd p2) {
     return (p1.x < p2.x);
}

int main () {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    srand(time(NULL));
    int n;
    sc("%d", &n);
    for (int i = 1; i <= 2 * n; i++) {
        sc("%d", &a[i].x);
        a[i].p = i;
    }
    sort(a + 1, a + 1 + 2 * n, cmp);
    int pos;
    for (int i = 1; i <= 2 * n; i++) {
        if (a[i].p == 1) {
           pos = i;
        }
    }
    int pos1, pos2;
    if (1 == pos) {
       pos2 = 2;
    } else {
       pos2 = 1;
    }
    if (2 * n == pos) {
       pos1 = 2 * n - 1;
    } else {
       pos1 = 2 * n;
    }
    was1[pos] = true;
    was1[pos2] = true;
    was[pos1] = true;
    was[pos] = true;
    d[++kol1].x = a[pos].x + a[pos2].x;
    c[++kol].x = a[pos].x + a[pos1].x;
    c[kol].s = true;
    d[kol1].s = true;
    int ps = 1;
    int f = 2 * n;
    while (kol != n) {
          while (was[f]) {
                f--;
          }
          while (was[ps]) {
                ps++;
          }
          was[f] = true;
          was[ps] = true;
          c[++kol].x = a[f].x + a[ps].x;
    }
    f = 2 * n;
    ps = 2 * n - 1;
    while (kol1 != n) {
          while (was1[f]) {
                f--;
          }
          while (was1[ps]) {
                ps--;
          }
          was1[f] = true;
          was1[ps] = true;
          d[++kol1].x = a[f].x + a[ps].x;
    }
    sort(c + 1, c + 1 + n, cmp);
    sort(d + 1, d + 1 + n, cmp);
    for (int i = 1; i <= n; i++) {
        if (c[i].s) {
           pos = i;
        }
        if (d[i].s) {
           pos1 = i;
        }
    }
    int l = 1;
    int r = n;
    int p;
    while (l <= r) {
          int mid = (l + r) / 2;
          if (c[mid].x > c[pos].x) {
             r = mid - 1;
          } else {
             l = mid + 1;
             p = mid;
          }
    }
    cout << n - p + 1 << " ";
    l = 1;
    r = n;
    while (l <= r) {
          int mid = (l + r) / 2;
          if (d[mid].x > d[pos1].x) {
             r = mid - 1;
          } else {
             l = mid + 1;
             p = mid;
          }
    }
    cout << n - p + 1;
    return 0;
}
