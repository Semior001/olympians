#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

long long t[300500];

struct asd{
       int p, h, c;
};

asd a[100500];
long long d[100500], s[100500];
bool cmp(asd p1, asd p2) {
     return ((p1.h < p2.h) || (p1.h == p2.h && p1.p < p2.p));
}

void update(int p) {
     p /= 2;
     while (p) {
           t[p] = t[p * 2] + t[p * 2 + 1];
           p /= 2;
     }
}

long long get_sum(int l, int r){
          long long rs = 0;
          while (l <= r) {
                if (l % 2 == 1) {
                   rs += t[l];
                }
                if (r % 2 == 0) {
                   rs += t[r];
                }
                l = (l + 1) / 2;
                r = (r - 1) / 2;
          }
          return rs;
}
int main () {
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);
    srand(time(NULL));
    int n;
    sc("%d", &n);
    for (int i = 1; i <= n; i++) {
        cin >> a[i].h;
        a[i].p = i;
    }
    for (int i = 1; i <= n; i++) {
        cin >> a[i].c;
        d[i] = d[i - 1] + a[i].c;
    }
    sort(a + 1, a + 1 + n, cmp);
    int k = 0;
    int sz = 1;
    while (sz < n) {
           sz *= 2;
    }
    long long ans = 0;
    for (int i = 1; i <= n; i++) {
        if (a[i].p + s[i] != i) {
           t[sz + a[i].p - 1] = a[i].c;
           update(sz + a[i].p - 1);
           ans += min((long long)(a[i].c), d[a[i].p] - get_sum(sz + i - 1, sz + a[i].p - 1));
           for (int j = a[i].p; j >= 1; j--) {
                s[j]++;
           }
           k++;
        }
    }
    cout << ans;
    return 0;
}
