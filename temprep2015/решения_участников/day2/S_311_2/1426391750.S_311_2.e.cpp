#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

struct asd{
       int x, p;
       bool s;
};

asd a[200500], c[200500], okt[200500];
bool was[200500], was1[200500];
int kol, kol1;

bool cmp(asd p1, asd p2) {
     return ((p1.x < p2.x) || (p1.x == p2.x && p1.p < p2.p));
}

int main () {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    srand(time(NULL));
    int n;
    sc("%d", &n);
    for (int i = 1; i <= 2 * n; i++) {
        sc("%d", &a[i].x);
        a[i].p = i;
    }
    sort(a + 1, a + 1 + 2 * n, cmp);
    int pos;
    for (int i = 1; i <= 2 * n; i++) {
        if (a[i].p == 1) {
           pos = i;
        }
    }
    int pos1, pos2;
    if (1 == pos) {
       pos2 = 2;
    } else {
       pos2 = 1;
    }
    if (2 * n == pos) {
       pos1 = 2 * n - 1;
    } else {
       pos1 = 2 * n;
    }
    was1[pos] = true;was1[pos2] = true;was[pos1] = true;was[pos] = true;
    int sum = a[pos].x + a[pos2].x;
    c[++kol].x = a[pos].x + a[pos1].x;
    c[kol].s = true;
    int ps = 1;
    int f = 2 * n;
    while (kol != n) {
          while (was[f]) {
                f--;
          }
          while (was[ps]) {
                ps++;
          }
          if (f!=ps) {
          was[f] = true;
          was[ps] = true;
          c[++kol].x = a[f].x + a[ps].x;
          }
    }
    int k = 0;
    for (int i = 1; i <= 2 * n; i++) {
        if (!was1[i]) {
           okt[++k].x = a[i].x;
           okt[k].p = i;
        }
    }
    int ans = 0;
    for (int i = 1; i <= k; i++) {
        if (!was1[okt[i].p])
        for (int j = i + 1; j <= k; j++) {
            if (!was1[okt[j].p] && !was1[okt[i].p]) {
               if (okt[i].x + okt[j].x > sum) {
                  was1[okt[j].p] = true;
                  was1[okt[i].p] = true;
                  ans++;
               }
            }
        }
    }
    sort(c + 1, c + 1 + n, cmp);
    for (int i = 1; i <= n; i++) {
        if (c[i].s) {
           pos = i;
        }
    }
    int j = pos;
    while (c[pos].x == c[j].x && j <= n) {
          j++;
    }
    cout << n - j + 2 << " ";
    cout << ans + 1;
    return 0;
}
