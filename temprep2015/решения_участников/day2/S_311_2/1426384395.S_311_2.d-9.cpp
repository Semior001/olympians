#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

int main () {
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    srand(time(NULL));
    int a, c, l, r;
    sc("%d%d%d%d", &a, &c, &l, &r);
    a -= c;
    int ans = 0;
    for (int i = 1; i <= sqrt(a); i++) {
        if (a % i == 0 && a / i >= l && a / i <= r && a / i != i) {
           ans++;
        }
    }
    pr("%d", ans);
    return 0;
}
