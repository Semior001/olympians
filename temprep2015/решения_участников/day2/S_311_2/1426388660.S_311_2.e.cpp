#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

struct asd{
       int x;
       int p;
};

asd a[200500];
bool w1[200500], w2[200500];
bool cmp(asd p1, asd p2) {
    return p1.x < p2.x;
}
int main () {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    srand(time(NULL));
    int n;
    sc("%d", &n);
    for (int i = 1; i <= 2 * n; i++) {
        sc("%d", &a[i].x);
        a[i].p = i;
    }
    sort(a + 1, a + 1 + 2 * n, cmp);
    int pos;
    for (int i = 1; i <= 2 * n; i++) {
        if (a[i].p == 1) {
           pos = i;
           break;
        }
    }
    int pos1, pos2;
    if (pos == 1) {
       pos1 = 2;
    } else {
       pos1 = 1;
    }
    if (pos == 2 * n) {
       pos2 = (2 * n) - 1;
    } else {
       pos2 = 2 * n;
    }
    int sum = a[pos].x + a[pos1].x;
    int sum1 = a[pos].x + a[pos2].x;
    w1[pos] = w1[pos1] = w2[pos] = w2[pos2] = true;
    int ans = 0;
    for (int i = 2 * n; i >= 1; i--) {
        if (!w2[i]) {
           int mn = i;
           for (int j = 1; j < i; j++) {
               if (!w2[j]) {
                  if (a[i].x + a[j].x > sum1 && (a[mn].x < a[j].x || mn == i)) {
                     mn = j;
                  }
               }
           }
           if (mn != i) {
              w2[mn] = w2[i] = true;
              ans++;
           }
        }
    }
    int ans1 = 0;
    for (int i = 1; i <= 2 * n; i++) {
        if (!w1[i]) {
           int mn = i;
           for (int j = i + 1; j <= 2 * n; j++) {
               if (!w1[j]) {
                  if (a[i].x + a[j].x > sum && (a[mn].x > a[j].x || mn == i)) {
                     mn = j;
                  }
               }
           }
           if (mn != i) {
              w1[mn] = w1[i] = true;
              ans1++;
           }
        }
    }
    cout << ans + 1 << " " << ans1 + 1;
    return 0;
}
