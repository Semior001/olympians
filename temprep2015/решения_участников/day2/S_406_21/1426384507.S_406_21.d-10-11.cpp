#include<iostream>
#include<fstream>

using namespace std;

int N;
long long L,R,A,ans;

int NOD(int a,int b)
{
  while(a*b!=0)
  {
      swap(a,b);
      a=a%b;
  }
  return (a+b);
}

int NOK(int a,int b)
{
    return ((a*b)/NOD(a,b));
}

void in()
{
    ifstream cin("D.in");
    cin >>N >>L >>R >>A;
}

void solution()
{

    if(N==1)
    {
        int c=L;
        while(c%A>0){c++;}
        while(c<=R)
        {
          ans++;ans=ans%(1000000000+7);
          c=c+A;
        }
    }

    if(N==2)
    {
      for(int c=L;c<=R;c++)
      {
          for(int v=c;v<=R;v++)
          {
            if(NOK(c,v)%A==0){ans++;ans=ans%(1000000000+7);}
          }
      }
    }


}

void out()
{
    ofstream cout("D.out");
    cout <<ans <<"\n";
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
