#include<iostream>
#include<fstream>
#include<set>
#include<algorithm>

using namespace std;

int n,am,mas[100000+10],ans_min,ans_max=1;

void in()
{
    ifstream cin("E.in");
    cin >>n >>am;
    for(int c=0;c<n*2-1;c++)
    {
      cin >>mas[c];
    }
}

int poisk_min(int mas_size)
{
    double our_team=(am+mas[0])/(double)2;
    for(int c=1;c<mas_size;c++)
    {
        for(int v=c+1;v<mas_size;v++)
        {
            double other_team=(mas[c]+mas[v])/(double)2;
            //cout <<our_team <<"VS" <<other_team <<"\n";
            if(our_team<other_team){return(n-c);}
        }
    }
    return 1;
}

int poisk_max(int mas_size)
{
    double our_team=(am+mas[mas_size-1]/(double)2);
    for(int c=mas_size-2;c>=0;c--)
    {
        for(int v=c-1;v>=0;v++)
        {
            double other_team=(mas[c]+mas[v])/(double)2;
            if(our_team>other_team){return (n-c);}
        }
    }
    return mas_size;
}

void solution()
{
  if(n==1)
  {
        ans_max=1;
        ans_min=1;
  }
  else
  {
    int mas_size=n*2-1;
    sort(mas,mas+mas_size);
    ans_min=poisk_min(mas_size);
  }

}

void out()
{
    ofstream cout("E.out");
    cout <<ans_max <<" " <<ans_min <<"\n";
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
