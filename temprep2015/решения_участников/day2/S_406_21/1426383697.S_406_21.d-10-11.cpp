#include<iostream>
#include<fstream>

using namespace std;

int N;
long long L,R,A,ans;

void in()
{
    ifstream cin("D.in");
    cin >>N >>L >>R >>A;
}

void solution(int n)
{
    if(N==1)
    {
        int c=L;
        while(c%A>0){c++;}
        while(c<=R)
        {
          ans++;ans=ans%(1000000000+7);
          c=c+A;
        }
    }
    if(N==2)
    {
      for(int c=L;c<=R;c++)
      {
          for(int v=c;v<=R;v++)
          {
            if(c*v%A==0){ans++;ans=ans%(1000000000+7);}
          }
      }
    }

}

void out()
{
    ofstream cout("D.out");
    cout <<ans <<"\n";
}

int main()
{
    in();
    solution(0);
    out();
    return 0;
}
