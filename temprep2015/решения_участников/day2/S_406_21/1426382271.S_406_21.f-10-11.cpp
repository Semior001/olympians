#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

int n,mas[100000+10],ans[100000+10];
long long prosch[4*1000000+10];
vector <int> vecpro[4*1000000+10];
void in()
{
    ifstream cin("F.in");
    cin >>n;
    for(int c=0;c<n;c++)
    {
        cin >>mas[c];
    }
}

void solution()
{
    for(int c=0;c<n;c++)
    {
        //cout <<mas[c] <<"." <<prosch[mas[c]] <<"\n";
        if(prosch[mas[c]]==0)
        {
          for(int v=c+1;v<n;v++)
          {
                 int mm=(!(mas[c]&mas[v]));
                 ans[c]=ans[c]+mm;
                 ans[v]=ans[v]+mm;
                 if(mm){vecpro[mas[c]].push_back(v);}
          }
          prosch[mas[c]]=ans[c];
        }
        else
        {
            ans[c]=prosch[mas[c]];
            for(int v=0;v<vecpro[mas[c]].size();v++)
            {
                if(vecpro[mas[c]][v]>c){ans[vecpro[mas[c]][v]]++;}
            }
        }
    }
    /*
    for(int c=0;c<11;c++)
    {
        cout <<prosch[c] <<" ";
    }
    */
}

void out()
{
    ofstream cout("F.out");
    for(int c=0;c<n;c++)
    {
        cout <<ans[c] <<" ";
    }
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
