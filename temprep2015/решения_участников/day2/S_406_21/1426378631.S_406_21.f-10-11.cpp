#include<iostream>
#include<fstream>

using namespace std;

int n,mas[100000+10],ans[100000+10];

void in()
{
    ifstream cin("F.in");
    cin >>n;
    for(int c=0;c<n;c++)
    {
        cin >>mas[c];
    }
}

void solution()
{
    for(int c=0;c<n;c++)
    {
        for(int v=c+1;v<n;v++)
        {
            ans[c]=ans[c]+(!(mas[c]&mas[v]));
            ans[v]=ans[v]+(!(mas[c]&mas[v]));
        }
    }
}

void out()
{
    ofstream cout("F.out");
    for(int c=0;c<n;c++)
    {
        cout <<ans[c] <<" ";
    }
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
