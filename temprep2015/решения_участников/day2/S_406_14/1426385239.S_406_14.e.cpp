# include <iostream>
# include <cstdio>
# include <cstdlib>
# include <iomanip>
# include <string>
# include <cstring>
# include <map>
# include <cstdio>
# include <cstdlib>
# include <set>
# include <algorithm>
# include <vector>
# include <valarray>
# include <unordered_set>
# include <bitset>
# include <ctime>
# include <clocale>

# define fi first
# define se second
# define mp make_pair
# define INF ll(1e17)
# define FOI(i,x,n) for(ll i=x;i<=n;i++)
# define FOD(i,x,n) for(ll i=n;i>=x;i--)

using namespace std;

typedef long long ll;

ll n,best=INF,worst=-INF;
pair<ll,ll>a[555555];
vector<pair<ll,ll> >cur;

int main(){
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    scanf("%lld",&n);
    for(ll i=1;i<=n*2;i++){
        scanf("%lld",&a[i].fi);
        a[i].se=i;
    }
    sort(a+1,a+1+(n*2));
    while(next_permutation(a+1,a+1+(n*2))){
        cur.clear();
        for(ll i=1;i<=n*2;i+=2){
            cur.push_back(mp((a[i].fi+a[i+1].fi)/2,(a[i].se==1||a[i+1].se==1)));
        }

        sort(begin(cur),end(cur),greater_equal<pair<ll,ll> >());
        ll repa=1;
        for(ll i=0;i<cur.size();i++){
            while(cur[i].fi==cur[i+1].fi){
                if(cur[i].se==1){
                    best=min(best,repa);
                    worst=max(worst,repa);
                }
                ++i;
            }
            if(cur[i].se==1){
                best=min(best,repa);
                worst=max(worst,repa);
            }
            ++repa;
        }
    }
    cout<<best<<" "<<worst<<"\n";
    return 0;
}
