#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

using namespace std;

const int MaxN = 10009;
const int M = 4000001;
const int INF = 1000000007;

static int m[11][1001];

/*int bin(int l, int r, int c) {
    while (l + 1 != r) {
        int m = (l + r) / 2;
        if (d[m] <= c) l = m;
        else r = m;
    }
    cout << l;
}*/

int gcd(int a, int b) {
    while (b) {
        a = a % b;
        swap(a, b);
    }
    return a;
}

int main() {
    //freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);

    int n, l, r, a;
    cin >> n >> l >> r >> a;
    m[0][a] = 1;
    if (r - l + 1 < n) {
        cout << 0;
        return 0;
    }
    for (int j = 1; j <= n; j++) {
        for (int i = l; i <= r; i++) {
            for (int k = 1; k <= a; k++) {
                int g = k / gcd(k, i);
                m[j][g] = ((long long)m[j][g] + m[j - 1][k]) % INF;
            }
        }
    }
    /*for (int i = 0; i <= n; i++) {
        for (int j = 1; j <= a; j++) {
            cout << m[i][j] << " ";
        }
        cout << endl;
    }*/
    cout << m[n][1];

    /*int n;
    cin >> n;
    int t;
    cin >> t;
    for (int i = 0; i < 2 * n - 1; i++) {
        int a;
        cin >> a;
        d.push_back(a);
    }
    sort(d.begin(), d.end());
    int Min = *(d.begin());
    int Max = *(d.end() - 1);
    d.erase(d.begin());
    d.erase(d.end() - 1);
    int s = Min + t;
    for (int i = 0; i < )*/

    return 0;
}
