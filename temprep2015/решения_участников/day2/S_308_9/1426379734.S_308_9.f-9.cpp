#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

const int MaxN = 10009;
const int M = 4000001;

vector <int> d;
vector <int> num(M, 0);

void print(int a) {
    int k = sizeof(int);
    //cout << "^ " << k << endl;
    for (int i = 8 * k - 1; i >= 0; i--) {
        cout << (bool)(a & (1 << i));
    }
    cout << endl;
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);

    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        int a;
        cin >> a;
        num[a]++;
        d.push_back(a);
    }
    //print(d[0]);
    //print(d[1]);
    for (int i = 0; i < n; i++) {
        int mask = (~d[i]) & ((1 << 7) - 1);
        //print(mask);
        int s = 0;
        //cout << "^ " << mask << endl;
        for (int j = mask; j > 0; j = (j - 1) & mask) {
            //cout << "^ ";
            //print(j);
            if (j < MaxN && num[j] > 0) s += num[j];
        }
        cout << s << " ";
    }

    return 0;
}
