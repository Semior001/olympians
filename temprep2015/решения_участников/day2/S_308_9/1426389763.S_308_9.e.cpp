#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

using namespace std;

const int MaxN = 20009;
const int M = 4000001;
const int INF = 1000000007;

vector <int> d, q;
static bool vis[MaxN];
static bool vis1[MaxN];

/*int bin(int l, int r, int c) {
    while (l + 1 != r) {
        int m = (l + r) / 2;
        if (d[m] <= c) l = m;
        else r = m;
    }
    cout << l;
}*/

int gcd(int a, int b) {
    while (b) {
        a = a % b;
        swap(a, b);
    }
    return a;
}

int main() {
    //freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);



    /*int n, l, r, a;
    cin >> n >> l >> r >> a;
    m[0][a] = 1;
    if (r - l + 1 < n) {
        cout << 0;
        return 0;
    }
    for (int j = 1; j <= n; j++) {
        for (int k = 1; k <= a; k++) {
            for (int i = l; i <= r; i++) {
                int g = k / gcd(k, i);
                m[j][g] = ((long long)m[j][g] + m[j - 1][k]) % INF;
            }
        }
    }
    for (int i = 0; i <= n; i++) {
        for (int j = 1; j <= a; j++) {
            cout << m[i][j] << " ";
        }
        cout << endl;
    }
    cout << m[n][1];*/

    int n;
    cin >> n;
    int t;
    cin >> t;
    for (int i = 0; i < 2 * n - 1; i++) {
        int a;
        cin >> a;
        d.push_back(a);
    }
    sort(d.begin(), d.end());
    q = d;
    reverse(q.begin(), q.end());
    int Min = *(d.begin());
    int Max = *(q.begin());
    d.erase(d.begin());
    q.erase(q.begin());
    int s = Max + t;
    int e = 0;
    for (int i = 0; i < 2 * (n - 1) - 1; i++) {
        //cout << "% " << i << endl;
        if (vis1[i] == false) {
            bool p = false;
            for (int j = i + 1; j < 2 * (n - 1); j++) {
                if (vis1[j] == false) {
                    if (q[j] + q[i] <= s) {
                        vis1[j] = true;
                        p = true;
                        break;
                    }
                }
            }
            if (!p) {
                cout << "% " << i << endl;
                e++;
                vis1[i + 1] = true;
            }
        }
    }
    cout << 1 + e << " ";
    s = Min + t;
    //cout << s << endl;
    int y = 0;
    //for (int i = 0; i < d.size(); i++) cout << d[i] << " ";
    for (int i = 0; i < 2 * (n - 1) - 1; i++) {
        //cout << "^ " << i << " " << vis[i] << endl;
        if (vis[i] == false) {
            bool p = false;
            for (int j = i + 1; j < 2 * (n - 1); j++) {
                if (vis[j] == false) {
                    if (d[j] + d[i] > s) {
                        vis[j] = true;
                        p = true;
                        break;
                    }
                }
            }
            if (!p) {
                y++;
                vis[i + 1] = true;
            }
        }
    }
    //cout << "Hello" << endl;
    cout << n - y << endl;

    return 0;
}
