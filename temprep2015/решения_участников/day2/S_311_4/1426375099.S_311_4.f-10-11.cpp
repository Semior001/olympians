#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "F."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];

void slow() {
	int cnt = 0;
	for (int i = 1; i <= n; i++) {
		cnt = 0;
		for (int j = 1; j <= n; j++)
			if ((a[i] & a[j]) == 0)
				cnt++;
		printf("%d ", cnt);
	}
	exit(0);
}

int cnt[N];
bool ok[102][102];

void solve() {
	for (int i = 1; i <= n; i++)
		cnt[a[i]]++;
	for (int i = 1; i <= 100; i++)	
		for (int j = i + 1; j <= 100; j++)
			if ((i & j) == 0)
				ok[i][j] = ok[j][i] = 1;
	int res = 0;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= 100; j++)
			res += ok[a[i]][j] * cnt[j];		
		printf("%d ", res);
	}
	exit(0);
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	if (n <= (int)1e4)
		slow();
	solve();
	return 0;
}