#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "E."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)2e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
pair < int, int > ans;
bool u[N];

void rec(int v = 1, int what = inf, int cnt = 0) {
    if (v == 2 * n + 1) {
    	ans.f = min(ans.f, cnt + 1);
    	ans.s = max(ans.s, cnt + 1);
    	return;
    }
	if (u[v]) {
		rec(v + 1, what, cnt);
		return;
	}	
	for (int i = v + 1; i <= 2 * n; i++)
		if (!u[i]) {
			u[i] = 1;
			rec(v + 1, (v == 1 ? a[1] + a[i] : what), cnt + (a[v] + a[i] > what));
			u[i] = 0;
		}
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	ans.f = 2 * n + 1;
	ans.s = 0;
	rec();
	printf("%d %d", ans.f, ans.s);
	return 0;
}