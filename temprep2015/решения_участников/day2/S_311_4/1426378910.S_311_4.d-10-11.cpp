#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "D."
#define sz(a) (int)((a).size())

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;
const int mod = (int)1e9 + 7;

using namespace std;

int d[11][2][2][2][2][1001];

int n, L, R, a;
vector < pair < int, int > > pw;

void fact(int x) {
	int cnt = 0;
	for (int i = 2; i * i <= x; i++) {
		if (x % i != 0)
			continue;
		cnt = 0;
		while(x % i == 0) {
			x /= i;
			cnt++;
		}		
		pw.pb(mp(i, cnt));
	}
	if (x > 1)
		pw.pb(mp(x, 1));
}

int power(int a, int b) {
	int res = 0;
	while(a % b == 0) {
		a /= b;
		res++;
	}
	return res;
}

int v[4];
int res = 0;

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d%d", &n, &L, &R, &a);		
	fact(a);
	d[0][0][0][0][0][L] = 1;
//	for (auto i : pw)
//		cout << i.f << " pw " << i.s << endl;
	for (int i = 0; i < n; i++)
		for (int last = L; last <= R; last++)
			for (int q = 0; q < 2; q++)
				for (int w = 0; w < 2; w++)
					for (int e = 0; e < 2; e++)
						for (int r = 0; r < 2; r++) {
							if (!d[i][q][w][e][r][last])
								continue;
//							cout << i << ' ' << q << ' ' << w << ' ' << e << ' ' << r << ' ' << last << endl;
//							cout << "in is " << d[i][q][w][e][r][last] << endl;
							for (int next = last; next <= R; next++) {
								v[0] = q, v[1] = w, v[2] = e, v[3] = r;	
								for (int i = 0; i < sz(pw); i++)
									if (power(next, pw[i].f) >= pw[i].s)
										v[i] |= 1;
								d[i + 1][v[0]][v[1]][v[2]][v[3]][next] = (d[i][q][w][e][r][last] + d[i + 1][v[0]][v[1]][v[2]][v[3]][next]) % mod;
//								cout << "upd with " << next << " is " << d[i + 1][v[0]][v[1]][v[2]][v[3]][next] << endl;
//								cout << q << ' ' << w << ' ' << e << ' ' << r << endl;
//								cout << v[0] << ' ' << v[1] << ' ' << v[2] << ' ' << v[3] << endl;
							}
						}
	memset(v, 0, sizeof v);
	for (int i = 0; i < sz(pw); i++)
		v[i] = 1;
	for (int i = L; i <= R; i++)
		res = (res + d[n][v[0]][v[1]][v[2]][v[3]][i]) % mod;
	printf("%d", res);			
	return 0;
}
