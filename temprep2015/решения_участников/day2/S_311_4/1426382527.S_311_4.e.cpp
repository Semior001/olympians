#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "E."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)2e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
pair < int, int > ans;
bool u[N];

void rec(int v = 2 * n - 1, int cnt = 0) {
    if (v == 1) {
    	ans.f = min(ans.f, cnt + 1);
    	return;
    }
	if (u[v]) {
		rec(v - 1, cnt);
		return;
	}	
	bool ok = 0;
	for (int i = v - 1; i > 1; i--)
		if (!u[i] && a[i] + a[v] <= a[1] + a[2 * n]) {
			u[i] = 1;
			rec(v - 1, cnt);
			u[i] = 0;
			ok = 1;
			break;
		}
	if (ok)
		return;			
	for (int i = v - 1; i > 1; i--)
		if (!u[i]) {
			u[i] = 1;
			rec(v - 1, cnt + (a[i] + a[v] > a[1] + a[2 * n]));
			u[i] = 0;
			break;
		}
}

void rec2(int v = 2 * n, int cnt = 0) {
    if (v == 2) {
    	ans.s = max(ans.s, cnt + 1);
    	return;
    }
	if (u[v]) {
		rec2(v - 1, cnt);
		return;
	}	
	bool ok = 0;
	for (int i = 3; i < v; i++)
		if (!u[i] && a[i] + a[v] > a[1] + a[2]) {
			u[i] = 1;
			rec2(v - 1, cnt + 1);
			u[i] = 0;
			ok = 1;
			break;
		}
	if (ok)
		return;
	for (int i = 3; i < v; i++)
		if (!u[i]) {
			u[i] = 1;
			rec2(v - 1, cnt + (a[i] + a[v] > a[1] + a[2]));
			u[i] = 0;
			break;
		}
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	sort(a + 2, a + 2 * n + 1);
	ans.f = 2 * n + 1;
	ans.s = 0;
	rec();
	rec2();
	printf("%d %d", ans.f, ans.s);
	return 0;
}