#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "D."
#define sz(a) (int)((a).size())

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;
const int mod = (int)1e9 + 7;

using namespace std;

int d[11][2][2][2][2][1001];
int sum[11][2][2][2][2][1001];

int n, L, R, a;
vector < pair < int, int > > pw;

void fact(int x) {
	int cnt = 0;
	for (int i = 2; i * i <= x; i++) {
		if (x % i != 0)
			continue;
		cnt = 0;
		while(x % i == 0) {
			x /= i;
			cnt++;
		}		
		pw.pb(mp(i, cnt));
	}
	if (x > 1)
		pw.pb(mp(x, 1));
}

int power(int a, int b) {
	int res = 0;
	while(a % b == 0) {
		a /= b;
		res++;
	}
	return res;
}

int v[4], p[4], now[4];
int res = 0;

bool check() {
	for (int i = 0; i < 4; i++)
		if (!v[i] && p[i] != now[i])
			return 0;
	return 1;
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d%d", &n, &L, &R, &a);		
	fact(a);
	d[0][0][0][0][0][L] = 1;
	for (int i = L; i <= R; i++)
		sum[0][0][0][0][0][i] = 1;
	for (int i = 1; i <= n; i++)
		for (int q = 0; q < 2; q++)
			for (int w = 0; w < 2; w++)
				for (int e = 0; e < 2; e++)
					for (int r = 0; r < 2; r++)
						for (int last = L; last <= R; last++) {	
							sum[i][q][w][e][r][last] = sum[i][q][w][e][r][last - 1];
						    now[0] = q, now[1] = w, now[2] = e, now[3] = r;
							memset(v, 0, sizeof v);
							for (int i = 0; i < sz(pw); i++)
								if (power(last, pw[i].f) >= pw[i].s)
									v[i] |= 1;
							if (v[0] > q || v[1] > w || v[2] > e || v[3] > r)
								continue;
							memset(p, 0, sizeof p);
							for (p[0] = 0; p[0] < 2; p[0]++)
								for (p[1] = 0; p[1] < 2; p[1]++)
									for (p[2] = 0; p[2] < 2; p[2]++)
										for (p[3] = 0; p[3] < 2; p[3]++) {
											if (!check())
												continue;												
											d[i][q][w][e][r][last] = (d[i][q][w][e][r][last] + sum[i - 1][p[0]][p[1]][p[2]][p[3]][last]) % mod;
										} 
							sum[i][q][w][e][r][last] = (sum[i][q][w][e][r][last] + d[i][q][w][e][r][last]) % mod;
						}
	memset(v, 0, sizeof v);
	for (int i = 0; i < sz(pw); i++)
		v[i] = 1;
	printf("%d", sum[n][v[0]][v[1]][v[2]][v[3]][R]);
	return 0;
}
