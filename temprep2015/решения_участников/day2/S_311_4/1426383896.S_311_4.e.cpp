#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "E."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)2e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
pair < int, int > ans;
multiset < int > q;
multiset < int > :: iterator f, s;

void rec(int cnt = 0) {
    if (q.empty()) {
    	ans.f = min(ans.f, cnt + 1);
    	return;
    }
	f = q.end(), f--;
	int x = *f;
	q.erase(f);
	s = q.upper_bound(a[1] + a[2 * n] - x);
	if (s != q.begin()) {
		s--;
		q.erase(s);
		rec(cnt);
		return;
	}
	s = q.end();
	s--;
	q.erase(s);
	rec(cnt + 1);
	return;
}

void rec2(int cnt = 0) {
    if (q.empty()) {
    	ans.s = max(ans.s, cnt + 1);
    	return;
    }
	f = q.end(), f--;
	int x = *f;
	q.erase(f);
	s = q.upper_bound(a[1] + a[2] - x);
	if (s != q.end()) {
		q.erase(s);
		rec2(cnt + 1);
		return;
	}
	q.erase(q.begin());
	rec2(cnt);
	return;
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	sort(a + 2, a + 2 * n + 1);
	ans.f = 2 * n + 1;
	ans.s = 0;
	for (int i = 2; i < 2 * n; i++)
		q.insert(a[i]);
	rec();
	q.clear();
	for (int i = 3; i <= 2 * n; i++)
		q.insert(a[i]);
	rec2();
	printf("%d %d", ans.f, ans.s);
	return 0;
}