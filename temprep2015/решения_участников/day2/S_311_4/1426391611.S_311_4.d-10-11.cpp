#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "D."
#define sz(a) (int)((a).size())

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e6 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;
const int mod = (int)1e9 + 7;

using namespace std;

vector < pair < int, int > > pw;

void fact(int x) {
	int cnt = 0;
	for (int i = 2; i * i <= x; i++) {
		if (x % i != 0)
			continue;
		cnt = 0;
		while(x % i == 0) {
			x /= i;
			cnt++;
		}		
		pw.pb(mp(i, cnt));
	}
	if (x > 1)
		pw.pb(mp(x, 1));
}

int power(int a, int b) {
	int res = 0;
	while(a % b == 0) {
		a /= b;
		res++;
	}
	return res;
}

bool bit(int mask, int i) {
	return (mask & (1 << i)) > 0;
}

bool check(int v, int p, int now) {
	for (int i = 0; i < 4; i++)
		if (!bit(v, i) && bit(p, i) != bit(now, i))
			return 0;
	return 1;
}

int n, L, R, a;
int sum[2][(1 << 7)][1002];
int v[1002];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d%d", &n, &L, &R, &a);		
	fact(a);
	for (int i = L; i <= R; i++)
		sum[0][0][i] = 1;
	for (int j = L; j <= R; j++)
		for (int i = 0; i < sz(pw); i++)
			if (power(j, pw[i].f) >= pw[i].s)
				v[j] |= (1 << i);
	for (int i = 1, id = 1; i <= n; i++, id ^= 1) {
		memset(sum[id], 0, sizeof sum[id]);
		for (int mask = 0; mask < (1 << sz(pw)); mask++)
			for (int last = L; last <= R; last++) {
				sum[id][mask][last] = sum[id][mask][last - 1];
				if ((mask & v[last]) != v[last])
					continue;
				for (int p = 0; p < (1 << sz(pw)); p++)
					if (check(v[last], p, mask))
						sum[id][mask][last] = (sum[id][mask][last] + sum[(id ^ 1)][p][last]) % mod;
			}
	}
	printf("%d", sum[n % 2][(1 << sz(pw)) - 1][R]);
	return 0;
}
