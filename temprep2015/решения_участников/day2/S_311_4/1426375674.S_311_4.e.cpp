#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "E."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)2e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
pair < int, int > ans;

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= 2 * n; i++)
		scanf("%d", &a[i]);
	sort(a + 2, a + 2 * n + 1);
	for (int l = 2, r = 2 * n - 1; l < r; l++, r--)
		if (a[l] + a[r] > a[1] + a[2 * n])
			ans.f++;
	for (int i = 3; i < 2 * n; i += 2)
		if (a[i] + a[i + 1] > a[1] + a[2])
			ans.s++;
	printf("%d %d", ans.f + 1, ans.s + 1);
	return 0;
}