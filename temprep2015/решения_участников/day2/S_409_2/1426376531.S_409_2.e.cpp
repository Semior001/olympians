#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define pr pair <int, int>
#define ELIBAY 1/0
using namespace std;
const int MAXN = 2e5 + 7, MAXM = 50;
vector <int> a;
int n;
int aman;
int mn = MAXN, mx = -1;
int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n += n;
    cin >> aman;
    for (int i = 0; i < n - 1 ; i++) {
        int x;
        scanf("%d", &x);
        a.pb(x);
    }
    sort (a.begin(), a.end());
    do {
        int F = 0;
        int mxx = aman + a[0];
        for (int i = 2; i < a.size(); i += 2) {
            if (a[i] + a[i - 1] > mxx) {
                F++;
            }
        }

        mn = min(mn, F);
        mx = max(mx, F);
    } while(next_permutation(a.begin(), a.end()));
    cout << mn + 1 << ' ' << mx + 1 << endl;
    return 0;
}
