#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define pr pair <int, int>
#define ELIBAY 1/0
using namespace std;
const int MAXN = 1e5 + 7, MAXM = 50;
int L, R, n, A;
int ans;
int main() {
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> n;
    cin >> L >> R  >> A;
    for (int i = L; i <= R; i++) {
        for (int j = i; j <= R; j++) {
            if ((i * j) / __gcd(i, j) == A) {
                ans++;
            }
        }
    }
    cout << ans;
    return 0;
}
