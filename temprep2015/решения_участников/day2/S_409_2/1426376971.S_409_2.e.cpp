#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define pr pair <int, int>
#define ELIBAY 1/0
using namespace std;
const int MAXN = 2e5 + 7, MAXM = 50;
int a[MAXN], n;
int mxx, mnn;
int ansmax, ansmax2, ansmin;
int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n += n;
    for (int i = 1; i <= n ; i++) {
        scanf("%d", &a[i]);
    }
    sort (a + 2, a + n + 1);
    mxx = a[1] + a[n];
    for (int i = 2,j = n - 1; i < j; i++, j--) {
        if (mxx < a[i] + a[j]) {
            ansmax++;
        }
    }
    for (int i = 3; i < n; i++) {
        if (mxx < a[i] + a[i - 1]) {
            ansmax2++;
        }
    }
    mnn = a[1] + a[2];
    int i = 3, j = n;
    while (i < j) {
        if (a[i] + a[j] > mnn) {
            ansmin++;
            i++;
            j--;
        } else {
            i++;
        }
    }
    cout << min(ansmax, ansmax2) + 1<< ' ' << ansmin + 1;
    return 0;
}
