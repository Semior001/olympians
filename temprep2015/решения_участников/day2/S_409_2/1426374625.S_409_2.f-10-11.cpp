#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define pr pair <int, int>
#define ELIBAY 1/0
using namespace std;
const int MAXN = 1e5 + 7, MAXM = 50;
int a[MAXN];
int n;
int main() {
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);
    cin >> n;
    for (int i = 1; i <= n; i++) {
        scanf("%d", &a[i]);
    }
    for (int i = 1; i <= n; i++) {
        int ans = 0;
        for (int j = 1; j <= n; j++) {
            if (!(a[i] & a[j])) {
                ans++;
            }
        }
        printf("%d ", ans);
    }
    return 0;
}
