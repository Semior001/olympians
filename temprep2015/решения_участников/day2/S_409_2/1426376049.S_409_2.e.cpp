#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define pr pair <int, int>
#define ELIBAY 1/0
using namespace std;
const int MAXN = 2e5 + 7, MAXM = 50;
int a[MAXN], n;
int mn = MAXN, mx = -1;
int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n += n;
    for (int i = 1; i <= n ; i++) {
        scanf("%d", &a[i]);
    }
    sort (a + 2, a+ 1 + n);
    do {
        int F = 0;
        int mxx = a[1] + a[2];
        for (int i = 4; i <= n; i += 2) {
            if (a[i] + a[i - 1] > mxx) {
                F++;
            }
        }
        mn = min(mn, F);
        mx = max(mx, F);
    } while(next_permutation(a + 2, a +  1 + n));
    cout << mx + 1 << ' ' << mn + 1 << endl;
    return 0;
}
