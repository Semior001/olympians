#include<iostream>
#include<set>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<stack>
#include<map>
#include<algorithm>
#include<vector>
using namespace std;
bool used[10000];
int n,a[10000],answer1=9999,answer2=0;
double ans;
vector<double >s;
void howmuchpairs(int kol)
{
    if(kol==n)
    {
        vector<double>q=s;
        int k=1;
        map<double,bool>u;
        sort(q.begin(),q.end());
        for(int i=q.size()-1;i>=0;i--)
        {

            if(q[i]==ans){if(k<answer1)answer1=k;break;}
            else if(!u[q[i]])
            {
                u[q[i]]=true;k++;
            }
        }
        return ;
    }
    for(int i=2;i<=n*2;i++)
    {
        for(int j=2;j<=n*2;j++)
        {
            if(i==j)continue;
            if(!used[i] && !used[j])
            {
                used[i]=true;used[j]=true;
                s.push_back((a[i]+a[j])/2 );
                howmuchpairs(kol+1);
                used[i]=false,used[j]=false;
                s.erase(s.begin()+s.size()-1);
            }
        }
    }
}
void findmax()
{

    int inde,h,h2;
    double ma=0.0;
    for(int i=2;i<=n*2;i++)
    {
        if((a[i]+a[1])/2>ma)ma=(a[i]+a[1])/2,inde=i;
    }
    ans=ma;
    used[inde]=true;
    s.push_back(ma);
    howmuchpairs(1);
}



void howmuchpairs1(int kol)
{
    if(kol==n)
    {
        vector<double>q=s;
        int k=1;
        map<double,bool>u;
        sort(q.begin(),q.end());
        for(int i=q.size()-1;i>=0;i--)
        {

            if(q[i]==ans){if(k>answer2)answer2=k;break;}
            else if(!u[q[i]])
            {
                u[q[i]]=true;k++;
            }
        }
        return ;
    }
    for(int i=2;i<=n*2;i++)
    {
        for(int j=2;j<=n*2;j++)
        {
            if(i==j)continue;

            if(!used[i] && !used[j])
            {
                used[i]=true;used[j]=true;
                s.push_back((a[i]+a[j])/2 );
                howmuchpairs1(kol+1);
                used[i]=false,used[j]=false;
                s.erase(s.begin()+s.size()-1);
            }
        }
    }
}
void findmin()
{
    int inde,h,h2;
    double ma=9999999;
    for(int i=2;i<=n*2;i++)
    {
        if((a[i]+a[1])/2<ma)ma=(a[i]+a[1])/2,inde=i;
    }
    ans=ma;
    used[inde]=true;
    s.push_back(ma);
    howmuchpairs1(1);
}
int main()
{
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    cin>>n;
    if(n==1){cout<<1<<" "<<1;return 0;}
    for(int i=1;i<=n*2;i++)cin>>a[i];
    memset(used,false,sizeof used);
    findmax();
    memset(used,false,sizeof used);
    s.clear();
    findmin();
    cout<<answer1<<" "<<answer2;
    return 0;
}
