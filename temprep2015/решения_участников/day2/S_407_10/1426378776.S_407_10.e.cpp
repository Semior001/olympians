#include<iostream>
#include<set>
#include<cstdio>
#include<cmath>
#include<cstring>
using namespace std;
bool used[10000];
int n,a[10000];
void findmax()
{
    set<double>s;
    int inde;
    double ma=0.0;
    for(int i=2;i<=n;i++)
    {
        if((a[i]+a[1])/2>ma)ma=(a[i]+a[1])/2,inde=i;
    }
    used[inde]=true;
    s.insert(ma);
    for(int i=2;i<=n;i++)
    {
        if(!used[i])
        {
            int mi=999999;
            for(int j=2;j<=n;j++)
            {
                if(j==i)continue;
                if(!used[j] && (a[i]+a[j])/2<mi)mi=(a[i]+a[j])/2,inde=j;
            }
            used[i]=true;used[inde]=true;
            s.insert(mi);
        }
    }
    int k=1;
    while(true)
    {
        set<double>::iterator h=s.begin();
        if(*h==ma){cout<<k<<" ";break;}
        else
            {
                s.erase(s.begin());
                k++;
            }
    }
}
void findmin()
{
    set<double>s;
    int inde;
    double ma=99999999;
    for(int i=2;i<=n;i++)
    {
        if((a[i]+a[1])/2<ma)ma=(a[i]+a[1])/2,inde=i;
    }
    used[inde]=true;
    s.insert(ma);
    for(int i=2;i<=n;i++)
    {
        if(!used[i])
        {
            int mi=0.000;
            for(int j=2;j<=n;j++)
            {
                if(j==i)continue;
                if(!used[j] && (a[i]+a[j])/2>mi)mi=(a[i]+a[j])/2,inde=j;
            }
            used[i]=true;used[inde]=true;
            s.insert(mi);
        }
    }
    int k=1;
    while(true)
    {
        set<double>::iterator h=s.begin();
        if(*h==ma){cout<<k;break;}
        else
            {
                s.erase(s.begin());
                k++;
            }
    }
}
int main()
{
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    cin>>n;
    for(int i=1;i<=n*2;i++)cin>>a[i];
    memset(used,false,sizeof used);
    findmax();
    memset(used,false,sizeof used);
    findmin();
    return 0;
}
