#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int n, ans;
int tree[100000+5], st[100000+5];

void in()
{
    ifstream cin ("F.in");
 cin >>n;
 for (int i=0; i<n; i++)
 {
     cin >>tree[i];
 }
 for (int i=0; i<n; i++)
 {
     cin >>st[i];
 }
}

void solution()
{
 for (int i=0; i<n-1; i++)
 {
     if (tree[i]>tree[i+1]) {ans+=st[i]; swap(tree[i], tree[i+1]);}
 }
}

void out()
{
    ofstream cout ("F.out");
    cout <<ans <<"\n";
}

int main()
{
in();
solution();
out();
return 0;
}
