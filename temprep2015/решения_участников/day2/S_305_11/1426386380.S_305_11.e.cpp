#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <vector>
#include <set>

using namespace std;

int n, f1=1, f2=1, maxi, mini, ma, mi, ans1, ans2, od=1, h=10;

int mass[200000+5];
pair < int, int > m1[2500+5], m2[2500+5];

pair < int, int > help;

void in()
{
    ifstream cin ("E.in");
    cin >>n;
    for (int i=0; i<2*n; i++)
    {
        cin >> mass[i];
        if (i>0)
        {
            if (mass[i]==mass[i-1]) {od++;}
        }
    }
}

void solution()
{
 for (int i=0; i<2*n-1; i++)
 {
   for (int g=i+1; g<2*n; g++)
   { if (i==0) {
       m1[f1].first=(mass[i]+mass[g])/2;
       m1[f1].second=(mass[i]+mass[g])%2;
       f1++;}
       else {
             m2[f2].first=(mass[i]+mass[g])/2;
             m2[f2].second=(mass[i]+mass[g])%2;
             f2++;
            }
   }
 }

 /*for(int i=1; i<=10; i++)
 {
     help=m2[i];
     m2[i]=m2[h]; m2[h]=help;
     h--;
 }*/

 maxi=m1[1].first;ma=1;
 mini=m1[1].first;mi=1;
 for (int i=2; i<=10; i++)
 {
     if (m1[i].first>maxi) {maxi=m1[i].first; ma=i;}
     if (m1[i].first<mini) {mini=m1[i].first; mi=i;}
 }
 //
 {
  if (m1[ma].first>m2[ma].first) {ans1=1;}
  if (m1[ma].first==m2[ma].first) {
                                    if ((m1[ma].second>=m2[ma].second)) {ans1=1;}
                                    else                               {ans1=2;}
                                  }
  if (m1[ma].first<m2[ma].first) {ans1=2;}

  if (m1[mi].first>m2[mi].first) {ans2=1;}
  if (m1[mi].first==m2[mi].first) {
                                    if ((m1[mi].second>=m2[mi].second)) {ans2=1;}
                                    else                               {ans2=2;}
                                  }
  if (m1[mi].first<m2[mi].first) {ans2=2;}
 }

}

void out()
{
    ofstream cout ("E.out");
    if (n==1) {cout <<"1 1\n";}
    else
    {
        if (od==2*n) {cout <<"1 1\n";}
        else        {cout <<ans1 <<" " <<ans2 <<"\n";}
    }

  /*  for (int i=1; i<=10; i++)
    {
        cout <<m1[i].first <<"  " <<m2[i].first <<"\n";
    }*/
}

int main()
{
in();
solution();
out();
return 0;
}
