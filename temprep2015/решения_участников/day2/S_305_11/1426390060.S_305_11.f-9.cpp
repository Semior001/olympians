#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int n, ans;
int tree[100000+5], st[100000+5];

void in()
{
    ifstream cin ("F.in");
 cin >>n;
 for (int i=0; i<n; i++)
 {
     cin >>tree[i];
 }
 for (int i=0; i<n; i++)
 {
     cin >>st[i];
 }
}

void solution()
{
 for (int i=0; i<n-1; i++)
 {
     for (int g=1; g<n; g++)
     {
       if (tree[i]>tree[g]) {ans+=st[i]; swap(tree[i], tree[g]); swap(st[i], st[g]); }
     }
 }
}

void out()
{
    ofstream cout ("F.out");
    cout <<ans <<"\n";
}

int main()
{
in();
solution();
out();
return 0;
}
