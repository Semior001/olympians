#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int a, c, L, R, ans;

void in()
{
    ifstream cin ("D.in");
    cin >>a >>c >>L >>R;
}

void solution()
{
  for (int i=L; i<=R; i++)
  {
      if (a%i==c) {ans++;}
  }
}

void out()
{
    ofstream cout ("D.out");
    cout <<ans <<"\n";
}

int main()
{
in();
solution();
out();
return 0;
}
