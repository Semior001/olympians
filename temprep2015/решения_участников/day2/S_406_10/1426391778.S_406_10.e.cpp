#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
struct node{
  int res,l,r;
}d[305000];
int n,x,a[305000];
bool cmp(node x,node y){
  return x.res<y.res;
}
bool used[305000];
int main(){             
  freopen("E.in","r",stdin);
  freopen("E.out","w",stdout);
  scanf("%d",&n);
  scanf("%d",&x);
  for(int i=2;i<=n+n;i++)scanf("%d",&a[i]);
  sort(a+2,a+n+n+1);
  int res=x+a[n+n],kol=0;
  for(int i=2;i<=n+n;i++){
    for(int j=2;j<=n+n;j++){
      d[++kol].res=a[i]+a[j];
      d[kol].l=i;
      d[kol].r=j;
    }
  }
  used[n+n]=1;
  sort(d+1,d+kol+1,&cmp);
  for(int i=kol;i>=1;i--){
    if(d[i].res>res)continue;
    if(!used[d[i].l]&&!used[d[i].r]){
       used[d[i].l]=1;
       used[d[i].r]=1;
    }
  }
  int ans=0;
  for(int i=1;i<=n+n;i++){
    if(used[i])ans++;
  }
  cout<<((n+n)-(ans-1))/2<<" ";
  memset(used,0,sizeof used);
  res=x+a[2];
  used[2]=1;
  for(int i=1;i<=kol;i++){
    if(d[i].res<res)continue;
    if(!used[d[i].l]&&!used[d[i].r]){
       used[d[i].l]=1;
       used[d[i].r]=1;
    }

  }
  ans=0;
  for(int i=1;i<=n+n;i++){
    if(used[i])ans++;
  }
  cout<<((n+n)-(ans-1))/2;
  return 0;
}
