#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
int n,l,r,x,ans=0;
int gcd(int x,int y){
  if(!y)return x;
  return gcd(y,x%y);
}
int main(){             
  freopen("D.in","r",stdin);
  freopen("D.out","w",stdout);
  scanf("%d%d%d%d",&n,&l,&r,&x);
  if(n==1){
    cout<<r/x-l/x;
    return 0;
  }
  for(int i=l;i<=r;i++){
     for(int j=i;j<=r;j++){
       if(((i*j)/gcd(i,j))%x==0)ans++;
     }
  }
  printf("%d",ans);

  return 0;
}
