#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
map<int,int> used,m;
int a[105000],n,res,len;
vector<int> p;
int kol(int l,int r){
  if(l>r)return 0;
  int *s=upper_bound(a+1,a+n+1,l-1);
  int *f=upper_bound(a+1,a+n+1,r);
  if(*s>r)return 0;
  return f-s;
}
void rec(int l,int r,int v){
  if(l>a[n]||l>r)return;
  if(v>=len){
   res+=kol(l,r);
   return;
  }
  int d=1;
  for(int i=l;i<=r;i+=p[v+1]){
    if(d)rec(i,min(i+p[v+1]-1,r),v+1);
    d^=1;
  }
}
int b[125000];
int main(){             
  freopen("F.in","r",stdin);
  freopen("F.out","w",stdout);
  scanf("%d",&n);
  for(int i=1;i<=n;i++){
    scanf("%d",&a[i]);
    b[i]=a[i];
    m[a[i]]++;
  }
  sort(a+1,a+n+1);
  for (int i=1;i<=n;i++){
    if(used[a[i]]){
      continue;
    }
    res=0;
    int kol=1;
    int x=a[i];
    while(x){
      if(x%2){p.push_back(kol);}
      kol*=2;
      x/=2;
    }
    reverse(p.begin(),p.end());
    len=p.size()-1;
    rec(0,a[n],-1);
    used[a[i]]=res;
    p.resize(0);
  }
  for(int i=1;i<=n;i++){
    printf("%d ",used[b[i]]);
  }
  return 0;
}
