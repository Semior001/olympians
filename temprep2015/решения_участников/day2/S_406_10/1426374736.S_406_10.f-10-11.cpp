#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
map<int,int> used;
int a[105000],n,res;
int main(){             
  freopen("F.in","r",stdin);
  freopen("F.out","w",stdout);
  scanf("%d",&n);
  for(int i=1;i<=n;i++){
    scanf("%d",&a[i]);
  }
  for (int i=1;i<=n;i++){
    if (used[a[i]]){
      printf("%d ",used[a[i]]);
      continue;
    }
    res=0;
    for (int j=1;j<=n;j++){
      if(j==i)continue;
      if ((a[i]&a[j])==0)res++;
    }
    printf("%d ",res);
    used[a[i]]=res;
  }
  return 0;
}
