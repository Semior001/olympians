#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
using namespace std;

int gcd(int a, int b) {
	while(b) {
		a %= b;
		swap(a, b);
	}
	return a;
}
 
int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("D.in", "r", stdin);
		freopen("D.out", "w", stdout);
	#endif

	int n, l, r, a, cnt = 0;

	cin >> n >> l >> r >> a;

	if(n == 1) {
		for(int i = a; i <= r; i += a)
			if(l <= i)
				cnt++;
	}
	else {
		for(int i = l; i <= r; i++)
			for(int j = i; j <= r; j++) {
				int g = i * j / gcd(i, j);
				if(g % a == 0)
					cnt++;
			}
	}
	

	cout << cnt;


	return 0;
}
