#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
 
int n, x, a[200005], res1, res2;

bool check(int cnt, int val, int l, int r) {
    int l1, l2, r1, r2;
	l1 = l, r1 = r - cnt * 2;
	l2 = r1 + 1, r2 = r;

	while(l2 < r2) {
		if(a[l2] + a[r2] <= val)
			return 0;
		l2++;
		r2--;
	}

	return 1;	
}

int bin_search(int val, int l2, int r2) {
	int l = 0, r = n + 1, m;
	while(r - l > 1) {
		int m = (r + l) / 2;
		if(check(m, val, l2, r2))
			l = m;
		else
			r = m;
	}
	return l + 1;
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("E.in", "r", stdin);
		freopen("E.out", "w", stdout);
	#endif

	int m;

	cin >> n >> x;

	m = 2 * n - 1;

	for(int i = 0; i < m; i++)
		cin >> a[i];

	sort(a, a + m);

	res1 = bin_search(x + a[m - 1], 0, m - 2);
	res2 = bin_search(x + a[0], 1, m - 1);

	cout << res1 << " " << res2;

	return 0;
}
