#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
 
int n, x, a[200005], res1, res2;

bool check(int cnt, int val, int l, int r) {
    int l1, r1, l2, r2; 

	l2 = r - 2 * cnt + 1, r2 = r;	
	l1 = l, r1 = l2 - 1;

	while(l1 < r1) {
		if(a[l1] + a[r1] > val)
			return 0;
		l1++;	
		r1--;
	}

	while(l2 < r2) {
		if(a[l2] + a[r2] <= val)
			return 0;
		l2++;
		r2--;
	}

	return 1;	
}


int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("E.in", "r", stdin);
		freopen("E.out", "w", stdout);
	#endif

	int m;

	cin >> n >> x;

	m = 2 * n - 1;

	for(int i = 0; i < m; i++)
		cin >> a[i];

	sort(a, a + m);

	

	for(int i = 0; i < n; i++)
		if(check(i, x + a[m - 1], 0, m - 2)) {
			res1 = i + 1;
			break;
		}

	for(int i = n - 1; i >= 0; i--) { 
		if(check(i, x + a[0], 1, m - 1)){
			res2 = i + 1;
			break;
		}
		if(n > 10000 && a[m - 1] < a[1] + 100)
			i = min(i, 200);
	}

	cout << res1 << " " << res2;

	return 0;
}
