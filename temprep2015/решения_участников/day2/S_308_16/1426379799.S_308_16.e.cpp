#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
 
int n, x, a[200005], b[200005], m, mx, mn, l, r, sz, res1 = 1, res2 = 1;

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("E.in", "r", stdin);
		freopen("E.out", "w", stdout);
	#endif

	cin >> n >> x;

	m = 2 * n - 1;

	for(int i = 0; i < m; i++)
		cin >> a[i];

	sort(a, a + m);

	mx = x + a[m - 1];
	l = 0;
	r = m - 2;

	while(l < r) { 
		if(a[l] + a[r] > mx) {
			b[sz++] = a[r] + a[r - 1];
			r -= 2;
		}
		else {
			b[sz++] = a[l] + a[r];
			l++;
			r--;
		}

	}

	sort(b, b + sz);

	for(int i = sz - 1; i >= 0; i--)
		if(mx < b[i])
			res1++;
		else
			break;

	mn = x + a[0];
	l = 1;
	r = m - 1;
	sz = 0;

	while(l < r) {
		if(a[l] + a[r] < mn) {
			b[sz++] = a[l] + a[l + 1];
			l += 2;
		}
		else {
			b[sz++] = a[l] + a[r];
			l++;
			r--;
		}
	}

	sort(b, b + sz);
	
	for(int i = sz - 1; i >= 0; i--)
		if(mn < b[i])
			res2++;
		else
			break;


	cout << res1 << " " << res2;

	return 0;
}
