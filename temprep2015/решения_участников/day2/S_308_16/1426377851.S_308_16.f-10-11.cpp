#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <bitset>
using namespace std;
 
int n, a[100005], mx, m, cnt;
bool u;
bitset<100005> mask[10], cur;

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("F.in", "r", stdin);
		freopen("F.out", "w", stdout);
	#endif

	scanf("%d", &n);

	for(int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
		mx = max(mx, a[i]);
	}

	while(mx) {
		mx /= 2;
		m++;
	}

	for(int i = 0; i < n; i++) {
		int k = 0;
		for(int j = 0; j < m; j++) {
			mask[j][i] = a[i] % 2;
			a[i] /= 2;
		}
	}

	for(int i = 0; i < n; i++) {
		u = 0;
		for(int j = 0; j < m; j++)
			if(mask[j][i]) {
				if(!u) {
					u = 1;
					cur = mask[j];
				}
				else
					cur |= mask[j];
			}
		cnt = n;
		if(u)
			cnt -= cur.count();

		printf("%d ", cnt);
	}

	return 0;
}
