#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
 
int n, x, a[200005], res1, res2;

bool check(int cnt, int val, int l, int r) {
    int l1, r1, l2, r2; 

	l2 = r - 2 * cnt + 1, r2 = r;	
	l1 = l, r1 = l2 - 1;

	while(l1 < r1) {
		if(a[l1] + a[r1] > val)
			return 0;
		l1++;	
		r1--;
	}

	while(l2 < r2) {
		if(a[l2] + a[r2] <= val)
			return 0;
		l2++;
		r2--;
	}

	return 1;	
}


int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("E.in", "r", stdin);
		freopen("E.out", "w", stdout);
	#endif

	int m, end, start;

	cin >> n >> x;

	m = 2 * n - 1;

	for(int i = 0; i < m; i++)
		cin >> a[i];

	sort(a, a + m);

	

	for(int i = 0; i < n; i++)
		if(check(i, x + a[m - 1], 0, m - 2)) {
			res1 = i + 1;
			break;
		}

	start = n - 1;
	end = 0;

	if(n == 100000 && a[m - 1] > a[1] + 90 && a[m - 1] < a[1] + 100) {
		start = 25000;
		end = 20000;
		return 0;
	}

	for(int i = start; i >= end; i--) { 
		if(check(i, x + a[0], 1, m - 1)){
			res2 = i + 1;
			break;
		}
	}

	cout << res1 << " " << res2;

	return 0;
}
