#include <iostream>
#include <fstream>

using namespace std;

int main() {
    int a, c, l, r, d = 0;
    ifstream in("D.in");
    ofstream out("D.out");

    in >> a >> c >> l >> r;

    for(int i = l; i <= r; ++i)
        if(a % i == c) d++;

    out << d;

    return 0;
}
