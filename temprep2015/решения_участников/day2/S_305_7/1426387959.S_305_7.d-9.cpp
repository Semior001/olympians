#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {
    int a, c, l, r, d = 0;
    ifstream in("D.in");
    ofstream out("D.out");

    in >> a >> c >> l >> r;

    if(l >= a) { out << 0; return 0; }

    int t = a < r ? a : r;
    for(int i = l; i <= t; ++i)
            if(a % i == c) d++;

    out << d;

    return 0;
}

