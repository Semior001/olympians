#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<cstring>
#include<cmath>
#include<vector>

using namespace std;
int nod(int a,int b)
{
    while(b)
    {
        a%=b;
        swap(a,b);
    }
    return a;
}
int nok(int a,int b)
{
    return a/nod(a,b)*b;
}
int main()
{
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);

    int n,l,r,a;
    long long int count=0;

    cin>>n>>l>>r>>a;
    if(n==1)
    {
        for(int i=l;i<=r;i++)
        {
            if(i%a==0){count++;}
        }
    }
    if(n==2)
    {
        for(int i=l;i<=r;i++)
        {
            for(int j=i;j<=r;j++)
            {
                int c=a;
                if(nok(i,j)%a==0){count++;}
            }
        }
    }
    if(n==3)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1);break;}
                    if(nok(nok(i,j),k)%a==0){count++;}
                    count=count%1000000007;
                }
            }
        }
    }
    if(n==4)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1);break;}
                        if(nok(nok(i,j),nok(k,x))%a==0){count++;}
                        count=count%1000000007;
                    }
                }
            }
        }
    }
    if(n==5)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1);break;}
                            if(nok(nok(nok(i,j),k),nok(x,y))%a==0){count++;}
                            count=count%1000000007;
                        }
                    }
                }
            }
        }
    }
    if(n==6)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1)*(r-y+1);break;
                            count=count%1000000007;}
                            for(int z=y;z<=r;z++)
                            {
                                c=c/nod(c,z);
                                if(c==1){count+=(r-z+1);break;}
                                if(nok(nok(nok(i,j),nok(k,x)),nok(y,z))%a==0){count++;}
                                count=count%1000000007;
                            }
                        }
                    }
                }
            }
        }
    }
    if(n==7)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1)*(r-y+1)*(r-y+1);break;
                            count=count%1000000007;}
                            for(int z=y;z<=r;z++)
                            {
                                c=c/nod(c,z);
                                if(c==1){count+=(r-z+1)*(r-z+1);break;
                                count=count%1000000007;}
                                for(int o=z;o<=r;o++)
                                {
                                    c=c/nod(c,o);
                                    if(c==1){count+=(r-o+1);break;}
                                    if(nok(nok(nok(i,j),nok(k,x)),nok(nok(y,z),o))%a==0){count++;}
                                    count=count%1000000007;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(n==8)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1);break;
                            count=count%1000000007;}
                            for(int z=y;z<=r;z++)
                            {
                                c=c/nod(c,z);
                                if(c==1){count+=(r-z+1)*(r-z+1)*(r-z+1);break;
                                count=count%1000000007;}
                                for(int o=z;o<=r;o++)
                                {
                                    c=c/nod(c,o);
                                    if(c==1){count+=(r-o+1)*(r-o+1);break;
                                    count=count%1000000007;}
                                    for(int p=o;p<=r;p++)
                                    {
                                        c=c/nod(c,p);
                                        if(c==1){count+=(r-p+1)*(r-p+1)*(r-p+1);break;}
                                        if( nok(nok(nok(i,j),nok(k,x)),nok(nok(y,z),nok(o,p)))%a==0 ){count++;}
                                        count=count%1000000007;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(n==9)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1);break;
                            count=count%(1000000000+7);}
                            for(int z=y;z<=r;z++)
                            {
                                c=c/nod(c,z);
                                if(c==1){count+=(r-z+1)*(r-z+1)*(r-z+1)*(r-z+1);break;
                                count=count%1000000007;}
                                for(int o=z;o<=r;o++)
                                {
                                    c=c/nod(c,o);
                                    if(c==1){count+=(r-o+1)*(r-o+1)*(r-o+1);break;
                                    count=count%1000000007;}
                                    for(int p=o;p<=r;p++)
                                    {
                                        c=c/nod(c,p);
                                        if(c==1){count+=(r-p+1)*(r-p+1);break;
                                        count=count%1000000007;}
                                        for(int q=p;q<=r;q++)
                                        {
                                            c=c/nod(c,q);
                                            if(c==1){count+=(r-q+1);break;}
                                            if( nok(nok(nok(nok(i,j),k),nok(x,y)),nok(nok(z,o),nok(p,q)) ) % a == 0 ){count++;}
                                            count=count%1000000007;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(n==10)
    {
        for(int i=l;i<=r;i++)
        {
            int c=a;
            c=c/nod(c,i);
            if(c==1){count+=(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1)*(r-i+1);break;
            count=count%1000000007;}
            for(int j=i;j<=r;j++)
            {
                c=c/nod(c,j);
                if(c==1){count+=(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1)*(r-j+1);break;
                count=count%1000000007;}
                for(int k=j;k<=r;k++)
                {
                    c=c/nod(c,k);
                    if(c==1){count+=(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1)*(r-k+1);break;
                    count=count%1000000007;}
                    for(int x=k;x<=r;x++)
                    {
                        c=c/nod(c,x);
                        if(c==1){count+=(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1)*(r-x+1);break;
                        count=count%1000000007;}
                        for(int y=x;y<=r;y++)
                        {
                            c=c/nod(c,y);
                            if(c==1){count+=(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1)*(r-y+1);break;
                            count=count%1000000007;}
                            for(int z=y;z<=r;z++)
                            {
                                c=c/nod(c,z);
                                if(c==1){count+=(r-z+1)*(r-z+1)*(r-z+1)*(r-z+1)*(r-z+1);break;
                                count=count%1000000007;}
                                for(int o=z;o<=r;o++)
                                {
                                    c=c/nod(c,o);
                                    if(c==1){count+=(r-o+1)*(r-o+1)*(r-o+1)*(r-o+1);break;
                                    count=count%1000000007;}
                                    for(int p=o;p<=r;p++)
                                    {
                                        c=c/nod(c,p);
                                        if(c==1){count+=(r-p+1)*(r-p+1)*(r-p+1);break;
                                        count=count%1000000007;}
                                        for(int q=p;q<=r;q++)
                                        {
                                            c=c/nod(c,q);
                                            if(c==1){count+=(r-q+1)*(r-q+1);break;
                                            count=count%1000000007;}
                                            for(int s=q;s<=r;s++)
                                            {
                                                c=c/nod(c,s);
                                                if(c==1){count+=(r-s+1);break;}
                                                if( nok(nok(nok(i,j),nok(k,x)),nok(nok(nok(y,z),nok(o,p)),nok(q,s))) % a == 0){cout<<i<<" "<<j<<" "<<k<<" "<<x<<" "<<y<<" "<<z<<" "<<o<<" "<<p<<" "<<q<<" "<<s<<endl;count++;}
                                                count=count%1000000007;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    cout<<count%1000000007;
    return 0;
}
