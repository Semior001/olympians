#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<cstring>
#include<cmath>
#include<vector>

using namespace std;

int main()
{
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);

    int n,l,r,a,count=0;

    cin>>n>>l>>r>>a;
    if(n==1)
    {
        for(int i=l;i<=r;i++)
        {
            if(i%a==0){count++;}
        }
    }
    if(n==2)
    {
        for(int i=l;i<=r;i++)
        {
            for(int j=i;j<=r;j++)
            {
                if((i*j)%a==0){count++;}
            }
        }
    }
    if(n==3)
    {
        for(int i=l;i<=r;i++)
        {
            for(int j=i;j<=r;j++)
            {
                for(int k=j;k<=r;k++)
                {
                    if((i*j*k)%a==0){count++;}
                }
            }
        }
    }
    if(n==4)
    {
        for(int i=l;i<=r;i++)
        {
            for(int j=i;j<=r;j++)
            {
                for(int k=j;k<=r;k++)
                {
                    for(int x=k;x<=r;x++)
                    {
                        if((i*j*k*x)%a==0){count++;}
                    }
                }
            }
        }
    }
    if(n==5)
    {
        for(int i=l;i<=r;i++)
        {
            for(int j=i;j<=r;j++)
            {
                for(int k=j;k<=r;k++)
                {
                    for(int x=k;x<=r;x++)
                    {
                        for(int y=x;y<=r;y++)
                        {
                            if((i*j*k*x*y)%a==0){count++;}
                        }
                    }
                }
            }
        }
    }

    cout<<count;
    return 0;
}
