#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;
#define fnm "E"
int n, a[100001], k;
double s[100001], A;
int main() {
	freopen(fnm".in", "r", stdin);
	freopen(fnm".out", "w", stdout);  
	cin>>n;
	n *= 2;
	for (int i = 1;i <= n;i++) {
		cin>>a[i];
		if (i != 1) k++, s[k] = a[i];
	}
	A = a[1];
	sort(s+1, s+k+1);
	double t = (s[5]+A)/2;
	if (t >= (s[3]+s[4])/2)
	cout<<1;
	else {
		if (t >= (s[1]+s[2])/2)
		cout<<2;
		else
		cout<<1;
	} 
	cout<<" ";
	t = (s[1]+A)/2;
	if (t < (s[2]+s[3])/2)
	cout<<3;
	else {
		if (t < (s[4]+s[5])/2)
		cout<<2;
		else
		cout<<1;
	}
	return 0;
}