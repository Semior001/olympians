#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "D"
#define pb push_back
#define mp make_pair          
#define F first
#define S second
const int mod = 1e9+7;
int n, l, r, A, k;
int lce(int a, int b) {
return (a/__gcd(a, b))*b;
}
int main() {
freopen(fnm".in", "r", stdin);
freopen(fnm".out", "w", stdout); 
cin>>n>>l>>r>>A;         
if (n == 1) {
for (int i = l;i <= r;i++)
if (i%A == 0)
k++;
cout<<k;
return 0;
}
for (int i = l;i <= r;i++)
for (int j = i;j <= r;j++)
if (lce(i, j)%A == 0) k++;
cout<<k;
return 0;
}
