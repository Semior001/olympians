#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "E"
#define pb push_back
#define mp make_pair          
#define F first
#define S second
int n, k, s[100001], a[100001];
int main() {
	freopen(fnm".in", "r", stdin);
	freopen(fnm".out", "w", stdout);
	cin>>n;
	for (int i = 1;i <= 2*n;i++) {
		cin>>a[i];
		if (i != 1) {
			k++;
			s[k] = a[i];
		}
	}  
    if (n==1) {
    	cout<<1<<" "<<1;
    	return 0;
    }
    sort(s+1, s+1+k);
    if (n==2) {
    	int A = a[1];
    	if (A+s[3] >= s[1]+s[2])
    	cout<<1;
    	else
    	cout<<2;
    	cout<<" ";
    	if (A+s[1] < s[2]+s[3])
    	cout<<2;
    	else
    	cout<<1;
    	return 0;
    }
    int A = a[1];
    if (A+s[5] >= s[1] + s[2]) {
    	if (A+s[5] >= s[3]+s[4])
    	cout<<1;
    	else cout<<2;
    }
    else cout<<3;
    cout<<" ";
    if (A+s[1] < s[4]+s[5]) {
    	if (A+s[1] < s[2]+s[3])
    	cout<<3;
    	else
    	cout<<2;
    }
    else cout<<1;
	return 0;
}
