#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "E"
#define pb push_back
#define mp make_pair          
#define F first
#define S second
int n, k, s[100001], a[100001];
int main() {
	freopen(fnm".in", "r", stdin);
	freopen(fnm".out", "w", stdout);
	cin>>n;
	for (int i = 1;i <= 2*n;i++) {
		cin>>a[i];
		if (i != 1) {
			k++;
			s[k] = a[i];
		}
	}  
    if (n==1) {
    	cout<<1<<" "<<1;
    	return 0;
    }
    sort(s+1, s+1+k);
    if (n==2) {
    	double A = a[1];
    	double x, y, z;
    	z = s[3];
    	double max_t = (z+A)/2;
    	x = s[1], y = s[2];
    	double t1 = (x+y)/2;
    	if (max_t >= t1)
    	cout<<1;
    	else cout<<2;
    	cout<<" ";
    	z = s[1];
    	max_t = (A+s[1])/2;
    	x = s[2], y = s[3];
    	t1 = (x+y)/2;
    	if (max_t < t1)
    	cout<<2;
    	else cout<<1;         
       	return 0;
    }
    int A = a[1];    
	double min_t = A, y = s[1];
	min_t = (min_t+y)/2;
	double max_t = A;
	y = s[5];
	max_t = (max_t+y)/2;    
	double t1, t2;
	double x = s[1], z = s[2];
	t1 = (x+z)/2;
	if (max_t >= t1) {
		x = s[3], y = s[4];
		t2 = (x+y)/2;
		if (max_t >= t2) 
		 cout<<1;
		else cout<<2;
	}
	else cout<<3;
	cout<<" ";
	x = s[4], y = s[5];
	t1 = (x+y)/2;
	if (min_t < t1) {
		x = s[2], y = s[3];
		t2 = (s[2]+s[3])/2;
		if (min_t < t2)
		 cout<<3;
		 else cout<<2;
	}      
	else cout<<1;
	return 0;
}
