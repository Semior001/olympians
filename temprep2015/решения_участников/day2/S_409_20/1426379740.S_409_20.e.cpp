#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "E"
#define pb push_back
#define mp make_pair          
#define F first
#define S second
int n, a[100000], k, p = 1, A, maxx = -10000, pmax, ppmax;
int used[100];
int main() {
freopen(fnm".in", "r", stdin);
freopen(fnm".out", "w", stdout);  
cin>>n;
n *= 2;
for (int i = 1;i <= n;i++)
{
cin>>a[i];
if (i==1) k = a[i];
else 
{
if (a[i] == k)
p++;
}
}          
if (n == 2 || p == n) 
{
cout<<1<<" "<<1;
return 0;
}    
if (n == 4) 
{
A = a[1];
int minn = 1000000;
for (int i = 2;i <= n;i++)
pmax = maxx, maxx = max(maxx, a[i]), minn = min(minn, a[i]);
if (pmax + A > maxx+minn || A + maxx > pmax + minn)
cout<<1;
else
cout<<2;
cout<<" ";
if (A+minn < pmax+maxx)
cout<<2;
else
cout<<1;
return 0;
}        
A = a[1];
for (int i = 2;i <= n;i++)
pmax = maxx, maxx = max(maxx, a[i]);
int ppos=0, pos=0;
for (int i = 1;i <= n;i++) 
{                           
if (a[i] == pmax && !ppos)
used[i] = 1, ppos = 1;
if (a[i] == maxx && !pos)
used[i] = 1, pos = 1;
}
int m = -100000, q = 100000;
for (int i = 2;i <= n;i++)
{
if (!used[i])
m = max(m, a[i]);
if (!used[i])
q = min(q, a[i]);
}
pos = 0, ppos = 0;
for (int i = 2;i <= n;i++)
{
if (a[i] == m && !pos)
used[i] = 1, pos = 1;
if (a[i] == q && !ppos)
used[i] = 1, ppos = 1;
}
int one = 0, two = 0;
for (int i = 2;i <= n;i++)
{
if (!used[i] && !one)
used[i] = 1, one = a[i];
if (!used[i] && !two)
used[i] = 1, two = a[i];
} 
int sum1 = 0, sum2 = 0, sum3 = 0;
sum1 = A+maxx;
sum2 = pmax+m;
sum3 = one+two;
if (sum1 > sum2)
cout<<1;
else
{              
if (sum1 > sum3)
cout<<2;
else
cout<<3;
}
cout<<" ";
sum1 = A + q;
sum2 = maxx+pmax;
if (sum1 < sum2)
{
if (sum1 < sum3)
cout<<3;
else
cout<<2;
}
else 
cout<<1;
}
