#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;
#define fnm "E"
int n, a[100001], k, flag, flag2, used[10000], used2[10000];
double s[100001], A, x, y ;
int main() {
	freopen(fnm".in", "r", stdin);
	freopen(fnm".out", "w", stdout);
	cin>>n;
	n *= 2;
	for (int i = 1;i <= n;i++) {
		cin>>a[i];
		if (i != 1) k++, s[k] = a[i];
	}

	A = a[1];
	sort(s+1, s+k+1);

	if (n == 2) {
		cout<<1<<" "<<1;
		return 0;
	}
	if (n == 4) {
		int maxx = -10000, pmax = 0, minn = 100000;
		for (int i = 2;i <= n;i++)
		pmax = maxx, maxx = max(maxx, a[i]), minn = min(minn, a[i]);
		x = maxx;
		y = pmax;
		double z = minn;
		if ((x+A)/2 >= (y+z)/2) cout<<1;
		else cout<<2;
		cout<<" ";
		if ((A+z)/2 >= (y+x)/2) cout<<1;
		else cout<<2;
		return 0;
	}
	x = s[3];
	y = (A+x)/2;
	if (y >= (s[5]+s[2])/2) cout<<1;
	else if (y >= (s[1]+s[4])/2) cout<<2;
	else cout<<3;
	cout<<" ";
	y = s[1];
	x = (A+y)/2;
	if (x < (s[5]+s[2])/2) {
		if (x<(s[1]+s[4])/2)
		 cout<<3;
		 else
		 cout<<2;
	}
	else cout<<1;

	return 0;
}
