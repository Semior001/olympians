#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
#define mp make_pair
#define F first
#define S second
using namespace std;

const int N = 2 * 1e5 + 5;

int n, mx, mn, k;

pair < int, int > a[N];

int main ()
{
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
    scanf ("%d", &n);
    mn = n + n;
    for (int i = 1;i <= n + n;i ++)	
    {
    	scanf ("%d", &a[i].F);
    	a[i].S = i;
    }
    int tmp = a[1].F;
    sort (a + 1, a + n + n + 1);
    for (int i = 1;i <= n + n;i ++)
    	if (a[i].F == tmp)
    		k = i;
    if (k == n + n)
    	mx = n;
    else
    {
    	if (k - 1 >= n + n - k - 1)
    		mx = n;
    	else
    	{
    		mx = n;	
    		int sum = a[k].F + a[n + n].F;
    		int tmp = n + n - k - k;
    		int l = k + 1, r = k + tmp;
    		while (l < r)
    		{
    			int cur = a[l].F + a[r].F;
    			if (cur > sum)
    				mx --;
    			l ++;
    			r --;
    		}
    	}
    }
   for (int i = n + n;i >= 1;i --)
    	if (a[i].F == tmp)
    		k = i;
   if (k == 1)
    	mn = 1;
    else
    {
    	if (n + n - k >= k - 2)
    		mn = 1;
    	else
    	{
    		mn = 1;
    		int sum = a[k].F + a[1].F;
    		int tmp = k - 2 - (n + n - k);
    		int l = 1 + tmp, r = k - 1; 
    		while (l < r)
    		{
    			int cur = a[l].F + a[r].F;
    			if (cur <= sum)
    				mn ++;
    			l ++;
    			r --;
    		}
    	}
    }
    cout << n - mx + 1 << " " << n - mn + 1;
	return 0;	
}