#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <map>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define mp make_pair
#define F first
#define S second
#define ll long long
#define pb push_back 

using namespace std;

const int N = 1e5 + 5;

int n, h[N], a[N];

pair < int, int > c[N];

map < int, int > p, cur_p;

ll res;

int main ()
{
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1;i <= n;i ++)
	{
		scanf ("%d", &h[i]);
		a[i] = h[i];
	}               
	for (int i = 1;i <= n;i ++)
	{
		scanf ("%d", &c[i].F);
		c[i].S = i;
	}
	sort (c + 1, c + n + 1);
	sort (h + 1, h + n + 1);
	for (int i = 1;i <= n;i ++)
		p[h[i]] = i;
	for (int i = 1;i <= n;i ++)
	{
		int x = c[i].S, tmp;
		for (int j = 1;j <= n;j ++)
			if (a[j] == x)
			{
				tmp = j;
				break;
			}
	   	if (tmp == p[x])
	   		continue;
	   	res += c[i].F;
	  	if (tmp < p[x])
	  		for (int j = tmp;j < p[x];j ++)
	  			a[j] = a[j + 1];
	  	else	
	  		for (int j = tmp;j > p[x];j --)
	  			a[j] = a[j - 1];
	  	a[p[x]] = x;
	}
	printf ("%lld", res);
	return 0;	
}
