#include <iostream>
#include <set>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#define ll long long
#define mp make_pair
#define pb push_back 

using namespace std;

const int N = 2 * 1e5 + 5;

int n, a[N], tp[N], sum_mx, sum_mn;

vector < int > mx, mn;

bool used[N];

int cnt[N];

int calc_mx ()
{           
	memset (cnt, 0, sizeof cnt);
	sort (mx.begin (), mx.end ());
	int ret = 1;
	set < int > st;
	set < int > :: iterator it;
	for (int i = 0;i < mx.size ();i ++)
	{
		st.insert (mx[i]);
		cnt[mx[i]] ++;
	}
	for (int i = 0;i < mx.size ();i ++)
	{	
		if (cnt[mx[i]] == 0)
			continue;
		cnt[mx[i]] --;
		st.erase (mx[i]);
		if (cnt[mx[i]])
			st.insert (mx[i]);
		int tmp = sum_mx - mx[i];
		it = st.upper_bound (tmp);
		if (it != st.begin ())
			{
				it --;
				st.erase (it);
				cnt[*it] --;
				if (cnt[*it])
					st.insert (*it);
		}
		else
		{
			ret ++;
			it = --st.end ();
			st.erase (it);
			cnt[*it] --;
			if (cnt[*it])
				st.insert (*it);
		}
	}
	return ret;	
}

int calc_mn ()
{
	memset (cnt, 0, sizeof cnt);
	sort (mn.begin (), mn.end ());
	int ret = 1;
	set < int > st;
	set < int > :: iterator it;
	for (int i = 0;i < mn.size ();i ++)
	{
		st.insert (mn[i]);
		cnt[mn[i]] ++;
	}
	for (int i = 0;i < mn.size ();i ++)
	{
		if (cnt[mn[i]] == 0)
			continue;
		st.erase (mn[i]);
		cnt[mn[i]] --;
		if (cnt[mn[i]])
			st.insert (mn[i]);
		int tmp = sum_mn - mn[i];
		it = st.upper_bound (tmp);
		if (it != st.end ())
		{
			st.erase (it);
			cnt[*it] --;
			ret ++;
			if (cnt[*it])
				st.insert (*it);
		}
		else
		{
			it = st.begin ();
			st.erase (it);
			cnt[*it] --;
			if (cnt[*it])
				st.insert (*it);
		}
	}
	return ret;	
}

int main ()
{
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	cin >> n;
	for (int i = 1;i <= n + n;i ++)
		cin >> a[i];
	int p, q = 0;
	for (int i = 2;i <= n + n;i ++)
		if (a[i] > q)
		{
			q = a[i];
			p = i;
		}
	sum_mx = a[1] + a[p];
	for (int i = 2;i <= n + n;i ++)
		if (i != p)
			mx.pb (a[i]);
	q = 1e9, p;
	for (int i = 2;i <= n + n;i ++)
		if (a[i] < q)
		{
			q = a[i];
			p = i;
		}
	sum_mn = a[1] + a[p];
	for (int i = 2;i <= n + n;i ++)	
		if (p != i)
			mn.pb (a[i]);
	cout << calc_mx () << " ";
	cout << calc_mn ();
	return 0;	
}