#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
using namespace std;

int l, r, res, a, c;

int main ()
{
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf ("%d%d%d%d", &a, &c, &l, &r);
	for (int i = l;i <= r;i ++)
		if (a % i == c)
			res ++;		
	printf ("%d", res);
	return 0;	
}