#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <map>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define mp make_pair
#define F first
#define S second
#define ll long long
#define pb push_back 

using namespace std;

const int N = 1e5 + 5;

int n, h[N], a[N];
            
map < int, int > p, c;

ll res;

int main ()
{
	freopen ("F.in", "r", stdin);
	freopen ("F.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1;i <= n;i ++)
	{
		scanf ("%d", &h[i]);
		a[i] = h[i];
	}
	for (int i = 1;i <= n;i ++)
	{
		int x;
		scanf ("%d", &x);
		c[h[i]] = x;
	}
	sort (h + 1, h + n + 1);
	for (int i = 1;i <= n;i ++)
		p[h[i]] = i;
	for (int it = 1;it <= n;it ++)
		for (int i = 1;i <= n;i ++)
        	if (p[a[i]] != i)
        	{
        		res += c[a[i]];
        		int tmp = a[i];
        		if (i < p[a[i]])
        			for (int j = i;j < p[a[i]];j ++)
        				a[j] = a[j + 1];
        		else
        			for (int j = i;j > p[a[i]];j --)
						a[j] = a[j - 1];        				
        		a[p[tmp]] = tmp;
        	}
	printf ("%lld", res);
	return 0;	
}
