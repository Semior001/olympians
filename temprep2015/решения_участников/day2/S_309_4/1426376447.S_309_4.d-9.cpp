#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define mp make_pair
#define pb push_back 
using namespace std;

int l, r, res, a, c;

map < int, bool > used;

vector < int > pr;

int main ()
{
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf ("%d%d%d%d", &a, &c, &l, &r);
	a -= c;
	for (int i = 1;1ll * i * i <= a;i ++)
		if (a % i == 0)
		{
			int k = a / i;
			int kk = i;
			if (!used[k])
			{
				pr.pb (k);
				used[k] = 1;
			}
			if (!used[kk])
			{
				pr.pb (kk);
				used[kk] = 1;
			}
		}
	int l1, r1;
	l1 = r1 = -1;
	sort (pr.begin (), pr.end ());
	if (pr.back () <= c)
	{
		cout << 0;
		return 0;
	}
	for (int i = 0;i < pr.size ();i ++)
		if (pr[i] > c && pr[i] <= r)
			r1 = i;
	for (int i = pr.size () - 1;i >= 0;i --)
		if (pr[i] >= l && pr[i] > c)
			l1 = i;	
	res = r1 - l1 + 1;
	cout << res;
	return 0;	
}