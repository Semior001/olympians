#include <iostream>
#include <set>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#define ll long long
#define mp make_pair
#define pb push_back 

using namespace std;

const int N = 2 * 1e5 + 5;

int n, a[N], tp[N], sum_mx, sum_mn;

vector < int > mx, mn;

bool used[N];

int calc_mx ()
{           
	sort (mx.begin (), mx.end ());
	int ret = 1;
	set < pair < int, int > > st;
	set < pair < int, int > > :: iterator it;
	for (int i = 0;i < mx.size ();i ++)
		st.insert (mp (mx[i], i));
	for (int i = 0;i < mx.size ();i ++)
	{
		if (!st.count (mp (mx[i], i)))
			continue;
		st.erase (mp (mx[i], i));
		int tmp = sum_mx - mx[i];
		it = st.upper_bound (mp (tmp, i));
		if (it == st.end ())
			st.erase (--st.end ());
		else
			if (it != st.begin ())
				st.erase (--st.begin ());
			else
				st.erase (--st.end ());
	}
	return ret;	
}

int calc_mn ()
{
	memset (used, 0, sizeof used);
	sort (mn.begin (), mn.end ());
	int ret = 1;
	for (int i = 0;i < mn.size ();i ++)
	{
		if (used[i])
			continue;
		int p = -1, last;
		for (int j = mn.size () - 1;j > i;j --)
		{
			if (used[j])
				continue;
			last = j;
			if (mn[i] + mn[j] > sum_mn)
				p = j;
		}
		used[i] = 1;
		if (p == -1)
			used[last] = 1;
		else
		{
			used[p] = 1;
			ret ++;
		}
	}
	return ret;	
}

int main ()
{
	freopen ("E.in", "r", stdin);
	freopen ("E.out", "w", stdout);
	cin >> n;
	for (int i = 1;i <= n + n;i ++)
		cin >> a[i];
	int p, q = 0;
	for (int i = 2;i <= n + n;i ++)
		if (a[i] > q)
		{
			q = a[i];
			p = i;
		}
	sum_mx = a[1] + a[p];
	for (int i = 2;i <= n + n;i ++)
		if (i != p)
			mx.pb (a[i]);
	q = 1e9, p;
	for (int i = 2;i <= n + n;i ++)
		if (a[i] < q)
		{
			q = a[i];
			p = i;
		}
	sum_mn = a[1] + a[p];
	for (int i = 2;i <= n + n;i ++)	
		if (p != i)
			mn.pb (a[i]);
	cout << calc_mx () << " ";
	cout << calc_mn ();
	return 0;	
}