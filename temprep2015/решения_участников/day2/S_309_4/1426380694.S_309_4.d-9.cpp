#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>
#define ll long long
#define mp make_pair
#define pb push_back 
using namespace std;

ll l, r, res, a, c;

map < ll, bool > used;

int main ()
{
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf ("%d%d%d%d", &a, &c, &l, &r);
	if (a == c)
	{
		int res = 0;
		if (r > a)
			res += r - a;
		l --;
		if (l > a)
			res -= l - a;
	    cout << res;
		return 0;
	}
	a -= c;
	ll tmp = c;
	int sqr = (int) sqrt (a);
	for (int i = 1;i <= sqr;i ++)
		if (a % i == 0)
		{
			int k = a / i;
			int kk = i;
			if (!used[k])
			{
				if (tmp % k == c && k >= l && k <= r)
					res ++;
				used[k] = 1;
			}
			if (!used[kk])
			{
				if (tmp % kk == c && kk >= l && kk <= r)
					res ++;
				used[kk] = 1;
			}
		}
	cout << res;
	return 0;	
}