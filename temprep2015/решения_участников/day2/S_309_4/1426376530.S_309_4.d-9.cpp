#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define mp make_pair
#define pb push_back 
using namespace std;

int l, r, res, a, c;

map < int, bool > used;

vector < int > pr;

int main ()
{
	freopen ("D.in", "r", stdin);
	freopen ("D.out", "w", stdout);
	scanf ("%d%d%d%d", &a, &c, &l, &r);
	a -= c;
	for (int i = 1;1ll * i * i <= a;i ++)
		if (a % i == 0)
		{
			int k = a / i;
			int kk = i;
			if (!used[k])
			{
				if (k > c && k >= l && k <= r)
					res ++;
				used[k] = 1;
			}
			if (!used[kk])
			{
				if (kk > c && kk >= l && kk <= r)
					res ++;
				used[kk] = 1;
			}
		}
	cout << res;
	return 0;	
}