#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>

#define pb push_back
#define llong long long
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int INF = 1e9 + 7;
int s, a, c, l, r, res, cnt;
vector<int> v;

int main() {
    //freopen("D.txt", "r", stdin);
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> a >> c >> l >> r;
    if(l>r){
        cout<<0;
        return 0;
    }
    if(a == c) {
        cout << max(0, r - max(a, l - 1));
        return 0;
    }
    s = a - c;
    if(s < 0) {
        cout<<0;
        return 0;
    }
    for(int i = 1; i * i <= s; i++) {
        if(s % i == 0) {
            if(i > c)
                v.pb(i);
            int nxt = s / i;
            if(nxt != i && nxt > c)
                v.pb(nxt);
        }
    }
    for(int i = 0; i < v.size(); i++)
        if(v[i] >= l && v[i] <= r)
            res++;
    printf("%d", res);
    return 0;
}
