#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>

#define pb push_back
#define llong long long
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e6 + 1;
const int INF = 1e9 + 7;
int n, it,sz,len;
int a[MXN];
bool u[MXN], u1[MXN];
int points[2][MXN];

int bs(int x) {
    int l = 1, r = n;
    while(r - l > 1) {
        int m = (l + r) / 2;
        if(a[m] <= x) l = m;
        else r = m;
    }
    if(a[r] == x) return r;
    return l;
}

int bs2(int x, int j) {
    int l = 1, r = n / 2;
    while(r - l > 1) {
        int m = (l + r) / 2;
        if(points[j][m] <= x) l = m;
        else r = m;
    }
    if(points[j][r] == x) return r;
    return l;
}

int bs3(int x) {
    int l = 1, r = n;
    while(r - l > 1) {
        int m = (l + r) / 2;
        if(a[m] < x) l = m;
        else r = m;
    }
    if(a[l] == x) return l;
    return r;
}

int main() {
    //freopen("E.txt", "r", stdin);
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    n += n;
    for(int i = 1; i <= n; i++)
        cin >> a[i];
    int al = a[1];
    sort(a + 1, a + n + 1);
    it = bs(al);

    int ps;
    if(it == n) {
        ps = (a[n - 1] + al) / 2;
        points[0][++sz] = ps;
        u[n]=u[n-1]=1;
    }
    else {
        ps = (a[n] + al) / 2;
        points[0][++sz] = ps;
        u[n]=u[it]=1;
    }
    int l = 1;
    for(int i = n; i >= 1; i--) {
        if(u[i]) continue;
        while(u[l])l++;
        points[0][++sz] = (a[i] + a[l]) / 2;
        u[i] = u[l] = 1;
    }
    sort(points[0] + 1, points[0] + sz + 1);
    int ind = bs3(al), fi;
    if(ind==1){
        fi = (a[1] + a[2]) / 2;
        points[1][++len] = fi;
        u1[1] = u1[2] = 1;
    }
    else {
        fi = (a[1] + a[ind]) / 2;
        points[1][++len] = fi;
        u1[1] = u1[ind] = 1;
    }
    for(int i = n; i >= 1; i--) {
        if(u1[i]) continue;
        for(int j = i - 1; j >= 1; j--) {
            if(!u1[j]) {
                it = j;
                break;
            }
        }
        points[1][++len] = (a[i] + a[it]) / 2;
        u1[i] = u1[it] = 1;
    }
    sort(points[1] + 1, points[1] + len + 1);
    cout << sz-bs2(ps, 0)+1 <<' '<< len-bs2(fi, 1)+1<<endl;
    return 0;
}

