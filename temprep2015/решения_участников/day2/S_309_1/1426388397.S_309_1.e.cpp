#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>

#define pb push_back
#define llong long long
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e6 + 1;
const int INF = 1e9 + 7;
int n, mn = INF, mx;
int a[MXN];
bool u[MXN];
int man[MXN];

void f(int groups, int first, int cnt) {
    if(groups == n + 1) {
        mn = min(mn, cnt);
        mx = max(mx, cnt);
    }
    else {
        for(int i = 1; i <= n + n; i++) {
            if(u[i]) continue;
            u[i] = 1;
            for(int j = i + 1; j <= n + n; j++) {
                if(u[j]) continue;
                u[j] = 1;
                man[groups] = (a[i] + a[j]) / 2;
                if(i==1)
                    f(groups + 1, groups, 1);
                else {
                    f(groups + 1, first, cnt + (man[groups] > man[first]));
                }
                u[j] = 0;
            }
            u[i] = 0;
        }
    }
}


int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    for(int i = 1; i <= n + n; i++) cin>>a[i];
    f(1, 0, 0);
    cout<<mn<<' '<<mx;
    return 0;
}

