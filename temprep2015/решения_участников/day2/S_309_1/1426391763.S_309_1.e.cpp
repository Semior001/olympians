#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>

#define pb push_back
#define llong long long
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e6 + 1;
const int INF = 1e9 + 7;
int n;
int a[MXN];
int pt[2][MXN], cur;
set<pair<int, int> > s, s1;

int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin>>n;
    n+=n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    int mx=0, it;
    for(int i=2;i<=n;i++)
        if(mx<a[i]) mx=a[i],it=i;
    cur = pt[0][1] = (a[it] + a[1]) / 2;
    for(int i=2;i<=n;i++)
        if(it!=i) {
            s.insert(mp(a[i], i));
            s1.insert(mp(-a[i], i));
        }
    int c=2;
    while(!s.empty()) {
         int now = s.begin() -> S, pr = s1.begin() -> S;
         s.erase(s.begin());
         s1.erase(s1.begin());
         pt[0][c++] = (a[now] * 1.0 + a[pr] * 1.0) / 2.0;
         s.erase(mp(a[pr], pr));
         s1.erase(mp(-a[now], now));
    }
    sort(pt[0] + 1, pt[0] + c);
    cout << c - (upper_bound(pt[0] + 1, pt[0] + c, cur) - pt[0]) + 1;
    int mn=INF, ind;
    for(int i = 2; i<= n; i++) {
        if(mn > a[i]) {
            mn = a[i];
            ind = i;
        }
    }
    int z;
    z = pt[1][1] = (a[1] + a[ind]) / 2;
    c = 2;
    for(int i = 2; i<= n; i++)
        if(ind!=i)
            s.insert(mp(a[i], i));
    while(!s.empty()) {
        int now = s.begin() -> S;
        s.erase(s.begin());
        int pr = s.begin() -> S;
        s.erase(s.begin());
        pt[1][c++] = (a[now] * 1.0 + a[pr] * 1.0) / 2.0;
    }
    sort(pt[1] + 1, pt[1] + c);
    cout << " " << c - (upper_bound(pt[1] + 1, pt[1] + c, z) - pt[1]) + 1;
    return 0;
}


