#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>

#define pb push_back
#define llong long long
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int INF = 1e9 + 7;
int s, a, c, l, r, res;

int main() {
    //freopen("D.txt", "r", stdin);
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> a >> c >> l >> r;
    if(a == 0 && c == 0) {
        cout << r - l + 1;
        return 0;
    }
    if(a == c) {
        cout << r - max(a, l);
        return 0;
    }
    s = a - c;
    if(s < 0) cout << 0;
    else {
        int t = sqrt(s * 1.0);
        for(int i = 1; i <= t; i++) {
            if(s % i == 0) {
                if(i > c && i >= l && i <= r)
                    res++;
                int nxt = s / i;
                if(nxt != i && nxt > c && nxt >= l && nxt <= r)
                    res++;
            }
        }
        cout << res;
    }
    return 0;
}
