#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;
#define ull unsigned long long
unsigned long long n,l,r,a,ans;
ull gc (ull a, ull b)
{
	while (b)
	{
		a=a%b;
		swap (a,b);
	}
	return a;
}	
ull gcd (ull a,ull b)
{
	ull q=a,w=b;
	ull qq;
	while (b)
	{
		a=a%b;
		swap (a,b);
	}
   qq=q/a;
   qq*=w;
   return qq;
}
void rec (ull l, ull r,ull a,ull cou,ull g,ull m)
{
	if (l>r || cou>n)
	{
		ans+=m;
		return;
	}
	if (gc(g,a)==a)
	{
	  	m*=(r-l+1);
     	m=m%1000000007;
   	rec (l,r,a,cou+1,g,m);
   }
	else
	{
		for (int i=l;i<=r;i++)
		{
			if (gc(a,gcd(g,i))!=gc(a,g))
			{
				if (gc(gcd(g,i),a)==a)
				{
					m++;
				}
					rec (i,r,a,cou+1,gcd(g,i),m);
			}
		}
	}
}
int main ()
{
	freopen ("D.in","r",stdin);
	freopen ("D.out","w",stdout);
	cin>>n>>l>>r>>a;
	if (n==1)
	{
		for (int i=l;i<=r;i++)
		{
			if (i%a==0)
			{
				ans++;
			}
		}
		cout<<ans;
		return 0;
	}
	rec(l,r,a,1,1,0);
	cout<<ans;
	return 0;
}				