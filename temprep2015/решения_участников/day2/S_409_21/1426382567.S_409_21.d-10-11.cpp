#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
using namespace std;
#define ull unsigned long long
unsigned long long n,l,r,a,ans;
ull gcd (ull a,ull b)
{
	ull q=a,w=b;
	ull qq;
	while (b)
	{
		a=a%b;
		swap (a,b);
	}
   qq=q/a;
   qq*=w;
   return qq;
}
int main ()
{
	freopen ("D.in","r",stdin);
	freopen ("D.out","w",stdout);                   
	cin>>n>>l>>r>>a;
	if (n==1)
	{
		for (int i=l;i<=r;i++)
		{
			if (i%a==0)
			{
				ans++;
				ans=ans%1000000007;
			}
		}
		cout<<ans;
		return 0;
	}
	else if (n==2)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{
				if (gcd(i,j)%a==0)
				{
					ans++;
					ans=ans%1000000007;
				}	      
			}
		}
		cout<<ans;
		return 0;
	}
	else if (n==3)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					if (gcd(q,gcd(i,j))%a==0)
					{
						ans++;
						ans=ans%1000000007;
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	}
	else if (n==4)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						if (gcd(gcd(q,w),gcd(i,j))%a==0)
						{
							ans++;
							ans=ans%1000000007;
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
	else if (n==5)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							if (gcd(e,gcd(gcd(q,w),gcd(i,j)))%a==0)
							{
								ans++;
								ans=ans%1000000007;
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
	else if (n==6)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							for (int t=e;t<=r;t++)
							{
								if (gcd(gcd(e,t),gcd(gcd(q,w),gcd(i,j)))%a==0)
								{
									ans++;
									ans=ans%1000000007;
								}
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
   else if (n==7)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							for (int t=e;t<=r;t++)
							{
								for (int y=t;y<=r;y++)
								{
									if (gcd(gcd(y,gcd(e,t)),gcd(gcd(q,w),gcd(i,j)))%a==0)
									{
										ans++;
										ans=ans%1000000007;
									}
								}
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
   else if (n==8)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							for (int t=e;t<=r;t++)
							{
								for (int y=t;y<=r;y++)
								{
									for (int u=y;u<=r;u++)
									{
										if (gcd(gcd(gcd(y,u),gcd(e,t)),gcd(gcd(q,w),gcd(i,j)))%a==0)
										{                
											ans++;
											ans=ans%1000000007;
										}
									}
								}
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
	else if (n==9)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							for (int t=e;t<=r;t++)
							{
								for (int y=t;y<=r;y++)
								{
									for (int u=y;u<=r;u++)
									{
										for (int o=u;o<=r;o++)
										{
											if (gcd(o,gcd(gcd(gcd(y,u),gcd(e,t)),gcd(gcd(q,w),gcd(i,j))))%a==0)
											{                
												ans++;
												ans=ans%1000000007;
											}
										}
									}
								}
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 
	else if (n==10)
	{
		for (int i=l;i<=r;i++)
		{
			for (int j=i;j<=r;j++)
			{  
				for (int q=j;q<=r;q++)
				{
					for (int w=q;w<=r;w++)
					{
						for (int e=w;e<=r;e++)
						{
							for (int t=e;t<=r;t++)
							{
								for (int y=t;y<=r;y++)
								{
									for (int u=y;u<=r;u++)
									{
										for (int o=u;o<=r;o++)
										{
											for (int p=o;p<=r;p++)
											{
												if (gcd(gcd(o,p),gcd(gcd(gcd(y,u),gcd(e,t)),gcd(gcd(q,w),gcd(i,j))))%a==0)
												{                
													ans++;
													ans=ans%1000000007;
												}
											}
										}
									}
								}
							}
						}
					}
				}	      
			}
		}
		cout<<ans;
		return 0;
	} 


}

