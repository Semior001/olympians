#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;

int n, a[maxn], t[1 << 24], sum[1 << 24], ans;

void update(int v, int x, int i) {
	if(i == 22) {
		t[v]++;
		return;
	}
	if((x & (1 << (i + 1))) == 0) 
		update(v + v, x, i + 1);
	else 
		update(v + v + 1, x, i + 1);
}

int get(int v, int x, int i) {
	if(i == 22) {
		return 0;
	}
	if((x & (1 << (i + 1))) != 0) {
		return get(v + v, x, i + 1) + sum[v + v + 1];
	}
	else {
		return get(v + v, x, i + 1) + get(v + v + 1, x, i + 1);
	}
}

void walk(int v, int i) {
	if(i == 22) {
		sum[v] = t[v];
		return;
	}
	walk(v + v, i + 1);
	walk(v + v + 1, i + 1);
	sum[v] = sum[v + v] + sum[v + v + 1];
}

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
		cin >> n;
		
		for(int i = 1; i <= n; i++) {
			cin >> a[i];
			update(1, a[i], -1);
		}
		walk(1, -1);
		for(int i = 1; i <= n; i++)
			cout << n - get(1, a[i], -1) << " ";

	return 0;
}
