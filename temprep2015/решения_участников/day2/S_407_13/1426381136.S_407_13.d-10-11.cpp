#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;

int n, l, r, a, ans;

int lcm(int a, int b) {
	return a / __gcd(a, b) * b;
}

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
		cin >> n >> l >> r >> a;
		if(n == 1) {
			for(int i = l; i <= r; i++)
				if(i % a == 0)	
					ans++;
		}
		if(n == 2) {
			for(int i = l; i <= r; i++)
				for(int j = i; j <= r; j++)
					if(lcm(i, j) % a == 0)
						ans++;
		}
		cout << ans;
	return 0;
}
