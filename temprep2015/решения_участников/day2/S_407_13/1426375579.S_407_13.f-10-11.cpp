#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;

int n, a[maxn], ans;

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
		cin >> n;
		for(int i = 1; i <= n; i++) cin >> a[i];
		for(int i = 1; i <= n; i++) {
			ans = 0;
			for(int j = 1; j <= n; j++) {
				if(i == j) continue;
				if((a[i] & a[j]) == 0) ans++;
			}
			cout << ans << " ";
		}
	return 0;
}
