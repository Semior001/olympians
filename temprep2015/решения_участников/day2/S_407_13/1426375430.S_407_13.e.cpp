#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;
const double eps = 1e-6;

int n, a[maxn], cnt, ans1 = inf, ans2 = -inf;
double him, other;
vector < int > v;

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
		cin >> n;
		for(int i = 1; i <= n + n; i++)
			cin >> a[i];
		for(int i = 2; i <= n + n; i++) {
			him = (a[i] + a[1] + .0) / 2;
			v.clear();
			for(int j = 2; j <= n + n; j++)
				if(i != j) 
					v.pb(a[j]);
			sort(v.begin(), v.end());
			do{
			cnt = 0;
			for(int j = 0; j < int(v.sz) / 2; j++) {
				other = (a[j] + a[int(v.sz) - 1 - j]) / 2.0;
				if(other - him > eps) cnt++;
			}
			ans1 = min(ans1, cnt);
			ans2 = max(ans2, cnt);
			} while(next_permutation(v.begin(), v.end()));
		}
		cout << ans1 + 1 << " " << ans2 + 1 << endl;
	return 0;
}
