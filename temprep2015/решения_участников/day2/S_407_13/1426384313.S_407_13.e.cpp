#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;

int n, a[2 * maxn], him, ans1 = 1, ans2 = 1, l, r, con;
multiset < int > s;

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
		cin >> n;
		n += n;
		for(int i = 1; i <= n; i++)   
			cin >> a[i];
		sort(a + 2, a + n + 1);
		him = a[n] + a[1];
		l = 2, r = n - 1;
		while(l < r) {
			if(a[l] + a[r] <= him) {
				l++, r--;
			}
			else {
				ans1++;
				r -= 2;
			}
		}
		him = a[1] + a[2];
		l = 3, r = n;
		while(l < r) {
			if(a[l] + a[r] > him) {
				ans2++;
				l++, r--;
			}
			else {
				l += 2;
			}
		}
		cout << ans1 << " " << ans2 << endl;
		
	return 0;
}
