#include<bits/stdc++.h>

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;
const double eps = 1e-6;

int n, a[maxn], cnt, ans1 = inf, ans2 = -inf;
int him, other;
vector < int > v;

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
		cin >> n;
		for(int i = 1; i <= n + n; i++)
			cin >> a[i];
		sort(a + 2, a + n + n + 1);
		for(int i = 2; i <= n + n; i++) {
			him = a[1] + a[i];
			v.clear();
			for(int j = 2; j <= n + n; j++)
				if(i != j) 
					v.pb(a[j]);
			do{
				cnt = 0;
				for(int j = 1; j < v.sz; j++) {
					other = v[j - 1] + v[j];
					if(other > him) cnt++;
				}
				ans1 = min(ans1, cnt);
				ans2 = max(ans2, cnt);
			} while(next_permutation(v.begin(), v.end()));
		}
		cout << ans1 + 1 << " " << ans2 + 1 << endl;
	return 0;
}
