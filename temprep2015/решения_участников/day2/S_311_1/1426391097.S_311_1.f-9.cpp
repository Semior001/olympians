#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e5 + 1;

int n;
bool u[501][501];
int h[N], c[N];
pair <int, llong> a[N], ans[N];

int main() {
    //freopen("F.in", "r", stdin);
    //freopen("F.out", "w", stdout);
    cin >> n;
    for(int i = 1; i <= n; ++i) cin >> h[i];
    for(int i = 1; i <= n; ++i) cin >> c[i];
    for(int i = 1; i <= n; ++i) a[i] = mp(h[i], c[i]);
    sort(a + 1, a + n + 1);
    llong col = 0, cost = 0;
    for(int i = 1; i <= n; ++i)
        if(h[i] != a[i].f)
            if(!u[h[i]][a[i].f] && !u[a[i].f][h[i]])
                col ++, cost += min(c[i] * 1LL, a[i].s), u[h[i]][a[i].f] = 1, u[a[i].f][h[i]] = 1;
    cout << cost;
    return 0;
}
