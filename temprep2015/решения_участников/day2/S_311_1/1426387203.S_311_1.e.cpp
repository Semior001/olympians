#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

int n, my;
int a[MXN], a1[MXN];
int mxd, mnd, mxr, mnr;
double mx = -INF, mn = INF;
set <int> s;

double f(int mid, int x) {
    double res = (x + a[mid]) / 2;
    return res;
}

double f1(int mid, int x) {
    double res = (x + a1[mid]) / 2;
    return res;
}

double check(int x, int y) {
    double res = (x + y) / 2;
    return res;
}

int main() {
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    mxr = 1, mnr = n, n *= 2;
    for(int i = 1; i <= n; ++i) {
        cin >> a[i];
        a1[i] = a[i];
    }
    int my = a[1];
    swap(a[1], a[n]), n--;
    for(int i = 1; i <= n; ++i)
        if(mx < a[i]) mx = a[i], mxd = i;
    mx += my, mx /= 2;
    swap(a[n], a[mxd]), n--;
    sort(a + 1, a + n + 1);
    for(int i = n; i >= 1; --i) {
        int r = i - 1, l = 1;
        while(r - l > 1) {
            int m = (l + r) / 2;
            if(f(m, a[i]) > mx) r = m;
            else l = m;
        }
        if(check(a[i], a[l]) > mx) mxr++, a[l] = -INF;
        else if(check(a[i], a[r]) > mx) mxr++, a[r] = -INF;
    }
    cout << mxr << ' ';
    n += 2;
    swap(a1[1], a1[n]), n--;
    for(int i = 1; i <= n; ++i)
        if(mn > a1[i]) mn = a1[i], mnd = i;
    mn += my, mn /= 2;
    swap(a1[n], a1[mnd]), n--;
    sort(a1 + 1, a1 + n + 1);
    for(int i = 1; i <= n; ++i) s.insert(a1[i]);
    for(int i = 1; i <= n; ++i) {
        int l = i + 1, r = n;
        while(r - l > 1) {
            int m = (l + r) / 2;
            if(f1(m, a1[i]) > mn) l = m;
            else r = m;
        }
        if(check(a1[i], a1[l]) < mn) mnr --, a1[l] = INF;
        else if(check(a1[i], a1[r]) < mn) mnr --, a1[r] = INF;
    }
    if(s.size() == 1) mnr = 1;
    cout << mnr;
    return 0;
}
