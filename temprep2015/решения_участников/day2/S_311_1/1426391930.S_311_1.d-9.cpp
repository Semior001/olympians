#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

llong ans;
llong a, c, l, r;

int main() {
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> a >> c >> l >> r;
    llong mx = sqrt(a);
    for(int i = l; i <= min(r, a); ++i)
        if(a % i == c)
            ans++;
    cout << ans;
    return 0;
}
