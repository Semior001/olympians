#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

int n;
int u[N][N];
int h[MXN], c[MXN];
pair <int, int> a[MXN], ans[MXN];

int main() {
    freopen("F.in", "r", stdin);
    freopen("F.out", "w", stdout);
    cin >> n;
    for(int i = 1; i <= n; ++i) cin >> h[i];
    for(int i = 1; i <= n; ++i) cin >> c[i];
    for(int i = 1; i <= n; ++i) a[i] = mp(h[i], c[i]);
    sort(a + 1, a + n + 1);
    for(int i = 1; i <= n; ++i) {
        int col = 0, cost = 0;
        for(int i = 1; i <= n; ++i) {
            if(h[i] != a[i].f)
            {
                u[h[i]][a[i].f] = a[i].s;
                if(u[h[i]][a[i].f] && u[a[i].f][h[i]])
                    col ++, cost += min(u[h[i]][a[i].f], u[a[i].f][h[i]]);
            }
        }
        ans[i] = mp(col, cost);
    }
    sort(ans + 1, ans + n + 1);
    cout << ans[1].s;
    return 0;
}
