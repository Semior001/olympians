#include<iostream>
#include<algorithm>
#include<cstdio>
#include<vector>
using namespace std;
#define pb push_back
#define f first
#define s second
int main(){
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);
    int n,a,l,r,counter=0;
    cin>>n>>l>>r>>a;
    if(n==2)
    for(int i=l;i<=r;i++){
        for(int j=l;j<=r;j++){
                int lcm=i*j;
                lcm/=__gcd(i,j);
                //cout<<i<<" "<<j<<" "<<lcm<<endl;
            if(lcm%a==0)counter++;
        }
    }
    if(n==1){
        for(int i=l;i<=r;i++){
            if(i%a==0)counter++;
        }
    }
    cout<<(counter+1)/2;
    return 0;
}
