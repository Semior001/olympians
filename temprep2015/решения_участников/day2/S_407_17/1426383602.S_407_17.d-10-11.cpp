#include<iostream>
#include<algorithm>
#include<cstdio>
#include<vector>
using namespace std;
#define pb push_back
#define f first
#define s second
bool used[1111][1111];
int main(){
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);
    int n,a,l,r,counter=0;
    cin>>n>>l>>r>>a;
    if(n==2)
    for(int i=l;i<=r;i++){
        for(int j=l;j<=r;j++){
                if(used[i][j])continue;
                int lcm=i*j;
                lcm/=__gcd(i,j);
                used[i][j]=true;
                used[j][i]=true;
                //cout<<i<<" "<<j<<" "<<lcm<<endl;
            if(lcm%a==0)counter++;
        }
    }
    if(n==1){
        for(int i=l;i<=r;i++){
            if(i%a==0)counter++;
        }
    }
    cout<<counter;
    return 0;
}
