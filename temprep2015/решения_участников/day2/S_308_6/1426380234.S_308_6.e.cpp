#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {

 freopen("E.in", "r", stdin);
 freopen("E.out", "w", stdout);

 int n;

 scanf("%d", &n);

 int *m = new int[n];

 for (int i = 0; i < n*2; i++) {
  scanf("%d", &m[i]);
 }

 int a = m[0];

 sort(m + 1, m + 2 * n);

 int max, min, t1, t2;

 if (n == 1) {
  printf("1 1");
  return 0;
 }

 max = m[0] + m[2*n-1];

 min = m[0] + m[1];

 t1 = m[2*n-2] + m[2*n-3];

 t2 = m[1] + m[2];

 if (max >= t1) {
  printf("1 ");
 }
 else {

 int j = 2;

 while (max < t1 && n * 2 > j) {
  t1 = m[n*2-j] + m[n*2-j-1];
  j = j + 2;
 }

 printf("%d ", j / 2);
 }

 if (min < t2) {
  printf("%d", n);
 }
 else {

 int j = 2;

 while (min >= t2 && n * 2 > j) {
  t2 = m[j] + m[j-1];
  j = j + 2;

 }

 printf("%d", j / 2 - 2);
 }

 return 0;
}
