#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {

 freopen("F.in", "r", stdin);
 freopen("F.out", "w", stdout);

 int n;

 scanf("%d", &n);

 int *m = new int[n];

 for (int i = 0; i < n; i++) {
  scanf("%d", &m[i]);
 }

 int tmp = 0;
 int sum = 0;
 for (int i = 0; i < n; i++) {
  tmp = m[i];
  sum = 0;
  for (int j = 0; j < n; j++) {
   if ((tmp & m[j]) == 0) {
    sum++;
   }
  }
  printf("%d ", sum);
 }

 return 0;
}
