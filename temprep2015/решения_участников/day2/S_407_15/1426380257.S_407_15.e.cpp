#include<iostream>
#include<fstream>
#include<deque>
using namespace std;
deque<int> a(2500);
void Qsort(int l,int r)
{
    int i=l;
    int j=r;
    int x=a[(l+r)/2];
    while(i<=j)
    {
        while(a[i]<x) ++i;
        while(a[j]>x) --j;
        if(i<=j)
        {
            swap(a[i],a[j]);
            --j;
            ++i;
        };
    };
    if(j>l) Qsort(l,j);
    if(i<r) Qsort(i,r);
};
int main()
{
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    int n;
    cin >> n;
    for(int i=0;i<2*n;++i) cin >> a[i];
    int k2 = 1;
    int S = a[0];
    a.pop_front();
    Qsort(0,2*n - 1);
    cout << endl;
    S += a[0];
    for(int i= 2*n -1;i>0;i -= 2)
    {
        if(a[i]+a[i-1] > S)
        {
            ++k2;
        };
    };
    int k1 = 1;
    S = S - a[0] + a[2*n-1];
    for(int i=0;i< 2*n -1;++i)
    {
        if(a[i]+a[2*n -i-2] > S)
        {
            ++k1;
        };
    };
    cout << k1 << " " << k2;
    fclose(stdin);
    fclose(stdout);
    return 0;
}
