#include<iostream>
#include<fstream>
#include<deque>
using namespace std;
deque<float> a(2500);
void Qsort(int l,int r)
{
    int i=l;
    int j=r;
    int x=a[(l+r)/2];
    while(i<=j)
    {
        while(a[i]<x) ++i;
        while(a[j]>x) --j;
        if(i<=j)
        {
            swap(a[i],a[j]);
            --j;
            ++i;
        };
    };
    if(j>l) Qsort(l,j);
    if(i<r) Qsort(i,r);
};
int main()
{
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    int n;
    cin >> n;
    a.resize(2*n);
    for(int i=0;i<2*n;++i) cin >> a[i];
    int k2 = 1;
    int S = a[0];
    a.pop_front();
    Qsort(0,a.size()-1);
    S += a[0];
    S = S/2;
    for(int i= a.size()-1;i>1;i -= 2)
    {
        if((a[i]+a[i-1])/2 > S)
        {
            ++k2;
        };
    };
    int k1 = 1;
    S *= 2;
    S = S - a[0] + a[a.size()-1];
    S = S/2;
    for(int i=0;i < a.size()/2;++i)
    {
        if((a[i]+a[a.size() -i-2])/2 > S)
        {
            ++k1;
        };
    };
    cout << k1 << " " << k2;
    fclose(stdin);
    fclose(stdout);
    return 0;
}
