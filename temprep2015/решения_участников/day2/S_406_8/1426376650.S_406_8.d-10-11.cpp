#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

int n, l, r, a;
int ans;
int ar[20];

inline int lcm(int x, int y){
  return x / __gcd(x, y) * y;
}

void rec(int x, int prev = l){
  if(x == n){
    int tmp = ar[0];
    for(int i = 0; i < n; ++i){
      tmp = lcm(ar[i], tmp);
    }
    //cerr << tmp << " " << ar[0] << " " << ar[1] << "\n";
    if(tmp % a == 0)
      ++ans;
  }
  else {
    for(int i = prev; i <= r; ++i){
      ar[x] = i;
      rec(x + 1, i);
    }
  }
}

int main(){
  #define fn "D"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d%d%d", &n, &l, &r, &a);
  rec(0);
  printf("%d", ans);
  return 0;
}
