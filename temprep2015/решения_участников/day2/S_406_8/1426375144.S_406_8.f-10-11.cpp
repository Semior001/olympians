#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

int n;
int a[MXN];
int ans = 0;

int main(){
  #define fn "F"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 0; i < n; ++i){
    scanf("%d", &a[i]);
  }
  for(int i = 0; i < n; ++i){
    ans = 0;
    for(int j = 0; j < n; ++j){
      ans += (a[i] & a[j]) == 0;
    }
    printf("%d ", ans);
  }
  return 0;
}
