#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

int n;
int a[MXN];
int ans = 0;
int nx[MXN][2], cnt[MXN];
int sz = 1;

inline bool bit(int id, int mask){
  return ((1 << id) & mask);
}

inline void add(int x){
  int v = 0;
  for(int i = 7; i >= 0; --i){
    //cnt[v]++;
    if(!nx[v][bit(i, x)])
      nx[v][bit(i, x)] = sz++;
    v = nx[v][bit(i, x)];
  }
  ++cnt[v];
}

int get(int x, int v, int id){
  if(id == -1){
    return cnt[v];
  }
  int tmp = 0;
  if(!bit(id, x) && nx[v][1])
    tmp += get(x, nx[v][1], id - 1);
  if(nx[v][0])
    tmp += get(x, nx[v][0], id - 1);
  return tmp;
}

int main(){
  #define fn "F"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 0; i < n; ++i){
    scanf("%d", &a[i]);
  }
  if(n <= 10000){
    for(int i = 0; i < n; ++i){
      ans = 0;
      for(int j = 0; j < n; ++j){
        ans += (a[i] & a[j]) == 0;
      }
      printf("%d ", ans);
    }
  }
  else {
    for(int i = 0; i < n; ++i){
      add(a[i]);
    }
    for(int i = 0; i < n; ++i){
      printf("%d ", get(a[i], 0, 7));
      //get(a[i], 0, 7);
    }
  }
  return 0;
}
