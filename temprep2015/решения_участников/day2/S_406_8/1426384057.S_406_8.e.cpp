#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)7000 + 10;
const double EPS = (double)1e-9;

int n;
int a[MXN];
vector <int> g[MXN];
int mt[MXN];
bool used[MXN];
int ans1, ans2;

bool dfs(int v){
  if(used[v])
    return false;
  used[v] = true;
  for(auto to : g[v]){
    if(!used[to] && (mt[to] == -1 || dfs(mt[to]))){
      mt[to] = v;
      return true;
    }
  }
  return false;
}

int main(){
  #define fn "E"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 0; i < n + n; ++i){
    scanf("%d", &a[i]);
  }
  sort(a + 1, a + n + n);
  int tmp;
  do {
    tmp = 0;
    for(int i = 2; i < n + n; i += 2){
      if(a[i] + a[i + 1] > a[0] + a[1])
        ++tmp;
    }
    ans1 = max(tmp, ans1);
  }while(next_permutation(a + 1, a + n + n));
  ++ans1;
  sort(a + 1, a + n + n, greater<int>());
  for(int i = 2; i <= n; ++i){
    g[i].clear();
    for(int j = n + 1; j < n + n; ++j){
      if(a[i] + a[j] <= a[0] + a[1])
        g[i].pb(j);
    }
  }
  fill(mt, mt + n + n, -1);
  for(int i = 2; i <= n; ++i){
    fill(used, used + n + n, false);
    dfs(i);
  }
  for(int i = n + 1; i < n + n; ++i){
    if(mt[i] == -1){
      ++ans2;
    }
  }
  ++ans2;
  printf("%d %d", ans2, ans1);
  return 0;
}
