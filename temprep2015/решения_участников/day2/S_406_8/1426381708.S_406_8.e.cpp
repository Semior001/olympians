#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)7000 + 10;
const double EPS = (double)1e-9;

int ans1 = INF, ans2 = -INF;
int tmp;
int n;
int a[MXN];

int main(){
  #define fn "E"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 0; i < n + n; ++i){
    scanf("%d", &a[i]);
  }
  sort(a + 1, a + n + n);
  do {
    tmp = 0;
    for(int i = 2; i < n + n; i += 2){
      if(a[i] + a[i + 1] > a[0] + a[1])
        ++tmp;
    }
    ans1 = min(tmp, ans1);
    ans2 = max(tmp, ans2);
  }while(next_permutation(a + 1, a + n + n));
  printf("%d %d", ans1 + 1, ans2 + 1);
  return 0;
}
