#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

int n, l, r, a;
int ans;

int main(){
  #define fn "D"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d%d%d", &n, &l, &r, &a);
  for(int i = l; i <= r; ++i){
    for(int j = i; j <= r; ++j){
      if((i / __gcd(i, j) * j) % a == 0)
          ++ans;
    }
  }
  printf("%d", ans);
  return 0;
}
