#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long
#define ullong unsigned long long
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18 + 7;
const int INF = (int)1e9 + 7;
const int MXN = (int)7000 + 10;
const double EPS = (double)1e-9;

int n;
int a[MXN];
vector <int> g[MXN];
int mt[MXN];
bool used[MXN];
int ans1, ans2;

bool dfs(int v){
  if(used[v])
    return false;
  used[v] = true;
  for(auto to : g[v]){
    if(!used[to] && (mt[to] == -1 || dfs(mt[to]))){
      mt[to] = v;
      return true;
    }
  }
  return false;
}

inline int solve(int l, int r){
  int tmp = 0;
  sort(a + 1, a + n + n);
  int m = (l + r) / 2;
  for(int i = l; i < m; ++i){
    for(int j = m; j < r; ++j){
      if(a[i] + a[j] > a[0] + a[1])
        g[i].pb(j);
    }
  }
  fill(mt, mt + n + n, -1);
  for(int i = l; i < m; ++i){
    fill(used, used + n + n, false);
    dfs(i);
  }
  for(int i = m; i < r; ++i){
    if(mt[i] != -1){
      ++tmp;
    }
  }
  return tmp;
}

inline int solve1(int l, int r){
  int tmp = 0;
  sort(a + 1, a + n + n, greater <int>());
  int m = (l + r) / 2;
  for(int i = l; i < m; ++i){
    for(int j = m; j < r; ++j){
      if(a[i] + a[j] <= a[0] + a[1])
        g[i].pb(j);
    }
  }
  fill(mt, mt + n + n, -1);
  for(int i = l; i < m; ++i){
    fill(used, used + n + n, false);
    dfs(i);
  }
  for(int i = m; i < r; ++i){
    if(mt[i] == -1){
      ++tmp;
    }
  }
  return tmp;
}

int main(){
  #define fn "E"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else // LOCAL
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 0; i < n + n; ++i){
    scanf("%d", &a[i]);
  }
  int ans1 = solve(2, n + n), cur;
  for(int i = 4; i <= n + n; i += 2){
    cur = solve(i, n + n);
    ans1 = max(ans1, cur);
  }
  ++ans1;
  ans2 = solve1(2, n + n);
  for(int i = 4; i < n + n; i += 2){
    cur = solve(i, n + n);
    ans2 = min(ans2, cur);
  }
  ++ans2;
  printf("%d %d", ans2, ans1);
  return 0;
}
