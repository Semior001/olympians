#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#define pb push_back
#define mp make_pair
#define len length()
#define f first
#define s second
#define ll long long

using namespace std;

ll a, b, l, r, ans;

int main(){
	freopen("D.in","r",stdin);
	freopen("D.out","w",stdout);
	cin >> a >> b >> l >> r;
	if (a == b){
		for (ll i = a + 1; i <= r; i++)
			ans++;
		cout << ans;
		return 0;
	}
	if (a < b){
		cout << 0;
		return 0;
	}
	a -= b;
	for (ll x = 1; x*x <= a; x++){
		if (a % x == 0){
			if (x > b && l <= x && x <= r)
				ans++;
			ll d = a/ x;
			if (d != x && d > b && l <= d && d <= r)
				ans++;
		}
	}
	cout << ans;
	return 0;
	return 0;
}