#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#define pb push_back
#define mp make_pair
#define len length()
#define f first
#define s second
#define ll long long

using namespace std;

const int maxn = (int)(1e5+5);

int n;
ll sum, h[maxn], c[maxn];

int main(){
	freopen("F.in","r",stdin);
	freopen("F.out","w",stdout);
	cin >> n;
	for (int i = 0; i < n; i++){
		cin >> h[i];
	}
	for (int i = 0; i < n; i++){
		cin >> c[i];
	}
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n - 1; j++){
			if (h[i] < h[j]){
				swap(h[i], h[j]);
				sum+=min(c[i], c[j]);
			}	
		}
	}
	cout << sum;
	return 0;
}