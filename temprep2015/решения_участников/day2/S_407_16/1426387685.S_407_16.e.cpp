#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "E"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int n, a[maxn], b[maxn], N, u[maxn], A, B, d[maxn], d1[maxn];
void get_max()
{
	int ans = 0;
	B = b[N - 1];
	reverse(b + 1, b + N);
	for(int i = 2; i <= N / 2; ++i)
	{
		int l = i, r = N, cur = -1;
		while(r - l > 1)
		{
			int m = (l + r) / 2;
			if(b[m] <= A + B - b[i])
				r = m;
			else
				l = m;
		}
		if(b[l] <= A + B - b[i])
			cur = l;
		if(b[r] <= A + B - b[i])
			cur = r;
		if(cur != -1)
			d[cur]++;
	}
	int cur = 0;
	for(int i = N - 1; i >= 2; --i)
	{
		if(i > N / 2)
			cur++;
		if(d[i] != 0)
		{
			ans += min(cur, d[i]);
			cur -= d[i];
			if(cur < 0)
				cur = 0;
		}
	}
	cout << n - ans << " ";
}
void get_min()
{
	int ans = 0;
	B = b[N - 1];
	for(int i = 1; i < N / 2; ++i)
	{
		int l = i, r = N - 1, cur = -1;
		while(r - l > 1)
		{
			int m = (l + r) / 2;
			if(b[m] > A + B - b[i])
				l = m;
			else
				r = m;
		}
		if(r == N - 1)
			r--;
		if(l == i)	
			l++;
		if(b[l] > A + B - b[i])
			cur = l;
		if(b[r] > A + B - b[i])
			cur = r;
		if(cur != -1)
			d1[cur]++;
//		cout << i << " " << cur << endl;
	}
	int cur = 0;
	for(int i = 1; i <= N - 1; ++i)
	{
		if(i <= N / 2)
			cur++;
		if(d1[i] != 0)
		{
			ans += min(cur, d1[i]);
			cur -= d1[i];
			if(cur < 0)
				cur = 0;
		}
	}
	cout << ans + 1 << " ";
}
int main()
{
	freopen("out", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= 2 * n; ++i)
	{
		cin >> a[i];
		if(i != 1)
			b[i - 1] = a[i];
	}
	A = a[1];
	N = 2 * n;
	sort(b + 1, b + N);
	get_max();
	get_min();
	return 0;
}

