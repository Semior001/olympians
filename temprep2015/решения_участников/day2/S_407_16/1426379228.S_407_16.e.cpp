#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "E"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int n, a[maxn], b[maxn], N, u[maxn], A, B;
void get_max()
{
	int ans = 1;
	B = b[N - 1];
	reverse(b + 1, b + N);
	for(int i = 2; i <= N - 1; ++i)
	{
		bool flag = 0;
		if(u[i])
			continue;
		for(int j = i + 1; j <= N - 1; ++j)
		{
			if(u[j])
				continue;
			if(b[j] <= A + B - b[i])
			{
				u[i] = 1;
				u[j] = 1;
				flag = 1;
				break;
			}
		}
		if(!flag)
			for(int j = i + 1; j <= N - 1; ++j)
				if(!u[j])
				{
					u[i] = 1;
					u[j] = 1;
					ans++;
					break;
				}
	}
	cout << ans << " ";
}
void get_min()
{
	for(int i = 1; i <= N + 2; ++i)
		u[i] = 0;
	int ans = n;
	B = b[N - 1];
	for(int i = 1; i <= N - 2; ++i)
	{
		bool f = 1;
		if(u[i])
			continue;
		for(int j = N - 2; j >= i + 1; --j)
		{
			if(u[j])
				continue;
			if(b[j] > A + B - b[i])
			{
				u[i] = 1;
				u[j] = 1;
				f = 0;
				break;
			}
		}
		if(f)
		{
			for(int j = N - 2; j >= i + 1; --j)
				if(!u[j])
				{
					u[i] = 1;
					u[j] = 1;
					ans--;
					break;
				}
		}
	}
	cout << ans;
}
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= 2 * n; ++i)
	{
		cin >> a[i];
		if(i != 1)
			b[i - 1] = a[i];
	}
	A = a[1];
	N = 2 * n;
	sort(b + 1, b + N);
	get_max();
	get_min();
	return 0;
}

