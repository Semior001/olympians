#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "D"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int ans, l, r, a, n;
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> a;
	if(n == 2)
	{
		for(int i = l; i <= r; ++i)
			for(int j = i; j <= r; ++j)
			{
				int cur = __gcd(i, j), nok = (i * j) / cur;
				if(nok % a == 0)
					ans++;
			}
	}
	else
	{
		for(int i = l; i <= r; ++i)
			if(i % a == 0)
				ans++;
	}
	cout << ans;
	return 0;
}

