#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "F"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int n, a[maxn], d[maxn];
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++i)
		cin >> a[i];
	for(int i = 1; i <= n; ++i)
	{
		for(int j = i + 1; j <= n; ++j)
			if((a[i] & a[j]) == 0)
			{
				d[i]++;
				d[j]++;
			}
	}
	for(int i = 1; i <= n; ++i)
		cout << d[i] << " ";
	return 0;
}

