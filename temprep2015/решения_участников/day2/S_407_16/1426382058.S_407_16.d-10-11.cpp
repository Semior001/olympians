#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "D"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int ans, l, r, a, n;
void go(int pos, int nok, int gcd, int last)
{
	if(pos == n)
	{
		if(nok % a == 0)
		{
			ans++;
		}
		return;
	}
	for(int i = last; i <= r; ++i)
	{
		int cur, cur2;
		if(gcd != -1)
			cur = __gcd(gcd, i), cur2 = (gcd * i) / cur;
		else
			cur = i, cur2 = i;
		go(pos + 1, cur2, cur, i);	
	}
}
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> l >> r >> a;
	go(0, 1, -1, l);
	cout << ans;
	return 0;
}

