#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "F"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
const int N = (int)(100);
using namespace std;
int n, a[maxn], d[maxn], u[maxn];
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++i)
	{
		cin >> a[i];
		u[a[i]]++;
	}
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= 100; ++j)
			if(u[j] && (a[i] & j) == 0)
				d[i] += u[j];
		cout << d[i] << " ";
	}
	return 0;
}


