#include <bits/stdc++.h>
#define fname "A"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;

vector<int>pr;
inline bool isPr(const int &x){
	if(x < 2)return 0;
	if(x == 2)return 1;
	if(x % 2 == 0)return 0;
	int c = sqrt(x);
	for (int i = 3; i <= c; i+=2)
		if(x % i == 0)
			return 0;
	return 1;
}
void era(){
	int n = 32000;
	for (int i = 1; i < 32000; ++i){
		if(isPr(i))pr.pb(i);
	}
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
//	era();
	ll n, x, a, c, l, r;
	cin >> a >> c >> l >> r;
	n = a - c;
	x = sqrt(n);
	vector<pair<int, int> > del;
	for (int i = 1; i <= x; ++i){
		if(n%i == 0){
			del.pb(mp(i, n/i));
		}
	}
	set<ll>s;
	forit (it, del){
		if(it.F >= l && it.F <=r && a % it.F == c){
			s.insert(it.F);
		}
		if(it.S >= l && it.S <=r && a % it.S == c){
			s.insert(it.S);
		}
	}
	cout << sz(s) << "\n";
//	forit(it,s)cout << it << " ";
	return 0;
}
