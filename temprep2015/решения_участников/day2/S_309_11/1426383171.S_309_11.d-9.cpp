#include <bits/stdc++.h>
#define fname "D"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
inline void no(){
	cout <<0;
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
//	era();
	ll n, x, a, c, l, r;
	cin >> a >> c >> l >> r;
	if(c >= a){
		no();
		return 0;
	}
	n = a - c;
	x = sqrt(n);
	vector<pair<ll, ll> > del;
	for (ll i = 1; i <= x; ++i){
		if(n%i == 0){
			del.pb(mp(i, n/i));
		}
	}
	set<ll>s;
	forit (it, del){
		if(it.F >= l && it.F <=r && a % it.F == c){
			s.insert(it.F);
		}
		if(it.S >= l && it.S <=r && a % it.S == c){
			s.insert(it.S);
		}
	}
	cout << sz(s) << "\n";
//	forit(it,s)cout << it << " ";
	return 0;
}
