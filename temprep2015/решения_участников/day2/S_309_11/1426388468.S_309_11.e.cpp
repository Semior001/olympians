#include <bits/stdc++.h>
#define fname "E"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()
#define allr(s) s.rbegin(), s.rend()
using namespace std;

typedef long long ll;
typedef double ld;
const int N = 1e5 + 100;
const int INF = 1e9 + 7;

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int n, b[N], nn;
	cin >> n;
	if(n == 1){
		cout << 1 << " " << 1;
		return 0;
	}
	nn = n;
	n *= 2;
	for (int i = 0; i < n; i++){
		cin >> b[i];
	}
	int f = b[0], l = INF, r = -l, pl, pr;
	sort(b + 1, b + n);
	l = b[1];
	r = b[n-1];
	pl = 1;
	pr = n - 1;
	vi c, d;
	for (int i = 1; i < n; i++){
		if(i != pl)c.pb(b[i]);
		if(i != pr)d.pb(b[i]);
	}
//	forit(it, c)cout << it << " ";
//	cout << "\n";
//	forit(it, d)cout << it << " ";
//	return 0;
	int q = 1, w = 1, minsum = f + l, maxsum = f + r, mn = INF, mx = -INF;
	for (int i = 0; i < sz(c); i+=2){
		if(c[i] + c[i + 1] > minsum)q++;
		if(d[i] + d[i + 1] > maxsum)w++;
	}
	mn = min(q, mn);
	mx = max(w, mx);
	while(next_permutation(all(c))){
	    q = 1;
		for (int i = 0; i < sz(c); i+=2){
		if(c[i] + c[i + 1] > minsum)q++;
//		if(d[i] + d[i + 1] > maxsum)w++;
	}
	mn = min(q, mn);
//	mx = max(w, mx);
	}
	while(next_permutation(all(d))){
			w = 1;
			for (int i = 0; i < sz(d); i+=2){
//		if(c[i] + c[i + 1] > minsum)q++;
		if(d[i] + d[i + 1] > maxsum)w++;
	}
//	mn = min(q, mn);
	mx = max(w, mx);
	}
	swap(mn, mx);
	cout << mn << " " << mx;
	return 0;
}