#include <bits/stdc++.h>
#define fname "D"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;

int main(){
	ifstream cin(fname".in");
	ofstream cout(fname".out");
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
        ll a, c, l, r, W = 1e7;
	cin >> a >> c >> l >> r;
	ll ans = 0, pos = -1;
	map<int, char>used;
	for (int i = l; i <= r; ++i)
		if(a % i == c){if(pos == -1)pos = i;break;}//cout << i << " ";}
	queue<int>q;
	q.push(pos);
	if(pos != -1)
	while(!q.empty()){
		int v = q.front();
		q.pop();
		for (int i = max(v - W, l); i <= min(v + W, r); ++i){
			if(a % i == c && !used[i]){
				if(i*2 <= r && !used[i * 2])q.push(i * 2);
				used[i] = 1;
				ans ++;
			}
		}
	}
	cout << ans;
//	cout << "\n" << clock()*1.0/CLOCKS_PER_SEC*1.0;
	return 0;
}
