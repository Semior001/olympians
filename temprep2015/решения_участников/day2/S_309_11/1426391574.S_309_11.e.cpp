#include <bits/stdc++.h>
#define fname "E"

#define pb emplace_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 100;

bool cmp(pair<int,int> a, pair<int,int> b){
	if(a.F != b.F)return a.F > b.F;
	return a.S < b.S;
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
        int n;
	vector <pair<int, int> >  a, cur;
	int x;
	cin >> n;
	n*=2;
	for (int i = 0; i < n; ++i){
		cin >> x;
		a.pb(x, i + 1);
	}
	int mn = (n>>1), mx = 1, ok = 1;                                      

	sort(all(a));
	reverse(all(a));

		cur.clear();
		for(int i = 0; i < sz(a); i+=2){
			cur.pb(a[i].F + a[i+1].F, min(a[i].S, a[i+1].S));
		}
		
		sort(all(cur), cmp);
		for(int i = 0; i < sz(cur); ++i)if(cur[i].S == 1){mn = min(mn, i + 1),mx = max(mx, i + 1);break;}
	while(prev_permutation(all(a))){
		if(clock()*1.0/CLOCKS_PER_SEC >= 0.497){
			cout << mn << " " << mx;
			return 0;
		}
		cur.clear();
		for(int i = 0; i < sz(a); i+=2){
			cur.pb(a[i].F + a[i+1].F, min(a[i].S, a[i+1].S));
		}
		
		sort(all(cur), cmp);
		for(int i = 0; i < sz(cur); ++i)if(cur[i].S == 1){mn = min(mn, i + 1),mx = max(mx, i + 1);break;}
	}
	cout << mn << " " << mx;                               
	return 0;
}
