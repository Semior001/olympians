#include <bits/stdc++.h>
#define fname "D"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
//	ios_base::sync_with_stdio(0);
        ll a, c, l, r;
	cin >> a >> c >> l >> r;
	ll ans = 0, pos = -1;
	for (int i = l; i <= r; ++i)
		if(a % i == c){if(pos == -1)pos = i;break;}//cout << i << " ";}
	if(pos != -1)
	for (int i = pos; i <= min(r, a); i+=pos){
		ans++;
	}
	cout << ans;
	return 0;
}
