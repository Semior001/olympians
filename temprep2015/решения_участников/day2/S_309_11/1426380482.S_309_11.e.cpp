#include <bits/stdc++.h>
#define fname "E"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 100;

bool cmp(pair<int,int> a, pair<int,int> b){
	if(a.F != b.F)return a.F > b.F;
	return a.S < b.S;
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
        int n;
	vector <pair<int, int> >  a, cur;
	int x;
	cin >> n;
	n*=2;
	for (int i = 0; i < n; ++i){
		cin >> x;
		a.pb(mp(x, i + 1));
	}
	int mn = 1e9 + 8, mx = -mn, ok = 1;                                      

	sort(all(a));
	while(next_permutation(all(a))){
		cur.clear();
		for(int i = 0; i < sz(a); i+=2){
			cur.pb(mp(a[i].F + a[i+1].F, min(a[i].S, a[i+1].S)));
		}
		
		sort(all(cur), cmp);
//		if (ok)forit(it, cur)cout << it.S <<" ";
//		ok = 0;
		for(int i = 0; i < sz(cur); ++i)if(cur[i].S == 1)mn = min(mn, i + 1),mx = max(mx, i + 1);
	}
	cout << mn << " " << mx;                               
	return 0;
}
