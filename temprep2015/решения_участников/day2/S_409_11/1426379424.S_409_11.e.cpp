#include<bits/stdc++.h>
#define pb(a) push_back(a)
#define mp(a,b) make_pair(a,b)
#define ob pop_back()
#define vvi vector<vector<int> >
#define vi vector<int>
#define ff first
#define ss second
#define ldd long long int
#define ld double
#define pii pair<int,int>
using namespace std;
ld avg;
vector<double> v;
vector<bool>u;
bool find_max(ld x,int l,int r,int vl){
   // cout<<x<<" "<<vl<<" "<<l<<" "<<r<<endl;
    if(l>r)return 0;
    if(l==r){
            if(u[l-1]==true && vl==-1)return false;
            if(!u[l-1])vl=l;
            u[vl-1]=true;
            return true;
    }
    if(r-l==1){
        if(u[r-1]==true && u[l-1]==true){
            if(vl==-1)return false;
            else {
                u[vl-1]=true;
                return true;
            }
        }
        else if(u[l-1]==true || (v[l-1]+x)/2<=avg){
            if((v[r-1]+x)/2<=avg || u[r-1])return false;
            u[r-1]=true;
            return true;
        }
        else {
            u[l-1]=true;
            return true;
        }
    }
    int m=((l+r)/2)-1;
  //  cout<<m<<" -M";
    if((v[m]+x)/2<=avg)return find_max(x,m+2,r,vl);
    else if((v[m]+x)/2>avg && !u[m])return find_max(x,l,m+1,m+1);
    else return find_max(x,l,m+1,vl);

}
int main(){
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    int n,k;
    cin>>n;
    cin>>k;
    for(int i=2;i<=2*n;i++){
        double a;
        cin>>a;
        if(i>1)v.push_back(a);
    }
    sort(v.begin(),v.end());
    avg=(k+v.back())/2;
    int amin=1,amax=n;
    int l=0,r=v.size()-2;
    while(l<r){
        if((v[l]+v[r])/2<=avg)l++,r--,amax--;
        else r--;
    }
    cout<<amax<<" ";
    avg=((v[0]+k)/2);
    u.assign(2*n+1,false);
    for(int i=1;i<v.size();i++){
        if(u[i]==true)continue;
        if(find_max(v[i],2,2*n-1,-1)==true)amin++;
    }
    cout<<amin;
    return 0;
}
