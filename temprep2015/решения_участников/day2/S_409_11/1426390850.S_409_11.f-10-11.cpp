#include<bits/stdc++.h>
#define pb(a) push_back(a)
#define mp(a,b) make_pair(a,b)
#define ob pop_back()
#define vvi vector<vector<int> >
#define vi vector<int>
#define ff first
#define ss second
#define ldd long long int
#define ld long double
#define pii pair<int,int>
using namespace std;
bool fun(int x,int y){
    while(x>0 && y>0){
        if(x%2==y%2 && x%2==1)return 1;
        x/=2;
        y/=2;
    }
    return 0;
}
int main(){
    freopen("F.in","r",stdin);
    freopen("F.out","w",stdout);
    int n,a[100000];
    cin>>n;
    for(int i=1;i<=n;i++){
        cin>>a[i];
    }
    int ans[100000]={0};
    for(int i=1;i<=n;i++){
        for(int j=i+1;j<=n;j++){
            if(fun(a[i],a[j])==0)ans[i]++,ans[j]++;
        }
    }
    for(int i=1;i<=n;i++){
        cout<<ans[i]<<" ";
    }
    return 0;
}
