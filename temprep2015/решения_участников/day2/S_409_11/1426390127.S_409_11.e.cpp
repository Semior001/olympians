#include<bits/stdc++.h>
#define pb(a) push_back(a)
#define mp(a,b) make_pair(a,b)
#define ob pop_back()
#define vvi vector<vector<int> >
#define vi vector<int>
#define ff first
#define ss second
#define ldd long long int
#define ld double
#define pii pair<int,int>
using namespace std;
double avg;
vector<double> v;
vector<bool>u;
int fin(int l,int r,int x){
    int m=(l+r)/2;
   // cout<<l<<" "<<r<<endl;
    if(l>r)return 0;
    if(l==r){
        if(u[l])return 0;
        else if((v[l]+x)/2>avg){
            u[l]=true;
            return l;
        }
        else return 0;
    }
    else if(u[m] && (v[m]+x)/2>avg){
        int q=fin(l,m,x);
        if(q==0)return fin(m+1,r,x);
        else return q;
    }
    else if((v[m]+x)/2>avg){
        return fin(l,m,x);
    }
    else return fin(m+1,r,x);
}
int main(){
    freopen("E.in","r",stdin);
    freopen("E.out","w",stdout);
    int n,k;
    cin>>n;
    cin>>k;
    for(int i=2;i<=2*n;i++){
        double a;
        cin>>a;
        if(i>1)v.push_back(a);
    }
    sort(v.begin(),v.end());
    avg=(k+v.back())/2;
    int amin=1,amax=n;
    int l=0,r=v.size()-2;
    while(l<r){
        if((v[l]+v[r])/2<=avg)l++,r--,amax--;
        else r--;
    }
    cout<<amax<<" ";
    avg=((v[0]+k)/2);
    u.assign(2*n+1,false);
    int t=v.size(),g=1;
    for(int i=1;i<v.size();i++){
        if(u[i])continue;
        u[i]=true;
        int q=fin(i+1,t-1,v[i]);
        if(q>0)amin++,t=q;
        else{
            int y=g;
            while(u[y])y++;
            if(y<v.size() && (v[y]+v[i])/2>avg)amin++,t=y;
            g=y;
        }
    }
    cout<<amin;
    return 0;
}
