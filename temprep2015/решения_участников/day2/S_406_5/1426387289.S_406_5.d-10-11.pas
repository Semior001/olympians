program D;
type mass = array[1..100] of longint;
var 	N, L, R, A: longint;
        Ns: mass;
	f: text;

function ModX(var Arr: mass):boolean;
var i, M: longint;
begin
        M:=0;
        for i:=1 to n do
                M:=M*Arr[i];
        if M mod A = 0 then ModX:=true
                else ModX:=false;
end;

function Count:longint;
var 	i, j, C:longint;
begin
        C:=0;
       	Case N of
		1: 	begin
				for i:=L to R do
					if i mod A = 0 then C:=C+1;
			end;
		2: 	begin
				for i:=L to R do
					for j:=i to R do
						if (i*j) mod A = 0 then C:=C+1;
			end;
	end;
        Count:=C mod (1000000000 + 7);
end;

begin
	assign(f, 'D.in');
	reset(f);
	readln(f, N, L, R, A);
	close(f);
	
	assign(f, 'D.out');
	rewrite(f);
	writeln(f, Count);
	close(f);
end.
