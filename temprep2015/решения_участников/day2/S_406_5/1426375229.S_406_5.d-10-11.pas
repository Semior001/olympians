program D;

var 	N, L, R, A: longint;
	f: text;

function Count:longint;
var i, j, C:longint;
begin
        C:=0;
	for i:=L to R-1 do
		for j:=i to R do
                        begin
			        if ((i*j) mod A) = 0 then
                                        begin
                                                C:=C+1;
                                                writeln(i*j);
                                        end;
                        end;
        Count:=C mod (1000000000 + 7);
end;

begin
	assign(f, 'D.in');
	reset(f);
	readln(f, N, L, R, A);
	close(f);
	
	assign(f, 'D.out');
	rewrite(f);
	writeln(f, Count);
	close(f);
end.
