program E;

const nmax = 100000;

type    arr = array[1..2*nmax-1] of longint;
        teams = array[1..(2*nmax-1)] of longint;
var 	N, A: longint;
	plrs: arr;
	f:text;

procedure ReadArr(var A: arr);
var i: longint;
begin
	for i:=1 to (2*n)-1 do
		read(f, A[i]);
end;

procedure Sort(var A: arr);
var i, j, m: longint;
begin
	for i:=1 to n-1 do
		for j:=i to n do
			if A[i]<A[j] then
				begin
				        m:=A[i]; A[i]:=A[j]; A[j]:=m;
				end
end;

procedure Ssort(var A: teams);
var i, j, m: longint;
begin
        for i:=1 to (2*N-1)-1 do
		for j:=i to (2*N-1) do
			if A[i]<A[j] then
				begin
				        m:=A[i]; A[i]:=A[j]; A[j]:=m;
                                end
                        else
                                if A[i] = A[j] then
                                        A[i]:=0;
        for i:=1 to (2*N-1)-1 do
                if A[i]=0 then
                        begin
                             A[i]:=A[i+1];
                             A[i+1]:=0;
                        end;
end;
function Max(var A: teams):longint;
var m, i: longint;
begin
        max:=1;
end;

function min(var A: teams):longint;
var m, i: longint;
begin
        for i:=(2*n-1) downto 1 do
                if A[i]<>0 then
                        begin
                                min:=i;
                                break
                        end
end;

procedure Result;
var     i, k:longint;
        score: teams;
begin
        Sort(plrs);
        k:=1;
        for i:=1 to (2*N-1) do
              begin
                score[k]:=(A+plrs[i]) div 2;
                k:=k+1;
              end;
        Ssort(score);
        writeln(f, Max(score),' ',min(score));
end;

begin
	assign(f, 'E.in');
	reset(f);
	readln(f, N);
	read(f, A);
	ReadArr(plrs);
	close(f);

	assign(f, 'E.out');
	rewrite(f);
	Result;
	close(f);
end.
