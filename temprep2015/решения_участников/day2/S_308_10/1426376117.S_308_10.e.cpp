#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "E"

using namespace std;
int k1, k2;
int main()
{
	freopen(TASK".in", "rt", stdin);
#ifdef EVAL
	freopen(TASK".out", "wt", stdout);
#endif
    int n;
    cin >> n;
    vector<int> a(2*n);
    for (int i = 0; i < 2*n; ++i)
    	cin >> a[i];
	sort(a.begin() + 1, a.end());
	double ourMax = (a[0] + a[a.size() - 1]) / 2.0,
	       ourMin = (a[0] + a[1]) / 2.0;
	for (int i = 1; i < n; ++i)
		if ((a[i] + a[a.size() - 1 - i]) / 2.0 > ourMax)
			k1++;
	for (int i = 2; i < (int)a.size(); i+=2)
	{
		if ((a[i] + a[i + 1]) / 2.0 > ourMin)
			k2++; 
	}
	cout << k1+1 << ' ' << k2+1;
	return 0;
}