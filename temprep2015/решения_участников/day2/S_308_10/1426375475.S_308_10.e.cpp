#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "E"

using namespace std;
int k1, k2;
int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);

    int n;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < 2*n; ++i)
    	cin >> a[i];
	sort(a.begin() + 1, a.end());
	double ourMax = (a[0] + a[a.size() - 1]) / 2.0,
	       ourMin = (a[0] + a[1]) / 2.0;
	for (int i = 1; i < n; ++i)
		if ((a[i] + a[a.size() - 1 - i]) / 2.0 > ourMax)
			k1++;
	int l = 2, r = 2*n - 1;
	while (l <= r)
	{
	 	if ((a[l] + a[r]) / 2.0 > ourMin)
	 		k2++;
	    l++, r--;
	}
	cout << k1+1 << ' ' << k2+1;
	return 0;
}