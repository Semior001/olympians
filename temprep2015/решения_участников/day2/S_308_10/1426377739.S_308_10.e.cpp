#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "E"

using namespace std;

int k1, k2;
bool used[1000000];

int main()
{
	freopen(TASK".in", "rt", stdin);
#ifdef EVAL
	freopen(TASK".out", "wt", stdout);
#endif
    int n;
    cin >> n;
    
    vector<int> a(2*n);
    for (int i = 0; i < 2*n; ++i)
    	cin >> a[i];
	
	sort(a.begin() + 1, a.end());
	
	double ourMax = (a[0] + a[a.size() - 1]) / 2.0,
	       ourMin = (a[0] + a[1]) / 2.0;
	for (int i = 1; i < (int)a.size() - 1; ++i)
		if(!used[i])
		{
			int f = 0;
			for (int j = i + 1; j < (int)a.size() - 1; ++j)
			{
				if (!used[j] && (a[i] + a[j]) / 2.0 <= ourMax)
					f = j;
			}
			if (f)
				used[f] = 1;
			else
				k1++;

		}
	memset(used, 0, sizeof used);
	for (int i = 2; i < (int)a.size(); ++i)
		if(!used[i])
			for (int j = i + 1; j < (int)a.size(); ++j)
				if (!used[j] && (a[i]+a[j])/2.0 > ourMin)
				{
			 		#ifndef EVAL
				 		cout << a[i] << ' ' << a[j] << endl;
				 	#endif
				 	k2++;
				 	used[j] = 1;
			 		used[i] = 1;
				 	break;	
				}

	cout << k1 + 1 << ' ' << k2 + 1;
	return 0;
}