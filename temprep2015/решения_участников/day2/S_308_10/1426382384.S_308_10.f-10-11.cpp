#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "F"

using namespace std;

int k[4000001], cnt[4000001];

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);
	
	int n;
	cin >> n;
	vector<int> a(n), b;	
	for (int i = 0; i < n; ++i)
		{cin >> a[i];
		cnt[a[i]]++;
		}
	b = a;
	sort(a.begin(), a.end());
	int newN = unique(a.begin(), a.end()) - a.begin();
	cerr << newN;
	for (int i = 0; i < newN; ++i)
		for (int j = 0; j < newN; ++j)
			if (!(a[i] & a[j]))
				k[a[i]] += cnt[a[j]];
	for (int i = 0; i < n; ++i)
		cout << k[b[i]] << ' ';	
	return 0;
}