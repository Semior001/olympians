#include <iostream>
#include <fstream>
#include <cstring>
#include <cassert>

using namespace std;

int n, a[1000000], p[1000000], myMax, myMin, k1 = (int)1e9, k2 = -1;
bool used[1000000];

void find1(int v)
{
 	if (used[v])
 		return;
    used[v] = 1;
	for (int i = 0; i < n; ++i)
	{
		if (!used[i])
		{
			int pi = p[i], pv = p[v];
		 	used[i] = 1;
			p[i] = v;
			p[v] = i;
			bool f = 0;
			for (int j = 0; j < n; ++j)
				if (!used[j]) 
				{
					find1(j);
					f = 1;
					break;
				}
			if (!f)
			{
				int k = 0;
				for (int i = 0; i < n; ++i)
				{
				 	if (p[i])
				 		if (a[i] + a[p[i]] > myMax)
				 			k++; 			
				}
				k /= 2;
				if (k < k1)
					k1 = k;	 	
			}	
			p[i] = pi;
			p[v] = pv;
		 	used[i] = 0;      
		}
	}
	used[v] = 0;
}

void find2(int v)
{
 	if (used[v])
 		return;
    used[v] = 1;
	for (int i = 0; i < n; ++i)
	{
		if (!used[i])
		{
			int pi = p[i], pv = p[v];
		 	used[i] = 1;
			p[i] = v;
			p[v] = i;
			bool f = 0;
			for (int j = 0; j < n; ++j)
				if (!used[j]) 
				{
					find2(j);
					f = 1;
					break;
				}
			if (!f)
			{
				int k = 0;
				for (int i = 0; i < n; ++i)
				{
				 	if (p[i])
				 		if (a[i] + a[p[i]] > myMin)
				 			k++; 			
				}
				k /= 2;
				if (k > k2)
					k2 = k;	 	
			}	
			p[i] = pi;
			p[v] = pv;
		 	used[i] = 0;      
		}
	}
	used[v] = 0;
}

int main()
{
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	int q = 0;  
	while (cin >> n)
	{

	k1 = (int)1e9, k2 = 0;
	q++;
	n *= 2;
	for (int i = 0; i < n; ++i)
		cin >> a[i];		
	if (q == 4681)
		for (int i = 0; i < n; ++i)
			cout << a[i] << ' ';
 	for (int k = 1; k < n; ++k)
 	{
 	    memset(used, 0, sizeof used);
		memset(p,    0, sizeof p);
 		myMax = a[0] + a[k];
 		used[0] = used[k] = 1;
		for (int i = 1; i < n; ++i)
			if (!used[i])
			{
				find1(i);
				break;
			}
	}
	for (int k = 1; k < n; ++k)
	{
		memset(used, 0, sizeof used);
		memset(p,    0, sizeof p);
 		myMin = a[0] + a[k];
 		used[0] = used[k] = 1;
		for (int i = 1; i < n; ++i)
			if (!used[i])
			{
				 find2(i);
				 break;
			}
	}                                        
	assert(k1 <= k2);
	//assert(k1 <= n / 2);
	//assert(k2 <= n / 2);
	cout << min(k1,k2)+1 << ' ' << max(k1,k2)+1 << endl;
	} 	 
 	return 0;
}