#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "D"

using namespace std;

int gcd(int a, int b)
{
 	if (a > b)
 		swap(a, b);
 	if (a == 0)
 		return b;
 	return gcd(b % a, a);
}

int NOK(int a, int b)
{
 	return a*b / gcd(a, b);
}

int ans, n, a, l, r;

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);

	cin >> n >> l >> r >> a;
	if (n == 1)
	{ 	for (int i = l; i <= r; ++i)
	 	 	if (i % a == 0)
	 	 		ans++;
	}
	else
		for (int i = l; i <= r; ++i)
			for (int j = i; j <= r; ++j)
				if (NOK(i, j) % a == 0)
				{
					ans++;
				}
	cout << ans;	
	return 0;
}