#include <bits/stdc++.h>
#define ll long long
#define maxn (int)(1e5 + 1e1)

ll n, h[maxn], c[maxn], ans, minn;

using namespace std;

int main(){
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);

	cin >> n;
	for (int i = 1; i <= n; i++) 
		cin >> h[i];
	for (int i = 1; i <= n; i++)
		cin >> c[i];

	for (int i = 1; i < n; i++) {
	        for (int j = 1; j < n; j++) {
	         	if (h[j] > h[j + 1]) {
	         		swap (h[j], h[j + 1]);
				swap (c[j], c[j + 1]);
				ans = c[j + 1];
			}  
		}
	}

	cout << ans;

	return 0;
}