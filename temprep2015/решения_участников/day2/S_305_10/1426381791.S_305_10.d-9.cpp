#include <iostream>
#include <math.h>
#include <stdio.h>
#define ll long long

using namespace std;

void sol1 (ll a, ll b, ll l, ll r) {
	int cnt = 0;
	a -= b;
	for (int x = l; x * x <= r; x++) {
		if (a % x == 0) {
			if (x > b)
				cnt++; 	
	       		int d = a / x;
			if (d != x && d > b)
				cnt++;
		}
	}
	cout << cnt;
}

void sol2 (ll a, ll b, ll l, ll r) {
	int cnt = 0;
	a -= b;
	for (int x = l; x <= r; x++) {
		if (a % x == 0) {
			cnt++; 	
		}
	}
	cout << cnt;
}



ll a, b, l, r, cnt, d;

int main(){
        freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	
	ios_base::sync_with_stdio(false);
	cin.tie(0);

	cin >> a >> b >> l >> r;

	if (a == b || a < b) {
		cout << 0;
		return 0;
	}

	
	if (l < sqrt(r))
		sol1(a, b, l, r);
	if (l >= sqrt(r))
		sol2(a, b, l, r);
	return 0;
}