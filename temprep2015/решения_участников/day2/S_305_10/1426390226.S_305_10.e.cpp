#include <bits/stdc++.h>

#include <algorithm>

#define ll long long
#define maxn (int)(1e5 + 1e1)

using namespace std;

int n, a[7], ant = 1, cnt = 1;

int main() {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	
	cin >> n;
	for (int i = 1; i <= n * 2; i++) {
		cin >> a[i];
	}
	
	for (int i = 1; i < n * 2; i++) {
		for (int j = 1; j < 2 * n; j++) {
			if (a[i] + a[j] == a[j + 1] + a[j + 2])
			{
				cout << 1 <<' '<< 2;
				return 0;
			}	
		}
		
	}

        sort (a + 1, a + 2 * n + 1);
	for (int i = 1; i < n * 2; i++) {
	        if (a[i] == a[i + 1])
			ant++;
		else {
			cnt++;
		}
	}
	
	if (ant == n * 2) {
		cout << 1 <<' '<< 1;
		return 0;
	}
	
	if (cnt == n * 2) {
		cout << 1 <<' '<< 3;
		return 0;
	}

}
