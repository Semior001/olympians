#include <iostream>
#include <math.h>
#include <stdio.h>
#define ll long long

using namespace std;

void sol1 (ll a, ll b, ll l, ll r) {
	ll cnt = 0;
	a -= b;
	for (ll x = l; x * x <= r; x++) {
		if (a % x == 0) {
			if (x > b)
				cnt++; 	
	       		int d = a / x;
			if (d != x && d > b)
				cnt++;
		}
	}
	cout << cnt;
}

void sol2 (ll a, ll b, ll l, ll r) {
	ll cnt = 0;
	a -= b;
	for (ll x = l; x <= r; x++) {
		if (a % x == 0) {
			if (x > b)
				cnt++; 	
	       		int d = a / x;
			if (d != x && d > b)
				cnt++;
		}
	}
	cout << cnt;
}



ll a, b, l, r, cnt, d;

int main(){
        freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);

	cin >> a >> b >> l >> r;
	
	if (l * l <= r)
		sol1(a, b, l, r);
	if (l * l > r)
		sol2(a, b, l, r);
	return 0;
}