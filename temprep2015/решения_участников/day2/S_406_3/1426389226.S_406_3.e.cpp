#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "E"

const int MaxN = (int)(2e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], p[MaxN], n;

struct team {
	int first, second;
	double average;
	bool operator < (team t) const {
		return average > t.average;
	}
	team (int x, int b) {
		first = x;
		second = b;
		average = (a[x] + a[b] + 0.0) / 2.0;
	}
};

bool cmp (int x, int y) {
	return a[x] < a[y];
}

pair <int, int> get (vector <int> &p) {
	int MinAns = n + 1, MaxAns = 1;
	vector <team> v;
	for (int i = 0; i < n + n; i += 2) {
		v.push_back (team (p[i], p[i + 1]));
	}
	sort (all (v));
	double cur = v[0].average;
	int pos = 1;
	if (!v[0].first || !v[0].second) {
		MinAns = min (MinAns, pos);
		MaxAns = max (MaxAns, pos);
	}
	for (int i = 1; i < n; ++i) {
		if (v[i].average != cur) {
			cur = v[i].average;
			pos++;
		}
		if (!v[i].first || !v[i].second) {
			MinAns = min (MinAns, pos);
			MaxAns = max (MaxAns, pos);
		}
	}
	return make_pair (MinAns, MaxAns);
}

int getMax () {
	vector <int> v; for (int i = 0; i < n + n; ++i) v.push_back (i);
	sort (v.begin() + 1, v.end(), cmp);
	vector <int> q; int l = 0, r = n + n - 1; while (l < r) { q.push_back (v[l++]); q.push_back (v[r--]); }
	/* for (int i = 0; i < q.size(); ++i)
		cout << a[q[i]] << " ";
	cout << endl; */
	pair <int, int> res = get (q);
	return min (res.first, res.second);
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d", &n);

	for (int i = 0; i < n + n; ++i) {
		scanf ("%d", a + i);
		p[i] = i;
	}

	int MinAns = n + 1, MaxAns = 1;
	do {
		vector <team> v;
		for (int i = 0; i < n + n; i += 2) {
			v.push_back (team (p[i], p[i + 1]));
		}
		sort (all (v));
		double cur = v[0].average;
		int pos = 1;
		if (!v[0].first || !v[0].second) {
			MinAns = min (MinAns, pos);
			MaxAns = max (MaxAns, pos);
		}
		for (int i = 1; i < n; ++i) {
			if (v[i].average != cur) {
				cur = v[i].average;
				pos++;
			}
			if (!v[i].first || !v[i].second) {
				MinAns = min (MinAns, pos);
				MaxAns = max (MaxAns, pos);
			}
		}
	} while (next_permutation (p, p + n + n));
	
	cout << getMax() << " " << MaxAns;
	
	return 0;
}
