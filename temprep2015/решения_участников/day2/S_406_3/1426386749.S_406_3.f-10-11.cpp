#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "F"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;
const int K = 22;

int a[MaxN], cnt[4 * (int)(1e6)];

struct node {
	int end, nxt[2];
	int cnt;
} t[500500];

int sz = 1, x;
int calced[4 * (int)(1e6)];

inline void insert (const int &x) {
	int v = 1;

	for (int i = K; i >= 0; --i) {
		bool c = (x & (1 << i));
		if (!t[v].nxt[c])
			t[v].nxt[c] = ++sz;
		v = t[v].nxt[c];
	}
	t[v].end = cnt[x];
}

int get (int &x, int v, int pos, int bits) {
	if (t[v].nxt[0] + t[v].nxt[1] == 0)
		return t[v].end;
	int res = 0;
	bool c = (x & (1 << pos));
	if (c) {
		if (t[v].nxt[0]) {
			res += get (x, t[v].nxt[0], pos - 1, bits - 1);
		}	
	} else {
		if (!bits) {
			res += t[v].cnt;
		} else {
			if (t[v].nxt[0]) {
				res += get (x, t[v].nxt[0], pos - 1, bits);
			}
			if (t[v].nxt[1]) {
				res += get (x, t[v].nxt[1], pos - 1, bits);
			}	
		}
	}
	return res;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	ios_base :: sync_with_stdio (false);
	cin.tie (NULL);

	int n; cin >> n; // scanf ("%d", &n);

	for (int i = 0; i < n; ++i) {
		// scanf ("%d", a + i);
		cin >> a[i];
		calced[a[i]] = -1;
		cnt[a[i]]++;
	}

	for (int i = 0; i < n; ++i) {
		if (cnt[a[i]] == -1)
			continue;
		insert (a[i]);
		cnt[a[i]] = -1;
	}

	for (int i = sz; i >= 1; --i) {
		t[i].cnt = t[i].end + t[t[i].nxt[0]].cnt + t[t[i].nxt[1]].cnt;
	}

	for (int i = 0; i < n; ++i) {
		if (~calced[a[i]])
			cout << calced[a[i]] << " "; // printf ("%d ", calced[a[i]]);
		else {
			cout << (calced[a[i]] = get (a[i], 1, K, __builtin_popcount (a[i]))) << " "; // printf ("%d ", calced[a[i]] = get (a[i], 1, K));
		}
	}

	return 0;
}
