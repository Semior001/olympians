#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "F"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;
const int K = 22;

int a[MaxN];

struct node {
	int end, nxt[2];
} t[500500];

int sz = 1, x;
int calced[4 * (int)(1e6)];

void insert () {
	int v = 1;

	for (int i = K; i >= 0; --i) {
		bool c = (x & (1 << i));
		if (!t[v].nxt[c])
			t[v].nxt[c] = ++sz;
		v = t[v].nxt[c];
	}
	t[v].end++;
}

int get (int v, int pos) {
	if (!t[v].nxt[0] && !t[v].nxt[1])
		return t[v].end;
	int res = 0;
	bool c = (x & (1 << pos));
	if (c) {
		if (t[v].nxt[0]) {
			res += get (t[v].nxt[0], pos - 1);
		}	
	} else {
		if (t[v].nxt[0]) {
			res += get (t[v].nxt[0], pos - 1);
		}
		if (t[v].nxt[1]) {
			res += get (t[v].nxt[1], pos - 1);
		}	
	}
	return res;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 0; i < n; ++i) {
		scanf ("%d", a + i);
		calced[a[i]] = -1;
		insert ();
	}

	for (int i = 0; i < n; ++i) {
	    x = a[i];
		if (~calced[x])
			printf ("%d ", calced[x]);
		else
			printf ("%d ", calced[x] = get (1, K));
	}

	return 0;
}
