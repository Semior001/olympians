#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "F"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], cnt[MaxN];

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 0; i < n; ++i) {
		scanf ("%d", a + i);
		for (int j = 0; j < i; ++j)
			if (!(a[i] & a[j])) {
				cnt[i]++; cnt[j]++;
			}
		// printf ("%d ", cnt);	
	}

	for (int i = 0; i < n; ++i)
		cout << cnt[i] << " ";
	
	return 0;
}
