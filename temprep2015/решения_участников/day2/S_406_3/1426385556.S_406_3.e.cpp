#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "E"

const int MaxN = (int)(2e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], p[MaxN];
int n;

struct team {
	int first, second;
	double average;
	bool operator < (team t) const {
		return average > t.average;
	}
	team (int x, int b) {
		first = x;
		second = b;
		average = (a[x] + a[b] + 0.0) / 2.0;
	}
};

pair <int, int> get (vector <int> &p) {
	int MinAns = n + 1, MaxAns = 1;
	vector <team> v;
	for (int i = 0; i < n + n; i += 2) {
		v.push_back (team (p[i], p[i + 1]));
	}
	sort (all (v));
	double cur = v[0].average;
	int pos = 1;
	if (!v[0].first || !v[0].second) {
		MinAns = min (MinAns, pos);
		MaxAns = max (MaxAns, pos);
	}
	for (int i = 1; i < n; ++i) {
		if (v[i].average != cur) {
			cur = v[i].average;
			pos++;
		}
		if (!v[i].first || !v[i].second) {
			MinAns = min (MinAns, pos);
			MaxAns = max (MaxAns, pos);
		}
	}
	return make_pair (MinAns, MaxAns);
}

bool cmp (int x, int y) {
	return a[x] < a[y];
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d", &n);

	vector <int> v;

	for (int i = 0; i < n + n; ++i) {
		scanf ("%d", a + i);
		v.push_back (i);
	}

	int MinAns = n + 1, MaxAns = 1;
	
	// int MinPos = min_element (a, a + n + n) - a;
	// int MaxPos = max_element (a, a + n + n) - a;
	
	sort (v.begin() + 1, v.end(), cmp);
	swap (v[n + n - 1], v[1]);
	sort (v.begin() + 2, v.end(), cmp);

	vector <int> q = {v[0], v[1]};
	int l = 2, r = n + n - 1;
	while (l < r) {
		q.push_back (v[l++]);
		q.push_back (v[r--]);
	}
	
	pair <int, int> x = get (q);
	MinAns = min (MinAns, x.first);
	MaxAns = max (MaxAns, x.second);
	
	sort (v.begin() + 1, v.end(), cmp);
	sort (v.begin() + 2, v.end(), cmp);
	x = get (v);
	MinAns = min (MinAns, x.first);
	MaxAns = max (MaxAns, x.second);
	cout << MinAns << " " << MaxAns;
	
	return 0;
}
