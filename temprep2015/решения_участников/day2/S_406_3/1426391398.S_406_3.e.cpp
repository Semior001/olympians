#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "E"

const int MaxN = (int)(2e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], p[MaxN], n;
int MinAns, MaxAns = 1;

struct team {
	int first, second;
	double average;
	bool operator < (team t) const {
		return average > t.average;
	}
	team (int x, int b) {
		first = x;
		second = b;
		average = (a[x] + a[b] + 0.0) / 2.0;
	}
};

void check (vector <int> &p) {
	// int MinAns = n + 1, MaxAns = 1;
	vector <team> v;
	for (int i = 0; i < n + n; i += 2) {
		v.push_back (team (p[i], p[i + 1]));
	}
	sort (all (v));
	double cur = v[0].average;
	int pos = 1;
	if (!v[0].first || !v[0].second) {
		MinAns = min (MinAns, pos);
		MaxAns = max (MaxAns, pos);
	}
	for (int i = 1; i < n; ++i) {
		if (v[i].average != cur) {
			cur = v[i].average;
			pos++;
		}
		if (!v[i].first || !v[i].second) {
			MinAns = min (MinAns, pos);
			MaxAns = max (MaxAns, pos);
		}
	}
	// return make_pair (MinAns, MaxAns);
}

vector <int> v;
bool used[MaxN];

void go (int x) {
	// cerr << x << endl;
	if (x == n + n) {
		check (v);
		return;
	}
	for (int i = 1; i < n + n; ++i) {
		if (used[i])
			continue;
		used[i] = true;
		v.push_back (i);
		go (x + 1);
		v.pop_back();
		used[i] = false;
	}
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d", &n);

	srand (time (0));

	for (int i = 0; i < n + n; ++i) {
		scanf ("%d", a + i);
		p[i] = i;
	}
	
	MinAns = n;

	v.push_back (0);
	go (1);

	cout << MinAns << " " << MaxAns;
	
	return 0;
}
