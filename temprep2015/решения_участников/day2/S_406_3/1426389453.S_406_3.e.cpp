#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "E"

const int MaxN = (int)(2e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], p[MaxN];

struct team {
	int first, second;
	double average;
	bool operator < (team t) const {
		return average > t.average;
	}
	team (int x, int b) {
		first = x;
		second = b;
		average = (a[x] + a[b] + 0.0) / 2.0;
	}
};

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 0; i < n + n; ++i) {
		scanf ("%d", a + i);
		p[i] = i;
	}

	int MinAns = n + 1, MaxAns = 1;
	
	do {
		vector <team> v;
		for (int i = 0; i < n + n; i += 2) {
			v.push_back (team (p[i], p[i + 1]));
		}
		sort (all (v));
		double cur = v[0].average;
		int pos = 1;
		if (!v[0].first || !v[0].second) {
			MinAns = min (MinAns, pos);
			MaxAns = max (MaxAns, pos);
		}
		for (int i = 1; i < n; ++i) {
			if (v[i].average != cur) {
				cur = v[i].average;
				pos++;
			}
			if (!v[i].first || !v[i].second) {
				MinAns = min (MinAns, pos);
				MaxAns = max (MaxAns, pos);
			}
		}
	} while (next_permutation (p + 1, p + n + n));
	
	cout << MinAns << " " << MaxAns;
	
	return 0;
}
