#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "D"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

inline int __lcm (int a, int b) {
	return (a * b) / __gcd (a, b);
}

unsigned ll p = 1031;

inline unsigned ll getHash (vector <int> &v) {
	unsigned ll h = 0;
	for (int i = 0; i < v.size(); ++i) {
		h = h * p + v[i];
	}
	return h;
}

int n, l, r, a;
vector <int> v;
int q[1500];
unordered_set <unsigned ll> S;

void go (int pos, int lcm) {
	if (pos == 0) {
		if (lcm % a == 0) {
			vector <int> w;
			unsigned ll h = 0;
			for (int i = l; i <= r; ++i)
				for (int j = 0; j < q[i]; ++j) {
					h = h * p + i;
					// w.push_back (i);
				}
			S.insert ((h));
			/* for (int i : v)
				cout << i << " ";
			cout << "|" << lcm << endl; */
		}
		return;
	}
	// int res = 0;
	for (int i = l; i <= r; ++i) {
		q[i]++;
		go (pos - 1, __lcm (lcm, i));
		q[i]--;
	}
	// return res % MOD;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d%d%d%d", &n, &l, &r, &a);

	int res = 0;

	for (int i = l; i <= r; ++i) {
		q[i]++;
		go (n - 1, i);
		// res = (res + go (n - 1, i)) % MOD;
		q[i]--;
	}

	printf ("%d", (int)(S.size())/*(res + (n - 1)) / n*/);
	
	return 0;
}
