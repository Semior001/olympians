#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "D"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

inline ll __lcm (ll a, ll b) {
	return (a * b) / __gcd (a, b);
}

int n, l, r, a;

map <int, int> d[11][1050];

int go (int pos, int lcm, int last) {
	if (pos == 0) {
		return (lcm % a == 0);
	}
	if (d[pos][lcm].count (last))
		return d[pos][lcm][last];
	int &res = d[pos][lcm][last];
	res = 0;
	for (int i = last; i <= r; ++i) {
		res = (res + go (pos - 1, __lcm (lcm, i), i)) % MOD;
	}
	return res % MOD;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d%d%d%d", &n, &l, &r, &a);

	int res = 0;

	// memset (d, 255, sizeof d);

	for (int i = l; i <= r; ++i) {
		res = (res + go (n - 1, i, i)) % MOD;
	}

	printf ("%d", (res));
	
	return 0;
}
