#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "D"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int __lcm (int a, int b) {
	return (a * b) / __gcd (a, b);
}

int n, l, r, a;
vector <int> v;
set <vector <int> > S;

int go (int pos, int lcm) {
	if (pos == 0) {
		if (lcm % a == 0) {
			vector <int> q = v;
			sort (all (q));
			S.insert (q);
			/* for (int i : v)
				cout << i << " ";
			cout << "|" << lcm << endl; */
			return 1;
		}
		return 0;
	}
	int res = 0;
	for (int i = l; i <= r; ++i) {
		v.push_back (i);
		res = (res + go (pos - 1, __lcm (lcm, i))) % MOD;
		v.pop_back ();
	}
	return res % MOD;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d%d%d%d", &n, &l, &r, &a);

	int res = 0;

	for (int i = l; i <= r; ++i) {
		v.push_back (i);
		res = (res + go (n - 1, i)) % MOD;
		v.pop_back ();
	}

	printf ("%d", (int)(S.size())/*(res + (n - 1)) / n*/);
	
	return 0;
}
