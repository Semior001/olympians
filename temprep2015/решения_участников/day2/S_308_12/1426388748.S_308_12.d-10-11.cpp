#include<bits/stdc++.h>
using namespace std;
#define task "D"
#define mod 1000000007
typedef long long ll;
ll n,l,r,a,d[111][1<<9][1<<9],ans,cnt[11],c[1<<9],n1,pr[11],aa,mx,cc[2][111]={1},dd[1<<9][111];
ll bin(ll a, ll n)
{
    ll ans=1;
    while(n)
    {
        if(n&1)
            ans*=a,ans%=mod;
        a*=a,a%=mod;
        n/=2;
    }
    return ans;
}
void add(ll &a, ll b)
{
    a+=b;
    if(a>=mod)
        a-=mod;
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>l>>r>>a;
    aa=a;
    for(ll i=2;i*i<=aa;i++)
        if(aa%i==0)
        {
            n1++;
            pr[n1]=i;
            while(aa%i==0)
                cnt[n1]++,aa/=i;
        }
    if(aa>1)
        n1++,pr[n1]=aa,cnt[n1]=1;
    for(ll i=l;i<=r;i++)
    {
        ll msk=0,calc,x=i;
        for(ll j=1;j<=n1;j++)
        {
            calc=0;
            while(x%pr[j]==0)
                x/=pr[j],calc++;
            if(calc>=cnt[j])
                msk|=(1<<j-1);
        }
        c[msk]++;
        mx=max(mx,c[msk]);
    }
    for(ll i=0;i<1<<n1;i++)
        if(c[i]==0)
        for(ll j=0;j<=n;j++)
        dd[i][j]=cc[0][j];
    for(ll i=1;i<=mx;i++)
    {
        for(ll j=0;j<=n;j++)
        cc[i&1][j]=cc[i&1][j-1]+cc[(i-1)&1][j],cc[i&1][j]%=mod;
        for(ll j=0;j<1<<n1;j++)
            if(c[j]==i)
            {
                for(ll k=0;k<=n;k++)
                    dd[j][k]=cc[i&1][k];
            }
    }
    for(ll i=0;i<=n;i++)
        d[i][0][0]=dd[0][i];
    for(ll i=1;i<1<<n1;i++)
    {
        for(ll nn=0;nn<=n;nn++)
        for(ll k=0;k<=nn;k++)
        for(ll j=0;j<1<<n1;j++)
            add(d[nn][i][j|(k>0?i:0)],(d[nn-k][i-1][j]*dd[i][k])%mod);
    }
    cout<<d[n][(1<<n1)-1][(1<<n1)-1];
}
