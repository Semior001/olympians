#include<bits/stdc++.h>
#include<unordered_map>
using namespace std;
#define task "D"
#define mod 1000000007
typedef long long ll;
ll n,l,r,a,d[111][1<<9],ans,cnt[11],c[1<<9],n1,pr[11],aa,f[111]={1};
unordered_map<ll,ll>v;
ll bin(ll a, ll n)
{
    ll ans=1;
    while(n)
    {
        if(n&1)
            ans*=a,ans%=mod;
        a*=a,a%=mod;
        n/=2;
    }
    return ans;
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>l>>r>>a;
    for(ll i=1;i<=n;i++)
        f[i]=f[i-1]*i,f[i]%=mod;
    aa=a;
    d[0][0]=1;
    for(ll i=2;i*i<=aa;i++)
        if(aa%i==0)
        {
            n1++;
            pr[n1]=i;
            while(aa%i==0)
                cnt[n1]++,aa/=i;
        }
    if(aa>1)
        n1++,pr[n1]=aa,cnt[n1]=1;
    for(ll i=l;i<=r;i++)
    {
        ll msk=0,calc,x=i;
        for(ll j=1;j<=n1;j++)
        {
            calc=0;
            while(x%pr[j]==0)
                x/=pr[j],calc++;
            if(calc>=cnt[j])
                msk|=(1<<j-1);
        }
        c[msk]++;
    }
    for(ll i=1;i<=n;i++)
        for(ll k=0;k<1<<n1;k++)
        for(ll j=0;j<1<<n1;j++)
        d[i][j|k]+=d[i-1][j]*c[k],d[i][j|k]%=mod;
    for(ll i=1;i<=n;i++)
    {
        for(ll k=0;k<1<<n1;k++)
            cout<<d[i][k]<<" ";
        cout<<endl;
    }
    cout<<(d[n][(1<<n1)-1]/**bin(f[n],mod-2)*/)%mod;
}
