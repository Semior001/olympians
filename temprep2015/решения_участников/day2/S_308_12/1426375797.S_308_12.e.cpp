#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define task "E"
#define fi first
#define se second
typedef long long ll;
ll n,a[111111],ans1,ans2,w[111111],x,pos,l,r,mid;
set<pair<ll,ll> >s;
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n;
    n*=2;
    for(ll i=1;i<=n;i++)
        scanf("%d",&a[i]);
    sort(a+2,a+n+1);
    for(ll i=2;i<=n-1;i++)
        s.insert(mp(-a[i],i));
    for(;!s.empty();)
    {
        set<pair<ll,ll> >::iterator cur=s.begin();
        x=cur->fi;
        s.erase(cur);
        set<pair<ll,ll> >::iterator i=s.lower_bound(mp(-x-a[1]-a[n],0));
        if(i==s.end()||i->fi<-x-a[1]-a[n])
            i=s.begin(),ans1++;
        s.erase(i);
    }
    for(ll i=3;i<=n;i++)
        s.insert(mp(a[i],i));
    for(;!s.empty();)
    {
        set<pair<ll,ll> >::iterator cur=s.begin();
        x=cur->fi;
        s.erase(cur);
        set<pair<ll,ll> >::iterator i=s.upper_bound(mp(a[1]+a[2]-x,0));
        ans2++;
        if(i==s.end()||i->fi+x<=a[1]+a[2])
            i=s.begin(),ans2--;
        s.erase(i);
    }
    cout<<ans1+1<<" "<<ans2+1;
}
