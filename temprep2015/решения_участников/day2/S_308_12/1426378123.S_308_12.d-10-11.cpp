#include<bits/stdc++.h>
using namespace std;
#define task "D"
#define mod 1000000007
typedef long long ll;
ll n,l,r,a,d[11][1111][1111],ans;
ll gcd(ll a, ll b)
{
    if(!b)
        return a;
    return gcd(b,a%b);
}
void add(ll &a, ll b)
{
    a+=b;
    if(a>=mod)
        a-=mod;
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>l>>r>>a;
    d[0][a][l]=1;
    for(ll i=0;i<n;i++)
        for(ll j=1;j<=a;j++)
        for(ll k=l;k<=r;k++)
        for(ll kk=k;kk<=r;kk++)
            add(d[i+1][j/gcd(j,kk)][kk],d[i][j][k]);
    for(ll i=l;i<=r;i++)
        ans+=d[n][1][i];
    cout<<ans;
}
