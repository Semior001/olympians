#include<bits/stdc++.h>
using namespace std;
#define task "D"
#define mod 1000000007
typedef long long ll;
ll n,l,r,a,d[11][1111][1111],ans,cnt;
ll gcd(ll a, ll b)
{
    if(!b)
        return a;
    return gcd(b,a%b);
}
void add(ll &a, ll b)
{
    a+=b;
    if(a>=mod)
        a-=mod;
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>l>>r>>a;
    if(n==1)
    {
        cout<<r/a-(l-1)/a;
        return 0;
    }
    for(ll i=l;i<=r;i++)
        for(ll j=i;j<=r;j++)
        {
            cnt=i*j/gcd(i,j);
            if(cnt%a==0)
                ans++;
        }
    cout<<ans;
}
