#include<bits/stdc++.h>
using namespace std;
#define task "D"
#define mod 1000000007
typedef long long ll;
ll n,l,r,a,d[11][16][1111],ans,cnt[11],n1,pr[11],aa,mask[1111];
ll gcd(ll a, ll b)
{
    if(!b)
        return a;
    return gcd(b,a%b);
}
void add(ll &a, ll b)
{
    a+=b;
    if(a>=mod)
        a-=mod;
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>l>>r>>a;
    aa=a;
    d[0][0][l]=1;
    for(ll i=2;i*i<=aa;i++)
        if(aa%i==0)
        {
            n1++;
            pr[n1]=i;
            while(aa%i==0)
                cnt[n1]++,aa/=i;
        }
    if(aa>1)
        n1++,pr[n1]=aa,cnt[n1]=1;
    for(ll i=l;i<=r;i++)
    {
        ll x=i,calc;
        for(ll j=1;j<=n1;j++)
        {
            calc=0;
            while(x%pr[j]==0)
                x/=pr[j],calc++;
            if(calc>=cnt[j])
                mask[i]|=(1<<j-1);
        }
    }
    for(ll i=1;i<=n;i++)
        for(ll j=0;j<1<<n1;j++)
        for(ll k=l;k<=r;k++)
        if(d[i-1][j][k])
        for(ll kk=k;kk<=r;kk++)
        add(d[i][mask[kk]|j][kk],d[i-1][j][k]);
    for(ll i=l;i<=r;i++)
        add(ans,d[n][(1<<n1)-1][i]);
    cout<<ans;
}
