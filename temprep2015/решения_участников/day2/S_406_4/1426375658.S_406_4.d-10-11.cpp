//Bayemirov Beket
#include <bits/stdc++.h>


#define pb push_back
#define mp make_pair
#define fname "D"
#define MOD 1000000007

using namespace std;

typedef long long ll;

const int N = 100500;

int n, l, r, val, a[N];
ll ans;
bool was[N];

inline int get(int a, int b) {
	return (a * b) / __gcd(a, b);
}

void go(int len, int var, int now) {
	if (len > n) {
		if (var % val == 0)
			ans++, ans %= MOD;
		return;
	}
	for (int i = now; i <= r; i++) {
		was[i] = true;
		a[i] = i;
		go(len + 1, get(var, a[i]), a[i]);
		was[i] = false;
	}
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d%d%d", &n, &l, &r, &val);

	if (n <= 2) {
		if (n == 1) {
			for (int i = l; i <= r; i++)
				if (i % val == 0)
					ans++;
			cout << ans;
			exit(0);
		}

		for (ll i = l; i <= r; i++) {
			for (ll j = i; j <= r; j++) {
				if ((i * j / (__gcd(i, j) * 1LL)) % val == 0)
				ans++, ans %= MOD;
			}
		}
		cout << ans;
		exit(0);
	}

	for (int i = l; i <= r; i++) {
		a[1] = i;
		was[1] = true;
		go(2, a[1], a[1]);
		was[1] = false;
	}

	cout << ans % MOD;

	return 0;
}
