//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define INF 1000000000
#define fname "E"

using namespace std;

typedef long long ll;

const int N = 100500;
const double EPS = 0.000000001;

int mn = INF, mx = -INF;
double a[N];

bool compare(double a, double b) {
	return a + EPS >= b || abs(a - b) < EPS;
}

int compare1(double a, double b, double c) {
	if ((a + EPS >= b || abs(a - b) < EPS) && (a + EPS >= c || abs(a - c) < EPS))
		return 1;
	else if ((a <= b + EPS) && (a <= c + EPS))
		return 3;
	else
		return 2;
}

void go1() {
	cout << "1 1";
	exit(0);
}

void go2() {
	double t1, t2;
	t1 = (a[1] + a[2]) / 2.0;
	t2 = (a[3] + a[4]) / 2.0;
	if (compare(t1, t2)) {
		mx = max(mx, 1),
		mn = min(mn, 1);
	}
	else {
		mx = max(mx, 2);
		mn = min(mn, 2);
	}
	t1 = (a[1] + a[3]) / 2.0;
	t2 = (a[2] + a[4]) / 2.0;
	if (compare(t1, t2)) {
		mx = max(mx, 1),
		mn = min(mn, 1);
	}
	else {
		mx = max(mx, 2);
		mn = min(mn, 2);
	}
	t1 = (a[1] + a[4]) / 2.0;
	t2 = (a[2] + a[3]) / 2.0;
	if (compare(t1, t2)) {
		mx = max(mx, 1),
		mn = min(mn, 1);
	}
	else {
		mx = max(mx, 2);
		mn = min(mn, 2);
	}
}

void go3() {
	double t1, t2, t3;
	int o;
	t1 = (a[1] + a[2]) / 2.0;
	t2 = (a[3] + a[4]) / 2.0;
	t3 = (a[5] + a[6]) / 2.0;
	o = compare1(t1, t2, t3); 
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[2]) / 2.0;
	t2 = (a[3] + a[5]) / 2.0;
	t3 = (a[4] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[3]) / 2.0;
	t2 = (a[3] + a[6]) / 2.0;
	t3 = (a[4] + a[5]) / 2.0;
    o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[3]) / 2.0;
	t2 = (a[2] + a[4]) / 2.0;
	t3 = (a[5] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[3]) / 2.0;
	t2 = (a[2] + a[5]) / 2.0;
	t3 = (a[4] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[3]) / 2.0;
	t2 = (a[2] + a[6]) / 2.0;
	t3 = (a[4] + a[5]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[4]) / 2.0;
	t2 = (a[2] + a[3]) / 2.0;
	t3 = (a[5] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[4]) / 2.0;
	t2 = (a[2] + a[5]) / 2.0;
	t3 = (a[3] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[4]) / 2.0;
	t2 = (a[2] + a[6]) / 2.0;
	t3 = (a[3] + a[5]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[5]) / 2.0;
	t2 = (a[2] + a[3]) / 2.0;
	t3 = (a[4] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[5]) / 2.0;
	t2 = (a[2] + a[4]) / 2.0;
	t3 = (a[3] + a[6]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[5]) / 2.0;
	t2 = (a[2] + a[6]) / 2.0;
	t3 = (a[3] + a[4]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[6]) / 2.0;
	t2 = (a[2] + a[3]) / 2.0;
	t3 = (a[4] + a[5]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[6]) / 2.0;
	t2 = (a[2] + a[4]) / 2.0;
	t3 = (a[3] + a[5]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
	t1 = (a[1] + a[6]) / 2.0;
	t2 = (a[2] + a[5]) / 2.0;
	t3 = (a[3] + a[4]) / 2.0;
	o = compare1(t1, t2, t3);
	mx = max(mx, o);
	mn = min(mn, o);
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

    int n;

	cin >> n;

	n *= 2;

	for (int i = 1; i <= n; i++)
		cin >> a[i];

	if (n == 2)
		go1();
	if (n == 4)
		go2();
	if (n == 6)
		go3();

	cout << mn << " " << mx;

	return 0;
}
