//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fname "E"
#define INF 1000000000

using namespace std;

typedef long long ll;

const int N = 100500;

const double EPS = 1e9;

int n, mn, mx;
double a[N];
vector <double> ans;

inline bool cmp(double a, double b) {
	return a <= b + EPS || (a - b) < EPS;
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	scanf("%d", &n);

	for (int i = 1; i <= n * 2; i++)
		scanf("%lf", &a[i]);
mn = INF;
	mx = -INF;

	if (n == 1) {
		if (max(a[1], a[2]) == a[1])
			cout << "1 1";
		else
			cout << "2 2";
		exit(0);
	}

	else if (n == 2) {
		n *= 2;
		double t1, t2;
		t1 = (a[1] + a[2]) / 2.0;
		t2 = (a[3] + a[4]) / 2.0;
		if (t1 >= t2)
			mx = max(mx, 1),
			mn = min(mn, 1);
		else
			mx = max(mx, 2),
			mn = min(mn, 2);
		t1 = (a[1] + a[3]) / 2.0;
		t2 = (a[2] + a[4]) / 2.0;
		if (t1 >= t2)
			mx = max(mx, 1),
			mn = min(mn, 1);
		else
			mx = max(mx, 2),
			mn = min(mn, 2);
		t1 = (a[1] + a[4]) / 2.0;
		t2 = (a[2] + a[3]) / 2.0;
		if (t1 >= t2)
			mx = max(mx, 1),
			mn = min(mn, 1);
		else
			mx = max(mx, 2),
			mn = min(mn, 2);
		cout << mn << " " << mx;
		exit(0);
	}

    n *= 2;

	mn = INF;
	mx = -INF;

	for (int j = 2; j <= n; j++) {
		for (int l = 2; l <= n; l++) {
			if (l == j)
				continue;
			for (int r = l + 1; r <= n; r++) {
				if (r == j)
					continue;
				ans.clear();
				ans.pb((a[1] + a[j]) / 2.0);
				ans.pb((a[l] + a[r]) / 2.0);
				for (int k = 2; k <= n; k++) {
					if (k == j || k == l || k == r)
						continue;
					ans[ans.size() - 1] = ans[ans.size() - 1] + a[k];
				}
				sort(ans.begin(), ans.end(), cmp);
				for (int p = 0; p < ans.size(); p++)
					if ((ans[p] - ((a[1] + a[j]) / 2.0)) < EPS) {
						mx = max(mx, p + 1);
						mn = min(mn, p + 1);
						break;
					}
			}
		}
	}

	cout << mn << " " << mx;

	return 0;
}
