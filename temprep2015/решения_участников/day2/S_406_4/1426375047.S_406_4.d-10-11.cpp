//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fname "D"
#define MOD 1000000007

using namespace std;

typedef long long ll;

const int N = 100500;

int n, l, r, a;
ll ans;

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d%d%d", &n, &l, &r, &a);

	if (n == 1) {
		for (int i = l; i <= r; i++)
			if (a % i == 0)
				ans++;
		cout << ans;
		exit(0);
	}

	for (int i = l; i <= r; i++) {
		for (int j = i; j <= r; j++) {
			if ((i * j / int(__gcd(i, j))) % a == 0)
				ans++, ans %= MOD; 
		}
	}

	cout << ans;

	return 0;
}
