//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fname "F"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, a[N], sz;

struct node {
	int next[5];
	ll cnt;
	bool leaf;
} t[N];

void add(int x) {
	int v = 0;
	for (int i = 31; i >= 0; i--) {
		int c;
		if (x & (1 << i))
			c = 1;
		else
			c = 0;
		if (!t[v].next[c])
			t[v].next[c] = ++sz;
		v = t[v].next[c];
		t[v].cnt++;
	}
	t[v].leaf = true;
}

ll _find(int v, int x, int bit) {
	if (t[v].leaf)
		return t[v].cnt;
	if ((x & (1 << bit))) {
		if (!t[v].next[0])
			return 0;
		return _find(t[v].next[0], x, bit - 1);
	}
	else {
		if (t[v].next[1] && t[v].next[0])
			return _find(t[v].next[1], x, bit - 1) +
			       _find(t[v].next[0], x, bit - 1);
		else if (t[v].next[1])
			return _find(t[v].next[1], x, bit - 1);
		else
			return _find(t[v].next[0], x, bit - 1);
	}
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	//add(0);

	for (int i = 1; i <= n; i++) {
		scanf("%d", &a[i]);
		add(a[i]);
	}
	for (int i = 1; i <= n; i++)
		cout << _find(0, a[i], 31) << " ";

	return 0;
}
