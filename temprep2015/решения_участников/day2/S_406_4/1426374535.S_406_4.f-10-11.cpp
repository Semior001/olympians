//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fname "F"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, a[N], b[N];

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);

	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (!(a[i] & a[j]))
				b[i]++;

	for (int i = 1; i <= n; i++)
		cout << b[i] << ' ';

	return 0;
}
