//Bayemirov Beket
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <stdio.h>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <queue>
#include <bitset>
#include <set>

#define pb push_back
#define mp make_pair
#define fname "F"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, sz, lvl[N];
int a[N];
set<int> q;

struct node {
	int next[2], pr;
	int cnt;
	bool leaf;
	bitset<50> bit;
} t[N];

inline void add(int x) {
	int v = 0;
	for (int i = 24; i >= 0; i--) {
		int c;
		if (x & (1 << i))
			c = 1;
		else
			c = 0;
		if (!t[v].next[c]) {
			t[v].next[c] = ++sz;
			t[t[v].next[c]].pr = v;
			t[t[v].next[c]].bit = t[v].bit;
			t[t[v].next[c]].bit[i] = c;
		}
		v = t[v].next[c];
		t[v].cnt++;
	}
	t[v].leaf = true;
	q.insert(v);
}

inline ll _find(int x) {
	ll res = 0;
	bitset <50> now;
	for (set<int>::iterator i = q.begin(); i != q.end(); i++) {
	    now = t[*i].bit;
	    now &= x;
	    //cout << t[q[i]].bit << endl;
		if (now == 0)
			res += t[*i].cnt;
	}
	return res;
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	//add(0);

	for (int i = 1; i <= n; i++) {
		scanf("%d", &a[i]);
		add(a[i]);
	}
	for (int i = 1; i <= n; i++)
		printf("%lld ", _find(a[i]));

	return 0;
}
