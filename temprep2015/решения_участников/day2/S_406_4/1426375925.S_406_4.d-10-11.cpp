//Bayemirov Beket
#include <bits/stdc++.h>


#define pb push_back
#define mp make_pair
#define fname "D"
#define MOD 1000000007

using namespace std;

typedef long long ll;

const int N = 100500;

int n, l, r;
ll val, a[N];
ll ans;
bool was[N];

inline ll mod(ll x) {
	if (x > MOD)
		x %= MOD;
	return x;
}

inline ll get(ll a, ll b) {
	return (a * b) / __gcd(a, b);
}

void go(int len, ll var, ll now) {
	if (len > n) {
		if (var % val == 0)
			ans++, mod(ans);
		return;
	}
	for (ll i = now; i <= r; i++) {
		if (was[i])
			continue;
		was[i] = true;
		a[i] = i;
		go(len + 1, get(var, a[i]), a[i]);
		was[i] = false;
	}
}

int main() {

	cin.sync_with_stdio(false);
	cout.sync_with_stdio(false);

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	cin >> n >> l >> r >> val;

	if (n <= 2) {
		if (n == 1) {
			for (int i = l; i <= r; i++)
				if (i % val == 0)
					ans++;
			cout << ans;
			exit(0);
		}

		for (ll i = l; i <= r; i++) {
			for (ll j = i; j <= r; j++) {
				if ((i * j / (__gcd(i, j) * 1LL)) % val == 0)
				ans++, ans %= MOD;
			}
		}
		cout << ans;
		exit(0);
	}

	for (int i = l; i <= r; i++) {
		a[1] = i;
		was[i] = true;
		go(2, a[1], a[1]);
		was[i] = false;
	}

	cout << ans % MOD;

	return 0;
}
