//Bayemirov Beket
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fname "F"

using namespace std;

typedef long long ll;

const int N = 300500;

int n, sz, lvl[N];
ll a[N];

struct node {
	int next[2], pr;
	ll cnt;
	bool leaf;
} t[N];

inline void add(ll x) {
	int v = 0;
	for (int i = 20; i >= 0; i--) {
		int c;
		if (x & (1 << i))
			c = 1;
		else
			c = 0;
		if (!t[v].next[c])
			t[v].next[c] = ++sz;
		t[t[v].next[c]].pr = v;
		v = t[v].next[c];
		t[v].cnt++;
	}
	t[v].leaf = true;
}

inline ll _find(ll x) {
	int c = 0;
	ll res = 0;
	queue <int> q;
	q.push(0);
	int v = 0;
	lvl[v] = 20;
	while(!q.empty()) {
		int v = q.front();
		q.pop();
		if (t[v].leaf) {
			res += t[v].cnt;
			continue;
		}
		if (v != 0)
			lvl[v] = lvl[t[v].pr] - 1;
		if (x & (1 << (lvl[v]))) {
			if (t[v].next[0])
				q.push(t[v].next[0]);
		}
		else {
			if (t[v].next[0])
				q.push(t[v].next[0]);
			if (t[v].next[1])
				q.push(t[v].next[1]);
		}
	}
	return res;
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	//add(0);

	for (int i = 1; i <= n; i++) {
		scanf("%lld", &a[i]);
		add(a[i]);
	}
	for (int i = 1; i <= n; i++)
		printf("%lld ", _find(a[i]));

	return 0;
}
