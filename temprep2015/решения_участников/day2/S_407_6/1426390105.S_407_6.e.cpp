#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <utility>
#define file "E."
#define F first
#define S second
#define mkp make_pair
#define pb push_back
#define N 200005
using namespace std;
int n;
int a[N];
bool u[N];
vector<int> g[N];
int mt[N];
int m;
bool dfs(int v)
{
	//cerr << v << endl;
	if(v == -1)
		return 0;
	if(u[v])
	{
		return 0;
	}
	u[v] = 1;
	for(int i = 0; i < g[v].size(); i++)
	{
		int to = g[v][i];
		if(mt[to] == -1 || dfs(mt[to]))
		{
			mt[to] = v;
			return 1;
		}
	}
	return 0;
}
map<int, int> m1;
bool u1[N];
	
int kuhn(int x)
{
	for(int i = 0; i <= 10000; i++)
		g[i].clear();
	for(int i = 0; i <= 10000; i++)
	{
		mt[i] = -1;
	}
	for(int i = 1; i <= m; i++)
	{
		for(int j = 1; j <= m; j++)
		{
			if(i != j && a[i] + a[j] > x && !m1[i] && !m1[j])
			{
				g[i].push_back(j);
			}
		}
	}	
	for(int i = 1; i <= m; i++)
	{
		memset(u, 0, sizeof(u));
		dfs(i);
	}
	int ans = 0;
	for(int i = 1; i <= m; i++)
	{
		if(mt[i] != -1)
			ans++;
	}
	return ans;
}
int main()
{
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	cin >> n;
	vector<pair<int, int> > vec; 
	m = 2 * n;
	for(int i = 1; i <= m; i++)
	{
		cin >> a[i];
		vec.pb(mkp(a[i], i));
	}           
	sort(vec.begin(), vec.end());
	int mn;
	int mx;
	memset(u1, 0, sizeof(u1));
	u1[1] = 1;
	for(int i = 0; i < vec.size(); i++)
	{
		if(vec[i].S != 1 && !u[vec[i].S])
		{
			u1[vec[i].S] = 1;
			mn = vec[i].F;
			m1[vec[i].S] = 1; 
			break;	
		}
	}
	m1[1] = 1;
	for(int i = vec.size() - 1; i >= 0; i--)
	{
		if(vec[i].S != 1 && !u[vec[i].S])
		{
			u1[vec[i].S] = 1;
			mx = vec[i].F;
			m1[vec[i].S] = 1;
			break;	
		}
	}
	cout << kuhn(mx + a[1]) + 1 << " " << kuhn(mn + a[1]) + 1;
	return 0;
}                        	