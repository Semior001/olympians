#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <utility>
#define file "D."
using namespace std;
int n, l, r, a;
int gcd(int a, int b)
{
	if(a == 0)
		return b;
	return gcd(b % a, a);
}
int main()
{
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	cin >> n >> l >> r >> a;
	int ans = 0;
	if(n == 1)
	{
		for(int i = l; i <= r; i++)
			if(i % a == 0)
				ans++;
	}
	if(n == 2)
	{
	for(int i = l; i <= r; i++)
	{
		for(int j = i; j <= r; j++)
		{
			if(((i * j) / gcd(i, j)) % a == 0)
			{
				ans++;
			}
		}
	}
	}
	cout << ans;
	return 0;
}