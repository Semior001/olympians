#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <utility>
#define file "E."
#define F first
#define S second
#define mkp make_pair
#define pb push_back
#define N 200005
using namespace std;
int n;
int m;
bool u[N];
pair<double, int> b[N], a[N];
int Max()
{
	vector<pair<double, int> > vec;
	int p = 1;
	int pos = 0;
	for(int i = 1; i <= 2 * n; i++)
	{
		if(a[i].S == 1)
		{
			pos = a[i].S;
			u[pos] = 1;
			break;
		}
		//cerr << a[i].F << " ";
	}
	int M = 1;
	if(u[M])
		M++;
	u[M] = 1;
	vec.pb(mkp((a[M].F + a[pos].F)/2, 1));
	int l = 0;
	for(int i = 1; i <= 2 * n; i++)
	{
		if(!u[i])
		{
			l = i;
			break;
		}
	}
	if(l == 0)
		return 1;
	for(int j = 1; j <= 2 * n; j++)
	{
		for(int i = 1; i <= m; i++)
		{
			if(!u[j] && !u[i] && i != j)
			{
				vec.pb(mkp((a[i].F + a[j].F)/ 2, 0));
				u[i] = 1;
				u[j] = 1;
				break;
			}
		}
		/*
		if(l == j)
			l++;
		if(u[l])
		{
			while(l <= n)
			{
				l++;
				if(!u[l] && j != l)
					break;
			}
		}    
		if(!u[l] && !u[j])
		{
			cerr << a[l].F << " " << a[j].F << endl;
			vec.pb(mkp((a[l].F + a[j].F) / 2, 0));
			u[l] = 1;
			u[j] = 1;
			l++;
		}  */
	}
	sort(vec.begin(), vec.end());
	for(int i = vec.size() - 1; i >= 0; i--)
	{
		if(vec[i].S == 1)
		{
			return vec.size() - i;
		}
	}
}
int Min()
{
	vector<pair<double, int> > vec;
	int p = 1;
	int pos = 0;
	for(int i = 1; i <= 2 * n; i++)
	{
		if(a[i].S == 1)
		{
			pos = a[i].S;
			u[pos] = 1;
			break;
		}
		//cerr << a[i].F << " ";
	}
	int M = 2 * n;
	if(u[M])
		M--;
	u[M] = 1;
	vec.pb(mkp((a[M].F + a[pos].F)/2, 1));
	for(int j = 1; j <= 2 * n; j++)
	{
		for(int i = m; i >= 1; i--)
		{
			if(!u[j] && !u[i] && i != j)
			{
				vec.pb(mkp((a[i].F + a[j].F)/ 2, 0));
				u[i] = 1;
				u[j] = 1;
				break;
			}
		}
	}
	sort(vec.begin(), vec.end());
	for(int i = vec.size() - 1; i >= 0; i--)
	{
		if(vec[i].S == 1)
		{
			return i;
		}
	}
}
int main()
{
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	cin >> n;
	m = 2 * n;
	for(int i = 1; i <= n * 2; i++)
	{
		cin >> b[i].F;
		b[i].S = i;
	}
	sort(b + 1, b + m + 1);
	int c = 1;
	for(int i = n * 2; i >= 1; i--)
	{
		a[c] = b[i];
		c++;
	}
	cout << Max() << " " << Min();
	return 0;
}