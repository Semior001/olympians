#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <utility>
#define file "F."
using namespace std;
int n;
int a[100005];
int u[100005];
int mx;
int num[1000005];
int main()
{
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	cin >> n;
	mx = -1;
	for(int i = 1; i <= n; i++)
	{
		cin >> a[i];
		mx = max(mx, a[i]);
		u[a[i]] += 1;
	}
	if(mx <= 100)
	{
		for(int i = 1; i <= 100; i++)
		{
			if(u[i])
			{
				for(int j = 1; j <= 100; j++)
				{            
					if(u[j] && (i & j) == 0)
					{
						num[i] += u[j];
						if(i == j)
							num[i]--;
					}
				}
			}
		}
		for(int i = 1; i <= n; i++)
			cout << num[a[i]] << " ";
	}
	else
	{
		for(int i = 1; i <= n; i++)
		{
			int ans = 0;
			for(int j = 1; j <= n; j++)
			{
				if(i != j)
				{
					if((a[i] & a[j]) == 0)
						ans++;
				}
			}
			cout << ans << " ";
		}
	}
	return 0;
}