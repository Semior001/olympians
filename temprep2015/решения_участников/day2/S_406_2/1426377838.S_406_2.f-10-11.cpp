#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e5 + 7;

struct node {
  int go[2];
  int isEnd;
};

node t[MAXN];
int sz = 1;

inline void add(const int &v) {
  int cur = 1;
  for (int bit = 21; bit >= 0; bit--) {
    int wh = (v >> bit) & 1;
    if (!t[cur].go[wh]) {
      t[cur].go[wh] = ++sz;
    }
    cur = t[cur].go[wh];
  }
  t[cur].isEnd++;
}

int calc(int cur, int val, int last) {
  if (!cur) return 0;
  if (last == -1) return t[cur].isEnd;
  for (int bit = last; bit >= 0; bit--) {
    int wh = (val >> bit) & 1;
    if (wh == 1) return calc(t[cur].go[0], val, bit - 1);
    else return calc(t[cur].go[0], val, bit - 1) + calc(t[cur].go[1], val, bit - 1);
  }
}

int n;
int a[MAXN];

int cnt[MAXN * 20];
int mx;

int main() {
  freopen("F.in", "r", stdin);
  freopen("F.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    mx=max(mx,a[i]);
    cnt[a[i]] = -1;
    add(a[i]);
  }
  assert(mx<=1000*1000);
  for (int i = 1; i <= n; i++) {
    if (cnt[a[i]] != -1) printf("%d ", cnt[a[i]]);
    else {
      cnt[a[i]] = calc(1, a[i], 21);
      printf("%d ", cnt[a[i]]);
    }
  }

  return 0;
}
