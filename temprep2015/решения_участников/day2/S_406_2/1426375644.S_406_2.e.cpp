#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e5 + 7;

int n;
int a[MAXN];
multiset<int> all;

inline int del(int x) {
  all.erase(all.find(x));
  return x;
}

inline int findBest() {
  int best = 1;
  for (int i = 1; i <= n + n; i++)
    all.insert(a[i]);

  int csum = 0;
  csum += del(a[1]);
  csum += del(*all.rbegin());
  for (int i = 2; i <= n; i++) {
    int now = del(*all.rbegin());
    auto it = all.upper_bound(csum - now);
    if (it != all.begin()) it--;
    now += del(*it);
    if (now > csum)
      ++best;
  }
  return best;
}

inline int findWorst() {
  int worst = 1;
  for (int i = 1; i <= n + n; i++)
    all.insert(a[i]);

  int csum = 0;
  csum += del(a[1]);
  csum += del(*all.begin());
  for (int i = 2; i <= n; i++) {
    int now = del(*all.rbegin());
    auto it = all.upper_bound(csum - now);
    if (it == all.end()) it--;
    now += del(*it);
    if (now > csum)
      ++worst;
  }
  return worst;
}

int main() {
  freopen("E.in", "r", stdin);
  freopen("E.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n + n; i++) {
    scanf("%d", &a[i]);
  }
  printf("%d %d", findBest(), findWorst());
  return 0;
}
