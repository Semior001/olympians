#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e5 + 7;

struct node {
  int go[2];
  int isEnd;
};

node t[MAXN];

int pos[MAXN * 40];
int sz = 1;

inline void add(const int &v) {
  int cur = 1;
  for (int bit = 22; bit >= 0; bit--) {
    bool wh = (v >> bit) & 1;
    if (!t[cur].go[wh]) {
      t[cur].go[wh] = ++sz;
    }
    cur = t[cur].go[wh];
  }
  pos[v] = cur;
  t[cur].isEnd++;
}

int calc(const int &cur, const int &val, const int &bit) {
  if (!cur) return 0;
  if (bit == -1) return t[cur].isEnd;

  bool wh = (val >> bit) & 1;
  if (wh) return calc(t[cur].go[0], val, bit - 1);
  else return calc(t[cur].go[0], val, bit - 1) + calc(t[cur].go[1], val, bit - 1);
}

int n, mx;
int a[MAXN];

int cnt[MAXN * 20];

bitset<MAXN> S[23];

int main() {
  freopen("F.in", "r", stdin);
  freopen("F.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    cnt[a[i]] = -1;
    for (int bit = 22; bit >= 0; bit--) {
      if ((a[i] >> bit) & 1)
        S[bit] |= (1 << i);
    }
  }
  for (int i = 1; i <= n; i++) {
    if (cnt[a[i]] == -1) {
      bitset<MAXN> ans;
      for (int bit = 22; bit >= 0; bit--) {
        if ((a[i] >> bit) & 1) {
          ans |= S[bit];
        }
      }
      cnt[a[i]] = n - ans.count();
    }
    printf("%d ", cnt[a[i]]);
  }
  return 0;
}
