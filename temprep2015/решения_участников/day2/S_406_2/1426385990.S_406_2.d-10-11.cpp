#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e5 + 7;

int n, L, R, a;

int main() {
  freopen("D.in", "r", stdin);
  freopen("D.out", "w", stdout);

  cin >> n >> L >> R >> a;
  if (n == 1) {
    int ans = 0;
    for (int d = a; d <= R; d += a) {
      if (d >= L) ans++;
    }
    cout << ans;
  }
  if (n == 2) {
    int ans = 0;
    for (int d1 = L; d1 <= R; d1++) {
      for (int d2 = d1; d2 <= R; d2++) {
        int lcm = d1 * d2 / __gcd(d1, d2);
        if (lcm % a == 0)
          ++ans;
      }
    }
    cout << ans;
  }

  return 0;
}
