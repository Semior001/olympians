#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e5 + 7;

int n, L, R, a;
int dp[11][1000001][16], sum[11][1000001][16];

vector<int> v;

int main() {
  freopen("D.in", "r", stdin);
  freopen("D.out", "w", stdout);

  cin >> n >> L >> R >> a;
  for (int i = 2; i * i <= a; i++) {
    int cur = 1;
    if (a % i == 0) {
      while (a % i == 0) {
        cur = cur * i;
        a /= i;
      }
      v.push_back(cur);
    }
  }
  if (a > 1)
    v.push_back(a);

  int len = v.size();
  dp[0][L][0] = 1;
  for (int i = 0; i < n; i++) {
    for (int cur = L; cur <= R; cur++) {
      for (int mask = 0; mask < (1 << len); mask++) {
        for (int nxt = cur; nxt <= R; nxt++) {
          int nmask = mask;
          for (int x = 0; x < len; x++)
            if (nxt % v[x] == 0)
              nmask |= (1 << x);

          dp[i + 1][nxt][nmask] += dp[i][cur][mask];
          if (dp[i + 1][nxt][nmask] >= INF)
            dp[i + 1][nxt][nmask] -= INF;
        }
      }
    }
  }
  int s = 0;
  for (int i = L; i <= R; i++)
    s += dp[n][i][(1<<len)-1], s %= INF;

  cout << s;
  return 0;
}
