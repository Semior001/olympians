#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 4e6 + 7;

struct node {
  int go[2];
  int isEnd;
};

node t[MAXN];
int sz = 1;

inline void add(const int &v) {
  int cur = 1;
  for (int bit = 16; bit >= 0; bit--) {
    bool wh = (v >> bit) & 1;
    if (!t[cur].go[wh]) {
      t[cur].go[wh] = ++sz;
    }
    t[cur].isEnd++;
    cur = t[cur].go[wh];
  }
  t[cur].isEnd++;
}

int val;

int calc(const int &cur, const int &bit) {
  if (!cur) return 0;
  if (bit == -1) return t[cur].isEnd;

  bool wh = (val >> bit) & 1;
  int lbits = (1 << (bit + 1)) - 1;
  if (!(lbits&val)) return t[cur].isEnd;
  if (wh) return calc(t[cur].go[0], bit - 1);
  else return calc(t[cur].go[0], bit - 1) + calc(t[cur].go[1], bit - 1);
}

int n, mx;
int a[MAXN];

int main() {
  freopen("F.in", "r", stdin);
  freopen("F.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    add(a[i]);
  }
  for (int i = 1; i <= n; i++) {
    val = a[i];
    printf("%d ", calc(1, 16));
  }
  return 0;
}
