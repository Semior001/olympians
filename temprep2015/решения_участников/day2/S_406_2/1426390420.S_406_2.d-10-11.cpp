#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 2e6 + 7;

int n, L, R, a;
int dp[11][16], cm[MAXN];

vector<int> v;

int bin(int x, int y) {
  int res = 1;
  while (y) {
    if (y & 1)
      res = (1ll * res * x) % INF;

    y >>= 1;
    x = (1ll * x * x) % INF;
  }
  return res;
}

int main() {
  freopen("D.in", "r", stdin);
  freopen("D.out", "w", stdout);

  cin >> n >> L >> R >> a;
  for (int i = 2; i * i <= a; i++) {
    int cur = 1;
    if (a % i == 0) {
      while (a % i == 0) {
        cur = cur * i;
        a /= i;
      }
      v.push_back(cur);
    }
  }
  if (a > 1)
    v.push_back(a);

  int len = v.size();
  for (int i = L; i <= R; i++) {
    for (int x = 0; x < len; x++)
      if (i % v[x] == 0)
        cm[i] |= (1 << x);
  }
  dp[0][0] = 1;
  for (int i = 0; i < n; i++) {
    for (int mask = 0; mask < (1 << len); mask++) {
      for (int go = i + 1; go <= n; go++) {
        for (int put = L; put <= R; put++) {
          int nm = (mask | cm[put]);
          dp[go][nm] += dp[i][mask];
          dp[go][nm] %= INF;
        }
      }
    }
  }
  int total = dp[n][(1 << len) - 1];
  total = (1ll * total * bin(n, INF - 2)) % INF;
  cout << total;
  return 0;
}
