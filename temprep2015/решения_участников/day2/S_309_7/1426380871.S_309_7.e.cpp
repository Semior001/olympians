#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int cnt, i, j, n, p, maxi, c, l, r, mini, x;
int a[300000], b[300000];

bool cmp(int x , int y)
{
    return x > y;
}

int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    cin >> x;

    for (i = 1; i <= n * 2 - 1; i++)
        cin >> a[i];

    if (n == 1)
    {
        cout << 1 << ' ' << 1;
        return 0;
    }

    sort(a + 1, a + n * 2, cmp);
   // for (i = 1; i <= n * 2 - 1; i++)
    //    cout << a[i] << ' ';
   // cout << endl;

    int maxi = (x + a[1]) / 2;
    for (i = 2; i <= n * 2 - 1; i++)
        if (b[i] == 0)
        {
            int f = 0;
                for (j = n * 2 - 1; j > i; j--)
                if (b[j] == 0 && maxi > (a[i] + a[j]) / 2)
                {
                    b[j] = 1;
                    cnt++;
                    f = 1;
                    break;
                }
            if (f == 0)
                break;
        }
    cout << n - cnt << ' ';

    cnt = 0;
    for (i = 1; i <= n * 2 + 1; i++)
        b[i] = 0;

    int mini = (x + a[n *  2 - 1]) / 2;

    for (i = 1; i <= n * 2 - 2; i++)
    if (b[i] == 0)
    {
        int f = 0;
        for (j = n * 2 - 2; j > i; j--)
            if (b[j] == 0 && mini < (a[i] + a[j]) / 2)
            {
                b[j] = 1;
                cnt++;
                f = 1;
                break;
            }
        if (f == 0)
            break;
    }
    cout << cnt + 1;
}
