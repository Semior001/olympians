#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int cnt, i, j, n, c, l, r, mini, x;
int a[300000];

bool cmp(int x , int y)
{
    return x > y;
}

int main()
{
    freopen("E.in", "r", stdin);
    freopen("E.out", "w", stdout);
    cin >> n;
    cin >> x;

    for (i = 1; i <= n * 2 - 1; i++)
        cin >> a[i];

    if (n == 1)
    {
        cout << 1 << ' ' << 1;
        return 0;
    }

    sort(a + 1, a + n * 2, cmp);
   // for (i = 1; i <= n * 2 - 1; i++)
    //    cout << a[i] << ' ';
   // cout << endl;

    double maxi = (double(x) + double(a[1])) / 2;
    //cout << maxi << endl;
    i = 0;
    cnt = 0;
    double p = 1e9;
    while (maxi < p && i <= n * 2 - 2)
    {
        i += 2;
        p = (double(a[i]) + double(a[i + 1])) / 2;
        if (maxi < p)
            cnt++;
    }

    cout << cnt + 1 << ' ';

    p = -1e9;
    double mini = (double(x) + double(a[n *  2 - 1])) / 2;
    i = n * 2 - 2;
    cnt = 0;
    while (mini >= p && i > 0)
    {
      //  cout << i << endl;
        p = (double(a[i]) + double(a[i - 1])) / 2;
     //   cout << p << ' ' << a[i] << ' ' << a[i - 1] << endl;
        i -= 2;
        if (mini >= p)
            cnt++;
    }
   // cout << cnt << endl;
    cout << n - cnt;
}
