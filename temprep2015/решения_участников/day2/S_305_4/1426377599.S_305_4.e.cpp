#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
#define F first
#define S second
using namespace std;
const int N = 200001;
int n, amanchik, a1;
pair<int, int> a[N];
vector<pair<int, bool> > sum1, sum2;
bool used[N];
bool cmp(pair<int, bool> a, pair<int, bool> b) {
	if(a.F == b.F) {
		return a.S > b.S;
	}
	return a.F < b.F;
}
int main() {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n + n; ++ i) {
		cin >> a[i].F;
		a[i].S = i;
	}
	amanchik = a[1].F;
	sort(a + 1, a + n + n + 1);
	if(a[1].S == 1) {
		sum1.PB(MP(a[2].F + amanchik, 1));
		for(int i = 3; i <= n + n; i += 2) {
			sum1.PB(MP(a[i].F + a[i + 1].F, 0));
		}
	}
	else {
		sum1.PB(MP(a[1].F + amanchik, 1));
		for(int i = 2; i <= n + n; i += 2) {
			if(a[i].S == 1) {
				if(i + 2 > n + n) {
					break;
				}
//				cout << i + 1 << " " << i + 2 << endl;
				sum1.PB(MP(a[i + 1].F + a[i + 2].F, 0));
				++ i;
				continue;
			}
			if(a[i + 1].S == 1) {
//				cout << i << " " << i + 2 << endl;
				sum1.PB(MP(a[i].F + a[i + 2].F, 0));
				++ i;
				continue;
			}
//			cout << i << " " << i + 1 << endl;
			sum1.PB(MP(a[i].F + a[i + 1].F, 0));
		}
	}
//	cout << sum1.size() << endl;
	sort(sum1.begin(), sum1.end(), &cmp);
	for(int i = 0; i < sum1.size(); ++ i) {
//		cout << sum1[i].F << " " << sum1[i].S << endl;
		if(sum1[i].S) {
			a1 = i + 1;
			break;
		}
	}
	reverse(a + 1, a + n + n + 1);
//	memset(used, 0, sizeof used);
	if(a[1].S == 1) {
		sum2.PB(MP(a[2].F + amanchik, 1));
		for(int i = 3; i <= n + n; ++ i) {
			if(used[i]) {
				break;
			}
//			cout << i << " " << n + n - i + 3 << endl;
			sum2.PB(MP(a[i].F + a[n + n - i + 3].F, 0));
			used[i] = 1;
			used[n + n - i + 3] = 1;
		}
	}
	else {
		sum2.PB(MP(a[1].F + amanchik, 1));
		int z = 2;
		int latest = n + n + 1;
		for(int i = 2; i <= n + n; ++ i) {                        
			if(used[i]) {
				break;
			}
			if(a[i].S == 1) {
				continue;
			}
			for(int j = latest - 1; j > 0; -- j) {
				if(a[j].S != 1) {
//					cout << i << " " << j << endl;
					latest = j;
					sum2.PB(MP(a[i].F + a[j].F, 0));
					used[i] = 1;
					used[j] = 1;
					break;
				}
			}
		}
	}
//	cout << sum2.size() << endl;
	sort(sum2.begin(), sum2.end());
	for(int i = 0; i < sum2.size(); ++ i) {
//		cout << sum2[i].F << " ";
		if(sum2[i].S) {
			cout << sum2.size() - i << " " << a1;
			return 0;
		}
	}
}
