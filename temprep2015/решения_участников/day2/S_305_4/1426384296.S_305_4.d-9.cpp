#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 1;
int a, b, l, r, ans, aa;
int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	cin >> a >> b >> l >> r;
	if(l > r) {
		swap(l, r);
	}
	if(b > a) {
		cout << 0;
		return 0;
	}
	if(b == a) {
		cout << max(0, r - (max(l - 1, a)));
		return 0;
	}
	if(min(a, r) - max(b, l) <= 50000000) {
		for(int i = max(b, l) + 1; i <= min(a, r); ++ i) {
			if(a % i == b) {
				ans ++;
			}
		}
		cout << ans;
	}
	else {
		aa = a;
		a -= b;
		for(int i = max(l, 2); i <= min((int)sqrt(a), r); ++ i) {
			if(a % i == 0) {
				if(aa % i == b) {
//					cout << i << " ";
					ans ++;
				}
				if(a / i != i && aa % (a / i) == b) {
//					cout << a / i << " ";
					ans ++;
				}
			}
		}
		cout << ans + (aa % a == b);
	}
}