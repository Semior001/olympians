#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 5001;
int a1 = INF, a2, n, a[N], mn = INF, mx, sum, mnid, mxid;
int b[N], c[N];
bool used[N];
inline void rec(int com = 1, int place = 1) {
	if(com + com == n) {
		a1 = min(a1, place);
		a2 = max(a2, place);
	}
	for(int i = 2; i <= n; ++ i) {
		if(!used[i]) {
			used[i] = 1;
			for(int j = i + 1; j <= n; ++ j) {
				if(!used[j]) {
					used[j] = 1;
					if(a[i] + a[j] <= sum) {
                		rec(com + 1, place);
	                }
    	            else {
						rec(com + 1, place + 1);
					}
					used[j] = 0;
				}
			}
			used[i] = 0;
		}
	}
}
int main() {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	n += n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i];
		b[i] = a[i];
		c[i] = a[i];
		if(i > 1) {
			if(a[i] < mn) {
				mn = a[i];
				mnid = i;
			}
			if(a[i] > mx) {
				mx = a[i];
				mxid = i;
			}
		}
	}
	if(n > 5000) {
		cout << 1 << " " << n;
		return 0;
	}
	if(n > 12) {
		a1 = 1, a2 = 1;
		sort(b + 2, b + n + 1);
		sort(c + 2, c + n + 1);
		reverse(c + 2, c + n + 1);
		int sum1 = a[1] + b[2], sum2 = a[1] + c[2];
		int latest = n;
		for(int i = 3; i < n; ++ i) {
			for(int j = latest; j > i; -- j) {
				if(c[i] + c[j] > sum2) {
					latest = j - 1;
					a2 ++;
				}
			}
		}
		latest = n;
		for(int i = 3; i < n; ++ i) {
			for(int j = latest; j > i; -- j) {
				if(b[i] + b[j] > sum1) {
					latest = j - 1;
					a1 ++;
				}
			}
		}
		cout << min(a1, a2) << " " << max(a1, a2);
		return 0;
	}
	sum = a[1] + mx;
	used[1] = 1;
	used[mxid] = 1;
	rec();
	used[mxid] = 0;
	sum = a[1] + mn;
	used[mnid] = 1;
	rec();
	cout << a1 << " " << a2;
}
