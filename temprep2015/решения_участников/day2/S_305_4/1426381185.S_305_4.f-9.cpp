#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
#define F first
#define S second
using namespace std;
const int N = 100001;
int n, hini[N], mini, maxi, mnid, mxid;
pair<int, int> a[N], palm[N];
LL ans;
int main() {
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i].F;
		a[i].S = i;
		hini[i] = a[i].F;
	}
	for(int i = 1; i <= n; ++ i) {
		cin >> palm[i].S;
	}
	mini = maxi = hini[1];
	mnid = mxid = 1;
	for(int i = 2; i <= n; ++ i) {
		if(hini[i] < mini) {
			if(i - mnid == 1) {
				ans += min(palm[i].S, palm[mnid].S);
			}
			else {
				ans += palm[i].S;
			}
			mini = hini[i];
			mnid = i;
		}
/*		if(hini[i] > maxi) {
			if(i - mxid == 1) {
				ans += min(palm[i].S, palm[mxid].S);
			}
			else {
				ans += palm[i].S;
			}
			maxi = hini[i];
			mxid = i;
		}*/
	}
	cout << ans;
}
