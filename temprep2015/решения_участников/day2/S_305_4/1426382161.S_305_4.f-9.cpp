#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
#define F first
#define S second
using namespace std;
const int N = 100001;
int n, hini[N], mini, maxi, mnid, mxid;
pair<int, int> a[N];
LL ans, c[N], ans2;
int main() {
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i].F;
		a[i].S = i;
		hini[i] = a[i].F;
	}
	for(int i = 1; i <= n; ++ i) {
		cin >> c[i];
	}
	mini = maxi = hini[1];
	mnid = mxid = 1;
	for(int i = 2; i <= n; ++ i) {
		if(hini[i] < mini) {
			if(i == 2) {
				ans += min(c[i], c[1]);
			}
			else {
				ans += c[i];
			}
			mini = hini[i];
			mnid = i;
			continue;
		}
		if(hini[i] < maxi) {
			ans += c[i];
		}
		if(hini[i] > maxi) {
			maxi = hini[i];
			mxid = i;
		}
	}
	mini = maxi = hini[1];
	mnid = mxid = 1;
	for(int i = 2; i <= n; ++ i) {
		if(hini[i] < mini) {
			if(i == 2) {
				ans2 += min(c[i], c[1]);
			}
			else {
				ans2 += c[i];
			}
			mini = hini[i];
			mnid = i;
			continue;
		}
		if(hini[i] > maxi) {
			ans2 += c[i];
			maxi = hini[i + 1];
			mxid = i;
		}
	}
	cout << min(ans, ans2);
}
