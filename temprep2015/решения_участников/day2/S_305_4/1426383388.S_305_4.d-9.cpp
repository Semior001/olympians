#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
#define _USE_MATH_DEFINES
using namespace std;
const int N = 1;
int a, b, l, r, ans, aa;
int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	cin >> a >> b >> l >> r;
	aa = a;
	if(b >= a) {
		cout << 0;
		return 0;
	}
	a -= b;
	for(int i = max(l, 2); i <= min((int)sqrt(a), r); ++ i) {
		if(a % i == 0) {
			if(aa % i == b) {
//				cout << i << " ";
				ans ++;
			}
			if(a / i != i && aa % (a / i) == b) {
//				cout << a / i << " ";
				ans ++;
			}
		}
	}
	cout << ans + (aa % a == b);
}