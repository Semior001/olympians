#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
#define F first
#define S second
using namespace std;
const int N = 100001;
int n, hini[N];
pair<int, int> a[N], palm[N];
LL ans;
int main() {
	freopen("F.in", "r", stdin);
	freopen("F.out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i].F;
		a[i].S = i;
		hini[i] = a[i].F;
	}
	for(int i = 1; i <= n; ++ i) {
		cin >> palm[i].S;
	}
	sort(a + 1, a + n + 1);
	for(int i = 1; i <= n; ++ i) {
		palm[i].F = a[i].F;
		if(a[i].F == hini[i]) {
			continue;
		}
		else {
			if(abs(a[i].S - a[i + 1].S) == 1 && i < n) {
				ans += min(palm[a[i].S].S, palm[a[i + 1].S].S);
				++ i;
			}
			else {
				ans += palm[a[i].S].S;
			}
		}
	}
	cout << ans;
}