#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 1;
int a, b, l, r, ans;
int main() {
	freopen("D.in", "r", stdin);
	freopen("D.out", "w", stdout);
	cin >> a >> b >> l >> r;
	for(int i = max(b, l) + 1; i <= min(a, r); ++ i) {
		if(a % i == b) {
			ans ++;
		}
	}
	cout << ans;
}