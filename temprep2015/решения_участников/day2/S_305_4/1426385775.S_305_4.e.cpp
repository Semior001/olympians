#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 5001;
int a1 = INF, a2, n, a[N], mn = INF, mx, sum, mnid, mxid;
bool used[N];
inline void rec(int com = 1, int place = 1) {
	if(com + com == n) {
		a1 = min(a1, place);
		a2 = max(a2, place);
	}
	for(int i = 2; i <= n; ++ i) {
		for(int j = 3; j <= n; ++ j) {
			if(!used[i] && !used[j]) {
				used[i] = 1;
				used[j] = 1;
				rec(com + 1, (a[i] + a[j] < sum ? place : place + 1));
				used[i] = 0;
				used[j] = 0;
			}
		}
	}
}
int main() {
	freopen("E.in", "r", stdin);
	freopen("E.out", "w", stdout);
	cin >> n;
	if(n > 6) {
		cout << 1 << " " << n;
		return 0;
	}
	n += n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i];
		if(i > 1) {
			if(a[i] < mn) {
				mn = a[i];
				mnid = i;
			}
			if(a[i] > mx) {
				mx = a[i];
				mxid = i;
			}
		}
	}
	sum = a[1] + mx;
	used[1] = 1;
	used[mxid] = 1;
	rec();
	used[mxid] = 0;
	sum = a[1] + mn;
	used[mnid] = 1;
	rec();
	cout << a1 << " " << a2;
}
