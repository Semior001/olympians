//#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <set>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <numeric>
#include <vector>

#define clear(a) memset(a, 0, sizeof(a))
#define inf(a) memset(a, -1, sizeof(a))

using namespace std;

int n, l, r, a;
int ans = 0;

main()
    {
    freopen("D.in", "r", stdin);
    freopen("D.out", "w", stdout);
    cin >> n >> l >> r >> a;
    if(n == 1)
        {
        for(int i = l; i <= r; i++)
            if(!(i % a))
                ans++;
        }
    else
        {
        for(int i = l; i <= r; i++)
                for(int j = i; j <= r; j++)
                    if(!((i * j) / __gcd(i, j) % a))
                        cout << i << ' ' << j << endl, ans++;
        }
    cout << ans;
    }
