#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;
int main() {
    freopen("f.in", "r", stdin);
    freopen("f.out", "w", stdout);
    int n;
    cin >> n;
    int a[n];
    int b[n];
    for(int i=0; i<n; i++){
        cin >> a[i];
        b[i]=0;
    }
    for(int i=0; i<n; i++){
        for(int j=i; j<n; j++){
            if(i!=j) {
                if(!(a[i]&a[j])) {
                    b[i]=b[i]+1;
                }
                if(!(a[j]&a[i])) {
                    b[j]=b[j]+1;
                }
            }
        }
    }
    for(int i=0; i<n; i++) {
        cout << b[i];
    }
    return 0;
}
