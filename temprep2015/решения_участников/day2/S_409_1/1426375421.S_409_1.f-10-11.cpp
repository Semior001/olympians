#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;
int main() {
    freopen("f.in", "r", stdin);
    freopen("f.out", "w", stdout);
    int n;
    cin >> n;
    int a[n];
    for(int i=0; i<n; i++){
        cin >> a[i];
    }
    int r;
    r=0;
    for(int i=0; i<n; i++){
        r=0;
        for(int j=0; j<n; j++){
            if(!(a[i]&a[j])&&(i!=j)) {
                r=r+1;
            }
        }
        cout << r << " ";
    }
    return 0;
}
