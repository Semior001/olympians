#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;
int main() {
    freopen("e.in", "r", stdin);
    freopen("e.out", "w", stdout);
    int n, a;
    int r, t;
    cin >> n >> a;
    int b[n];
    int c[n];
    for(int i=0; i<2*n-1; i++) {
        cin >> b[i];
    }
    for(int i=0; i<2*n-1; i++) {
        for(int j=0; j<2*n-1; j++) {
            if(b[i]>b[j]) {
                t=b[i];
                b[i]=b[j];
                b[j]=t;
            }
        }
    }
    c[0]=(a+b[0]);
    r=0;
    for(int i=0; i<2*n-2; i=i+2){
        r++;
        c[r]=(b[i+1]+b[2*n-2-i]);
    }
    for(int i=1; i<n; i++) {
        for(int j=1; j<n; j++) {
            if(c[i]>c[j]) {
                t=c[i];
                c[i]=c[j];
                c[j]=t;
            }
        }
    }
    r=n;
    for(int i=1; i<n; i++) {
        if(c[0]>=c[i]) {
            r=i;
            break;
        }
    }
    cout << r << " ";
    c[0]=(a+b[2*n-2]);
    r=0;
    for(int i=2*n-3; i>0; i=i-2){
        r++;
        c[r]=(b[i]+b[i-1]);
    }
    for(int i=1; i<n; i++) {
        for(int j=1; j<n; j++) {
            if(c[i]<c[j]) {
                t=c[i];
                c[i]=c[j];
                c[j]=t;
            }
        }
    }
    r=n;
    for(int i=1; i<n; i++) {
        if(c[0]<c[i]) {
            break;
        }
        r--;
    }
    cout << r;
    fclose(stdin);
    fclose(stdout);
return 0;
}
