#include <iostream>
#include <cstdio>

using namespace std;

int n,l,r,a,res;

int gcd (int a,int b){
if (b==0) return a;
return gcd(b,a%b);
}

int lcm (int a,int b){
return a*b/gcd(a,b);
}

int main(){
freopen("D.in","r",stdin);
freopen("D.out","w",stdout);
cin >> n >> l >> r >> a;
if (n==1){for (int i=l;i<=r;i++){if (i%a==0) res++;}}
else {for (int i=l;i<=r;i++){
    for (int j=i;j<=r;j++){
    if (i*j>=a){if (lcm(j,i)%a==0) {res++;}}
    }
}}
cout << res;
return 0;
}
