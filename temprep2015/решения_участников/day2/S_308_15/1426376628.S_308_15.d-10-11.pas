var
  n,l,r,a,i,j,ans:longint;
function gcd(a,b:longint):longint;
begin
     if b=0 then gcd:=a
     else
       begin
            gcd:=gcd(b,a mod b);
       end;
end;
function lcm(a,b:longint):longint;
begin
     lcm:=(a div gcd(a,b)) * b;
end;

  begin
    ans:=0;
    assign(input,'D.in');
    reset(input);
    read(input,n,l,r,a);
    close(input);
    assign(output,'D.out');
    rewrite(output);
    if n=1 then
    begin
         for i:=l to r do begin
             if i mod a = 0 then
             begin
                inc(ans);
             end;
         end;
    end else if n=2 then
    begin
         for i:=l to r do begin
             for j:=l to r do begin
                 if lcm(i,j) mod a = 0 then
                   begin
                   inc(ans);
                   if i=j then inc(ans);
                   end;
             end;
         end;
         ans:=ans div 2;
    end;
    writeln(output,ans);
    close(output);
end.
