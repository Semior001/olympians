program project1;
var a:array[1..12] of longint;
    ctm:array[1..12] of boolean;
  n,i,j,k,l,maxplace,minplace,sum,doctm:longint;
  rating,erating:real;

begin
  assign(Input,'E.in');
  reset(Input);
  read(n);
  for i:=1 to 2*n do begin
      read(a[i]);
  end;
  close(Input);
  assign(Output,'E.out');
  rewrite(Output);
  case n of
  1:begin
      write(Output,'1 1');
  end;
  2:begin
      sum:=0;
      k:=-10;
      j:=40000000;
      for i:=2 to n*2 do begin
          sum:=sum+a[i];
          if a[i]>k then k:=a[i];
          if a[i]<j then j:=a[i];
      end;
      if sum-k-a[1]>k+a[1] then
         write(Output,'2 ') else write(Output,'1 ');
      if sum-j-a[1]>j+a[1] then
         write(Output,'2') else write(Output,'1');
  end;
  3:begin
      k:=-10;
      for i:=2 to n*2 do begin
          if a[i]>k then begin k:=a[i]; doctm:=i; end;
      end;
      ctm[doctm]:=true;
      rating:=a[1]+k;
      j:=-10;
      for i:=2 to n*2 do begin
          if (a[i]>j) and (not ctm[i]) then begin j:=a[i]; doctm:=i; end;
      end;
      ctm[doctm]:=true;
      l:=-10;
      for i:=2 to n*2 do begin
          if (a[i]>l) and (not ctm[i]) then begin l:=a[i]; doctm:=i; end;
      end;
      ctm[doctm]:=true;
      erating:=j+l;
      if rating>erating then
         write('1 ') else write('2 ');
      for i:=1 to 12 do begin
          ctm[i]:=false;
      end;
      k:=400000000;
      j:=-10;
      l:=-10;
      for i:=2 to 12 do begin
          if a[i]<k then begin k:=a[i]; doctm:=i; end;
      end;
      ctm[doctm]:=true;
      rating:=a[1]+k;
      for i:=2 to 12 do begin
          if (a[i]>j) and (not ctm[i]) then begin j:=a[i]; doctm:=i; end;
      end;
      ctm[doctm]:=true;
      for i:=2 to 12 do begin
          if (a[i]>l) and (not ctm[i]) then begin l:=a[i]; doctm:=i; end;
      end;
      erating:=j+l;
      ctm[doctm]:=true;
      if rating>erating then
        write('1') else write('2');
      end;
  end;
  Close(Output);
end.

