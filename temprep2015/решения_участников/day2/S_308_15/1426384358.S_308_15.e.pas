program NAURIZCUP;
var
  contestants:array[1..200000] of longint;
  n,i,gteamm,bteamm,bteami,gteami,bestPlace,tmaxi,tnmaxi,worstPlace:longint;
  bteamr,gteamr,otherr:real;
  taken:array[1..200000] of boolean;

procedure findbest();
var i,max,notsomax,maxi,notsomaxi:longint;
begin
  max:=-1;
  notsomaxi:=1;
  for i:=2 to n*2 do begin
      if (not taken[i]) and (contestants[i]>max) then
         begin
              maxi:=i;
              notsomax:=max;
              max:=contestants[i];
         end else
      if (not taken[i]) and (contestants[i]>notsomax) then
         begin
              notsomaxi:=i;
              notsomax:=contestants[i];
         end;
  end;
  otherr:=(notsomax+max)/2;
  taken[notsomaxi]:=true;
  taken[maxi]:=true;
  tmaxi:=maxi;
  tnmaxi:=notsomaxi;
end;

begin
  Assign(Input,'E.in');
  Reset(Input);
  Read(n);
  if n=1 then begin
       close(Input);
       assign(output,'E.out');
       rewrite(output);
       writeln(output,'1 1');
       close(output);
       halt;
  end;
  gteamm:=-1;
  bteamm:=4010101;
  bestPlace:=0;
  for i:=1 to 200000 do
      taken[i]:=false;
  for i:=1 to 2*n do begin
      Read(contestants[i]);
      if i<>1 then begin
      if contestants[i]>gteamm then
         begin
         gteamm:=contestants[i];
         gteami:=i;
         end;
      if contestants[i]<bteamm then
         begin
         bteamm:=contestants[i];
         bteami:=i;
         end;
      end;
  end;
  taken[gteami]:=true;
  bteamr:=(bteamm+contestants[1])/2;
  gteamr:=(gteamm+contestants[1])/2;
  Close(Input);
  {writeln('MY GOOD TEAM: ',gteamm,' ',contestants[1]);
  writeln('MY RATING: ',gteamr:0:3);   }
  repeat
    findbest;
    {writeln('THEIR BEST TEAM NOW: ',contestants[tmaxi],' ',contestants[tnmaxi]);
    writeln('THEIR RATING: ',otherr:0:3); }
    inc(bestPlace);
  until otherr<=gteamr;
  taken[bteami]:=false;
  for i:=1 to 200000 do
      taken[i]:=false;
  {writeln('MY BAD TEAM: ',bteamm,' ',contestants[1]);
  writeln('MY RATING: ',bteamr:0:3);}
  repeat
    findbest;
    {writeln('THEIR BEST TEAM NOW: ',contestants[tmaxi],' ',contestants[tnmaxi]);
    writeln('THEIR RATING: ',otherr:0:3);  }
    inc(worstPlace);
  until otherr<=bteamr;
  assign(output,'E.out');
  rewrite(output);
  writeln(output,bestPlace,' ',worstPlace);
  close(output);
end.

