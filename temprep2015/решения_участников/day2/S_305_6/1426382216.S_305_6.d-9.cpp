#include <iostream>
#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <deque>
#include <queue>
#include <cmath>
using namespace std;
int main()
{
    freopen("D.in","r",stdin);
    freopen("D.out","w",stdout);
    ios_base::sync_with_stdio();
    long long int a,c,l,r;
    cin>>a>>c>>l>>r;
    long long int x=a-c,counter=0;
    if(l>x||x<=0)
    {
        cout<<0; return 0;
    }
    for(int i=1;i*i<=max(r,a);i++)
    {
        if(x%i==0)
        {
            long long int k=i,p=x/i;
            if(k>=p)break;
            if(x%k==0&&a%k==c&&k>=l&&k<=r)
            {
                counter++;
            }
            if(x%p==0&&a%p==c&&p<=r&&p>=l)
            {
                counter++;
            }
        }

    }
    cout<<counter;
}
