#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "D."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define sz(v) v.size()
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);
const ll MOD=(ll)(1e9+7);

int n,l,r,c,ans,q,w,a[MAXN],t[4*MAXN];

int gcd(int a,int b)
{
	while(b)
	{
		a%=b;
		swap(a,b);
	}
	return a;
}
int lcm(int a,int b)
{
	return (a*b)/gcd(a,b);
}         
vector<int> v;
vector<pair<int,int> > p;
int main()
{
	fr fw
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  cin>>n>>l>>r>>c;
  forr(l,r*r+100,i)
  	if(i%c==0)
  		v.pb(i);
  p.pb(mp(1,c));
  if(n==1)
  {
  	forr(0,sz(v)-1,i)
  		if(v[i]>=l && v[i]<=r)
  			ans++;
  }
  else
  {
  	forr(l,r,i)
  	{
  		forr(l,r,j)
  		{  
  			bool flag=0;
  			if(lcm(i,j)%c==0 && i!=j)
  			{
  				forr(0,sz(p)-1,k)
  				{
  					if(p[k].S==i && p[k].F==j)
  					{
  						flag=1;
  						break;
  					}
  				} 
  				if(!flag) 
  					ans++,p.pb(mp(i,j));
  			} 
  			//cout<<lcm(i,j)<<' ';
  		}
  	}	
  }					
  cout<<ans%MOD;
	return 0;
}