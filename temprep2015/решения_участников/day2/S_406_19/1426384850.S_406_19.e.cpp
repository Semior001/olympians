#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "E."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define sz(v) v.size()
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);

ll n,z,a[MAXN],ans,w,res;
double q;
vector<double> v,b;

int main()
{
	fr fw
  ios_base::sync_with_stdio(false);
  cin.tie(0);
	cin>>n;
	cin>>z;
	forr(2,n+n,i)
	{
		cin>>a[i];
	}
	if(n==1)
	{
		cout<<"1 1";
			return 0;
	}
	sort(a+2,a+n+n+1);
	q=(double)(z+a[n+n])/2;
	for(int i=2,j=n+n-1;i<j;++i,j--)
	{
		v.pb((double)(a[i]+a[j])/2);		
	}
	w=(double)(z+a[2])/2;
	for(int i=3;i<=n+n;i+=2)
		b.pb((double)(a[i]+a[i+1])/2);
	sort(all(v));
	//reverse(all(b));
	forr(0,sz(b)-1,i)
	{
		if(b[i]<=w)
			res++;	
		else
			break;
	}
	if(v.back()<=q)
		ans=1;
	else
	{
		ans=n;
		reverse(all(v));
		for(int i=0;i<sz(v);i++)
		{
			if(v[i]<q)
			{
					ans++;
					break;
			}	
		}
	}
	cout<<ans<<' '<<n-res;
	return 0;
}