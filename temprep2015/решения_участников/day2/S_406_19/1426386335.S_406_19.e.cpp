#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "E."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define sz(v) v.size()
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);
int z,ans1,ans2,n,a[MAXN],h;
double q,w;
vector<double> v,b;
int main()
{
  fr fw
  ios_base::sync_with_stdio(false);
  cin.tie(0);
	cin>>n;
	n+=n;
	cin>>z;
	n--;
	forr(1,n,i)
		cin>>a[i];
	if(n==1)
	{
		cout<<"2 2";
		return 0;
	}
	sort(a+1,a+n+1);
	q=(double)(z+a[n])/2;
	w=(double)(z+a[1])/2;
	for(int i=1,j=n-1;i<j;i++,j--)
		v.pb(double(a[i]+a[j])/2);
	for(int i=2;i<=n;i+=2)
		b.pb(double(a[i]+a[i+1])/2);
	sort(all(v)); /*
	cout<<w<<' ';
	forr(0,sz(b)-1,i)
		cout<<b[i]<<' ';*/
	forr(0,sz(v)-1,i)
	{
		if(q>v[i])
			ans1++;
		else
			break;
	}
	sort(all(b));
	forr(0,sz(b)-1,i)
	{
		if(w<b[i])
			ans2++;	
	}    
	h=(n+1)>>1;
	cout<<h-ans1<<' '<<h-ans2;
	return 0;
}