#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "D."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define sz(v) v.size()
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);
const ll MOD=(ll)(1e9+7);

int n,l,r,c,ans,q,w,a[MAXN],t[4*MAXN];

int gcd(int a,int b)
{
	while(b)
	{
		a%=b;
		swap(a,b);
	}
	return a;
}
int lcm(int a,int b)
{
	return (a*b)/gcd(a,b);
}         /*
void build(int v,int tl,int R)
{
	if(tl==R)
		t[v]=a[tl];
	else
	{
		int m=(tl+R)>>1;
		build(v+v,tl,m);
		build(v+v+1,m+1,R);
		t[v]=lcm(t[v+v],t[v+v+1]);
	}
}       
int get(int v,int tl,int R,int l,int r)
{
	if(l>r)
		return 0;
	if(tl==l && R==r)
		return t[v];
	int m=(tl+R)>>1;
	return lcm(get(v+v,tl,m,l,min(m,r)),get(v+v+1,m+1,R,max(m+1,l),r));
}           
void update(int v,int tl,int R,int pos,int x)
{
	if(tl==R)
		t[v]=x;
	else
	{
		int m=(tl+R)>>1;
		if(pos<=m)
			update(v+v,tl,m,pos,x);
		else
			update(v+v,m+1,R,pos,x);
		t[v]=lcm(t[v+v],t[v+v+1]);
	}
}           */
vector<int> v;
int main()
{
	fr fw
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  cin>>n>>l>>r>>c;
  /*forr(l,r*r,i)
  	if(i%a==0)
  		v.pb(i);*/
  forr(1,n,i)
  	a[i]=l;
  w=l+1;
  forr(1,n,i)
  {
  	q=1;
  	int g=0;
  	forr(1,i-1,j)
  		q+=lcm(q,a[j]);
  	forr(i+1,n,j)
  		q+=lcm(q,a[j]);
  	while(a[i]+g<r)
  	{
  		if(lcm(q,a[i]+g)%c==0)
  			ans++;
  		ans%=MOD;
  		g++;
  	}
  	a[i]=w;
  	w++;
  	if(w==r+1)
  		w=l;
  }			
  cout<<ans;
	return 0;
}