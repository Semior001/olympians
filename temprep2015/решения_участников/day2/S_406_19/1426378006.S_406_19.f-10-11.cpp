#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "F."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);

ll q,n,a[MAXN],b[MAXN];

int main()
{
	fr fw
	ios_base::sync_with_stdio(0);
	cin.tie(0);
  cin>>n;
  forr(1,n,i)
  	cin>>a[i];
  forr(1,n,i)
  {
  	q=0;
  	forr(1,n,j)
  	{
  		if(i!=j && a[i]&a[j]==0)
  			q++;
  	}
  	b[i]=1;
  }
  forr(1,n,i)
  	cout<<b[i]<<' ';
	return 0;
}