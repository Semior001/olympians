#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "E."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()
#define F first
#define S second
#define sz(v) v.size()
#define forr(xx,yy,zz) for(int zz=xx;zz<=yy;zz++)


using namespace std;

const int MAXN=(int)(1e5+11);

int n,z,a[MAXN],ans,res,h;
double q,w;
vector<double> v,b;

int main()
{
	fr fw
  ios_base::sync_with_stdio(false);
  cin.tie(0);
	cin>>n;
	n+=n;
	cin>>h;
	n--;
	for(int i=1;i<=n;i++)	cin>>a[i];
	if(n==1)
	{
		cout<<"1 1";
		return 0;
	}
	sort(a+1,a+n+1);
	q=(double)(h+a[n])/2;
	forr(1,(n-1)>>1,i)
		v.pb((double)(a[i]+a[n-i])/2);
	sort(all(v));
	if(v.back()<=q)
		ans=1;
	else
	{
		ans=(n+1)/2;
		reverse(all(v));
		for(int i=0;i<sz(v);i++)
		{
			if(v[i]<=q)
			{
				ans=i;
					break;
			}	
		}
	}   
	w=(double)(h+a[1])/2;
	for(int i=2;i<=n;i+=2)
		b.pb((double)(a[i]+a[i+1])/2);	
/*	cout<<w<<' ';
	forr(0,sz(b)-1,i)
		cout<<b[i]<<' ';
	*/
	forr(0,sz(b)-1,i)
	{
		if(b[i]<=w)
			res++;
		else
			break;		
	} 
	cout<<ans<<' '<<3;
	return 0;
}