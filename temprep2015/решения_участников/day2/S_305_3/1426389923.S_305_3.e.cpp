#include <iostream>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;
#define fname "E"
int a[1234567], kol = 0, kol1 = 0;
double kom[123456], kom1[123456];
bool was[1234567];
int main () {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int n;
	cin >> n;
	int Amantai;
	cin >> Amantai;
	for (int i = 1; i < n * 2; ++i) {
		scanf("%d", &a[i]);
	}
	sort(a + 1, a + n * 2);
	double Amantai_team1 = double((double(Amantai + a[n * 2 - 1])) / 2);
	double Amantai_team2 = double((double(Amantai + a[1])) / 2);
	int ans = 1, l, r, pos, mid, kol = n * 2 - 2;
	bool ot;
	for (int i = n * 2 - 3; i >= 1; --i) {
		if (was[i])
            continue;
		l = i + 1;
		ot = false;
		r = n * 2 - 2;
		pos = -1;
		while (l <= r) {
			mid = (l + r) / 2;
			if (double((double(a[i] + a[mid])) / 2) <= Amantai_team1 && !was[mid]) {
				if (mid > pos)
					pos = mid;
			    l = mid + 1;
			}
			else {
				r = mid - 1;
                if (r < l && pos == -1)
                    ot = true;
            }
		}
		if (pos == -1) {
            if (ot)
                ++ans;
			for (int j = kol; j >= 1; --j)
				if (!was[j]) {
					kol = j;
					was[j] = true;
					break;
				}
		}
		else
			was[pos] = true;
	}
   cout << ans << " ";
	kol = 2;
	ans = n;
	bool ok;
	for (int i = n * 2 - 1; i >= 3; --i) {
		if (was[i])
            continue;
		l = 2;
		ok = false;
		r = i - 1;
		pos = -1;
		while (l <= r) {
			mid = (l + r) / 2;
			if (double((double(a[i] + a[mid])) / 2) > Amantai_team2 && !was[mid]) {
				if (mid < pos)
					pos = mid;
				r = mid - 1;
			}
			else {
				l = mid + 1;
                if (l > r && pos == -1)
                    ok = true;
			}
		}
		if (pos == -1) {
            if (ok)
                ans--;
			for (int j = kol; j <= n * 2 - 1; --j)
				if (!was[j]) {
					kol = j;
					was[j] = true;
					break;
				}
		}
		else
            was[pos] = true;
	}
    cout << ans << " ";
	return 0;
}
