#include <iostream>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;

#define fname "D"
int main () {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int a, c, l, r;
	cin >> a >> c >> l >> r;
	if (l == 0) 
		l = 1;
	int ans = 0;
	for (int i = l; i <= r; ++i) {
		if (a % i == c)
			ans++;
	}
	cout << ans;
	return 0;
}
