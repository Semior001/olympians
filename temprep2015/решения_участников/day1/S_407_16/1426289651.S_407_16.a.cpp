#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "A"
const int inf = (int)(1e9);
const int maxn = (int)(1e5) + 123;
using namespace std;
int n, m, l, r, c;
vector< int > v[5000];
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m;
	for(int i = 1; i <= m; ++i)
	{
		cin >> l >> r >> c;
		if(c == 0)
		{
			for(int j = l; j <= r; ++j)
				v[j].pop_back();
		}
		else
			for(int j = l; j <= r; ++j)
				v[j].pb(c);
	}
	for(int i = 1; i <= n; ++i)
		cout << (!v[i].empty() ? v[i].back() : 0) << " ";
	return 0;
}

