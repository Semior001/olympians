#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "B"
const int inf = (int)(1e9);
const int mx = (int)(1e2) + 123;
using namespace std;
int n, nn, m, k, a[mx], b[mx], sa[mx][mx], sb[mx][mx], cura[mx], curb[mx], ans, cnta[mx], cntb[mx];
vector< pair< int, int > > st;
vector< int > v, stud;
void make_mask(int x)
{
	v.clear();
	while(x > 1)
	{
		v.pb(x % 2);
		x /= 2;
	}
	if(x == 1)
		v.pb(x);
	reverse(v.begin(), v.end());
}
void rec(int pos)
{
	if(pos == nn)
	{
		bool f = 1;
		for(int i = 1; i <= m; ++i)
			cura[i] = a[i];
		for(int i = 1; i <= k; ++i)
			curb[i] = b[i];
		for(int i = 0; i < st.size(); ++i)
		{
			cura[st[i].first]--, curb[st[i].second]--;
			if(cura[st[i].first] < 0 || curb[st[i].second] < 0)
				f = 0;
		}
		if(f == 1)
			ans = max(ans, pos);
		return;	
	}
	for(int i = 1; i <= cnta[stud[pos]]; ++i)
		for(int j = 1; j <= cntb[stud[pos]]; ++j)
		{
			st.pb(mp(sa[stud[pos]][i], sb[stud[pos]][j]));
			rec(pos + 1);
			st.pop_back();
		}	
}
int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m >> k;
	for(int i = 1; i <= m; ++i)
		cin >> a[i];
	for(int i = 1; i <= k; ++i)
		cin >> b[i];
	for(int i = 1; i <= n; ++i)
	{
		cin >> cnta[i];
		for(int j = 1; j <= cnta[i]; ++j)
			cin >> sa[i][j];
	}
	for(int i = 1; i <= n; ++i)
	{
		cin >> cntb[i];
		for(int j = 1; j <= cntb[i]; ++j)
			cin >> sb[i][j];
	}
	for(int mask = 0; mask < (1 << n); ++mask)
	{
		make_mask(mask);	
		stud.clear();
		for(int i = 0; i < v.size(); ++i)
			if(v[i])
				stud.pb(i + 1);
		nn = stud.size();
		rec(0);
	}
	cout << ans;
	return 0;
}



