#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "C"
const int inf = (int)(1e9);
const int mx = (int)(2e3) + 123;
using namespace std;
ll n, a[mx], u[mx], d[mx], ans, r, l;
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++i)
		cin >> a[i];
	/*
	for(int i = n; i >= 1; --i)
	{
		if(u[a[i]] == 0)
			d[i] = n + 1;
		else
			d[i] = u[a[i]];
		u[a[i]] = i;
		//cout << i << " " << d[i] << endl;	
	}
	*/	
	for(int i = 1; i <= n; ++i)
	{
		for(int j = i; j <= n; ++j)
		{
			u[a[j]] = 1;
			int k = j + 1;
			while(true)
			{
				int l = -1, r = -1;
				for(int id = k; id <= n; ++id)
				{
					if(!u[a[id]] && l == -1)
						l = id;
					if(u[a[id]] && l != -1)	
					{
						r = id - 1;
						break;
					}
				}
			//	cout << l << " " << r << endl;
				if(l == -1)
					break;
				if(l != -1 && r == -1)
					r = n;
				ans = ans + (r - l + 1) * (r - l + 2) / 2;
				k = r + 1;
				if(k > n)
					break;
			}
		}
		for(int j = 1; j <= n; ++j)
			u[j] = 0;
	}
	cout << ans;
	return 0;
}




