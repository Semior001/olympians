#include<bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define fname "C"
const int inf = (int)(1e9);
const int mx = (int)(2e3) + 123;
using namespace std;
ll n, a[mx], u[mx], d[mx], ans, r, l;
int main()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++i)
		cin >> a[i];
	for(int i = n; i >= 1; --i)
	{
		if(u[a[i]] == 0)
			d[i] = n + 1;
		else
			d[i] = u[a[i]];
		u[a[i]] = i;
	//	cout << i << " " << d[i] << endl;	
	}		
	for(int i = 1; i <= n; ++i)
	{
		for(int j = i; j <= n; ++j)
		{
			l = j + 1;
			while(true)
			{
				r = n + 1;
				for(int k = i; k <= j; ++k)
					if(d[k] > j)
						r = min(r, d[k]);
				r--;
				if(r >= l)
					ans = ans + (r - l + 1) * (r - l + 2) / 2;
			//	cout << l << " " << r << " " << ans << endl;
				l = r + 1;
				if(l >= n)
					break;		
			}
		//	cerr << i << " " << j << " " << ans << endl;
		}																																																													
	}
	cout << ans;
	return 0;
}



