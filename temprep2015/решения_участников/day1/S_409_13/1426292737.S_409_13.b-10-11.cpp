//#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <set>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <numeric>
#include <vector>

#define clear(a) memset(a, 0, sizeof(a))
#define inf(a) memset(a, -1, sizeof(a))

using namespace std;

int n, m, k;
int c[555], p[555];
bool used_c[555][555];
bool used_p[555][555];
int ans = 0;
int x, q;

main()
    {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> n >> m >> k;
    for(int i = 1; i <= m; cin >> c[i], i++);
    sort(c + 1, c + 1 + m);
    for(int i = 1; i <= k; cin >> p[i], i++);
    sort(p + 1, p + 1 + m);
    for(int i = 1; i <= n; i++)
        {
        cin >> x;
        for(int j = 1; j <= x; j++)
                cin >> q, used_c[i][q]++;
        }
    for(int i = 1; i <= n; i++)
        {
        cin >> x;
        for(int j = 1; j <= x; j++)
                cin >> q, used_p[i][q]++;
        }
    for(int i = 1; i <= n; i++)
        {
        int id_c = 1;
        while(!used_c[i][id_c] && id_c <= m && c[id_c])
            id_c++;
        if(id_c > m)
                continue;
        int id_p = 1;
        while(!used_p[i][id_p] && id_p <= m && p[id_p])
            id_p++;
        if(id_p > k)
                continue;
        c[id_c]--, p[id_p]--, ans++;
        }
    cout << ans;
    }
/*
2 3 1
5 1 3
2
3 1 2 3
1 2
1 1
1 1
*/
