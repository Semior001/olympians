//#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <set>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <numeric>
#include <vector>

#define clear(a) memset(a, 0, sizeof(a))
#define inf(a) memset(a, -1, sizeof(a))

using namespace std;

struct no
    {
    long long int s, t;
    }c[666], p[666];
long long int key1[555], key2[666];
long long int n, m, k;
bool used_c[666][666];
bool used_p[666][666];
long long int ans = 0;
long long int x, q;

bool cmp(no z, no w)
    {
    return z.s < w.s;
    }

main()
    {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> n >> m >> k;
    for(int i = 1; i <= m; cin >> key1[i], c[i].s = 0, c[i].t = i, i++);
    for(int i = 1; i <= k; cin >> key2[i], p[i].s = 0, p[i].t = i, i++);
    for(int i = 1; i <= n; i++)
        {
        cin >> x;
        for(int j = 1; j <= x; j++)
                cin >> q, used_c[i][q]++, c[q].s++;
        }
    for(int i = 1; i <= n; i++)
        {
        cin >> x;
        for(int j = 1; j <= x; j++)
                cin >> q, used_p[i][q]++, p[q].s++;
        }
    sort(c + 1, c + 1 + m, &cmp);
    sort(p + 1, p + 1 + k, &cmp);
    for(int i = 1; i <= n; i++)
        {
        int id_c = 1;
        while((!used_c[i][c[id_c].t] || !key1[c[id_c].t]) && id_c <= m)
            id_c++;
        if(id_c > m)
                continue;
        int id_p = 1;
        while((!used_p[i][p[id_p].t] || !key2[p[id_p].t]) && id_p <= k)
            id_p++;
        if(id_p > k)
                continue;
        key1[c[id_c].t]--, key2[p[id_p].t]--, ans++;
        }
    cout << ans;
    }
/*
2 3 1
1 1 3
2
3 1 2 3
1 1
1 1
1 1
*/
