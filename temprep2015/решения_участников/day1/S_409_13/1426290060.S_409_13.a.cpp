//#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <set>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <numeric>
#include <vector>

#define clear(a) memset(a, 0, sizeof(a))
#define inf(a) memset(a, -1, sizeof(a))

using namespace std;

int n, m;
int a[111111];
vector < int > k[111111];

main()
    {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    clear(a);
    cin >> n >> m;
    for(int i = 1; i <= m; i++)
        {
        int l, r, c;
        cin >> l >> r >> c;
        if(c)
            for(l; l <= r; l++)
                    k[l].push_back(c);
        else
            for(l; l <= r; l++)
                k[l].pop_back();
        }
    for(int i = 1; i <= n; i++)
            cout << ((!k[i].size()) ? 0 : k[i].back()) << ' ';
    }
/*
5 3
1 5 1
2 4 0
4 5 10
*/
