#include <bits/stdc++.h>
#define fname "A"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int n, m, l, r, c;
	cin >> n >> m;
	vector<vector<int> > v(n);
	for(int i = 0; i < m; ++i){
		cin>> l >> r >> c;
		l--;
		if(!c)
			for(int j = l; j < r; ++j)
				v[j].pop_back();
		else {
			for(int j = l; j < r; ++j)
				v[j].pb(c);
		}
	}
	for (int i = 0; i < n; ++i)
		cout << (sz(v[i]) ? v[i].back() : 0) << " ";
	return 0;
}
