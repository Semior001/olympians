#include <bits/stdc++.h>
#define fname "B"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 1000;
const int INF = 1e9 + 7;
string s;
map<char, int> cur;
inline int get_k(const int &l, const int &r){
	cur.clear();
	for(int i = l; i <= r; ++i)
		cur[s[i]]++;
	return sz(cur);
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> s;
	int k;
	cin >> k;
	int n = sz(s);
	int cur_min = INF;
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
			//cout << i << " " << j  << " -> " << get_k(i, j) << "\n";
	        	if((j - i + 1) < cur_min && get_k(i, j) == k){
	        		cur_min = j - i + 1;	      
			}
		}
	}
	cout << (cur_min == INF ? -1 : cur_min);
	return 0;
}
