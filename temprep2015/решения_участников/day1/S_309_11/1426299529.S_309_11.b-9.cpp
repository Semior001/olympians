#include <bits/stdc++.h>
#define fname "B"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 1000;
const int INF = 1e9 + 7;
char s[MAXN];
set<int> m [26];

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
//	ios_base::sync_with_stdio(0);
//	cin.tie(NULL);
	gets(s);
	int k;
	scanf("%d", &k);
	int n = strlen(s);
	for (int i = 0; i < n; i++){
		m[s[i] - 'a'].insert(i);
	}	
	int cur_min = INF, ans = 0;
	auto pos1 = m[0].begin();
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
	        	if((j - i) < cur_min && k <= (j - i + 1)){
				ans = 0;
					for (int p = 0; p < 26; ++p){	
						if(sz(m[p]) == 0)continue;
						pos1 = m[p].lower_bound(i);
						if(pos1 == m[p].end())
							continue;
					ans += (*pos1 <= j);
					}        		
				if(ans == k)cur_min = j - i;	      
			}
		}
	}
	printf("%d", (cur_min == INF ? -1 : cur_min + 1));

//	cout << tim();
	return 0;
}