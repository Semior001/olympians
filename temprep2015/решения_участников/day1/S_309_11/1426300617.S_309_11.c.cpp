#include <bits/stdc++.h>
#define fname "C"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 5000 + 10;

map<int, int> cur;

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
        int n, ans = 0, a[MAXN];
	cin >> n;
	for (int i = 0; i < n; ++i){
		cin >> a[i];
	}
	for (int i = 0; i < n; ++i){
		cur.clear();
		for (int j = i; j < n; ++j){
		        cur[a[j]]++;
//			for(int l = i; l <= j; ++l)cur[a[l]]++;//todo optimize

			for (int l = j + 1; l < n; ++l){
				if(cur[a[l]] > 0)continue;
				for (int r = l; r < n; ++r){
					bool ok = 1;
			        	for (int k = l; k <= r; ++k)
						if(cur[a[k]] > 0){ok = 0; break;}
					
//					if(ok)cout << i << " " << j << " - " << l << " " << r << "\n";
					ans += ok;
					if(!ok)break;
				}
			}
		}
	}
	cout << ans;
	return 0;
}
