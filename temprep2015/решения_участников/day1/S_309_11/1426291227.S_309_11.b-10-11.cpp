#include <bits/stdc++.h>
#define fname "B"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 1000;
const int INF = 1e9 + 7;
char s[MAXN];
int cur[26];
inline int get_k(const int &l, const int &r){
	for(int i = 0; i < 26; ++i)cur[i] = 0;
	for(int i = l; i < r; ++i)
		cur[s[i]-'a']++;
	int ans = 0;
	for(int i = 0; i < 26; ++i)
		ans += (cur[i] > 0);
	return ans;
}
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	gets(s);
	int k;
	cin >> k;
	int n = strlen(s);
	int cur_min = INF;
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
	        	if((j - i) < cur_min && get_k(i, j) == k){
	        		cur_min = j - i;	      
			}
		}
	}
	cout << (cur_min == INF ? -1 : cur_min);
	return 0;
}
