#include <bits/stdc++.h>
#define fname "B"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 1000;
const int INF = 1e9 + 7;
string s;
set<int> m [26], se;
vector<int>v;

int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	cin >> s;
	int k;
	cin >> k;
	int n = sz(s);
	for (int i = 0; i < n; i++){
		m[s[i] - 'a'].insert(i);
		se.insert(s[i] - 'a');
	}
	forit(it, se)v.pb(it);
	int cur_min = INF, ans = 0;
	int ok = 0;
	set<int>::iterator pos1;
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
	        	if((j - i) < cur_min && k <= (j - i + 1)){
				ans = 0; 
					for (int pp = 0; pp < sz(v); ++pp){
						int p = v[pp];//cout << (char)(p + 'a') << " ";
						pos1 = m[p].lower_bound(i);
						if(pos1 == m[p].end())
							continue;
					ans += (*pos1 <= j);
					}ok++;        		
				if(ans == k)cur_min = j - i;	      
			}
		}
	}
	cout << (cur_min == INF ? -1 : cur_min + 1);

//	cout << tim();
	return 0;
}