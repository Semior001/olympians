#include <bits/stdc++.h>
#define fname "C"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int N = 5555;
int cur[N];
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
        int n, a[N];
	cin >> n;
	for (int i = 0; i < n; ++i){
		cin >> a[i];
		//v[a[i]].insert(i);
		//s.insert(a[i]);
	}
	ll res = 0;
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < n; ++j)cur[a[j]] = 0;
	
		for (int j = i; j < n; ++j){
			cur[a[j]]++;
			ll ans = 0;
			for(int l = j + 1; l < n; ++l){
		        	if(cur[a[l]] == 0)ans++;
				else res += (ans* 1ll *(ans + 1))/2ll, ans = 0;
			}	
			res += (ans* 1ll *(ans + 1))/2ll, ans = 0;
		}
	}
	cout << res;
	return 0;
}
