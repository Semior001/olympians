#include <bits/stdc++.h>
#define fname "B"

#define pb push_back
#define mp make_pair
#define F first
#define S second
#define forit(it, s) for(auto it :s)
#define vi vector<int>
#define sz(s) (int)s.size()
#define all(s) s.begin(), s.end()

using namespace std;

typedef long long ll;
typedef double ld;
const int MAXN = 1e5 + 1000;
const int INF = 1e9 + 7;
string s;
set<int> m [26];



inline int get_k(const int &l, const int &r){
	int ans = 0;
	auto pos1 = m[0].begin();
	for (int i = 0; i < 26; ++i){
		pos1 = m[i].lower_bound(l);
		if(pos1 == m[i].end())
			continue;
		ans += (*pos1 <= r);
	}
//	cout << l << " " << r << " -> " << ans <<"\n";
	return ans;
}
inline ld tim(){
	return clock()*1.0 / CLOCKS_PER_SEC*1.0;
}                 
int main(){
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	cin >> s;
	int k;
	cin >> k;
	int n = sz(s);
	for (int i = 0; i < n; i++){
		m[s[i] - 'a'].insert(i);
	}	
	//getm(0, 3, 5);
	//return 0;//todo
	int cur_min = INF;
	for (int i = 0; i < n; ++i){
		for (int j = i; j < n; ++j){
	        	if((j - i) < cur_min && k <= (j - i + 1)){
	        		if(get_k(i, j) == k)cur_min = j - i;	      
			}
		}
	}
	cout << (cur_min == INF ? -1 : cur_min + 1);

//	cout << tim();
	return 0;
}