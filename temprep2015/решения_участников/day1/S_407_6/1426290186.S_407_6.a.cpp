#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <stack>
using namespace std;
int n, m;
stack<int> st[100010];
int main()
{
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	cin >> n >> m;
	//cerr << "Q";
	for(int i = 1; i <= m; i++)
	{
		int l, r, c;
		cin >> l >> r >> c;
		if(c == 0)
		{
			for(int j = l; j <= r; j++)
			{
				st[j].pop();
				//cerr << "W" << endl;
			}
		}
		else
		{
			for(int j = l; j <= r; j++)
			{
				st[j].push(c);
				//cerr << "T";
			}
		}
	}
	for(int i = 1; i <= n; i++)
	{
		if(!st[i].empty())
			cout << st[i].top();
	   	else
	   		cout << 0;
		cout << " ";
	}
	return 0;
}