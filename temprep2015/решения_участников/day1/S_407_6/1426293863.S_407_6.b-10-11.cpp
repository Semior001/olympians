#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <stack>
#include <string>
#include <string.h>
#define N 2015
#define INF 1e9
using namespace std;
int n, m, k;
int c[N][N];
int f[N][N];
int C[N];
int P[N];
int ptr[N];
int s, t;
int d[N];
bool bfs()
{
	int q[N];
	int qt = 0, qh = 0;
	q[qt] = s;
	qt++;
	bool u[N];
	memset(u, 0, sizeof(u));
	d[s] = 1;
	u[s] = 1;
	while(qh < qt)
	{
		int v = q[qh];
		qh++;
		//cerr << v << " " << d[v]<< endl;
		for(int i = 1; i <= 1500; i++)
		{
			if(c[v][i] > f[v][i] && !u[i])
			{
				u[i] = 1;
				q[qt] = i;
				qt++;
				d[i] = d[v] + 1;
			}
		}
	}
	return d[t] != 0;
}
int dfs(int v, int flow = INF)
{
	if(v == t)
		return flow;
	if(flow == 0)
		return 0;
	//if(v == 1)
	//	cerr << ptr[v] << endl;
	for(int &i = ptr[v]; i <= 1500; i++)
	{
		if(d[v] + 1 == d[i])
		{
			//cerr << v << " " << d[3] << endl;
			//cerr << d[v] << " " << d[i] << endl;
			int fl = min(flow, c[v][i] - f[v][i]);
			int push = dfs(i, fl);
			//cerr << push << " " << v << endl;
			if(push > 0)
			{
				f[v][i] += push;
				f[i][v] = -1 *  f[v][i];
				return push;
			}
		}
	}
	//cerr << v << endl;
	return 0;
}
int main()
{
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	cin >> n >> m >> k;
	for(int i = 1; i <= m; i++)
	{
		cin >> C[i];
	}
	for(int i = 1; i <= k; i++)
	{
		cin >> P[i];
	}
	s = 1;
	for(int i = 1; i <= n; i++)
	{
		c[s][i + 1] = 1;
	}
	t = 1 + n + m + 1;
	for(int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		for(int j = 1; j <= x; j++)
		{
			int a;
			cin >> a;
			c[i + 1][1 + n + a] = 1;
		}
	}
	//cerr << "a";
	for(int i = 1; i <= m; i++)
	{
		c[i + n + 1][t] = C[i];
	}
	//cerr << "a";
	int flow = 0;
	while(true)
	{
		if(!bfs())
			break;
		memset(ptr, 0, sizeof(ptr));
		//cerr << "q";
		int pushed;
		while(pushed = dfs(s))
		{
			//cerr << pushed << "\n";
			//cerr << "w";
			flow += pushed;
			//cerr << endl;
			//cerr << ptr[2] << endl;
		}
		//cerr << ptr[1];
		memset(d, 0, sizeof(d));
		//cerr << d[t] << "<\n";
	}
	memset(c, 0, sizeof(c));
	memset(f, 0, sizeof(f));
	t = 1 + n + k + 1;
	for(int i = 1; i <= n; i++)
	{
		c[s][i + 1] = 1;
	}
	for(int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		for(int j = 1; j <= x; j++)
		{
			int a;
			cin >> a;
			c[i + 1][a + 1 + n] = 1;
		}
	}
	for(int i = 1; i <= k; i++)
	{
		c[i + 1 + n][t] = P[i];
	}
	int flow1 = 0;
	while(true)
	{
		if(!bfs())
			break;
		memset(ptr, 0, sizeof(ptr));
		//cerr << "q";
		int pushed;
		while(pushed = dfs(s))
		{
			//cerr << pushed << "\n";
			//cerr << "w";
			flow1 += pushed;
			//cerr << endl;
			//cerr << ptr[2] << endl;
		}
		//cerr << ptr[1];
		memset(d, 0, sizeof(d));
		//cerr << d[t] << "<\n";
	}
	/*
	for(int i = 1; i <= 5; i++)
	{
		for(int j = 1; j <= 5; j++)
		{
			cout << c[i][j] << " ";
		}
		cout << endl;
	}
	*/
	
	//cout  << ptr[2] << endl;
	/*
	int v = 0;
	int &j = v;
	j += 2;
	cout << v <<"<<<<\n";
	*/
	cout << min(flow, flow1);
	return 0;
}