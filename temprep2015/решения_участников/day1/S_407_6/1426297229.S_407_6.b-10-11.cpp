#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <stack>
#include <string>
#include <string.h>
#define N 2015
#define INF 1e9
#define ll long long
using namespace std;
ll n, m, k;
ll c[N][N];
ll f[N][N];
ll C[N];
ll P[N];
ll ptr[N];
ll s, t;
ll d[N];
bool bfs()
{
	ll q[N];
	ll qt = 0, qh = 0;
	q[qt] = s;
	qt++;
	bool u[N];
	memset(u, 0, sizeof(u));
	d[s] = 1;
	u[s] = 1;
	while(qh < qt)
	{
		ll v = q[qh];
		qh++;
		//cerr << v << " " << d[v]<< endl;
		for(ll i = 1; i <= 1500; i++)
		{
			if(c[v][i] > f[v][i] && !u[i])
			{
				u[i] = 1;
				q[qt] = i;
				qt++;
				d[i] = d[v] + 1;
			}
		}
	}
	return d[t] != 0;
}
ll dfs(ll v, ll flow = INF)
{
	if(v == t)
		return flow;
	if(flow == 0)
		return 0;
	//if(v == 1)
	//	cerr << ptr[v] << endl;
	for(ll &i = ptr[v]; i <= 1500; i++)
	{
		if(d[v] + 1 == d[i])
		{
			//cerr << v << " " << d[3] << endl;
			//cerr << d[v] << " " << d[i] << endl;
			ll fl = min(flow, c[v][i] - f[v][i]);
			ll push = dfs(i, fl);
			//cerr << push << " " << v << endl;
			if(push > 0)
			{
				f[v][i] += push;
				f[i][v] = -1 *  f[v][i];
				return push;
			}
		}
	}
	//cerr << v << endl;
	return 0;
}
int main()
{
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	cin >> n >> m >> k;
	s = 1;
	t = 1 + n + m + k + 1 + n;
	for(ll i = 1; i <= m; i++)
	{
		cin >> C[i];
		c[s][1 + i] = C[i];
		//cerr << s << " " << 1 + i << " = " << C[i] << endl;
	}
	for(ll i = 1; i <= k; i++)
	{
		cin >> P[i];
		c[1 + m + n + n + i][t] = P[i];
	}
	/*for(ll i = 1; i <= n; i++)
	{
		//c[s][i + 1] = 1;
	}*/
	for(ll i = 1; i <= n; i++)
	{
		ll x;
		cin >> x;
		for(ll j = 1; j <= x; j++)
		{
			ll a;
			cin >> a;
			c[a + 1][1 + m + i] = 1;
		}
	}
	for(ll i = 1; i <= n; i++)
	{
		c[1 + m + i][1 + m + i + n] = 1;
	}
	for(ll i = 1; i <= n; i++)
	{
		ll x;
		cin >> x;
		for(ll j = 1; j <= x; j++)
		{
			ll a;
			cin >> a;
			c[i + m + 1 + n][a + 1 + m + n + n] = 1;
		}
	}
	//cerr << "a";
	/*for(ll i = 1; i <= m; i++)
	{
		c[i + n + 1][t] = C[i];
	}
	*/
	//cerr << "a";
	ll flow = 0;
	while(true)
	{
		if(!bfs())
			break;
		memset(ptr, 0, sizeof(ptr));
		//cerr << "q";
		ll pushed;
		while(pushed = dfs(s))
		{
			//cerr << pushed << "\n";
			//cerr << "w";
			flow += pushed;
			//cerr << endl;
			//cerr << ptr[2] << endl;
		}
		//cerr << ptr[1];
		memset(d, 0, sizeof(d));
		//cerr << d[t] << "<\n";
	}
	/*
	for(ll i = 1; i <= 1 + m + n + n + k + 1; i++)
	{
		for(ll j = 1; j <= 1 + m + n + n + k + 1; j++)
		{
			cout << c[i][j] << " ";
		}
		cout << endl;
	}
	*/
	cout << flow;
	return 0;
}