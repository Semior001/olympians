#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
long long n, m, l[1009], r[1009], a[1009], x[1009], ubr[1009], done[1009];
int main()
{
    ifstream cin("A.in");
    ofstream cout("A.out");
    cin>>n>>m;
    for(int i=1;i<=m;i++)
    {
        cin>>l[i]>>r[i]>>x[i];
    }
    for(int i=m;i>=1;i--)
    {
        if(x[i]==0)
        {
            for(int j=l[i];j<=r[i];j++)
                ubr[j]++;
        }
        else
        {
            for(int j=l[i];j<=r[i];j++)
            {
                if(done[j])
                    continue;
                if(ubr[j]==0)
                {
                    done[j] = x[i];
                }
                else
                    ubr[j]--;
            }
        }
    }
    for(int i=1;i<=n;i++)
        cout<<done[i]<<" ";
}
