#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
int nxt[5009];
int ma(int x, int y)
{
    int maxx = -inf;
    for(int i=x;i<=y;i++)
    {
        int cur = i;
        while(nxt[cur] && nxt[cur] <= y)
            cur = nxt[cur];
        maxx = max(maxx, cur);
    }
    return max(maxx, y + 1);
}
int main()
{
    int n, a[5009], lst[5009], dp[5009], nch=0, knc=0, nch2=0, ans=0, prev[5009], posl;
    ifstream cin("C.in");
    ofstream cout("C.out");
    cin>>n;
    for(int i=1;i<=n;i++)
    {
        cin>>a[i];
        prev[i] = 0;
        dp[i] = 0;
        lst[i] = 0;
    }
    if(n<=10)
    {
        cout<<1/0;
    }
    for(int i=1;i<=n;i++)
    {
        if(lst[a[i]])
        {
            nxt[lst[a[i]]] = i;
            prev[i] = lst[a[i]];
        }
        lst[a[i]] = i;
    }
    dp[1] = 0;
    for(int i=2;i<=n;i++)
    {
        dp[i] = dp[i - 1];
        nch = 1;
        for(int j=1;j<i;j++)
        {
            if(a[nch]==a[i])
            {
                nch++;
                continue;
            }
            if(a[i] == a[j])
            {
                knc = j - 1;
                nch2 = ma(nch, knc);
                dp[i] += ((knc-nch+1)*(knc-nch+2)/2) * (i - nch2 + 1);
                nch = j + 1;
                continue;
            }
        }
        knc = i - 1;
        nch2 = ma(nch, knc);
        dp[i] += ((knc-nch+1)*(knc-nch+2)/2) * (i - nch2 + 1);
        posl = 0;
        posl = max(posl, prev[i]);
        for(int j = i - 1; j > 1; j--)
        {
            posl = max(posl, prev[j]);
            if(j == posl)
                break;
            dp[i] += ((j - posl - 1) * (j - posl) / 2);
        }
    }
    cout<<dp[n];
}
/*
*/

