#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
long long n, a[5009], nxt[5009], sum[5009], u[5009], x, y, ans, lst[5009];
void f(int x, int ind)
{
    while(nxt[ind])
    {
        u[a[ind]] = 1;
        ind = nxt[ind];
    }
}
void cnt(int beg, int en)
{
    int nch = en+1, knc;
    for(int i=en+1;i<=n;i++)
    {
        if(u[a[nch]] == 1)
        {
            nch++;
            continue;
        }
        if(u[a[i]])
        {
            knc = i - 1;
            ans += ((knc-nch+1)*(knc-nch+2) / 2);
            nch = i + 1;
        }
    }
    knc = n;
    ans += ((knc-nch+1)*(knc-nch+2) / 2);
}
int main()
{
    ifstream cin("C.in");
    ofstream cout("C.out");
    cin>>n;
    for(int i=1;i<=n;i++)
    {
        cin>>a[i];
        if(lst[a[i]])
            nxt[lst[a[i]]] = i;
        lst[a[i]] = i;
    }
    for(int i=1;i<=n;i++)
    {
        memset(u, 0, sizeof(u));
        for(int j = i; j < n;j++)
        {
            f(a[j], j);
            cnt(i, j);
            //cout<<i<<" "<<j<<" "<<ans<<endl;
        }
    }
    cout<<ans;
}
/*
*/

