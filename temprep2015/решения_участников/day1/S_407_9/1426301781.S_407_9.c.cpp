#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
int main()
{
    long long n, a[5009], lst[5009], dp[5009], nch=0, knc=0, nch2=0, ans=0, prev[5009], posl, en;
    ifstream cin("C.in");
    ofstream cout("C.out");
    cin>>n;
    for(int i=1;i<=n;i++)
    {
        cin>>a[i];
        dp[i] = 0;
    }
    dp[1] = 0;
    for(int i=2;i<=n;i++)
    {
        dp[i] = dp[i - 1];
        for(int j=1;j<i;j++)
        {
            if(a[j] == a[i])
                continue;
            en = 0;
            for(int k = j - 1;k>=1;k--)
            {
                if(a[k]==a[i])
                {
                   en = k;
                   break;
                }
            }
            nch = j;
            for(int k = i - 1; k >= 1; k--)
            {
                if(a[k] == a[j])
                {
                    nch = k;
                    break;
                }
            }
            dp[i] += (j - en) * (i - nch);
        }
    }
    cout<<dp[n];
}
/*
50
1 1 1 1 1 1 1 1 1 1
2 2 2 2 2 2 2 2 2 2
3 3 3 3 3 3 3 3 3 3
4 4 4 4 4 4 4 4 4 4
5 5 5 5 5 5 5 5 5 5
*/

