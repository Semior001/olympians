#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
long long n, a[5009], nxt[5009], sum[5009], p[5009], x, y, ans;
int main()
{
    ifstream cin("C.in");
    ofstream cout("C.out");
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    for(int i=1;i<=n;i++)
    {
        nxt[i] = n;
        for(int j=i+1;j<=n;j++)
        {
            if(a[j]==a[i])
            {
                if(nxt[i]==n)
                    nxt[i] = j;
                sum[i]++;
            }
        }
        p[i] = p[i - 1] + sum[i];
    }
    for(int i=1;i<=n;i++)
    {
        long long j = i, en = nxt[i];
        while(j<=n)
        {
            if(j > en)
                break;
            en = min(en, nxt[j]);
            x = p[j] - p[i - 1];
            y = n - j - x;
            ans += ((y + 1) * y / 2);
            j++;
        }
    }
    cout<<ans;
}
/*

*/

