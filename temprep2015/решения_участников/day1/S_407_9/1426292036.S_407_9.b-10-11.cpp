#include<iostream>
#include<algorithm>
#include<cstdio>
#include<string.h>
#include<string>
#include<cmath>
#include<queue>
#include<set>
#include<fstream>
#include<iomanip>
#include<utility>
#define ff first
#define ss second
#define inf 1000*1000*1000
using namespace std;
int n, m, k, kof[509], pir[509], l, kk[2009], pp[2009], tk[2009][2009], tp[2009][2009], x, y, curd[2009], u[2009], u2[2009], sum, ma = -inf;
vector<pair<int, int> > g;
int main()
{
    ifstream cin("B.in");
    ofstream cout("B.out");
    cin>>n>>m>>k;
    for(int i=1;i<=m;i++)
    {
        cin>>kof[i];
    }
    for(int i=1;i<=k;i++)
    {
        cin>>pir[i];
    }
    for(int i=1;i<=n;i++)
    {
        cin>>kk[i];
        for(int j=1;j<=kk[i];j++)
        {
            cin>>tk[i][j];
            x = i;
            y = tk[i][j]+n;
            g.push_back(make_pair(x, y));

        }
    }
    for(int i=1;i<=n;i++)
    {
        cin>>pp[i];
        for(int j=1;j<=pp[i];j++)
        {
            cin>>tp[i][j];
            x = i;
            y = tp[i][j]+n+m;
            g.push_back(make_pair(x, y));
        }
    }
    l = g.size();
    for(int i=0;i<(1<<l);i++)
    {
        //cout<<i<<":   ";
        memset(curd, 0, sizeof(curd));
        memset(u, 0, sizeof(u));
        memset(u2, 0, sizeof(u2));
        for(int j=0;j<l;j++)
        {
            if(i & (1<<j))
            {
                curd[g[j].ss]++;
                if(g[j].ss <= n + m)
                    u[g[j].ff] = 1;
                else
                    u2[g[j].ff] = 1;
                //cout<<1;
            }
            //else
            //    cout<<0;
        }
        //cout<<" ";
        bool p=1;
        for(int j=n+1;j<=n+m;j++)
        {
            if(curd[j] > kof[j - n])
            {
                p = 0;
                break;
            }
        }
        for(int j=n+m+1;j<=n+m+k;j++)
        {
            if(curd[j] > pir[j - n - m])
            {
                p=0;
                break;
            }
        }
        //cout<<p<<endl;
        if(!p)
        {
            continue;
        }
        sum = 0;
        for(int j=1;j<=n;j++)
        {
            //cout<<u[j]<<" "<<u2[j]<<endl;
            if(u[j]+u2[j]==2)
            {
                sum++;
            }
        }
        ma=max(ma, sum);
    }
    cout<<ma;
}
/*
2 3 1
5 1 3
2
3 1 2 3
1 2
1 1
1 1
*/

