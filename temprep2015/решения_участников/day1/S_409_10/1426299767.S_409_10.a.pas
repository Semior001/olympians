program z1;

var
        input, output: text;
        i, j: longint;
        Konteynery: array[1..10000, 0..10000] of longint;
        First, Last, Znach: longint;
        N, M: longint;

begin
        assign(input, 'A.in'); reset(input);
        assign(output, 'A.out'); rewrite(output);
        readln(input, N, M);
        for i:= 1 to n do begin
            Konteynery[i, 0]:= 0;
        end;
        for j:= 1 to M do begin
                readln(input, First, Last, Znach);
                if Znach = 0 then begin
                        for i:= First to Last do dec(Konteynery[i, 0]);
                end
                else begin
                        for i:= First to Last do begin
                                inc(Konteynery[i, 0]);
                                Konteynery[i, Konteynery[i, 0]]:= Znach;
                        end;
                end;
        end;
        for i:= 1 to N do begin
                write(output, Konteynery[i, Konteynery[i, 0]], ' ');
        end;
        close(output);
end.