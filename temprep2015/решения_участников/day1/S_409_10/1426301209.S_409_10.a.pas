program z1;

var
        input, output: text;
        i, j: longint;
        Konteynery, Konteyneryznach: array[1..100000] of longint;
        First, Last, Znach: longint;
        N, M: longint;
	firstar, lastar, znachar: array[1..100000] of longint;

begin
        assign(input, 'A.in'); reset(input);
        assign(output, 'A.out'); rewrite(output);
        readln(input, N, M);
        for i:= 1 to n do begin
            Konteynery[i]:= 0;
        end;
        for j:= 1 to M do begin
                readln(input, First, Last, Znach);
		firstar[j]:= First;
		Lastar[j]:= Last;		
		znachar[j]:= znach;
                if Znach = 0 then begin
                        for i:= First to Last do dec(Konteynery[i]);
                end
                else begin
                        for i:= First to Last do begin
                                inc(Konteynery[i]);
				Konteyneryznach[i]:= znach;		
                        end;
                end;
        end;
        for i:= 1 to N do begin
                if Konteynery[i] > 0 then begin
			write(output, Konteyneryznach[i], ' ');	
		end else write(output, 0, ' ');
        end;
        close(output);
end.