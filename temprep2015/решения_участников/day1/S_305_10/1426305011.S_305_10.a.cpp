#include <iostream>
#include <stdio.h>
#include <cmath>
#include <algorithm>
#define maxn (int)(1e5+1e1)
using namespace std;


int n, a[1007], m, l, r, c, ans, b[1007], d[1001];
                                          
int main() {  
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
			      	
	cin >> n >> m;
	for (int i = 1; i <= m; i++) {
		cin >> l >> r >> c;
		for (int k = l; k <= r; k++) {
			if  (c == 0) {
				a[k] = 0;
			}  else {
				b[k]++;	
				if (b[k] > 1)  
					d[k] = a[k];	
				a[k] = c;
			}
		}
	}
        for (int i = 1; i <= n; i++) {
		if (a[i] == 0)
			ans++;
		if (ans == n) {
			cout << '0';
			return 0;
		}
	}
	for (int i = 1; i <= n; i++) {
		if (b[i] > 1 && a[i] == 0)
			cout << d[i] << ' ';
		else 
			cout << a[i] <<' ';    
	}

	
	return 0;
}
