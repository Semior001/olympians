#include <bits/stdc++.h>
using namespace std;
int n, m;
int a[1010][1010];
int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; i++) {
        int x, y, c;
        scanf("%d%d%d", &x, &y, &c);
        if (!c) {
            for (int j = x; j <= y; j++) {
                a[j][0]--;
                a[j][0] = max(0, a[j][0]);
            }
        } else {
            for (int j = x; j <= y; j++) {
                a[j][0]++;
                a[j][a[j][0]] = c;
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        printf("%d ", a[i][a[i][0]]);
    }
    return 0;
}
