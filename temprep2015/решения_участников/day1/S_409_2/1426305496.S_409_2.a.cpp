#include <bits/stdc++.h>
#define pb push_back
#define pitem item*
using namespace std;
const int MAXN = 1e5 + 7, MAXM = 50;
int n, m;
int er[MAXN];
vector <int> a[MAXN];
 void push(int x) {
    if (er[x]) {
        int mn = min(er[x], (int)a[x].size());
        a[x].resize((int)a[x].size() - mn);
        er[x] = 0;
        er[x + x] = mn;
        er[x + x + 1] = mn;

    }
    if (!a[x].empty()) {
        //cout << x << ' ' << a[x].size() << endl;
        for (int i = 0; i < a[x].size(); i++) {
            a[x + x].pb(a[x][i]);
            a[x + x + 1].pb(a[x][i]);
        }
        a[x].clear();
    }
}
void change(int x, int TL, int TR, int L, int R) {

    if (TR != TL)
        push(x);
    else {
         if (er[x]) {
            int mn = min(er[x], (int)a[x].size());
            a[x].resize((int)a[x].size() - mn);
            er[x] = 0;
         }
    }
    if (R < TL || TR < L) {
        return;
    }
    if (L <= TL && TR <= R) {
        er[x]++;
    } else {
        int m = (TL + TR) / 2;
        change(x + x, TL , m, L, R);
        change(x + x + 1, m + 1, TR, L, R);
    }
}
void add(int x, int TL, int TR, int L, int R, int C) {

    if (TR != TL)
    push(x);
    if (R < TL || TR < L) {
        return;
    }
    if (L <= TL && TR <= R) {
        a[x].push_back(C);
    } else {
        int m = (TL + TR) / 2;
        add(x + x, TL , m, L, R, C);
        add(x + x + 1, m + 1, TR, L, R, C);
    }
}
void print(int x, int TL, int TR) {
    if (TR != TL)
        push(x);
    else {
         if (er[x]) {
            int mn = min(er[x], (int)a[x].size());
            a[x].resize((int)a[x].size() - mn);
            er[x] = 0;
         }
    }
    if (TL == TR) {
        printf("%d ", a[x].back());
    } else {
        int m = (TL + TR) / 2;
        if (!a[x].empty()) {
            a[x + x + 1].pb(a[x].back());
            a[x + x].pb(a[x].back());
        }
        print(x + x, TL, m);
        print(x + x + 1, m + 1, TR);
    }
}
int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    scanf("%d%d", &n, &m);
    add(1, 1, n, 1, n, 0);
    for (int i = 1; i <= m; i++) {
        int x, y, c;
        scanf("%d%d%d", &x, &y, &c);
        if (!c) {
            change(1, 1, n, x, y);
        } else {
            add(1, 1, n, x, y, c);
        }

       // cout << endl;
    }

        print(1, 1, n);

    return 0;
}
