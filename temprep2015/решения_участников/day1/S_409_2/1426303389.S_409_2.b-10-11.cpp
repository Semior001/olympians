#include <bits/stdc++.h>
#define pb push_back
using namespace std;
const int MAXN = 1e5 + 7, MAXM = 50;
int n, m, k, ans;
int c[MAXN], p[MAXN];
vector <int> a[MAXN], b[MAXN];
void rec(int x, int cnt) {
    if (x == n + 1) {
        ans = max(ans, cnt);
    } else {
        rec(x + 1, cnt);
        for (int i = 0; i < a[x].size(); i++) {
            if (c[a[x][i]]) {
                c[a[x][i]]--;
                for (int j = 0; j < b[x].size(); j++) {
                    if (p[b[x][j]]) {
                        p[b[x][j]]--;
                        rec(x + 1, cnt + 1);

                        p[b[x][j]]++;
                    }
                }
                c[a[x][i]]++;
            }
        }
    }
}
int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 1; i <= m; i++) {
        scanf("%d", &c[i]);
    }

    for (int i = 1; i <= k; i++) {
        scanf("%d", &p[i]);
    }
    for (int i = 1; i <= n; i++) {
        int x;
        scanf("%d", &x);
        for (int j = 1; j <= x; j++) {
            int y;
            scanf("%d", &y);
            a[i].pb(y);
        }
    }

    for (int i = 1; i <= n; i++) {
        int x;
        scanf("%d", &x);
        for (int j = 1; j <= x; j++) {
            int y;
            scanf("%d", &y);
            b[i].pb(y);
        }
    }
    rec(1, 0);
    cout << ans;
    return 0;
}
