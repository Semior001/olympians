#include <bits/stdc++.h>
#define pb push_back
using namespace std;
const int MAXN = 1e5 + 7, MAXM = 50;
int n;
long long ans;
int a[MAXN];
int used[MAXN];
int main() {
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        scanf("%d", &a[i]);
    }
    int cnt = 0;
    for (int L = 1; L <= n; L++) {
        cnt++;
        int len = 0;
        for (int R = L; R <= n; R++) {
            used[a[R]] = cnt;
            for (int k = R + 1; k <= n; k++) {
                if (used[a[k]] < cnt) {
                        len++;
                    ans += 1ll * len;
                    //cout << L << ' ' << R << ' ' << k - len + 1 << ' ' << k << endl;
                } else {
                    len = 0;
                }
            }
            len = 0;
        }
    }
    cout << ans;
    return 0;
}
