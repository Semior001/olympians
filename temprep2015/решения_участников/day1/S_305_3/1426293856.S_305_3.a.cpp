#include <iostream>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;

#define fname "A"
int R[1010], a[1010][1011];
int main () {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		R[i] = 1;
	}
	int l, r, c;
	for (int i = 1; i <= m; ++i) {
		cin >> l >> r >> c;
		if (c != 0) {
			for (int j = l; j <= r; ++j) {
				a[j][R[j]] = c;
				++R[j];
			}
		}
		else {
			for (int j = l; j <= r; ++ j) {
				if (R[j] == 1)
					continue;
			    --R[j];
			}
		}
	}
	for (int i = 1; i <= n; ++i) {
		cout << a[i][R[i] - 1] << " ";
	}
	return 0;
}
