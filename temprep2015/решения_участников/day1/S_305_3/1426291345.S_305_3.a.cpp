#include <iostream>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;

#define fname "C"
bool was[1001];
int main () {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	string s;
	cin >> s;
	int k, n = s.size(), mn = 100000000;
	cin >> k;
	for (int i = 1; i < n; ++i) {
		for (int j = 1; j <= 26; ++j)
			was[j] = false;
		was[s[i - 1] - 'a' + 1] = true;
		int kol = 1;
		for (int j = i + 1; j <= n; ++j) {
			if (kol == k && mn > j - i)
				mn = j - i;
		   	if (kol > k)
		   		break;
			if (!was[s[j - 1] - 'a' + 1]) {
				++kol;
				was[s[j - 1] - 'a' + 1] = true;
			}
		}
	}
	cout << mn;
	return 0;
}
