#include <iostream>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <stack>
using namespace std;

#define fname "A"
stack <int> a[123456];
int main () {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	int n, m;
	cin >> n >> m;
	int l, r, c;
	for (int i = 1; i <= m; ++i) {
		cin >> l >> r >> c;
		if (c != 0) {
			for (int j = l; j <= r; ++j) {
				a[j].push(c);
			}
		}
		else {
			for (int j = l; j <= r; ++ j) {
				a[j].pop();
			}
		}
	}
	for (int i = 1; i <= n; ++i) {
        int x = a[i].size();
        if (x == 0)
            cout << 0 << " ";
        else
            cout << a[i].top() << " ";
	}
	return 0;
}
