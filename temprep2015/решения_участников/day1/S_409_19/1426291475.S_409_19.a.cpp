
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef stack<int> sint;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "A"

int n, m;
int len = 1;
stack<int> a[maxn];
deque<int> s[maxn];

void pre()
{
	len = int(sqrt(n + .0)) + 1;
}

void push(int k)
{
	if (s[k].empty())
		return;
	int l = k * len;
	while (!s[k].empty())
	{
		int j = s[k].front();
		s[k].pop_back();
		if (j)
			for (int i = 0; i < len; i++)
				a[l + i].push(j);
		else
			for (int i = 0; i < len; i++)
				a[l + i].pop();
	}
}

void add(int l, int r, int x)
{
	push(l / len);
	push(r / len);
	for (int i = l; i <= r; )
	{
		if (i % len == 0 && i + len - 1 <= r)
		{
			if (!x && !s[i / len].empty() && s[i / len].back())
				s[i / len].pop_back();
			else
				s[i / len].push_back(x);
			i += len;
		}
		else
		{
			if (x)
				a[i].push(x);
			else
				a[i].pop();
			i++;
		}
	}
}

int l, r, c;

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d", &n, &m);
	pre();
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		add(l, r, c);
	}
	for (int i = 1; i <= n; i++)
		push(i / len);
	for (int i = 1; i <= n; i++)
		printf("%d ", a[i].empty() ? 0 : a[i].top());
}