
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "A"

int n, m;
stack <int> a[maxn];

void upd(int l, int r, int x)
{
	for (int i = l; i <= r; i++)
		if (x)
			a[i].push(x);
		else
			a[i].pop();
}

int l, r, c;

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		a[i].push(0);
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		upd(l, r, c);
	}
	for (int i = 1; i <= n; i++)
		printf("%d ", a[i].top());
}