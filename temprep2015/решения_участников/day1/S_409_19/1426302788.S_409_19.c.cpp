
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "C"

int n,  a[maxn];
multiset <int> p;
vector <int> t;

bool check()
{
	for (auto i : t)
		if (p.find(i) != p.end())
			return 0;
	return 1;
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn"2.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", a + i);
	ll ans = 0ll;
	for (int l1 = 1; l1 < n; l1++)
	{
		for (int r1 = l1; r1 < n; r1++)
		{
			t.push_back(a[r1]);
			for (int l2 = r1 + 1; l2 <= n; l2++)
			{
				for (int r2 = l2; r2 <= n; r2++)
				{
					p.insert(a[r2]);
					ans += check();
				}
				p.clear();
			}
		}
		t.clear();
	}
	printf("%lld", ans);
}