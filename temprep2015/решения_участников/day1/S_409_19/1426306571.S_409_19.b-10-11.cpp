
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = 5000 + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "B"

int n, m, k;
int ans, now;
vector <int> c, p;
vector <int> a[maxn], b[maxn];
vector <int> us;

void C(int v)
{
	//cerr << "C" << v;
	if (v == n + 1)
	{
		now = 0;
		for (int i = 1; i <= n; i++)
			now += (us[i] > 1);
		ans = max(ans, now);
		return;
	}
	for (int j = 0; j < a[v].size(); j++)
	{
		int to = a[v][j];
		if (c[to] > 0)
		{
			us[v]++;
			c[to]--;
			C(v + 1);
			c[to]++;
			us[v]--;
		}
	}
	C(v + 1);
}

void P(int v)
{
	//cerr << "P" << v;
	if (v == n + 1)
	{
		C(1);
		return;
	}
	for (int j = 0; j < b[v].size(); j++)
	{
		int to = b[v][j];
		if (p[to] > 0)
		{
			us[v]++;
			p[to]--;
			P(v + 1);
			p[to]++;
			us[v]--;
		}
	}
	P(v + 1);
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	c.assign(m + 1, 0);
	p.assign(k + 1, 0);
	us.assign(n + 1, 0);
	for (int i = 1; i <= m; i++)
		scanf("%d", &c[i]);
	for (int i = 1; i <= k; i++)
		scanf("%d", &p[i]);
	for (int i = 1; i <= n; i++)
	{
		int sz, x;
		scanf("%d", &sz);
		a[i].assign(sz + 1, 0);
		for (int j = 0; j < sz; j++)
			scanf("%d", &a[i][j]);
	}
	for (int i = 1; i <= n; i++)
	{
		int sz, x;
		scanf("%d", &sz);
		b[i].assign(sz + 1, 0);
		for (int j = 0; j < sz; j++)
			scanf("%d", &b[i][j]);
	}
	P(1);
	printf("%d", ans);

}
