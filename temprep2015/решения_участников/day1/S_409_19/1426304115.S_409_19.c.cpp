
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "C"

int n,  a[maxn];
int Next[maxn], last[maxn];
ll ans;

inline ll f(int sz)
{
	return (sz + 1ll) * sz / 2;
}

void solve(int l)
{
	vector <int> us(n + 1, 0);
	set <int> nexts;
	nexts.insert(n + 1);
	for (int r = l; r < n; r++)
	{
		//printf("%d %d\n", l, r);
		if (!us[a[r]])
		{
			int v = Next[r];
			while (v != -1)
			{
				nexts.insert(v);
				us[a[v]]++;
				v = Next[v];
			}
		}
		int last = r;
		while (*nexts.begin() <= r)
		{
			nexts.erase(nexts.begin());
		}
		for (auto i : nexts)
		{
			//printf("%d %d %I64d\n", i, i - last - 1, f(i - last - 1));
			ans += f(i - last - 1);
			last = i;
		}
		//puts("");
	}
}

int main()
{
	int buf;
	FILE * in = freopen(fn".in", "r", stdin);
	FILE * out =freopen(fn".out", "w", stdout);
	buf = scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		buf = scanf("%d", a + i);
	for (int i = n; i > 0; i--)
	{
		if (!last[a[i]])
			Next[i] = -1;
		else
			Next[i] = last[a[i]];
		last[a[i]] = i;
	}
	for (int l = 1; l < n; l++)
		solve(l);
	printf("%lld", ans);
}