
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "A"

struct item
{
	stack <int> s;
	queue <int> add;
};

int n, m;
item t[4 * maxn];

void push(int v, int tl, int tr)
{
	if (t[v].add.empty())
		return;
	if (tl == tr)
	{
		while (!t[v].add.empty())
		{
			int j = t[v].add.front();
			t[v].add.pop();
			if (j)
				t[v].s.push(j);
			else
				t[v].s.pop();
		}
	}
	else
	{
		while (!t[v].add.empty())
		{
			int j = t[v].add.front();
			t[v].add.pop();
			t[v + v].add.push(j);
			t[v + v + 1].add.push(j);
		}
	}
}

void upd(int v, int tl, int tr, int l, int r, int x)
{
	if (l > r)	
		return;
	push(v, tl, tr);
	if (tl == l && tr == r)
	{
		t[v].add.push(x);
		return;
	}
	int tm = (tl + tr) >> 1;
	upd(v + v, tl, tm, l, min(r, tm), x);
	upd(v + v + 1, tm + 1, tr, max(l, tm + 1), r, x);
}

vector <int> ans;

void get(int v, int tl, int tr)
{
	push(v, tl, tr);
	if (tl == tr)
	{
		ans.push_back(t[v].s.empty() ? 0 : t[v].s.top());
		return;
	}
	int tm = (tl + tr) >> 1;
	get(v + v, tl, tm);
	get(v + v + 1, tm + 1, tr);
}

int l, r, c;

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		upd(1, 1, n, l, r, c);
	}
	get(1, 1, n);
	for (auto i : ans)
		printf("%d ", i);
}