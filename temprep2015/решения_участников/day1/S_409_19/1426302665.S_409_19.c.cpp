
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "C"

int n,  a[maxn];
int Next[maxn];
ll ans;
map <int, int> last;

ll f(int sz)
{
	return (sz + 1ll) * sz / 2;
}

void solve(int l, int r)
{
	//printf("%d %d:\n", l, r);
	queue <int> q;
	set <int> nexts;
	vector <int> us(n + 1, 0);
	for (int i = l; i <= r; i++)
	{
		int to = Next[i];
		if (to == -1 || to <= r || us[to])
			continue;
		us[to] = 1;
		nexts.insert(to);
		q.push(to);
	}
	while (!q.empty())
	{
		int v = q.front();
		q.pop();
		int to = Next[v];
		if (to == -1 || to <= r || us[to])
			continue;
		us[to] = 1;
		nexts.insert(to);
		q.push(to);
	}
	nexts.insert(n + 1);
	int last = r;
	for (auto i : nexts)
	{
		//printf("%d %d %I64d\n", i, i - last - 1, f(i - last - 1));
		ans += f(i - last - 1);
		last = i;
	}
	//puts("");
}

int main()
{
	freopen(fn".in", "r", stdin);
	freopen(fn".out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", a + i);
	for (int i = n; i > 0; i--)
	{
		if (last.find(a[i]) == last.end())
			Next[i] = -1;
		else
			Next[i] = last[a[i]];
		last[a[i]] = i;
	}
	for (int l = 1; l < n; l++)
		for (int r = l; r < n; r++)
			solve(l, r);
	printf("%lld", ans);
}