
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef stack<int> sint;

const int maxn = int(1e5) + 12;
const int inf = int(1e9) + 7;
const ll linf = ll(1e18) + 7;

#define F fisrt
#define S second
#define mp make_pair
#define pb push_back
#define fn "C"

int n, m;
int len = 1;
vector <stack<int>> a;
vector <deque<int>> s;

void pre()
{
	len = int(sqrt(n + .0)) + 1;
	stack <int> empty_stack;
	deque <int> empty_deque;
	a.assign(n, empty_stack);
	s.assign(len, empty_deque);
}

void push(int k)
{
	if (s[k].empty())
		return;
	int l = k * len;
	while (!s[k].empty())
	{
		int j = s[k].front();
		s[k].pop_front();
		if (j)
			for (int i = 0; i < len; i++)
				a[l + i].push(j);
		else
			for (int i = 0; i < len; i++)
				a[l + i].pop();
	}
}

void add(int l, int r, int x)
{
	push(l / len);
	push(r / len);
	for (int i = l; i <= r; )
	{
		if (i % len == 0 && i + len - 1 <= r)
		{
			if (!x && !s[i / len].empty() && s[i / len].back())
				s[i / len].pop_back();
			else
				s[i / len].push_back(x);
			i += len;
		}
		else
		{
			if (x)
				a[i].push(x);
			else
				a[i].pop();
			i++;
		}
	}
}

int l, r, c;

int main()
{
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	scanf("%d%d", &n, &m);
	pre();
	int i = 1;
	for (; i <= (m / 2) + 1; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		add(l - 1, r - 1, c);
	}
	
	for (int i = 0; i < len; i++)
		push(i);

	for (int j = 0; j < n; j++)
	{
		int t = a[j].empty() ? -1 : a[j].top();
		if (t == -1)
			continue;
		while (!a[j].empty())
			a[j].pop();
		a[j].push(t);
	}

	for (; i <= m; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		add(l - 1, r - 1, c);
	}

	for (int i = 0; i < len; i++)
		push(i);

	for (int i = 0; i < n; i++)
		printf("%d ", a[i].empty() ? 0 : a[i].top());
}