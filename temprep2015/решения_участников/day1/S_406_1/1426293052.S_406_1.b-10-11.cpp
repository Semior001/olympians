// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <vector>

# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = 2002;
int n, m, k, c[MAXN], p[MAXN], a[MAXN][MAXN], b[MAXN][MAXN], x[MAXN], y[MAXN];
int ans, curans;

void solve (int id = 1) {
    FOR (i, 0, x[id])
        FOR (j, 0, y[id]) {
            bool ok = false;
            int A = a[id][i], B = b[id][j];
            if (c[A] && p[B]) {
                c[A]--;
                c[B]--;
                curans++;
                ok = true;
            }
            if (id != n) solve (id + 1);
            else ans = max (ans, curans);
            if (ok) {
                curans--;
                c[A]--;
                c[B]--;
            }
        }
}

int main () {
freopen ("B.in", "r", stdin);
freopen ("B.out", "w", stdout);

    scanf ("%d%d%d", &n, &m, &k);

    FOR (i, 1, m) scanf ("%d", &c[i]);
    FOR (i, 1, k) scanf ("%d", &p[i]);

    FOR (i, 1, n) {
        scanf ("%d", &x[i]);
        FOR (j, 1, x[i]) scanf ("%d", &a[i][j]);
    }
    FOR (i, 1, n) {
        scanf ("%d", &y[i]);
        FOR (j, 1, y[i]) scanf ("%d", &b[i][j]);
    }

    solve();
    printf ("%d\n", ans);

    return 0;
}

/*
2 3 1
5 1 3
2
3 1 2 3
1 2
1 1
1 1
*/
