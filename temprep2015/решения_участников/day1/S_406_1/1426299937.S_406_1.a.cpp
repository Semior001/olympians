// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <stack>

# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = (int) 4e5 + 5;
stack <int> st[MAXN];

inline void push (int v, int l, int r) {
    while (!st[v].empty()) {
        if (l != r) {
            st[v * 2].push (st[v].top());
            st[v * 2 + 1].push (st[v].top());
        }
        st[v].pop();
    }
}

inline void update (int v, int l, int r, int L, int R, int x) {
    if (l > R || r < L) return;
    if (l >= L && r <= R) st[v].push (x);
    else {
        if (st[v].size()) push (v, l, r);
        int mid = (l + r) / 2;
        update (v * 2, l, mid, L, R, x);
        update (v * 2 + 1, mid + 1, r, L, R, x);
    }
}

inline int get (int v, int l, int r, int pos) {
    if (l == r) {
        int del = 0;
        while (!st[v].empty()) {
            if (!st[v].top()) del++;
            else {
                if (!del) return st[v].top();
                else del--;
            }
            st[v].pop();
        }
        return 0;

    }
    push (v, l, r);
    int mid = (l + r) / 2;
    if (pos <= mid) return get (v * 2, l, mid, pos);
    else return get (v * 2 + 1, mid + 1, r, pos);
}

int main () {
freopen ("A.in", "r", stdin);
freopen ("A.out", "w", stdout);
    int n, m;
    scanf ("%d%d", &n, &m);
    FOR (i, 1, m) {
        int l, r, x;
        scanf ("%d%d%d", &l, &r, &x);
        update (1, 1, n, l, r, x);
    }

    FOR (i, 1, n) printf ("%d ", get (1, 1, n, i));

    return 0;
}
/*
5 3
1 5 1
2 4 0
4 5 10
*/
