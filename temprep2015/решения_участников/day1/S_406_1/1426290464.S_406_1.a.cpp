// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <stack>

# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = (int) 1e5 + 5;
int a[MAXN], n, m;
stack <int> st[MAXN];

int main () {
    freopen ("A.in", "r", stdin);
    freopen ("A.out", "w", stdout);
    scanf ("%d%d", &n, &m);

    FOR (i, 1, m) {
        int l, r, c;
        scanf ("%d%d%d", &l, &r, &c);
        FOR (j, l, r) {
            if (c) st[j].push (c);
            else if (!st[j].empty()) st[j].pop();
        }
    }

    FOR (i, 1, n) {
        if (st[i].empty()) printf ("0 ");
        else printf ("%d ", st[i].top());
    }

    return 0;
}
