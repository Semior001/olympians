// *CoDeD by Ye.A
# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <vector>
# define FOR(x, y, z) for(int x = y; x <= z; x++)

using namespace std;

const int MAXN = 5005;
int n, a[MAXN], ans;

int main () {
freopen ("C.in", "r", stdin);
freopen ("C.out", "w", stdout);
    scanf ("%d", &n);
    FOR (i, 1, n) scanf ("%d", &a[i]);

    FOR (l, 1, n - 1) {
        vector <char> used (n + 1, false);

        FOR (r, l, n - 1) {
            used[a[r]] = true;
            int last = r + 1;
            FOR (i, r + 1, n) {
                if (used[a[i]]) {
                    if (i != last) ans += (i - last) * (i - last + 1) / 2;
                    last = i + 1;
                }
                else if (i == n) ans += (i - last + 1) * (i - last + 2) / 2;
            }
        }
    }

    printf ("%d\n", ans);

    return 0;
}
