#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>

using namespace std;

const int MAX_SIZE = 10000;
const int INF = (int) 1e9;

int n, m, k, x;

struct edge {
	int from, to, capacity, current_flow;
	edge (int from = 0, int to = 0, int capacity = 0, int current_flow = 0) : 
	from (from), to (to), capacity (capacity), current_flow (current_flow) {}
};
vector < edge > edges;
vector < int > g [MAX_SIZE];

void add_edge (int from, int to, int capacity) {
	g [from].push_back (edges.size ());	
	edges.push_back (edge (from, to, capacity, 0));
	g [to].push_back (edges.size ());
	edges.push_back (edge (to, from, 0, 0));
}

int source, sink; 

queue < int > q;
int d [MAX_SIZE];
bool bfs () {
	memset (d, -1, sizeof d);
	d [source] = 0;
	q.push (source);
	while (q.size ()) {
		int v = q.front ();
		q.pop ();
		for (int i = 0; i < g [v].size (); ++ i) {
			if (edges [g [v][i]].capacity - edges [g [v][i]].current_flow > 0 && d [edges [g [v][i]].to] == -1) {
				q.push (edges [g [v][i]].to);
				d [edges [g [v][i]].to] = d [v] + 1;
			}
		}
	}
	return (d [sink] != -1);
}

int ptr [MAX_SIZE];
int dfs (int v, int flow) {
	if (v == sink) {
		return flow;
	}
	if (!flow) {
		return 0;
	}
	for (; ptr [v] < g [v].size (); ++ ptr [v]) {
		int i = ptr [v];
		if (d [edges [g [v][i]].to] == d [v] + 1) {
			int cur = dfs (edges [g [v][i]].to, min (flow, edges [g [v][i]].capacity - edges [g [v][i]].current_flow));
			if (cur) {
				edges [g [v][i]].current_flow += cur;
				edges [g [v][i] ^ 1].current_flow -= cur;
				return cur;
			}
		}		
	}
	return 0;
}

void show () {
	for (int i = 0; i < edges.size (); ++ i) {
		cout << edges [i].from << " " << edges [i].to << " " << edges [i].capacity << " " << edges [i].current_flow << endl;
	}
}

void dinic () {
	while (bfs ()) {
		//cout << "bfs is ok" << endl;
		//show ();
		memset (ptr, 0, sizeof ptr);
		while (dfs (source, INF) > 0) {

		}		
	} 	
}

int p [MAX_SIZE];
bool dfs_slow (int v, int flow) {
	if (v == sink) {
		return 1;
	}
	if (!flow) {
		return 0;
	}
	for (int i = 0; i < g [v].size (); ++ i) {
		
	}
} 

void slow_flow () {
	
}

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	scanf ("%d%d%d", &n, &m, &k);
	source = n + m + k;
	sink = n + m + k + 1;
	for (int i = 0; i < m; ++ i) {
		scanf ("%d", &x);
		add_edge (n + i, sink, x);
	}
	for (int i = 0; i < k; ++ i) {
		scanf ("%d", &x);
		add_edge (n + m + i, sink, x);
	}
	for (int i = 0; i < n; ++ i) {
		scanf ("%d", &x);
		add_edge (i, sink + 1 + i, 1);
		for (int j = 0, xx; j < x; ++ j) {
			scanf ("%d", &xx);
			-- xx;
			add_edge (sink + 1 + i, n + xx, 1);
		}		
	}
	for (int i = 0; i < n; ++ i) {
		scanf ("%d", &x);	
		add_edge (i, sink + 1 + n + i, 1);
		for (int j = 0, xx; j < x; ++ j) {
			scanf ("%d", &xx);
			-- xx;
			add_edge (sink + 1 + n + i, n + m + xx, 1);		
		}		
	}
	for (int i = 0; i < n; ++ i) {
		add_edge (source, i, 2);
	}
	dinic ();
	int ans = 0;
	for (int i = 0; i < g [source].size (); ++ i) {
		if (edges [g [source][i]].current_flow == 2) {
			++ ans;
		}		
	}
	cout << ans << endl;
}