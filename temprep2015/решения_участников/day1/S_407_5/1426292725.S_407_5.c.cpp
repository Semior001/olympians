#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>

using namespace std;

const int MAX_N = 2000;

int n, a [MAX_N], cnt [MAX_N];
long long sum [MAX_N];
long long ans;

int main () {
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 0; i < n; ++ i) {
		scanf ("%d", &a [i]);
		-- a [i];
	}
	for (int i = 1; i < MAX_N; ++ i) {
		sum [i] = sum [i - 1] + i;	
	}
	for (int l = 0; l < n; ++ l) {
		for (int r = l; r < n; ++ r) {
			for (int k = l; k <= r; ++ k) {
				++ cnt [a [k]];
			}
			for (int k = r + 1, cur = 0; k < n; ++ k) {
				if (cnt [a [k]] > 0) {
					ans += sum [cur];
					cur = 0;
				} else {
					++ cur;				 
				}
				if (k == n - 1) {
					ans += sum [cur];					
				}
			}
			for (int k = l; k <= r; ++ k) {
				-- cnt [a [k]];
			}
		}
	}
	cout << ans << endl;
}