#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <functional>
#include <ctime>

using namespace std;

const int MAX_N = 2000;

int n, m;
int l, r, c;

stack < int > st [MAX_N];

int main () {
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	scanf ("%d%d", &n, &m);
	for (int i = 0; i < m; ++ i) {
		scanf ("%d%d%d", &l, &r, &c);
		-- l; -- r;
		for (int j = l; j <= r; ++ j) {
			if (c > 0) {
				st [j].push (c);				
			} else {
				st [j].pop ();			
			}
		}
	}
	for (int i = 0; i < n; ++ i) {
		if (st [i].empty ()) {
			printf ("0 ");
		} else {
			printf ("%d ", st [i].top ());		
		}
	}
}