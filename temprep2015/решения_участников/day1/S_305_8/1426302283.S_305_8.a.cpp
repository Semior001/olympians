#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll N=111111;
ll n,m,a[N],l[N],r[N],c[N],b[N],pos,nex[N];
int main(){
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    cin>>n>>m;
    for(ll i=1;i<=m;i++)
        cin>>l[i]>>r[i]>>c[i];
    for(ll i=m;i>0;i--){
        for(ll j=l[i];j<=r[i];j=(nex[j]?nex[j]:j+1)){
            cout<<pos<<" ";
            if(!c[i])
                b[j]++;
            else{
                if(!b[j]){
                    a[j]=c[i],
                    b[j]=2e9;
                    if(!pos)pos=j;
                }
                else{
                    b[j]--;
                    nex[pos]=j,pos=0;
                }
            }
            if(j==r[i]) nex[pos]=j+1,pos=0;
        }
        cout<<"\n";
    }
    for(ll i=1;i<=n;i++)
        cout<<a[i]<<" ";
    cout<<"\n";
    for(ll i=1;i<=n;i++)
        cout<<nex[i]<<" ";
    return 0;
}
