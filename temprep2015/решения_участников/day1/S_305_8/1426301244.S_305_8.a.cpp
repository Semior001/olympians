#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll N=111111;
ll n,m,a[N],l[N],r[N],c[N],b[N],pos,next[N];
int main(){
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    cin>>n>>m;
    for(ll i=1;i<=m;i++)
        cin>>l[i]>>r[i]>>c[i];
    for(ll i=m;i>0;i--)
        for(ll j=l[i];j<=r[i];j=(next[j]?next[j]:j+1)){
            if(!c[i])
                b[j]++;
            else{
                if(!b[j])
                    a[j]=c[i],
                    b[j]=2e9,
                    pos=j-1;
                else{
                    b[j]--;
                    next[pos]=j,pos=0;
                }
            }
            if(j==r[i]) next[pos]=j+1,pos=0;
        }
    for(ll i=1;i<=n;i++)
        cout<<a[i]<<" ";
    return 0;
}
