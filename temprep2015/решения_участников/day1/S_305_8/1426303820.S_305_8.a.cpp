#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=111111;
int n,m,a[N],l[N],r[N],c[N],b[N],pos,nex[N];
int main(){
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;i++)
        scanf("%d%d%d",&l[i],&r[i],&c[i]);
    for(int i=m;i>0;i--){
        for(int j=l[i];j<=r[i];j=(nex[j]?nex[j]:j+1)){
            //cout<<pos<<" ";
            if(!c[i])
                b[j]++;
            else{
                if(!b[j]){
                    a[j]=c[i];
                    b[j]=2e9;
                    if(!pos)pos=j;
                }
                else{
                    b[j]--;
                    if(pos)nex[pos]=j,pos=0;
                }
            }
        }
        if(pos)nex[pos]=r[i]+1,pos=0;
    }
    for(int i=1;i<=n;i++)
        printf("%d ",a[i]);
    /*cout<<"\n";
    for(ll i=1;i<=n;i++)
        cout<<nex[i]<<" ";*/
    return 0;
}
