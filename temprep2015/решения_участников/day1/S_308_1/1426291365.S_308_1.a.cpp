#include <iostream>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, m, l, r, c;
vector <llong> v[MXN];

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
    for(int i = 1; i <= m; i ++){
        cin >> l >> r >> c;
        if(c){
            for(int j = l; j <= r; j ++){
                v[j].PB(c);
            }
        }
        else
        {
            for(int j = l; j <= r; j ++){
                v[j].pop_back();
            }
        }
    }
    for(int i = 1; i <= n; i ++){
        if(v[i].back() <= INF){
            cout << v[i].back() << ' ';
        }
        else
        {
            cout << 0 << ' ';
        }
    }
    return 0;
}
