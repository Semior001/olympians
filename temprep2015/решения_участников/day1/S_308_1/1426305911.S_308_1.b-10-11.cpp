#include <iostream>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, m, k, c[MXN], p[MXN], x[MXN], y[MXN], a[MXN], b[MXN], cf[MXN], ck[MXN], ans, mx;
bool u, cx[MXN], px[MXN];

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> n >> m >> k;
    for(int i = 1; i <= m; i ++){
        cin >> c[i];
    }
    for(int i = 1; i <= k; i ++){
        cin >> p[i];
    }
    for(int i = 1; i <= n; i ++){
        cin >> x[i];
        for(int j = 1; j <= x[i]; j ++){
            cin >> a[j];
            if(mx < c[a[j]]){
                mx = c[a[j]];
                c[a[j]] --;
            }
        }
        if(!mx){
            cx[i] = true;
        }
    }
    mx = 0;
    for(int i = 1; i <= n; i ++){
        cin >> y[i];
        for(int j = 1; j <= y[i]; j ++){
            cin >> b[j];
            if(mx < p[b[j]]){
                mx = p[b[j]];
                p[b[j]] --;
            }
        }
        if(!mx){
            px[i] = true;
        }
    }
    for(int i = 1; i <= n; i ++){
        if(px[i] || cx[i]){
            ans ++;
        }
    }
    cout << n - ans;
    return 0;
}
