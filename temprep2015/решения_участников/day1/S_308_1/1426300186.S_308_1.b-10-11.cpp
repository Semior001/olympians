#include <iostream>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, m, k, c[MXN], p[MXN], x[MXN], y[MXN], a[MXN], b[MXN], cf[MXN], ck[MXN], ans;
bool u;

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> n >> m >> k;
    for(int i = 1; i <= m; i ++){
        cin >> c[i];
    }
    for(int i = 1; i <= k; i ++){
        cin >> p[i];
    }
    for(int i = 1; i <= n; i ++){
        cin >> x[i];
        for(int j = 1; j <= x[i]; j ++){
            cin >> a[j];
            cf[a[j]] ++;
        }
    }
    for(int i = 1; i <= n; i ++){
        u = false;
        for(int j = 1; j <= x[i]; j ++){
            if(cf[a[j]] <= c[a[j]]){
                u = true;
            }
        }
        if(!u){
            ans ++;
        }
        else
        {
            for(int j = 1; j <= x[i]; j ++){
                cf[a[j]] --;
            }
        }
    }
    for(int i = 1; i <= n; i ++){
        cin >> y[i];
        for(int j = 1; j <= y[i]; j ++){
            cin >> b[j];
            ck[b[j]] ++;
        }
    }
    for(int i = 1; i <= n; i ++){
        u = false;
        cin >> y[i];
        for(int j = 1; j <= y[i]; j ++){
            if(ck[b[j]] <= p[b[j]]){
                u = true;
            }
        }
        if(!u){
            ans ++;
        }
        else
        {
            for(int j = 1; j <= y[i]; j ++){
                ck[b[j]] --;
            }
        }
    }
    cout << n - ans;
    return 0;
}

