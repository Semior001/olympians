#include <iostream>
#include <set>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define SZ size()
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, a[MXN], cnt;

bool check(int x, int y, int z, int d){
    set <int> s;
    set <int> t;
    set <int> k;
    for(int i = x; i <= y; i ++){
        s.insert(a[i]);
        k.insert(a[i]);
    }
    for(int i = z; i <= d; i ++){
        t.insert(a[i]);
        k.insert(a[i]);
    }
    if(s.SZ + t.SZ != k.SZ){
        return false;
    }
    return true;
}

int main(){
    //freopen("input.txt", "r", stdin);
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);
    cin >> n;
    for(int i = 1; i <= n; i ++){
        cin >> a[i];
    }
    for(int i = 1; i <= n; i ++){
        for(int j = i; j <= n; j ++){
            for(int k = j + 1; k <= n; k ++){
                for(int p = k; p <= n; p ++){
                    if(check(i, j, k, p)){
                        //cout << i << ' ' << j << ' ' << k << ' ' << p << endl;
                        cnt ++;
                    }
                }
            }
        }
    }
    cout << cnt;
    return 0;
}


