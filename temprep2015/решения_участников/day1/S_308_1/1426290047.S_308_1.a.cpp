#include <iostream>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdio>

#define llong long long int
#define F first
#define S second
#define MP make_pair
#define PB push_back

const int INF = 1e9 + 7;
const int MXN = 1e5 + 1;
const int N = 1e3 + 1;

using namespace std;

llong n, m, l, r, c, a[MXN];

int main(){
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
    for(int i = 1; i <= m; i ++){
        cin >> l >> r >> c;
        for(int j = l; j <= r; j ++){
            a[j] = c;
        }
    }
    for(int i = 1; i <= n; i ++){
        cout << a[i] << ' ';
    }
    return 0;
}
