#include <bits/stdc++.h>
using namespace std;
#define MAXN 5555
typedef long long ll;
ll ans;
ll n, a[MAXN], b[MAXN], g[MAXN];

void go(ll v)
{

    if (b[g[v-1]])
    {
        if (a[g[v-1]])
            return;
        ans++;
    }
    if (v == n+1)
        return;
    if (v < n)
    {
        a[g[v]]++;
        go(v+1);
        a[g[v]]--;
    }
        b[g[v]]++;
        go(v+1);
        b[g[v]]--;
}

int main ()
{
    freopen ("C.in", "r", stdin);
    freopen ("C.out", "w", stdout);
    cin>>n;
    for (ll i = 1; i <= n; i++)
    {
        scanf("%lld", &g[i]);
    }
    if (n < 2)
    {
        cout<<0;
        return 0;
    }
    go(1);
    cout<<ans-2;

}
