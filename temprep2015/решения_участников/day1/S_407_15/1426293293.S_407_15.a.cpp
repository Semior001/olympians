#include<iostream>
#include<fstream>
#include<deque>
#include<vector>
using namespace std;
int main()
{
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    int n,m,L,R,C;
    cin >> n >> m;
    vector<vector<int> > A (100001);
    for(int k=1;k<=m;++k)
    {
        cin >> L >> R >> C;
        if(C!=0)
            for(int i=L;i<=R;++i) A[i].push_back(C);
        if(C==0)
            for(int i=L;i<=R;++i) A[i].pop_back();
    };
    for(int i=1;i<=n;++i)
    {
        if(A[i].size())cout << (*(--A[i].end())) << " ";
        else cout << "0 ";
    };
    fclose(stdin);
    fclose(stdout);
    return 0;
}
