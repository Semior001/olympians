#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

using namespace std;

int i, j, n, mini = 1e9, k, an, b[2000];
string s;

int main()
{
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> s;
    cin >> k;
    n = s.length() - 1;
    //cout << n << endl;
    for (int i = 0; i <= n; i++)
        for (int j = i; j <= n; j++)
        {
            an = 0;
            for (int l = i; l <= j; l++)
                if (b[s[l]] == 0)
                {
                    b[s[l]]++;
                    an++;
                }
            for (int l = 1; l <= 200; l++)
                b[l] = 0;

            if (an == k && j - i + 1 < mini)
                mini = j - i + 1;
        }
    if (mini > n + 2)
        cout << 1;
    else
        cout << mini;
}
