#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<int> a[200000];
int i, n, m, j, c, l, r;

int main()
{
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
    for (i = 1; i <= m; i++)
    {
        cin >> l >> r >> c;
       if (c == 0)
        {
            for (j = l; j <= r; j++)
                if (a[j].size() != 0)
                    a[j].pop_back();
        }
        else
            for (j = l; j <= r; j++)
                a[j].push_back(c);
    }
    for (i = 1; i <= n; i++)
        if (a[i].size() == 0)
            cout << 0 << ' ';
        else
            cout << a[i][a[i].size() - 1] << ' ';
}
