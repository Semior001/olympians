var  n,m,i,j : longint;
     last,first,c,t : array [0..10000] of longint;
     q : array [0..1010,0..1010] of longint;
begin
 assign (input,'A.in'); reset (input);
 assign (output,'A.out'); rewrite (output);

 read (n,m);

 for i := 1 to m do 
 	read (first[i],last[i],c[i]);

 for i := 1 to m do begin
 	if (c[i] <> 0) then begin
	 	for j := first[i] to last[i] do begin
 			inc (t[j]);
 			q[j,t[j]] := c[i];
 		end;
 	end else begin
 		for j := first[i] to last[i] do begin
 			dec (t[j]);
 		end;
 	end;
 end;

 for i := 1 to n do begin
 	write (q[i,t[i]],' ');
 end;

 close (input); close (output);
end.