#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;

int n, a[5003], cnt[5003], res;

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("C.in", "r", stdin);
		freopen("C.out", "w", stdout);
	#endif

	cin >> n;

	for(int i = 1; i <= n; i++)
		scanf("%d", &a[i]);

	for(int l = 1; l <= n; l++) {
		memset(cnt, 0, sizeof cnt);
		for(int r = l; r <= n; r++) {
			cnt[a[r]] = 1;
			int l2 = r + 1, r2 = r + 1, len;

			while(l2 <= n) {

				while(cnt[a[l2]] && l2 <= n)
					l2++, r2++;

				while(!cnt[a[r2]] && r2 <= n)
					r2++;

				len = r2 - l2;
				res += len * (len + 1) / 2;
				l2 = r2 + 1;
				r2 = l2;
			}
		}
	}

	cout << res;

	return 0;
}
