#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;

const int inf = (int)(1e+9);

int n, m, k, s, t, c[1003][1003], f[1003][1003], p[1003], used[1003], push[1003];

void init() {
	for(int i = 1; i <= n + m + 1; i++)
		used[i] = p[i] = push[i] = 0;
}

bool bfs() {
	init();
	queue<int> q;
	used[s] = 1;
	p[s] = 1;
	push[s] = inf;
	q.push(s);
	while(!q.empty() && !used[t]) {
		int u = q.front();
		q.pop();
		for(int v = 0; v <= t; v++)
			if(!used[v] && (c[u][v] - f[u][v] > 0)) {
				used[v] = 1;
				push[v] = min(push[u], c[u][v] - f[u][v]);
				p[v] = u;
				q.push(v);
			}
	}
	return used[t];
}

int max_flow() {
	int flow = 0, u, v;
	while(bfs()) {
		int add = push[n];
		v = t;
		u = p[v];
		while(v != 1) {
			f[u][v] += add;
			f[v][u] -= add;
			v = u;
			u = p[v];
		}
		flow += add;
	}
	return min(n, flow);
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("B.in", "r", stdin);
		freopen("B.out", "w", stdout);
	#endif

	int x, y, sz;
	vector <int> a[502], b[502];

	scanf("%d%d%d", &n, &m, &k);

	s = 0;
	t = n + m + 1;

	for(int i = 1; i <= m; i++) {
		scanf("%d", &x);
		c[0][i] = x;
	}

	for(int i = 1; i <= k; i++) {
		scanf("%d", &x);
		c[m + i][t] = x;
	}

	for(int i = 1; i <= n; i++) {
		scanf("%d", &sz);
		for(int j = 0; j < sz; j++) {
			scanf("%d", &x);
			a[i].push_back(x);
		}
	}
	
	for(int i = 1; i <= n; i++) {
		scanf("%d", &sz);
		for(int j = 0; j < sz; j++) {
			scanf("%d", &x);
			b[i].push_back(x);
		}
	}

	for(int i = 1; i <= n; i++) {
		int sz1 = a[i].size();
		int sz2 = b[i].size();
		for(int j = 0; j < sz1; j++)
			for(int l = 0; l < sz2; l++)
				c[a[i][j]][m + b[i][l]]++; 
	}

	cout << max_flow();

	return 0;
}
