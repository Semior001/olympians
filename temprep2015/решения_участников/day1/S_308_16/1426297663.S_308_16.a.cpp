#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#define null NULL

struct node {
	vector <int> vec;
	node *l, *next, *r;

	node(): l(null) , next(null), r(null) {}
};

int N = 1;
node *t[400005];

void build(int v, int l, int r) {
	if(l > r)
		return;
	t[v] = new node();
	if(l == r)
		return;
	int m = (l + r) / 2;

	build(v * 2, l, m);
	build(v * 2 + 1, m + 1, r);
}
 
void push(int v, int l, int r) {
	if(l == r)
		return;

	int sz = t[v].size();
	
	if(sz) {
		for(int i = 0; i < sz; i++) {
			t[v * 2].push_back()
			t[v * 2 + 1].push_back();
		}
	}
}


void add(int v, int l, int r, int l2, int r2, int val) {
	if(l > r || l2 > r || r2 < l)
		return;
	push(v, l, r);
	
	if(l2 <= l && r <= r2) {
		t[v] -> vec.push_back(val);
		return;
	}

	int m = (l + r) / 2;
	
	add(v * 2, l, m, l2, r2, val);
	add(v * 2 + 1, m + 1, r, l2, r2, val);
}

void del(int v, int l, int r, int l2, int r2) {
	if(l > r || l2 > r || r2 < l)
		return;
	if(l2 <= l && r <= r2) {
		t[v].pop_back();
		return;
	}

	int m = (l + r) / 2;
	
	del(v * 2, l, m, l2, r2);
	del(v * 2 + 1, m + 1, r, l2, r2);
}
 
int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("A.in", "r", stdin);
		freopen("A.out", "w", stdout);
	#endif

	int n, m, l, r, c;

	scanf("%d%d", &n, &m);

	while(N < n)
		N *= 2;

	build(1, 1, N);
	
	for(int i = 0; i < m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		if(c == 0) 
			del(1, 1, N, l, r);
		else
			add(1, 1, N, l, r, c);
	}

	go(1, 1, N);

	return 0;
}
