#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;

int n, m, k, s, t, c[1003][1003], f[1003][1003], d[1003], ptr[1003];

bool bfs() {
	queue<int> q;
	memset(d, -1, sizeof d);
	d[s] = 0;
	q.push(s);

	while(!q.empty()) {
		int u = q.front();
		q.pop();
		for(int v = 0; v <= t; v++) 
			if(d[v] == -1 && c[u][v] > f[u][v]) {
				d[v] = d[u] + 1;
				q.push(v);
			}
	}

	return d[t] != -1;
}

int dfs(int v, int flow) {
	if(!flow)	return 0;
	if(v == t)	return flow;
	int end = t;
	if(v == 0) {
		ptr[v] = max(ptr[v], 1);
		end = m;
	}
	else if(1 <= v && v <= m) {
		ptr[v] = max(ptr[v], m + 1);
		end = m + k;
	}
	else
		ptr[v] = max(ptr[v], t);

	for(int &to = ptr[v]; to <= end; to++) {
		if(d[to] != d[v] + 1)	continue;
		int pushed = dfs(to, min(flow, c[v][to] - f[v][to]));	
		if(pushed) {
			f[v][to] += pushed;
			f[to][v] -= pushed;
			return pushed;
		}
	}
	return 0;
}

int dinic() {
	int flow = 0;
	for(;;) {
		if(!bfs()) break;
		memset(ptr, 0, sizeof ptr);	
		while(int pushed = dfs(s, n)) 
			flow += pushed;
	}
	return flow;
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("B.in", "r", stdin);
		freopen("B.out", "w", stdout);
	#endif

	int x, y;
	vector <int> a[502], b[502];

	scanf("%d%d%d", &n, &m, &k);

	s = 0;
	t = m + k + 1;

	for(int i = 1; i <= m; i++) {
		scanf("%d", &x);
		c[0][i] =  x;
	}

	for(int i = 1; i <= k; i++) {
		scanf("%d", &x);
		c[m + i][t] = x;
	}

	for(int i = 1; i <= n; i++) {
		scanf("%d", &x);
		for(int j = 0; j < x; j++) {
			scanf("%d", &y);
			a[i].push_back(y);
		}
	}

	for(int i  = 1; i <= n; i++) {
		scanf("%d", &x);
		for(int j = 0; j < x; j++) {
			scanf("%d", &y);
			b[i].push_back(y);
		}
	}

	for(int i = 1; i <= n; i++) {
		int sz1 = a[i].size();
		int sz2 = b[i].size();
		for(int j = 0; j < sz1; j++)
			for(int l = 0; l < sz2; l++)
				c[a[i][j]][m + b[i][l]]++;
	}

	cout << dinic();

	return 0;
}
