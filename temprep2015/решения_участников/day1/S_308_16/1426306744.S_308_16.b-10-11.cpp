#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;

const int inf = (int)(1e+9);

int n, m, k, s, t, c[1103][1103], f[1103][1003], d[1103], ptr[1103];

bool bfs() {
	queue<int> q;
	memset(d, -1, sizeof d);
	d[s] = 0;
	q.push(s);

	while(!q.empty()) {
		int u = q.front();
		q.pop();
		for(int v = 0; v <= t; v++) { 
			if(d[v] == -1 && c[u][v] > f[u][v]) {
				d[v] = d[u] + 1;
				q.push(v);
			}
		}
	}

	return d[t] != -1;
}

int dfs(int v, int flow) {
	if(!flow)	return 0;
	if(v == t)	return flow;

	for(int &to = ptr[v]; to <= t; to++) {
		if(d[to] != d[v] + 1)	continue;
		int pushed = dfs(to, min(flow, c[v][to] - f[v][to]));	
		if(pushed) {
			f[v][to] += pushed;
			f[to][v] -= pushed;
			return pushed;
		}
	}
	return 0;
}

int dinic() {
	int flow = 0;
	for(;;) {
		if(!bfs()) break;
		memset(ptr, 0, t * sizeof ptr[0]);	
		while(int pushed = dfs(s, n))  
			flow += pushed;
	}
	return min(n, flow);
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("B.in", "r", stdin);
		freopen("B.out", "w", stdout);
	#endif

	int x, y, used[1102] = {0}, z = 0;
	vector <int> a[1102], b[1102];

	scanf("%d%d%d", &n, &m, &k);

	s = 0;
	t = m + k + 1;

	for(int i = 1; i <= m; i++) {
		scanf("%d", &x);
		c[0][i] =  x;
	}

	for(int i = 1; i <= k; i++) {
		scanf("%d", &x);
		c[m + i][t] = x;
	}

	for(int i = 1; i <= n; i++) {
		scanf("%d", &x);
		if(x == 0)
			used[i] = 1;
		for(int j = 0; j < x; j++) {
			scanf("%d", &y);
			if(1 <= y && y <= m)
				a[i].push_back(y);
		}
	}

	for(int i  = 1; i <= n; i++) {
		scanf("%d", &x);
		if(x == 0)
			used[i] = 1;
		for(int j = 0; j < x; j++) {
			scanf("%d", &y);
			if(m + 1 <= y && y <= m + k)
				b[i].push_back(y);
		}
	}

	for(int i = 1; i <= n; i++) {
		if(used[i]) {
			z++;
			continue;
		}
		int sz1 = a[i].size();
		int sz2 = b[i].size();
		for(int j = 0; j < sz1; j++)
			for(int l = 0; l < sz2; l++)
				c[a[i][j]][m + b[i][l]]++;
	}

	n -= z;

	cout << dinic();

	return 0;
}
