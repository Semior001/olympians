#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

vector<int> t[100005];

void add(int l, int r, int val) {
	for(int i = l; i <= r; i++)
		t[i].push_back(val);
}

void del(int l, int r) {
	for(int i = l; i <= r; i++)
		t[i].pop_back();
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("A.in", "r", stdin);
		freopen("A.out", "w", stdout);
	#endif

	int n, m, l, r, c;

	scanf("%d%d", &n, &m);

	for(int i = 1; i <= n; i++)
		t[i].push_back(0);

	for(int i = 0; i < m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		if(c == 0) 
			del(l, r);
		else
			add(l, r, c);
	}

	for(int i = 1; i <= n; i++) 
		printf("%d ", t[i].back());


	return 0;
}
