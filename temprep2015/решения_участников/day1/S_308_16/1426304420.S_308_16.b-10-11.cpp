#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;

const int inf = (int)(1e+9);

int n, m, k, s, t, c[503], p[503], c1[502], p1[502], res;
vector <int> a[502], b[502];

void rec(int v, int s) {
	if(v == n + 1) {
		res = max(res, s);
		return;
	} 
	rec(v + 1, s);
	int sz1 = a[v].size();
	int sz2 = b[v].size();

	for(int i = 0; i < sz1; i++)
		for(int j = 0; j < sz2; j++) {
			int x = a[v][i];
			int y = b[v][j];
			c1[x]++;
			p1[y]++;
			if(c1[x] <= c[x] && p1[y] <= p[y])
				rec(v + 1, s + 1);
	
			c1[x]--;
			p1[y]--;
		}
}

int main() {

	#ifdef local	
		freopen("in.txt", "r", stdin);
	#else
		freopen("B.in", "r", stdin);
		freopen("B.out", "w", stdout);
	#endif

	int x, y, sz;

	scanf("%d%d%d", &n, &m, &k);

	s = 0;
	t = n + m + 1;

	for(int i = 1; i <= m; i++) {
		scanf("%d", &c[i]);
	}

	for(int i = 1; i <= k; i++) {
		scanf("%d", &p[i]);
	}

	for(int i = 1; i <= n; i++) {
		scanf("%d", &sz);
		for(int j = 0; j < sz; j++) {
			scanf("%d", &x);
			a[i].push_back(x);
		}
	}
	
	for(int i = 1; i <= n; i++) {
		scanf("%d", &sz);
		for(int j = 0; j < sz; j++) {
			scanf("%d", &x);
			b[i].push_back(x);
		}
	}

	rec(1, 0);
	cout << res;

	return 0;
}
