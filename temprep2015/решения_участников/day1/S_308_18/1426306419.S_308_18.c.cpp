# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>
# include <map>
# include <set>

using namespace std;

const int N = int (1e6)+1;
const int INF = int (1e9)+1;

int a[N], d[N], cnt;


int make (int i, int j) {
    set <int> st;
    for (int ii = i; ii <= j; ++ii)
        st.insert (a[ii]);
    return j-i+1 - int(st.size());
}

int main () {

    freopen ("C.in", "r", stdin);
    freopen ("C.out", "w", stdout);

    int n;
    cin >> n;
    for (int i = 1; i <= n; ++i) {cin >> a[i]; d[i] = n;}

    for (int i = 1; i <= n; ++i)
        for (int j = i+1; j <= n; ++j) {
            int x = make (i, j);
            int p = j-i;
            cnt += (p * (p+1) / 2 - x * (x+1) / 2);
        }

    cout << cnt << endl;
    return 0;
}
