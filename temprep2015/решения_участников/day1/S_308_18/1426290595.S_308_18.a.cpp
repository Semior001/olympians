# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>

using namespace std;

const int N = int (1e6)+1;
const int INF = int (1e9)+1;
int a[N], d[N], ans[N];

int main () {
    freopen ("A.in", "r", stdin);
    freopen ("A.out", "w", stdout);

    int n, m, l, r, x;
    cin >> n >> m;
    for (int i = 1; i <= m; ++i) {
        scanf ("%d%d%d", l, r, x);
        for (int j = l; j <= r; ++j) ans[j] = x;
    }
    for (int i = 1; i <= n; ++i) printf ("%d ", ans[i]);
    return 0;
}
