# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>

using namespace std;

const int N = int (1e6)+1;
const int INF = int (1e9)+1;
int a[N], d[N], ans[N], l[N], r[N], x[N];

int main () {
    freopen ("A.in", "r", stdin);
    freopen ("A.out", "w", stdout);

    int n, m;
    cin >> n >> m;
    for (int i = 1; i <= m; ++i) cin >> l[i] >> r[i] >> x[i];

    for (int i = 1; i <= n; ++i) {
        int cnt = 0, ans = INF;
        for (int j = m; j >= 1; --j) {
            if (x[j] == 0 && l[j] <= i && i <= r[j]) cnt++;
            else if (l[j] <= i && i <= r[j]) {
                if (!cnt) {ans = x[j]; break;}
                else cnt--;
            }
        }
        if (ans == INF) cout << 0 << " ";
        else cout << ans << " ";
    }
    return 0;
}
/*
5 3
1 5 1
2 4 0

*/
