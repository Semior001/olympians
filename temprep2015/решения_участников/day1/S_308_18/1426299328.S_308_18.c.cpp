# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>

using namespace std;

const int N = int (1e6)+1;
const int INF = int (1e9)+1;
int a[N], ans;

bool check (int L, int R, int l, int r) {
    for (int i = L; i <= R; ++i)
        for (int j = l; j <= r; ++j)
            if (a[i] == a[j]) return false;
    return true;
}

int main () {
    freopen ("C.in", "r", stdin);
    freopen ("C.out", "w", stdout);
    int n;
    scanf ("%d", &n);
    for (int i = 1; i <= n; ++i) scanf ("%d", &a[i]);

    for (int i = 1; i <= n; ++i)
     for (int j = i; j <= n; ++j)
      for (int k = j+1; k <= n; ++k)
       for (int l = k; l <= n; ++l)
            if (j <= k)
                if (check (i, j, k, l)) ans++;

    printf ("%d", ans);
    return 0;
}
