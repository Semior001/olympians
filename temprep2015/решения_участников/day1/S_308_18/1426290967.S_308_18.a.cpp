# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>
# include <map>
# include <stack>

using namespace std;

const int N = int (1e6)+1;
const int INF = int (1e9)+1;
int a[N], d[N], ans[N];

map <int, stack <int> > mp;

int main () {
    freopen ("A.in", "r", stdin);
    freopen ("A.out", "w", stdout);

    int n, m, l, r, x;
    cin >> n >> m;
    for (int i = 1; i <= n; ++i) mp[i].push (0);

    for (int i = 1; i <= m; ++i) {
        cin >> l >> r >> x;
        if (x) for (int j = l; j <= r; ++j) mp[j].push(x);
        else for (int j = l; j <= r; ++j) mp[j].pop();
    }
    for (int i = 1; i <= n; ++i) cout << mp[i].top() << " ";
    return 0;
}
