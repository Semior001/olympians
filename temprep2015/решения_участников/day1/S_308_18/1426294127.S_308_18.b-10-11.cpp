# include <iostream>
# include <fstream>
# include <cstdio>
# include <cstdlib>
# include <set>
# include <vector>

using namespace std;

# define F first
# define S second

const int N = 550;
const int INF = int (1e9)+1;
int a[N], b[N], used[N], q[N], p[N];
int x[N][N], y[N][N], MAX;

vector <pair <int, pair <int, int> > > d;

bool ith_bit (int i, int j) {
    return i & (1 << j);
}

int main () {
    freopen ("B.in", "r", stdin);
    freopen ("B.out", "w", stdout);



    int n, m, k;
    cin >> n >> m >> k;
    for (int i = 1; i <= m; ++i) cin >> a[i];
    for (int i = 1; i <= k; ++i) cin >> b[i];

    for (int i = 1; i <= n; ++i) {
        cin >> x[i][0];
        for (int j = 1; j <= x[i][0]; ++j) cin >> x[i][j];
    }

    for (int i = 1; i <= n; ++i) {
        cin >> y[i][0];
        for (int j = 1; j <= y[i][0]; ++j) cin >> y[i][j];
    }

    for (int k = 1; k <= n; ++k)
        for (int i = 1; i <= x[k][0]; ++i)
            for (int j = 1; j <= y[k][0]; ++j)
                d.push_back (make_pair (k, make_pair (x[k][i], y[k][j])));

    int sz = d.size();
    for (int i = 0; i < (1 << sz); ++i) {
        set <int> st;
        for (int j = 1; j <= m; ++j) q[j] = a[j];
        for (int j = 1; j <= k; ++j) p[j] = b[j];
        for (int ii = 1; ii <= n; ++ii) used[ii] = 0;

        for (int j = 0; j < sz; ++j)
            if (ith_bit (i, j)) {
                int num = d[j].F;
                int v = d[j].S.F;
                int to = d[j].S.S;
                if (q[v] && p[to] && !used[num]) st.insert (num);
                q[v]--, p[to]--;
                used[num] = 1;
            }

        MAX = max (MAX, int(st.size()));
    }
    cout << MAX << endl;
    return 0;
}


/*
2 3 1
5 1 3
2
3 1 2 3
1 2
1 1
1 1
*/
