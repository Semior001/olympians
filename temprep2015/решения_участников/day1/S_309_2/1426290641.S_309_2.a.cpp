#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int n, m, a[MaxN];

int main () {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> n >> m;
	while (m--) {
		int l, r, c;
		cin >> l >> r >> c;
		for (int i = l; i <= r; ++i) {
			a[i] = c;
		}
	}	
	for (int i = 1; i <= n; ++i) {
		cout << a[i] << " ";
	}
	return 0;
}

