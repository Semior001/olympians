#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);
struct edge {
	int first, second, third;
} a[MaxN];

int n, m, i = 1, q, p = 0, z, x, y, f, s;
vector <int> cur;

int main () {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> n >> m;
	for (int j = 1; j < MaxN; ++j) {
		cur.pb(0);
	}
	while (m--) {
		int l, r, c;
		cin >> l >> r >> c;
		if (c == 0) {
			cur.pop_back();		
		} else {
			cur.pb(c);
		}		
		a[l].first = cur[cur.size() - 1];
		a[l].second = i;
		i++;
		a[l].third = r;
	}                           
	for (int i = 1; i <= n; ++i) {
		if (a[i].second > p) {
			p = a[i].second;
			x = a[i].first;
			y = a[i].third;
		}
		if (y >= i) {
			if (a[i].second > f && a[i].third > y) {
				f = a[i].second;
				s = a[i].third;
				z = a[i].first;										
			}
		}
		a[i].first = x;
		if (y == i) {
			p = f;
			y = s;
			x = z;
			f = 0;
			s = 0;
			z = 0;
		}
	}
	for (int i = 1; i <= n; ++i) {
		cout << a[i].first << ' '; //<< ' ' << a[i].second << ' ' << a[i].third << endl;
	}
	return 0;
}

