#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int n, m;
vector <int> a[MaxN];

int main () {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> n >> m;
	while (m--) {
		int l, r, c;
		cin >> l >> r >> c;
		if (c == 0) {
			for (int i = l; i <= r; ++i) {
				a[i].pop_back();
			}
		} else {
			for (int i = l; i <= r; ++i) {
				a[i].pb(c);
			}
		}
	}	
	for (int i = 1; i <= n; ++i) {
		if (a[i].size() == 0) {
			cout << 0 << ' ';
		} else {
			cout << a[i][a[i].size() - 1] << ' ';
		}
	}
	return 0;
}

