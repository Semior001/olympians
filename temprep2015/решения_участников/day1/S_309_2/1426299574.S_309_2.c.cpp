#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int n, a[5005], res;
    
bool check (int l, int r, int tl, int tr) {
	for (int i = l; i <= r; ++i) {
		for (int j = tl; j <= tr; ++j) {
			if (a[i] == a[j]) {
				return false;
			}
		}
	}
	return true;
} 

int main () {
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);   
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}
	for (int i = 1; i <= n; ++i) {
		for (int j = i; j <= n; ++j) {
			for (int k = j + 1; k <= n; ++k) {
				for (int q = k; q <= n; ++q) {
					if (check(i, j, k, q)) {
						res++;	
					}
				}
			}
		}
	}		
	cout << res;
	return 0;
}

