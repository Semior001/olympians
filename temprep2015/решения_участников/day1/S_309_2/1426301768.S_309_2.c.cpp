#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

int n, a[5005], sum;

int calc (int x) {
	return x * (x + 1) / 2;
}

int kol(int l, int r) {
	bool u[5005];
	fill(u, u + 5005, 0);
	int res = 0;
	vector <int> v; 
	for (int i = l; i <= r; ++i) {
		for (int j = r + 1; j <= n; ++j) {
			if (a[i] == a[j] && !u[a[i]]) {
				v.pb(j);
				u[a[i]] = 1;	
			}
		}
	}		
	v.pb(r);       
	v.pb(n + 1);
	sort(all(v));
	for (int i = 1; i < v.size(); ++i) {
		res += calc(v[i] - v[i - 1] - 1);
	}
	return res;
} 

int main () {
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);   
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}             
	for (int i = 1; i <= n; ++i) {
		for (int j = i; j <= n; ++j) {
			//cout << kol(i, j) << ' ' << i << ' '<< j << endl;
			sum += kol(i, j);		
		}
	}			
	cout << sum;
	return 0;
}

