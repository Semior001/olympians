#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#define all(x) x.begin(), x.end()
#define endl "\n"
#define pb push_back

using namespace std;

const int MaxN = int(1e5) + 256;
const int INF = int(1e9);

string s;
int d[MaxN][27], u[111], n, mn = INF, sz = INF;

int get(int l, int r) {
	int res = 0;
	for (int i = 0; i < 26; ++i) {
		if (l - 1 < 0) {
			res += (d[r][i] > 0);
		} else {
			res += (d[r][i] > d[l - 1][i]);
		}	
	}                         
	return res;
}

/*int binser(int x) {
	int l = x, r = MaxN;
	while (r - l > 1) {
		int mid = (r + l) >> 1;
		if ()
	}			
}*/

int main () {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	ios_base :: sync_with_stdio(false);
	cin.tie(0);
	cin >> s;
	cin >> n;
	d[0][s[0] - 'a']++;  
	for (int i = 1; i < s.size(); ++i) {
		for (int j = 0; j < 26; ++j) {
			d[i][j] = d[i - 1][j];
		}
		d[i][s[i] - 'a'] = d[i - 1][s[i] - 'a'] + 1;
	}             
	for (int i = 0; i < s.size(); ++i) {
		for (int j = 0; j <= i; ++j) {
			if (get(j, i) == n) {
//			    cout << j << " " << i << endl;
				sz = min(sz, (i - j + 1));
			}
		}
	}	
	if (sz == INF) {
		cout << -1;
		return 0;
	}
	cout << sz;  
	return 0;
}

