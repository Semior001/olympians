#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int n, m, l, r, c, a[1111][1111];

int main () {
	
	freopen ("a.in", "r", stdin);
	freopen ("a.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		if (!c) {
			for (int j = l; j <= r; j++)
				a[j][0]--;	
		}
		else {
			for (int j = l; j <= r; j++)
				a[j][0]++, a[j][a[j][0]] = c;
		}
	}
	for (int i = 1; i <= n; i++) {
		if (!a[i][0]) { cout << 0 << " "; continue; }
		else cout << a[i][a[i][0]] << " ";
	}
	return 0;
}