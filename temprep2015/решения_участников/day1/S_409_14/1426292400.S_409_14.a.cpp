#include <iostream>
#include <vector>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int n, m, l, r, c;
vector < int > v[111111];

int main () {
	
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		if (!c) {
			for (int j = l; j <= r; j++)
				v[j].pop_back();	
		}
		else {
			for (int j = l; j <= r; j++)
				v[j].push_back(c);
		}
	}
	for (int i = 1; i <= n; i++) {
		if (!v[i].size()) cout << 0 << " ";
		else cout << v[i][v[i].size() - 1] << " ";
	}
	return 0;
}