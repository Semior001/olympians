#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int n, m, l, r, c, w[1001];
int t[55555][5001];

void upd(int v, int l, int r, int x, int y, int c) {
	if (l > y || r < x) return;
	x = max(x, l); y = min(y, r);
	if (l == x && r == y) { 
		if (c) t[v][0]++, t[v][t[v][0]] = c;
		else t[v][0]--;
		return;
	}
	int mid = (l + r) >> 1;
	if (t[v][0] >= 1) {
		for (int i = 1; i <= t[v][0]; i++)
			t[v * 2][0]++, t[v * 2][t[v * 2][0]] = t[v][i], t[v * 2 + 1][0]++, t[v * 2 + 1][t[v * 2 + 1][0]] = t[v][i];
		t[v][0] = 0;	
	}
	upd(v * 2, l, mid, x, y, c);
	upd(v * 2 + 1, mid + 1, r, x, y, c);
}

void get(int v, int l, int r) {	
	if (l > r) return;
	if (t[v][0]) {
		for (int i = l; i <= r; i++)
			w[i] = t[v][t[v][0]];
		return;
	}
	if (l == r) {
		w[l] = 0;
		return;
	}
	int mid = (l + r) >> 1;
	get(v * 2, l, mid);
	get(v * 2 + 1, mid + 1, r);
}

int main () {
	
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	
	//scanf("%d%d", &n, &m);  
	cin >> n >> m;
	for (int i = 1; i <= m; i++) {
		//scanf("%d%d%d", &l, &r, &c);
		cin >> l >> r >> c;
		upd(1, 1, n, l, r, c);	
	}	
	
	get(1, 1, n);

	for (int i = 1; i <= n; i++)
		cout << w[i] << " ";
	return 0;
}