#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

const int maxn = 555;

int n, m, k, cf[maxn], p[maxn], x, a[maxn][maxn], b[maxn][maxn], aa[maxn], bb[maxn], wc[maxn][maxn], wp[maxn][maxn], ma; 

void rec(int l) {
	if (l > n) {
		
		int ccf[m + 1], cp[k + 1], ans = 0;
		for (int i = 1; i <= m; i++) ccf[i] = cf[i];
		for (int i = 1; i <= k; i++) cp[i] = p[i];

		for (int i = 1; i <= n; i++) {
			if (!aa[i]) ccf[aa[i]]--;
			if (!bb[i]) cp[bb[i]]--;
			if (aa[i] && bb[i] && wc[i][aa[i]] && wp[i][bb[i]] && ccf[aa[i]] >= 0 && cp[bb[i]] >= 0)
				ans++;
		}                                                  
		ma = max(ma, ans);
	} 
	else {
		for (int i = 0; i <= m; i++) {
			aa[l] = i;
			for (int j = 0; j <= k; j++) {
				bb[l] = j;
				rec(l + 1);
			}
		}
	}
}

int main () {
	
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= m; i++) 
		scanf("%d", &cf[i]);
	for (int i = 1; i <= k; i++)      
		scanf("%d", &p[i]);
	for (int i = 1; i <= n; i++) {
		scanf("%d", &x);
		for (int j = 1; j <= x; j++)
			scanf("%d", &a[i][j]), wc[i][a[i][j]] = 1;
	}
	for (int i = 1; i <= n; i++) {
		scanf("%d", &x);
		for (int j = 1; j <= x; j++)
			scanf("%d", &b[i][j]), wp[i][b[i][j]] = 1;
	}       	
	
	rec(1);

	cout << ma;
	return 0;
}