#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <cmath>
#include <cstring>
#include <stack>
#include <deque>
#include <vector>

#define fi first
#define se second
#define mp make_pair
#define pb push_back

using namespace std;
typedef long long ll;

int n, a[5555], ans;

int main () {
	
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	
	for (int i = 1; i <= n; i++) {
		for (int j = i; j <= n; j++) {
			for (int ii = j + 1; ii <= n; ii++) {
				for (int jj = ii; jj <= n; jj++) {
					map <int, bool>	m;
					for (int l = i; l <= j; l++)
						m[a[l]] = 1;
					bool ch = 0;
					for (int l = ii; l <= jj; l++)
						if (m[a[l]]) ch = 1;
					if (!ch) ans++;
				}
			}
		}
	}
	cout << ans;
	return 0;
}