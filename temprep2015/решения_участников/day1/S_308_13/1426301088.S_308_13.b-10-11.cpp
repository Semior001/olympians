#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(5000);
const int inf = (int)1e9;
int d[N], ptr[N];
int n, m, k, s, t, qq[N], p[N], q[N];
int sz;
int to;
int x;
struct edge{
	int a, b, c, f;
};
vector <int> g[N];
vector <edge> e;
bool was[5000];

void add_edge(int a, int b, int cap) {
	edge e1 = {a, b, cap, 0};
	edge e2 = {b, a, 0, 0};
	g[a].pb(e.size());
	e.pb(e1);
	g[b].pb(e.size());
	e.pb(e2);	
}

int bfs() {
	int qh = 0, qt = 0;
	memset(d, -1, sizeof d);
	d[s] = 0;
	q[qt++] = s;
	while(qh < qt) {
		int v = q[qh++];
		for(int i = 0; i < g[v].size(); ++i) {
		   int id = g[v][i];
		   int to = e[id].b;
			if(d[to] == -1 && e[id].c > e[id].f) {
				d[to] = d[v] + 1;
				q[qt++] = to;
			}
		}
	}
	return d[t] != -1;
}

int dfs(int v, int flow) {
	if(flow == 0) return 0;
	if(v == t) return flow;
	for(int i = 0; i < g[v].size(); ++i) {
		int id = g[v][i];
		int to = e[id].b;
		if(d[to] != d[v] + 1) continue;
		int pushed = dfs(to, min(flow, e[id].c - e[id].f));
		if(pushed) {
			if(e[id].c == 1) {
				for(int j = 0; j < g[v].size(); ++j) {
					int ID = g[v][j];
					d[v] = 0;
					e[ID].c = 0;
				}
			}      
			e[id].f += pushed;
			e[id ^ 1].f -= pushed;
			return pushed;
		}
	}
	return 0;
}

int dinic() {
	int flow = 0;
	for(;;) {
		if(!bfs()) break;
		while(int pushed = dfs(s, inf))
			flow += pushed;
	}
	return flow;
}

int main() {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m >> k;
	for(int i = 0; i < m; ++i) {
		cin >> qq[i];
	}
	for(int i = 0; i < k; ++i)
		cin >> p[i];
	s = n + m + k;
	for(int i = 0; i < m; ++i) {
		add_edge(s, i, qq[i]);
	}
	for(int i = 0; i < n; ++i) {
		cin >> x;
		for(int j = 0; j < x; ++j) {
			cin >> to;
			--to;
			add_edge(to, i + m, 1);
		}
	}
	for(int i = 0; i < n; ++i) {
		cin >> x;
		for(int j = 0; j < x; ++j) {
			cin >> to;
			--to;
			add_edge(i + m, n +m  + to, 1);
		}
	}
	t = n + m + k + 1;
	for(int i = 0; i < k; ++i) {
		add_edge(n +m + i, t, p[i]);
	}

	cout << dinic();


}