#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

stack <int> q[N];
int n, m, l, r, c;

int main() {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m;
	for(int i = 0; i < m; ++i) {
		cin >> l >> r >> c;
		if(!c) {
			for(int u = l; u <= r; ++u)
				if(!q[u].empty()) q[u].pop();
		} else {
			for(int u = l; u <= r; ++u)
				q[u].push(c);
		}
	}
	for(int i = 1; i <= n; ++i)
		cout << (q[i].empty() ? 0 : q[i].top()) << " ";
}