#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(5000);
const int inf = (int)1e9;
int d[N], ptr[N], c[5000][5000], f[5000][5000];;
int n, m, k, s, t, q[N], p[N];
int sz;
int to;
int x;

int bfs() {
	queue <int> q;
	memset(d, -1, sizeof d);
	d[s] = 0;
	q.push(s);
	while(!q.empty()) {
		int v = q.front();
		q.pop();
		for(int to = 0; to < sz; ++to)
			if(d[to] == -1 && c[v][to] > f[v][to]) {
				d[to] = d[v] + 1;
				q.push(to);
			}
	}
	return d[t] != -1;
}

int dfs(int v, int flow) {
	if(flow == 0) return 0;
	if(v == t) return flow;
	for(int & to = ptr[v]; to < sz; ++to) {
		if(d[to] != d[v] + 1) continue;
		int pushed = dfs(to, min(flow, c[v][to] - f[v][to]));
		if(pushed) {
			f[v][to] += pushed;
			f[to][v] -= pushed;
			return pushed;
		}
	}
	return 0;
}

int dinic() {
	int flow = 0;
	for(;;) {
		if(!bfs()) break;
		memset(ptr, 0, sizeof ptr);
		while(int pushed = dfs(s, inf))
			flow += pushed;
	}
	return flow;
}

int main() {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m >> k;
	for(int i = 0; i < m; ++i) {
		cin >> q[i];
	}
	for(int i = 0; i < k; ++i)
		cin >> p[i];
	s = n + m + k;
	for(int i = 0; i < m; ++i) {
		c[s][i] = q[i];
	}
	for(int i = 0; i < n; ++i) {
		cin >> x;
		for(int j = 0; j < x; ++j) {
			cin >> to;
			--to;
			c[to][i + m ] = 1;
		}
	}
	for(int i = 0; i < n; ++i) {
		cin >> x;
		for(int j = 0; j < x; ++j) {
			cin >> to;
			--to;
			c[i + m][n + m + to] = 1;
		}
	}
	t = n + m + k + 1;
	sz = n + m + k + 2;
	for(int i = 0; i < k; ++i)
		c[n + m + i][t] = p[i]; 
	cout << dinic();
}