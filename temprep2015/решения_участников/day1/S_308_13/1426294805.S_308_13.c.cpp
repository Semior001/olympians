#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;
unordered_map <int, int> was;
int n, a[N];

LL ans;
map <int, vector <int>> p;
LL prevans = 0;
int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);

	cin >> n;

	was.rehash(n + 1);
	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
		p[a[i]].push_back(i);
	}

	for(int i = n - 1; i >= 1; --i) {
		set <int> s;
		s.insert(i);
		s.insert(n + 1);
		prevans = (n - i) *1ll*(n - i + 1) / 2;
		for(int j = i; j >= 1; --j) {
			if(was[a[j]]) continue;
			was[a[j]] = 1;
			for(int u = 0; u < (int) p[a[j]].size(); ++u) {
				int x = p[a[j]][u];
				if(x <= i) continue;
				auto it = s.upper_bound(x);
				int r = *it;
				it--;
				int l = *it;
				prevans -= (r - l - 1) * 1ll * (r - l) / 2;
				prevans += (r - x - 1) * 1ll * (r - x) / 2;
				prevans += (x - l - 1) * 1ll * (x - l) / 2;
			}
			ans += prevans;
		}
		for(int j = i; j >= 1; --j) 
			was[a[j]] =0;
	}
	cout << ans;
}