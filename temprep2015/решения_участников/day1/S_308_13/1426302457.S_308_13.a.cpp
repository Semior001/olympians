#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, m, l, r, c[N];
int a[4 *N];
int U[N];
vector <int> t[N * 4];

void upd(int v, int tl, int tr, int l, int r, int x) {
	if(l > r) return;
	if(a[v] && !t[v].empty()) {
		while(a[v] && !t[v].empty()) {
			t[v].pop_back();
		}
	  	if(a[v]) {
	  		a[v + v] += a[v];
	  		a[v + v + 1] += a[v];
	  		a[v] = 0;
	  	}
	}
	if(tl == l && tr == r) {
		if(x == 0) ++a[v];
		else t[v].push_back(x);
		return;
	}
	int tm = (tl + tr) >> 1;
	while(!t[v].empty()) {
		t[v + v].push_back(t[v].back());
		t[v + v + 1].push_back(t[v].back());
		t[v].pop_back();
	}
	upd( v + v, tl, tm, l, min(r, tm), x);
	upd(v + v + 1, tm + 1, tr, max(tm + 1,l), r ,x);
}

void walk(int v, int tl, int tr) {
	if(a[v] && !t[v].empty()) {
		while(a[v] && !t[v].empty()) {
			t[v].pop_back();
		}
	  	if(a[v]) {
	  		a[v + v] += a[v];
	  		a[v + v + 1] += a[v];
	  		a[v] = 0;
	  	}
	}
	if(tl == tr) {
		if(!t[v].empty()) {
			cout << t[v].back() << " ";
		}
		else cout << 0 << " ";
		return;
	}
	if(!t[v].empty()) {
		for(int i = tl; i <= tr; ++i)
			cout << t[v].back() << " ";
		return; 
	}
	int tm = (tl + tr) >> 1;
	walk(v + v, tl, tm);
	walk(v + v + 1, tm + 1, tr);
}

int main() {  
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m;
	vector <int> q;
	for(int i = 0; i < m; ++i) {
		cin >> l >> r >> c[i];
			upd(1, 1, n, l, r, c[i]);
	}
	walk(1, 1, n);
}