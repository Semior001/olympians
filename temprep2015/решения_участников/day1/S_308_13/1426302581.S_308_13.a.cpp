#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

vector <int> t[N * 8];
int n, m, l, r, c[N];
int b[4 *N];
int U[N];

void push(int v) {
	if(!t[v].empty()) {
		while(b[v] && !t[v].empty())
			--b[v], t[v].pop_back();
	}
	
	if(!t[v].empty()) {
		while(!t[v + v].empty() && b[v + v]) {
			--b[v + v], t[v + v].pop_back();
		}
		while(!t[v + v + 1].empty() && b[v + v + 1]) {
			--b[v + 1 + v], t[v + v + 1].pop_back();
		}
		while(!t[v].empty()) {
			t[v + v].pb(t[v].back());
			t[v + v + 1].pb(t[v].back());
			t[v].pop_back();
		}
	}
	if(b[v]) {
		b[v + v] += b[v];;
		b[v + v + 1] += b[v];
		b[v] = 0; 
	}
}

void Update(int v, int tl, int tr, int l, int r, int q) {
	if(l > r) return;
	if(tl == l && tr == r) {                                                           
		t[v].pb(q);
		return;
	}
	push(v);
	int tm = (tl + tr) >> 1;
	Update(v + v, tl, tm, l, min(r, tm), q);
	Update(v + v + 1, tm + 1, tr, max(tm + 1, l), r, q);
}

void Delete(int v, int tl, int tr, int l, int r) {	
	if(l > r) return;
	if(tl == l && tr == r) {
		b[v]++;
		if(!t[v].empty()) {
			while(b[v] && !t[v].empty())
				--b[v], t[v].pop_back();
		}
		return;
	}
	push(v);
	int tm = (tl + tr) >> 1;
	Delete(v + v, tl , tm, l, min(r, tm));
	Delete(v + v + 1, tm + 1, tr, max(tm + 1, r), r);
}

int get(int v, int tl, int tr, int pos) {
	if(tl == tr) {
		if(t[v].empty()) return 0;
		return t[v].back();
	}
	push(v);
	int tm = (tl + tr) >> 1;
	if(pos <= tm) return get(v + v, tl, tm, pos);
	else return get(v + v + 1, tm + 1, tr, pos);
}

int main() {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m;
	for(int i = 0; i < m; ++i) {
		cin >> l >> r >> c[i];
		if(c[i]) {
			Update(1, 1, n, l, r, c[i]);
		} else {
			Delete(1, 1, n,l, r);
		}
	}
	for(int i = 1; i <= n; ++i) {
		cout << get(1, 1, n, i) << " ";	
	}
}