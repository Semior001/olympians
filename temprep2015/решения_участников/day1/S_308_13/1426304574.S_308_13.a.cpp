#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long
#define F first
#define S second

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;

int n, m, l, r, c[N];
vector <pair <int, int> > t[N];
vector <pair <int, int> > q[N];

int main() {  
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m;
	int sz = 300;
	int block = (n + sz - 1) / sz;
	for(int i = 0; i < m; ++i) {
		cin >> l >> r >> c[i];
		int bl1 = (l + sz - 1) / sz;
		int bl2 = (r + sz - 1) / sz;
		if(bl1 == bl2) {
			for(int j = l; j <= r; ++j)
				q[j].push_back(mp(c[i], i));
				continue;
		}
			for(int j = bl1 + 1; j <= bl2 - 1; ++j) {
				t[j].push_back(mp(c[i], i));
			}
			int lst = min(bl1 * sz, r);
			for(int j = l; j <= lst; ++j)
				q[j].push_back(mp(c[i], i));
			int beg = max(l, (bl2 - 1) * sz + 1);
			for(int j = beg; j <= r; ++j)
				q[j].pb(mp(c[i], i));
	}
	for(int i = 1; i <= n; ++i) {
		int bl = (i + sz - 1) / sz;
		int p = (int)t[bl].size() - 1, p2 = (int)q[i].size() - 1;
		while(true) {
			if(p < 0) {
				while(p2 >= 0 && q[i][p2].F == 0) p2 -= 2;
				if(p2 >= 0) {
					cout << q[i][p2].F << " ";
				}  else {
					cout << 0 << " ";
				}
				break;
			} else if(p2 < 0) {
				while(p >= 0 && t[bl][p].F == 0) p -= 2;
				if(p >= 0) {
					cout << t[bl][p].F << " ";
				} else {
					cout <<"0 ";
				}
			} else {
				if(t[bl][p].S < q[i][p2].S) {
					if(q[i][p2].F == 0) {
						p2--;
						if(q[i][p2].S < t[bl][p].S)
							p2--;
						else p--;
						continue;
					} else {
						cout << q[i][p2].F << " ";
						break;
					}
				} else {
					if(t[bl][p].F == 0) {
						p--;
						if(q[i][p2].S < t[bl][p].S)
				   		p2--;
				   	else p--;
				   	continue;
					} else {
						cout << t[bl][p].F << " ";
					}
				}
			}
		}
	}
}