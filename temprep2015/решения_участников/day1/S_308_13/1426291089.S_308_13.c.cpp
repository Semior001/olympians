#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const int N = (int)(1e5 + 15);
const int inf = (1 << 31) - 1;
unordered_map <int, int> was;
int n, a[N];

LL ans;
int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);

	cin >> n;

	was.rehash(n + 1);
	for(int i = 1; i <= n; ++i) {
		cin >> a[i];
	}

	for(int i = 1; i <= n; ++i) {
		for(int r = i; r <= n; ++r) {
			was[a[r]]++;
			int cnt = 0;
			for(int u = r + 1; u <= n; ++u) {
				if(!was[a[u]]) ++cnt;
				else {
					ans += cnt *1ll* (cnt + 1) / 2;
					cnt = 0;
				}
			}
			ans += cnt *1ll* (cnt + 1) / 2;
		}
		for(int r = i; r <= n; ++r)
			was[a[r]]--;
	}
	cout << ans;
}