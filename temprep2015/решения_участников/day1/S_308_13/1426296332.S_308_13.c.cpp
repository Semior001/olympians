#include<bits/stdc++.h>

using namespace std;

#define pb push_back
#define mp make_pair
#define LL long long

const LL N = (LL)(1e5 + 15);
const LL inf = (1 << 31) - 1;
unordered_map <LL, LL> was;
LL n, a[N];

LL ans;
unordered_map <LL, LL> p;
vector <int> g[10000];;
int cnt;
LL prevans = 0;
int main() {
	ios::sync_with_stdio(0); cin.tie(0);
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);

	cin >> n;

	was.rehash(n + 1);
	for(LL i = 1; i <= n; ++i) {
		cin >> a[i];
		if(!p.count(a[i])) p[a[i]] = ++ cnt;
			int x = p[a[i]];
			g[x].pb(i);
	}
	p.rehash(n + n);

	set < LL > s;
	for(LL i = n; i >= 1; --i) {
		s.clear();
		s.insert(i);
		s.insert(n + 1);
		prevans = ((n - i) * 1ll * (n - i + 1)) / 2;
		for(LL j = i; j >= 1; --j) {
			if(prevans == 0) break;
			if(was[a[j]]) {
				ans += prevans;
				continue;
			}
			was[a[j]] = 1;
			
			int T = p[a[j]];                                   
			int beg = upper_bound(g[T].begin(), g[T].end(), i) - g[T].begin();
		
			for(LL u = beg; u < (LL) g[T].size(); ++u) {
				LL x = g[T][u];
				if(x <= i) 
					continue;
				auto it = s.upper_bound(x);
				LL r = *it;
				it--;
				LL l = *it;
				prevans -= ((r - l - 1) * 1ll * (r - l)) / 2;
				prevans += ((r - x - 1) * 1ll * (r - x)) / 2;
				prevans += ((x - l - 1) * 1ll * (x - l)) / 2;
				s.insert(x);
			}
			ans += (prevans >= 0 ? prevans : 0);
		}
		for(LL j = i; j >= 1; --j) 
			was[a[j]] = 0;
	}
	cout << ans ;
}