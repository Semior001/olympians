#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "B."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define F first
#define sz(x) x.size()
#define S second
#define all(x) x.begin(),x.end()

using namespace std;

const int MAXN=(int)(600);

ll ans,n,m,k,c[MAXN],p[MAXN];
ll a[MAXN][MAXN],b[MAXN][MAXN];
vector< pair<pair<ll,ll>, pair<ll,ll> > > v;
vector<ll> z(550),x(550);
vector<pair<int,pair<int,int> > > g;
bool r[MAXN];

int main()
{
  fr fw
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cin>>n>>m>>k;
  for(int i=1;i<=m;++i)
		cin>>c[i];
	for(int i=1;i<=k;++i)
		cin>>p[i];
	for(int i=1;i<=n;++i)
	{
		int t;
		cin>>t;
		for(int j=1;j<=t;++j)
			cin>>a[i][j];
		z[i]=t;
	}
	for(int i=1;i<=n;++i)
	{
		int t;
		cin>>t;
		for(int j=1;j<=t;++j)
			cin>>b[i][j];
		x[i]=t;
	}
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=z[i];++j)
			for(int h=1;h<=x[i];++h)
				v.pb(mp(mp(z[i]*x[i],i),mp(a[i][j],b[i][h])));
	}
	sort(all(v));           /*
	for(int i=0;i<sz(v);++i)
		cout<<v[i].F.F<<' '<<v[i].F.S<<' '<<v[i].S.F<<' '<<v[i].S.S<<'\n';
	*/for(int i=0;i<sz(v);++i)
	{
		if(!r[v[i].F.S])
		{
			for(int j=0;j<sz(v);++j)
			{                                   
				if(v[j].F.S==v[i].F.S)
				{	
					if(c[v[j].S.F]>0 && p[v[j].S.S]>0)
					{
						g.pb(mp(v[j].S.F+v[j].S.S,mp(v[j].S.F,v[j].S.S)));
						//ans++;
						//r[v[i].F.S]=1;		
					}	
				}
			}
			sort(all(g));
			c[g.back().S.F]--;
			p[g.back().S.S]--;
			if(!g.empty())
				ans++;
			r[v[i].F.S]=1;
		}
	}
	cout<<ans;
	return 0;
}