#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "A."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()


using namespace std;

const int MAXN=(int)(1e5+11);

int n,m,l,r,c;
vector<int> v[MAXN];

int main()
{
	fr fw
	//ios_base::sync_with_stdio(0);
	//cin.tie(0);
  //cin>>n>>m;
  scanf("%d%d",&n,&m);
  while(m--)
  {
  	//cin>>l>>r>>c;
  	scanf("%d%d%d",&l,&r,&c);
  	if(c!=0)
  	{
  		for(int i=l;i<=r;i++)
  			v[i].pb(c);	
  	}
  	else
  		for(int i=l;i<=r;++i)
  				v[i].pop_back();
  }
  for(int i=1;i<=n;++i)
  {
  	if(v[i].empty())
  		printf("0 ");     
  	else
  		printf("%d ",v[i].back());//cout<<v[i].back()<<" ";
  }
	return 0;
}