#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define fname "A."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair
#define all(v) v.begin(),v.end()


using namespace std;

const int MAXN=(int)(1e5+11);

ll n,m,l,r,c,mm[MAXN];
set<ll> v[MAXN];
bool b[MAXN];

int main()
{
	fr fw
	ios_base::sync_with_stdio(0);
	cin.tie(0);
  cin>>n>>m;
  while(m--)
  {
  	cin>>l>>r>>c;
  	if(c!=0)
  	{
  		for(int i=l;i<=r;i++)
  			v[i].insert(c),mm[i]=max(mm[i],c);	
  	}
  	else
  	{
  		for(int i=l;i<=r;++i)
  		{
  			if(!v[i].empty())
  				v[i].erase(mm[i]);
  		}
  	}
  }
  for(int i=1;i<=n;++i)
  {
  	if(v[i].empty())
  		cout<<"0 ";     
  	else
  	{
  		cout<<mm[i]<<' ';
  	}               
  }
	return 0;
}