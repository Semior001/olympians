#include <bits/stdc++.h>

#define sqr(x) ((x)*(x))
#define ll long long
#define pb push_back
#define fname "C."
#define fr freopen(fname"in","r",stdin);
#define fw freopen(fname"out","w",stdout);
#define mp make_pair


using namespace std;

const int MAXN=(int)(2000+11);

ll ans;
int n,cnt[MAXN],a[MAXN],c[MAXN];

int main()
{
	fr fw
	ios_base::sync_with_stdio(0);
	cin.tie(0);
  cin>>n;
  for(int i=1;i<=n;++i)
  {
  	cin>>a[i];
  	cnt[a[i]]++;
  }
  for(int i=1;i<=n;++i)
  {
  	if(cnt[a[i]]!=1)
  		c[i]++;
  	c[i]+=c[i-1];
  }
  for(int i=1;i<n;++i)
  {
  	vector<int> q,w;
  	for(int j=1;j<=i;++j)
  		q.pb(a[j]);
  	for(int j=i+1;j<=n;++j)
  		w.pb(a[j]);
  	ans+=(sqr(min(c[i],c[n]-c[i]))+1);	
  }   
  cout<<ans;
	return 0;
}