#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <assert.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "A."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;
typedef const int c_i;

const int maxn = 100100;
const int INF = 1e9 + 9;

struct query {
	int val, l, r;
};

int n, m;
query q[maxn];

typedef struct pst_node* node_ptr;

struct pst_node {
    int node_sum;
	node_ptr l, r;
	pst_node(const int &x) : node_sum(x), l(0), r(0) {}
	pst_node(node_ptr l1, node_ptr r1) : l(l1), r(r1) {
		node_sum = l -> sum() + r -> sum();
	}
	int sum() {
		return this ? node_sum : 0;
	}                                             
	node_ptr upd(c_i &qx, c_i &delta, int tl = 1, int tr = -1) {
		if (tr == -1) tr = n + 1;
		if (tl == tr - 1)
			return new pst_node(delta + node_sum); 
		int mid = (tl + tr) >> 1;
		if (qx < mid)
			return new pst_node (
				l -> upd(qx, delta, tl, mid),
				r
			);
		else
			return new pst_node (
				l,
				r -> upd(qx, delta, mid, tr)
			);
	}
	int get(c_i &ql, c_i &qr, int tl = 1, int tr = -1) {          
		if (tr == -1) tr = n + 1;                                 
		if (tr <= ql || qr <= tl) return 0;
		if (ql <= tl && tr <= qr) 
			return node_sum;             
		int mid = (tl + tr) >> 1;
		int ans = l -> get(ql, qr, tl, mid); 
		return ans + r -> get(ql, qr, mid, tr);
	}
} *root;                   

node_ptr build(c_i &tl, c_i &tr) {
	if (tl == tr - 1)
		return new pst_node(0);	
	int mid = (tl + tr) >> 1;
	return new pst_node (
		build(tl, mid),
		build(mid, tr)
	);	
}

node_ptr save[maxn];
int res[maxn];

const int magic_const = 25000;

int bin_search(const int &x) {
	int end_val = save[m] -> get(1, x + 1);
	for1(i, max(0, m / 2 + magic_const), min(m, m / 2 - magic_const)) {
		if (q[i].val == 0) continue;
		if (save[i - 1] -> get(1, x + 1) == end_val - 1)
			return i;
	}
	assert(0);
}

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%d %d", &n, &m);
	forn(i, 1, m)
		scanf("%d %d %d", &q[i].l, &q[i].r, &q[i].val);

	save[0] = build(1, n + 1); 
	forn(i, 1, m) {
		int delta = (q[i].val ? 1 : -1); 
		node_ptr temp = save[i - 1] -> upd(q[i].l, delta);	
		if (q[i].r < n) 
			save[i] = temp -> upd(q[i].r + 1, -delta);
		else
			save[i] = temp;
	}
	
	forn(i, 1, n) {
		if (save[m] -> get(1, i + 1) == 0) {
			res[i] = 0;
			continue;
		}
		res[i] = q[bin_search(i)].val;	
	}

	forn(i, 1, n)
		printf("%d ", res[i]);

	return 0;
}
          	