#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "A."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 100100;
const int INF = 1e9 + 9;
        
struct sqrt_decomposition {
	int n, sq_block, head[maxn];
	int a[maxn], u[maxn], e[maxn];
	void init (const int &_n) {
		n = _n;
	    sq_block = sqrt(n);
	    int next_head = 1;
	    forn(i, 1, n) {
	    	if (i == next_head) {
	    		head[i] = i;
	    		next_head += sq_block;
	    		continue;
	    	}
	    	head[i] = head[i - 1];
	    }
	}
	void upd(int l, const int &r, const int &val) {
		while (l <= r && l != head[l]) { 
			a[l] += val;
			++l;
		}
		while (l <= r && l != head[r]) {
			u[l] += val;
			l += sq_block;
		}
		while (l <= r) {
			a[l] += val;
			++l;
		}
	}
	void eql_push(const int &x) {
		if (!e[x]) return;
		forn(i, x, x + sq_block - 1)
			a[i] = e[x];
		e[x] = 0;	
	}
	void eql(int l, const int &r, const int &val) {
		eql_push(head[l]);
		eql_push(head[r]);
		while (l <= r && l != head[l]) { 
			a[l] = val;
			++l;
		}
		while (l <= r && l != head[r]) {
			e[l] = val;
			l += sq_block;
		}
		while (l <= r) {
			a[l] = val;
			++l;
		}
	}
	void e_push() {              
		int next_head = 1;
		while (next_head <= n) {
			eql_push(next_head);
			next_head += sq_block;
		}
	}          
	void push() {
		forn(i, 1, n)        
			a[i] += u[head[i]];
		forn(i, 1, n)
			u[i] = 0;
	}
} sqrt_dec, last_dec;

struct query {
	int val, l, r;
};

int n, m;
query q[maxn];
int a[maxn];
int block[maxn], bal[maxn];
        
int res[maxn];

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%d %d", &n, &m);
	forn(i, 1, m)
		scanf("%d %d %d", &q[i].l, &q[i].r, &q[i].val);
	
	sqrt_dec.init(n);
	last_dec.init(n);
	
	forn(i, 1, m) {
		if (q[i].val == 0)
			last_dec.eql(q[i].l, q[i].r, -1);
		else
			last_dec.eql(q[i].l, q[i].r, q[i].val);	
	}
	
	reverse(q + 1, q + 1 + m);
	int sq_block = sqrt(m);
	int last_end = 0;                 

	forn(i, 1, m) {                                                
		if (q[i].val == 0) 
			sqrt_dec.upd(q[i].l, q[i].r, -1);	
		else 
			sqrt_dec.upd(q[i].l, q[i].r, 1);	
		if (i == last_end + sq_block || i == m) {
			sqrt_dec.push();
			forn(j, 1, n) {
				if (sqrt_dec.a[j] > 0 && !block[j])  
					block[j] = last_end + 1, bal[j] = a[j];
				else
					a[j] = sqrt_dec.a[j];
			}
			last_end = i;
		}
	}
                    
	last_dec.e_push();
	forn(i, 1, n) {
		if (last_dec.a[i] > 0) {
			res[i] = last_dec.a[i];
			continue;
		} 
		if (!block[i]) { 
			res[i] = 0;
			continue;
		}                       
		forn(j, block[i], min(block[i] + sq_block - 1, m)) {
			if (!(q[j].l <= i && i <= q[j].r)) continue;
			if (q[j].val == 0)
				--bal[i];
			else
				++bal[i];
			if (bal[i] > 0) {
				res[i] = q[j].val;
				break;
			}	
		}
	}

	forn(i, 1, n)
		printf("%d ", res[i]);

	return 0;
}
