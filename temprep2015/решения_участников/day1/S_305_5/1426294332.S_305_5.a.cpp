#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "A."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 1024;
const int INF = 1e9 + 9;

struct query {
	int val, l, r;
};

int n, m;
query q[maxn];
int st[maxn][maxn], sz[maxn];

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%d %d", &n, &m);
	forn(i, 1, m) {
		scanf("%d %d %d", &q[i].l, &q[i].r, &q[i].val);
    	if (q[i].val) 
    		forn(j, q[i].l, q[i].r)
    			st[j][++sz[j]] = q[i].val;
    	else
    		forn(j, q[i].l, q[i].r)
    			--sz[j];	
    }

    forn(i, 1, n)
    	printf("%d ", st[i][sz[i]]);

	return 0;
}
