#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "B."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 100100;
const int INF = 1e9 + 9;

int n, k;
char s[maxn];             

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%s\n%d", s, &k);
	n = strlen(s);

	int ans = INF;
	forn(l, 0, n - 1) {
		forn(r, l, n - 1) {
			set <char> st;
			forn(k, l, r)
				st.insert(s[k]);
			if (st.size() == k)
				ans = min(ans, r - l + 1);	
		}
	}

	printf("%d\n", ans == INF ? -1 : ans);
	
	return 0;
}
