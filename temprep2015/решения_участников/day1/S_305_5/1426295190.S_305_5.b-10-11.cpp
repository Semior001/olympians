#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "B."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 100100;
const int INF = 1e9 + 9;

int n, k;
char s[maxn];

map <char, int> mp;

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%s\n%d", s, &k);
	n = strlen(s);

	int ans = INF;
	int ptr = 0;            
	forn(i, 0, n - 1) {      
		ptr = max(ptr, i); 
		while (ptr < n && mp.size() < k)               
			++mp[s[ptr++]];              
		if (ptr == n && mp.size() < k) break;
		ans = min(ans, ptr - i);
		--mp[s[i]];
		if (mp[s[i]] == 0)
			mp.erase(s[i]); 
	}

	printf("%d\n",ans == INF ? -1 : ans);
	
	return 0;
}
