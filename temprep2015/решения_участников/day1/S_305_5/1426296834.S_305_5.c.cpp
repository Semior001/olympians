#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <math.h>
#include <set>
#include <map>

using namespace std;

#define forn(i, x, n) for (int i = int(x); i <= int(n); ++i) 
#define for1(i, n, x) for (int i = int(n); i >= int(x); --i)
#define file "C."
#define F first
#define S second
#define mk_pr make_pair

typedef long long ll;
typedef pair <int, int> PII;

const int maxn = 100100;
const int INF = 1e9 + 9;

int n;
int a[maxn];
bool blocked[maxn];

ll pairs(const ll &x) {
	return (x * (x + 1)) >> 1;	
}

int main() {
	freopen(file"in", "r", stdin);
	freopen(file"out", "w", stdout);
	
	scanf("%d", &n);
	forn(i, 1, n) 
		scanf("%d", &a[i]);

	ll ans = 0;
	forn(len, 1, n) {
		forn(l, 1, n - len + 1) {
			int r = l + len - 1;
			forn(k, l, r)
				blocked[a[k]] = 1;
			int last = r;                 
			forn(k, r + 1, n) {
				if (!blocked[a[k]]) continue; 
				ans += pairs(k - last - 1);	
				last = k;
			}                                 
			ans += pairs(n - last);	
			forn(k, l, r)
				blocked[a[k]] = 0;
		}
	} 
            
    cout << ans << "\n";

	return 0;
}
