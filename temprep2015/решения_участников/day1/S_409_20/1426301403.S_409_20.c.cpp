#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
using namespace std;
#define fnm "C"
#define pb push_back
#define mp make_pair
#define all(s) s.begin(), s.end()
int n;
int q[5001];
int a[5001];
int ans;
void check(int f, int s) {
	if (a[f] == a[s] || s > n || f >= s) return;
	else {
		ans++;
		check(f, s+1);
		check(f+1,s);
	}
}
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);

  	cin>>n;
  	for (int i = 1;i <= n;i++) {
  		cin>>a[i];
  	}
  	for (int i = 1;i <= n;i++) {
  		for (int j = i+1;j <= n;j++) {
  			check(i,j);
  		}
  	}
  	if (ans) ans--;
  	cout<<ans;
  	return 0;
}