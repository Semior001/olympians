#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "A"
#define pb push_back
#define mp make_pair
const int MAXN = 1e5+1;
int n, m, c, l, r;
vector<int> q[MAXN], q2[MAXN];
int ans[100001];
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m;              
    for (int i = 0;i <= n;i++) q[i].push_back(0);
 	for (int i = 1;i <= m;i++) {
 		cin>>l>>r>>c;
 		if (c != 0) 
 			for (int j = l;j <= r;j++) {
 				if (q[j].size() <= 50000)  q[j].push_back(c);
 				else q2[j].pb(c);
 			}
 		else 
 		for (int j = l;j <= r;j++)
 		if (q[j].size() <= 50000 || q2[j].size() == 1)  q[j].pop_back();
 		else q2[j].pop_back();
 	} 	
 	for (int i = 1;i <= n;i++)
 	 if (q2[i].size() > 1) cout<<q2[i].back()<<" ";
 	 else cout<<q[i].back()<<" ";
   
    return 0;
}
