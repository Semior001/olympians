#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "A"
#define pb push_back
#define mp make_pair
const int MAXN = 1e5+1;
int n, m, c, l, r;
vector<int> q[MAXN], q2[MAXN];
int ans[100001];
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m;              
    for (int i = 0;i <= n;i++) q[i].push_back(0);
 	for (int i = 1;i <= m;i++) {
 		cin>>l>>r>>c;
 		if (c != 0) 
 			for (int j = l;j <= r;j++) {
            	q[j].pb(c);
            	ans[j] = q[j].back();    
          			}
 		else 
 		for (int j = l;j <= r;j++)   {
 			q[j].pop_back();          
 			ans[j] = q[j].back();
 			q[j].pop_back();
 			}
 	} 	
 	for (int i = 1;i <= n;i++)
 	cout<<ans[i]<<" ";   
    return 0;
}
