#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
using namespace std;
#define fnm "B"
#define F first
#define S second
#define pb push_back
#define mp make_pair
#define all(s) s.begin(), s.end()
const int MAXN = 550;
int n, m, k, ans;
int x[MAXN], A[MAXN][MAXN], B[MAXN][MAXN], c[MAXN], p[MAXN], C[MAXN], y[MAXN];
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m>>k;
	
	for (int i = 1;i <= m;i++) cin>>c[i];
	for (int i = 1;i <= k;i++) cin>>p[i];

    for (int i = 1;i <= n;i++) {
    	cin>>x[i];
    	for (int j = 1;j <= x[i];j++)
    	 cin>>A[i][j];
    }          
    
    for (int i = 1;i <= n;i++) {
    	  cin>>y[i];
    	  for (int j = 1;j <= y[i];j++) {
    	  	cin>>B[i][j];
    	  }
    }

    for (int i = 1;i <= n;i++) {
    	int minn = 1000, pos = 0, pos2 = 0;
    	for (int j = 1;j <= x[i];j++) {
    	     if (minn > A[i][j] && A[i][j] && c[A[i][j]]) {
    	           minn = A[i][j];
    	           pos = j;
    	     }
    	}
    	minn = 1000;  
    	for (int j = 1;j <= y[i];j++) {
    		if (minn > B[i][j] && B[i][j] && p[B[i][j]]) {
    			minn = B[i][j];
    			pos2 = j;
    		}
    	}
    	if (pos && pos2)
    	 c[pos]--, p[pos2]--, ans++;
    }

    cout<<ans;
	
	return 0;
}
