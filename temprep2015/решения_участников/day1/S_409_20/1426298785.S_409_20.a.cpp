#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>
using namespace std;
#define fnm "A"
#define pb push_back
#define mp make_pair
#define sz size
const int MAXN = 1e5+1;
const int num = 10000;
int n, m, c, l, r;
vector<int> q[MAXN], q2[MAXN], q3[MAXN], q4[MAXN], q5[MAXN], q6[MAXN], q7[MAXN], q8[MAXN], q9[MAXN], qq[MAXN];
int ans[100001];
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m;              
    for (int i = 0;i <= n;i++) q[i].push_back(0);
 	for (int i = 1;i <= m;i++) {
 		cin>>l>>r>>c;
 		if (c != 0) 
 			for (int j = l;j <= r;j++) {
 				if (q[j].sz() <= num)  q[j].push_back(c);
 				else if (q2[j].size() <= num) q2[j].pb(c);
 				else if (q3[j].sz() <= num) q3[j].pb(c);
 				else if (q4[j].sz() <= num) q4[j].pb(c);
 				else if (q5[j].sz() <= num) q5[j].pb(c);
 				else if (q6[j].sz() <= num) q6[j].pb(c);
 				else if (q7[j].sz() <= num) q7[j].pb(c);
 				else if (q8[j].sz() <= num) q8[j].pb(c);
 				else if (q9[j].sz() <= num) q9[j].pb(c);
 				else if (qq[j].sz() <= num) qq[j].pb(c);
 			}             
 		else 
 		for (int j = l;j <= r;j++)
 		if (q[j].size() <= num || q2[j].size() == 1)  q[j].pop_back();
 		else  if (q2[j].size() <= num || q3[j].size() == 1)  q2[j].pop_back();
 		else  if (q3[j].size() <= num || q4[j].size() == 1)  q3[j].pop_back();
 		else  if (q4[j].size() <= num || q5[j].size() == 1)  q4[j].pop_back();
 		else  if (q5[j].size() <= num || q6[j].size() == 1)  q5[j].pop_back();
 		else  if (q6[j].size() <= num || q7[j].size() == 1)  q6[j].pop_back();
 		else  if (q7[j].size() <= num || q8[j].size() == 1)  q7[j].pop_back();
 		else  if (q8[j].size() <= num || q9[j].size() == 1)  q8[j].pop_back();
 		else  if (q9[j].size() <= num || qq[j].size() == 1)  q9[j].pop_back();
 		else  qq[j].pop_back();
 	} 	
 	for (int i = 1;i <= n;i++)
 	 if (qq[i].size() > 1) cout<<qq[i].back()<<" ";
 	 else if (q9[i].size() > 1) cout<<q9[i].back()<<" ";
 	 else if (q8[i].size() > 1) cout<<q8[i].back()<<" ";
 	 else if (q7[i].size() > 1) cout<<q7[i].back()<<" ";
 	 else if (q6[i].size() > 1) cout<<q6[i].back()<<" ";
 	 else if (q5[i].size() > 1) cout<<q5[i].back()<<" ";
 	 else if (q4[i].size() > 1) cout<<q4[i].back()<<" ";
 	 else if (q3[i].size() > 1) cout<<q3[i].back()<<" ";
 	 else if (q2[i].size() > 1) cout<<q2[i].back()<<" ";
 	 else cout<<q[i].back()<<" ";
   
    return 0;
}
