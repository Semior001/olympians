#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
using namespace std;
#define fnm "B"
#define F first
#define S second
#define pb push_back
#define mp make_pair
#define all(s) s.begin(), s.end()
const int MAXN = 550;
int n, m, k, a, x, A[MAXN], B[MAXN], c[MAXN], p[MAXN], C[MAXN], ans;
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m>>k;
	
	for (int i = 1;i <= m;i++) cin>>c[i], A[c[i]] = 1;
	for (int i = 1;i <= k;i++) cin>>p[i], C[p[i]] = 1;
	sort(c+1, c+m+1);
	sort(p+1, p+k+1);
	for (int i = 1;i <= n;i++) {
		cin>>x;
		for (int j = 1;j <= x;j++) {
			cin>>a;
			if (A[a] == 1 || C[a] == 1) B[i] = 1; 
		}     
	}

	for (int i = 1;i <= n;i++) {
		cin>>x;
		for (int j = 1;j <= x;j++) {
			cin>>a;
			if (B[i] == 1) {
				if (A[a] == 1 || C[a] == 1)
				 ans++, B[i] = 0;
			}
		}
	}
	cout<<ans;
	return 0;
}
