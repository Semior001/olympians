#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
using namespace std;
#define fnm "B"
#define F first
#define S second
#define pb push_back
#define mp make_pair
#define all(s) s.begin(), s.end()
const int MAXN = 550;
int n, m, k, ans;
int x[MAXN], A[MAXN][MAXN], B[MAXN][MAXN], c[MAXN], p[MAXN], C[MAXN], y[MAXN], AA[MAXN], BA[MAXN], CA[MAXN], ans2, cc[MAXN], pp[MAXN], ans3;
int main() {
  	freopen(fnm".in", "r", stdin);
  	freopen(fnm".out", "w", stdout);
 	cin>>n>>m>>k;
	                                          
	for (int i = 1;i <= m;i++) cin>>c[i], AA[c[i]] = 1, cc[i] = c[i];
	for (int i = 1;i <= k;i++) cin>>p[i], CA[p[i]] = 1, pp[i] = p[i];
	sort(cc+1, cc+m+1);
	sort(pp+1, pp+k+1);

    for (int i = 1;i <= n;i++) {
    	cin>>x[i];
    	for (int j = 1;j <= x[i];j++)           {
    	 cin>>A[i][j];
			if (AA[A[i][j]] == 1 || CA[A[i][j]] == 1) BA[i] = 1;} 

    }          
    
    for (int i = 1;i <= n;i++) {
    	  cin>>y[i];
    	  for (int j = 1;j <= y[i];j++) {
    	  	cin>>B[i][j];
    	  	if (BA[i] == 1) {
				if (AA[B[i][j]] == 1 || CA[B[i][j]] == 1)
				 ans2++, BA[i] = 0;
			}
                                                                   
    	  }
    }

    for (int i = 1;i <= n;i++) {
    	int maxx = -1000, pos = 0, pos2 = 0;
    	for (int j = 1;j <= x[i];j++) {
    	     if (maxx < c[A[i][j]] && A[i][j] && c[A[i][j]]) {
    	           maxx = c[A[i][j]];
    	           pos = j;
    	     }
    	}
    	maxx = -1000;  
    	for (int j = 1;j <= y[i];j++) {
    		if (maxx < p[B[i][j]] && B[i][j] && p[B[i][j]]) {
    			maxx = p[B[i][j]];
    			pos2 = j;
    		}
    	}
    	if (pos && pos2)
    	 c[pos]--, p[pos2]--, ans++;   
    }



    for (int i = 1;i <= n;i++) {
    	int minn = 1000, pos = 0, pos2 = 0;
    	for (int j = 1;j <= x[i];j++) {
    	     if (minn > c[A[i][j]] && A[i][j] && c[A[i][j]]) {
    	           minn = c[A[i][j]];
    	           pos = j;
    	     }
    	}
    	minn = 1000;  
    	for (int j = 1;j <= y[i];j++) {
    		if (minn > p[B[i][j]] && B[i][j] && p[B[i][j]]) {
    			minn = p[B[i][j]];
    			pos2 = j;
    		}
    	}
    	if (pos && pos2)
    	 c[pos]--, p[pos2]--, ans3++;   
    }

    cout<<max(ans, max(ans2, ans3));
	
	return 0;
}
                            
