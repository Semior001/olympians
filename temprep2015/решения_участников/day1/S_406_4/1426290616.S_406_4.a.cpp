//Bayemirov Beket
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <math.h>
#include <map>
#include <set>
#include <vector>

#define pb push_back
#define fname "A"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, m;
vector <int> a[N];

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &m);

    for (int i = 1; i <= n; i++)
            a[i].pb(0);

	while(m--) {
		int l, r, c;
		scanf("%d%d%d", &l, &r, &c);
		if (c != 0) {
			for (int i = l; i <= r; i++)
				a[i].pb(c);
		}
		else {
			for (int i = l; i <= r; i++)
				a[i].erase(a[i].end() - 1);
		}
	}

	for (int i = 1; i <= n; i++)
		printf("%d ", a[i].back());

	return 0;
}
