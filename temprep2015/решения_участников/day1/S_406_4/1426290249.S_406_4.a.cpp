//Bayemirov Beket
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <math.h>
#include <map>
#include <set>
#include <vector>

#define pb push_back
#define fname "A"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, m, a[N];

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &m);

	while(m--) {
		int l, r, c;
		scanf("%d%d%d", &l, &r, &c);
		if (c != 0) {
			for (int i = l; i <= r; i++)
				a[i] = c;
		}
		else {
			for (int i = l; i <= r; i++)
				a[i] = 0;
		}
	}

	for (int i = 1; i <= n; i++)
		printf("%d ", a[i]);

	return 0;                
}                                                                                                                                                                                                                    