//Bayemirov Beket
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <math.h>
#include <map>
#include <set>
#include <vector>

#define fname "C"

using namespace std;

typedef long long ll;

const int N = 100500;

int n, a[N], cnt;
ll ans;
bool was[N];

inline ll get(ll x) {
	return ((x + 1) * (x)) / 2ll;
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);

	for (int l = 1; l <= n; l++) {
		for (int r = l; r <= n; r++) {
			for (int k = l; k <= r; k++)
				was[a[k]] = true;  //cout << l << " " << r << "";
			cnt = 0;
			for (int k = r + 1; k <= n; k++) {
				if (was[a[k]])
					ans += get(cnt),
					cnt = 0;
				cnt++;
			}
			ans += get(cnt);
			for (int k = l; k <= r; k++)
				was[a[k]] = false;
		}
	}

	printf("%I64d", ans);

	return 0;
}
