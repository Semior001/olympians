//Bayemirov Beket
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <math.h>
#include <map>
#include <set>
#include <vector>

#define pb push_back
#define fname "B"

using namespace std;

typedef long long ll;

const int N = 1500;

int n, m, k, cnt, p1[N], p2[N], res, res_a, res_b, ans;
vector <int> g1[N], g2[N], a_bl[N], b_bl[N];
vector <bool> used(N);
bool was[N];

bool dfs1(int v) {
	if (used[v])
		return false;
	used[v] = true;
	for (int i = 0; i < g1[v].size(); i++) {
		int to = g1[v][i];
		if (p1[to] == -1 || dfs1(p1[to])) {
			p1[to] = v;
			return true;
		}
	}
	return false;
}

bool dfs2(int v) {
	if (used[v])
		return false;
	used[v] = true;
	for (int i = 0; i < g2[v].size(); i++) {
		int to = g2[v][i];
		if (p2[to] == -1 || dfs2(p2[to])) {
			p2[to] = v;
			return true;
		}
	}
	return false;
}

int main() {

	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d%d", &n, &m, &k);

	cnt = 0;

	for (int i = 1; i <= m; i++) {
		int x;
		scanf("%d", &x);
		for (int j = 1; j <= min(n, x); j++)
			cnt++,
			a_bl[i].pb(cnt);
	}

	res_a = cnt;

	cnt = 0;

	for (int i = 1; i <= k; i++) {
		int x;
		scanf("%d", &x);
		for (int j = 1; j <= min(n, x); j++)
			cnt++,
			b_bl[i].pb(cnt);
	}

	res_b = cnt;

	for (int i = 1; i <= n; i++) {
	 	int x;
	 	scanf("%d", &x);
	 	for (int j = 1; j <= x; j++) {
	 		int y;
	 		scanf("%d", &y);
			for (int k = 0; k < a_bl[y].size(); k++)
				g1[res_a + i].pb(a_bl[y][k]);
	 	}
	}

	for (int i = 1; i <= n; i++) {
	 	int x;
	 	scanf("%d", &x);
	 	for (int j = 1; j <= x; j++) {
	 		int y;
	 		scanf("%d", &y);
			for (int k = 0; k < b_bl[y].size(); k++)
				g2[res_b + i].pb(b_bl[y][k]);
	 	}
	}

	for (int i = 1; i <= max(res_a, res_b) + n; i++)
		p1[i] = -1,
		p2[i] = -1;

	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= max(res_a, res_b) + n; j++)
            used[j] = false;
		dfs1(res_a + i);
	}

	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= max(res_a, res_b) + n; j++)
            used[j] = false;
		dfs2(res_b + i);
	}

	for (int i = 1; i <= res_a; i++)
		if (p1[i] != -1)
			was[p1[i] - res_a] = true;

	for (int i = 1; i <= res_b; i++)
		if (p2[i] != -1)   
			if (was[p2[i] - res_b])
				ans++;

	/*for (int i = res_a + 1, j = res_b + 1; i <= res_a + n, j <= res_b + n; i++, j++) {
		if (p1[i] != -1 && p2[j] != -1)
			ans++;
	}*/

	printf("%d", ans);

	return 0;
}
