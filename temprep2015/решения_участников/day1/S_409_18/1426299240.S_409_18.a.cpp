#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
using namespace std;
const int SZ=(1<<18)+5;
int a[SZ]={0},n,m,lg=1;
int build()
{
    while (lg<n) lg+=lg;
    for (int i=1;i<=lg;i++)
    {
        a[i+lg-1]=0;
    }
    for (int i=1;i<lg;i++)
    {
        a[i]=-1;
    }
    return 0;
}

int ud(int l,int r,int tl,int tr,int v,int val2)
{
    if (tr<l||tl>r) {if (val2!=-1){a[v]=val2;}return 0;}
    if (tl>=l&&tr<=r) a[v]=0;
    else { if (val2==-1){val2=a[v]; a[v]=-1;}
            int tm=(tl+tr)/2;
            ud(l,r,tl,tm,v+v,val2);
            ud (l,r,tm+1,tr,v+v+1,val2);
    }
    return 0;
}
int pl(int l,int r,int tl,int tr,int val,int val2=-1,int v=1)
{
    if (tr<l||tl>r) {a[v]=val2;return 0;}
    if (tl>=l&&tr<=r) a[v]=val;
    else{ int tm=(tl+tr)/2;
            if (a[v]!=-1)  val2=a[v];
    pl (l,r,tl,tm,val,val2,v+v);
    pl (l,r,tm+1,tr,val,val2,v+v+1);
    }
    return 0;
}
int oout(int tl,int tr,int v=1)
{
    if (a[v]!=-1) for (int i=tl;i<=tr&&i<=n;i++) printf ("%d ",a[v]);
    else {int tm=(tl+tr)/2;
         oout(tl,tm,v+v);
         oout (tm+1,tr,v+v+1);
    }
    return 0;
}
int main()
{
    freopen ("A.in","r",stdin);
    freopen ("A.out","w",stdout);
    scanf("%d%d",&n,&m);
    build();
    for (int i=1;i<=m;i++)
    {int l,r,c;
        cin>>l>>r>>c;
        if (c==0) ud(l,r,1,lg,1,-1);
        else pl(l,r,1,lg,c);
    }
    oout(1,lg);
    return 0;
}
