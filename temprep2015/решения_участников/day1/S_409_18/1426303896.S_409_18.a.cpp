#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
using namespace std;
const int SZ=(1<<18)+5;
int n,m,lg=1,l,r,c;
struct {
         stack<int> t;
       }a[SZ];
int main()
{
    freopen ("A.in","r",stdin);
    freopen ("A.out","w",stdout);
    cin>>n>>m;
    for (int i=1;i<=n;i++)
    {a[i].t.push(0);
    }
    for (int i=1;i<=m;i++)
    {
        cin>>l>>r>>c;
        for (int j=l;j<=r;j++)
            {if (c!=0)a[j].t.push(c);
            else a[j].t.pop();}
    }
    for (int i=1;i<=n;i++)
        cout<<a[i].t.top()<<" ";
    return 0;

}
