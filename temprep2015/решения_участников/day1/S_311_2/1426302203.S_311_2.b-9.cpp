#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

int t[300500];
int ls[200], nx[100500];
int k;
char st[100500];
void add(int pos) {
     pos /= 2;
     while (pos) {
           t[pos] = t[pos * 2] + t[pos * 2 + 1];
           pos /= 2;
     }
}

int get_max(int l, int r) {
    int res = 0 ;
    while (l <= r) {
          if (l % 2 == 1) {
             res = res + t[l];
          }
          if (r % 2 == 0) {
             res = res + t[r];
          }
          l = (l + 1) / 2;
          r = (r - 1) / 2;
    }
    return res;
}

int main () {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    srand(time(NULL));
    string s;
    sc("%s", &st);
    sc("%d", &k);
    s = string(st);
    int n = s.size();
    int sz = 1;
    while (sz < n) {
          sz *= 2;
    }
    int p1 = 0;
    for (int i = 1; i <= n; i++) {
        int c = 0;
        if (!ls[s[i - 1] - 'a']) {
           c = 1;
           ls[s[i - 1] - 'a'] = i;
        } else {
           nx[ls[s[i - 1] - 'a']] = i;
           ls[s[i - 1] - 'a'] = i;
        }
        t[sz + i - 1] = c;
        add(sz + i - 1);
    }

    int ans = INF;
    for (int i = 1; i <= n; i++) {
        int l = i;
        int r = n;
        int p = i;
        while (l <= r) {
              int mid = (l + r) / 2;
              if (get_max(sz + i - 1, sz + mid - 1) >= k) {
                 r = mid - 1;
                 p = mid;
              } else {
                 l = mid + 1;
              }
        }
        if (get_max(sz + i - 1, sz + p - 1) >= k) {
           ans = min(ans, p - i + 1);
        }
        if (nx[i]) {
            t[sz + nx[i] - 1] = 1;
            add(sz + nx[i] - 1);
        }
    }
    if (ans == INF) {
        pr("-1");
    } else {
        pr("%d", ans);
    }
    return 0;
}
