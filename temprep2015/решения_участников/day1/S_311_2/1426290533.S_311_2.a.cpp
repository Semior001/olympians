#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

struct asd{
        int b[2000];
        int r;
        void pb(int v) {
            b[++r] = v;
            return;
        }
        void pf() {
            r--;
        }
        int co() {
            return b[r];
        }
};

asd a[2000];

int main () {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    srand(time(NULL));
    int n, m;
    sc("%d%d", &n, &m);
    for (int i = 1; i <= m; i++) {
        int l, r, c;
        sc("%d%d%d", &l, &r, &c);
        if (c == 0) {
           for (int j = l; j <= r; j++) {
               a[j].pf();
           }
        } else {
           for (int j = l; j <= r; j++) {
               a[j].pb(c);
           }
        }
    }
    for (int i = 1; i <= n; i++) {
        pr("%d ", a[i].co());
    }
    return 0;
}
