#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define y1  y5
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

int ls[600], nx[600], a[600];

bool pred(int pos, int x, int y) {
     if (pos >= x && pos <= y) {
        return false;
     }
     if (!nx[pos]) {
        return true;
     }
     return nx[pos];
}

bool tek(int x, int y, int x1, int y2) {
     for (int h = x; h <= y; h++) {
         if (!pred(nx[h], x1, y2)) {
            return false;
         }
     }
     return true;
}

int main () {
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);
    srand(time(NULL));
    int n;
    sc("%d", &n);
    for (int i = 1; i <= n; i++) {
        sc("%d", &a[i]);
        if (!ls[a[i]]) {
           ls[a[i]] = i;
        } else {
           nx[ls[a[i]]] = i;
           ls[a[i]] = i;
        }
    }
    long long ans = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = i; j <= n; j++) {
            for (int t = j + 1; t <= n; t++) {
                for (int t1 = t; t1 <= n; t1++) {
                    if (tek(i, j, t, t1)) {
                       ans++;
                    }
                }
            }
        }
    }
    pr("%I64d", ans);
    return 0;
}
