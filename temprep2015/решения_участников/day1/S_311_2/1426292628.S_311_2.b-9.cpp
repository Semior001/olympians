#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

bool was[100];

int main () {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    srand(time(NULL));
    string s;
    cin >> s;
    int k;
    sc("%d", &k);
    int mn = INF;
    for (int i = 0; i < s.size(); i++) {
        int j = i;
        int kol = 0;
        while (1) {
              if (j >= s.size()) {
                 break;
              }
              if (!was[s[j] - 'a']) {
                 kol++;
                 was[s[j] - 'a'] = true;
              }
              if (kol == k) {
                 break;
              }
              j++;
        }
        if (kol == k) {
           mn = min(mn, j - i + 1);
        }
        for (j = 0; j <= 30; j++) {
            was[j] = false;
        }
    }
    if (mn == INF) {
        pr("-1");
    } else {
        pr("%d", mn);
    }
    return 0;
}
