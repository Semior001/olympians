#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <ctime>

using namespace std;

#define INF 2147483647
#define MOD 1000000007
#define pb push_back
#define sc scanf
#define pr printf
#define ex exit(0)
#define tim pr("%.4lf\n", (clock() * 1.0) / CLOKS_PER_SEC)

char st[100500];

struct asd{
       int link, len;
       map <char,int> nx;
};

asd a[300500];
int sz, last;
void add(char c) {
     int nw = ++sz;
     a[nw].len = a[last].len + 1;
     int p = last;
     while (1) {
           if (p == -1) {
              break;
           }
           if (a[p].nx[c]) {
              break;
           }
           a[p].nx[c] = nw;
           p = a[p].link;
     }
     if (p == -1) {
        a[nw].link = 1;
     } else {
        int q = a[p].nx[c];
        if (a[p].len + 1 == a[q].len) {
           a[nw].link = q;
        } else {
           int clone = ++sz;
           a[clone].len = a[p].len + 1;
           a[clone].link = a[q].link;
           a[clone].nx = a[q].nx;
           while (1) {
                 if (p == -1) {
                    break;
                 }
                 if (a[p].nx[c] != q) {
                    break;
                 }
                 a[p].nx[c] = clone;
                 p = a[p].link;
           }
           a[q].link = a[nw].link = clone;
        }
     }
     last = nw;
}

inline void init() {
       a[1].link = -1;
       a[1].len = 0;
       last = sz = 1;
}

int k;
int mn = INF;
bool was[500];

void dfs(int v, int kol, string s1) {
     if (kol == k) {
        int siz = s1.size();
        mn = min(mn, siz);
        return;
     }
     for (char j = 'a'; j <= 'z'; j++) {
         if (a[v].nx[j]) {
            int c = 0;
            if (!was[j]) {
               c = 1;
               was[j] = true;
            }
            dfs(a[v].nx[j], kol + c, s1 + char(j));
            was[j] = false;
         }
     }
}

int main () {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    srand(time(NULL));
    string s;
    sc("%s", &st);
    sc("%d", &k);
    s = string(st);
    init();
    for (int i = 0; i < s.size(); i++) {
        add(s[i]);
    }
    dfs(1, 0, "");
    if (mn == INF) {
       pr("-1");
    } else {
       pr("%d", mn);
    }
    return 0;
}
