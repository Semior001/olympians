#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <queue>

using namespace std;

#define name "B"
#define pb push_back
#define inf 1000000000

const int N = 1505;
const int M = 200005;

int start, End;

struct edge {
	int a, b, flow, cap;
};

int ptr[N];
vector < edge > e;
vector < int > g[N];

void add_edge(int a, int b, int c){
	edge e1 = {a, b, 0, c};
	edge e2 = {b, a, 0, 0};
	g[a].pb(e.size());
	e.pb(e1);
	g[b].pb(e.size());
	e.pb(e2);
}

queue < int > q;
int d[N];

bool bfs(){
	memset(d, -1, sizeof d);
	d[1] = 0;
	q.push(1);
	while(!q.empty()){
		int v = q.front();
		q.pop();
		for(int i = 0;i < g[v].size();++ i){
			int id = g[v][i];
			int to = e[id].b;
			if(e[id].flow < e[id].cap && d[to] == -1){
				d[to] = d[v] + 1;
				q.push(to);
			}
		}
	}
	return d[End] != -1;		
}

int dfs(int v, int flow){
	if(v == End || !flow)
		return flow;
	for(; ptr[v] < g[v].size();++ ptr[v]){
		int id = g[v][ptr[v]];
		int to = e[id].b;
		if(d[to] != d[v] + 1)
			continue;
		int push = dfs(to, min(flow, e[id].cap - e[id].flow));
		if(push){
			e[id].flow += push;
			e[id ^ 1].flow -= push;
			return push;
		}
	}
	return 0;
}

int MaxFlow(){
	int res = 0;
	while(bfs()){
		memset(ptr, 0, sizeof ptr);
		while(int pushed = dfs(1, inf))
			res += pushed;
	}
	return res;
}

int n, m, k;
int c[N], p[N];

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d", &n, &m, &k);
	start = 1;
	End = n + m + k + 2;
	for(int i = 1;i <= m;++ i){
		scanf("%d", &c[i]);
		add_edge(1, i + 1, c[i]);
	}
	for(int i = 1;i <= k;++ i){
		scanf("%d", &p[i]);
		add_edge(n + m + 1 + i, End, p[i]);
	}
	for(int i = 1;i <= n;++ i){
		int cnt;
		scanf("%d", &cnt);
		for(int j = 1;j <= cnt;++ j){
			int x;
			scanf("%d", &x);
			add_edge(x + 1, m + i + 1, 1);
		}
	}
	for(int i = 1;i <= n;++ i){
		int cnt;
		scanf("%d", &cnt);
		for(int j = 1;j <= cnt;++ j){
			int x;
			scanf("%d", &x);
			add_edge(m + i + 1, m + n + 1 + x, 1);
		}
	}
	printf("%d\n", MaxFlow());
	return 0;
}