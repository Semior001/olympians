#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <queue>
#include <map>

using namespace std;

#define name "B"
#define pb push_back
#define mp make_pair
#define inf 1000000000

const int N = 25;

int n, m, k;
int c[N], p[N];
vector < int > likea[N], likeb[N];
int mx = 0;

void rec(int cur, int taked = 0){
	if(cur == n + 1){
		mx = max(mx, taked);
		return;	
	}
	for(int i = 0;i < likea[cur].size();++ i){
		int id1 = likea[cur][i];
		if(c[id1] > 0){
			-- c[id1];
			for(int j = 0;j < likeb[cur].size();++ j){
				int id2 = likeb[cur][j];
				if(p[id2] > 0){
					-- p[id2];
					rec(cur + 1, taked + 1);
					++ p[id2];
				}
			}
			++ c[id1];
		}
	}
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d%d", &n, &m, &k);
	for(int i = 1;i <= m;++ i)
		scanf("%d", &c[i]);
	for(int i = 1;i <= k;++ i)
		scanf("%d", &p[i]);
	for(int i = 1;i <= n;++ i){
		int cnt;
		scanf("%d", &cnt);
		for(int j = 1;j <= cnt;++ j){
			int x;
			scanf("%d", &x);
			likea[i].pb(x);
		}
	}
	for(int i = 1;i <= n;++ i){
		int cnt;
		scanf("%d", &cnt);
		for(int j = 1;j <= cnt;++ j){
			int x;
			scanf("%d", &x);
			likeb[i].pb(x);
		}
	}
	rec(1);
	printf("%d\n", mx);
	return 0;
}