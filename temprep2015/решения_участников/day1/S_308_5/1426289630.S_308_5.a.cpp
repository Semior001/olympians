#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

#define name "A"

const int N = 100005;

int n, m;
vector < int > containers[N];

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d", &n, &m);
	for(int query = 1;query <= m;++ query){
		int l, r, c;
		scanf("%d%d%d", &l, &r, &c);
		if(!c){
			for(int i = l;i <= r;++ i)
				containers[i].pop_back();
		}
		else {
			for(int i = l;i <= r;++ i)
				containers[i].push_back(c);		
		}
	}
	for(int i = 1;i <= n;++ i){
		int sz = containers[i].size();
	    if(sz)
			printf("%d ", containers[i][sz - 1]);
		else
			printf("%d ", 0);
	}
	return 0;
}