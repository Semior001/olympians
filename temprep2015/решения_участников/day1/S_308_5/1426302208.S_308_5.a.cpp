#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>
#include <set>

using namespace std;

#define name "A"
#define pb push_back
#define mp make_pair
#define F first
#define S second

const int N = 100005;
const int SZ = (1 << 18) + 5;

int n, m, lg = 1;
int l, r, c;
vector < int > DelTimes[SZ];
vector < pair < int , int > > all[SZ];
set < pair < int , int > > s[SZ];
set < pair < int , int > > ::iterator it;

void del(int L, int R, int vakit, int v = 1, int tl = 1, int tr = lg){
	if(tr < L || tl > R)
		return;
	if(L <= tl && tr <= R){
	    DelTimes[v].pb(vakit);
		return;
	}
	int tm = (tl + tr) >> 1;
	del(L, R, vakit, v + v, tl, tm);
	del(L, R, vakit, v + v + 1, tm + 1, tr);
}

void put(int L, int R, int vakit, int value, int v = 1, int tl = 1, int tr = lg){
	if(tr < L || tl > R)
		return;
	if(L <= tl && tr <= R){
	    all[v].pb(mp(vakit, value));
		return;
	}
	int tm = (tl + tr) >> 1;
	put(L, R, vakit, value, v + v, tl, tm);
	put(L, R, vakit, value, v + v + 1, tm + 1, tr);
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d%d", &n, &m);
	while(lg < n)
		lg += lg;
	for(int query = 1;query <= m;++ query){
		scanf("%d%d%d", &l, &r, &c);
		if(!c)
			del(l, r, - query);		
		else
			put(l, r, - query, c);
	}
	for(int i = 1;i < lg + lg;++ i){
		for(int j = 0;j < all[i].size();++ j)
			s[i].insert(all[i][j]);
		for(int j = 0;j < DelTimes[i].size();++ j){
			int Time = DelTimes[i][j];
			it = s[i].lower_bound(mp(Time, 0));
			if(it != s[i].end())
				s[i].erase(it);
			else {
				if(i < lg){
					DelTimes[i + i].pb(Time);
					DelTimes[i + i + 1].pb(Time);
				}
			}					
		}

		if(i < lg){
			s[i + i] = s[i];
			s[i + i + 1] = s[i];
		}
		else {
			if(i - lg + 1 <= n){
				if(s[i].empty())
					printf("%d ", 0);
				else 
					printf("%d ", (s[i].begin() -> S));
			}
		}
	}
	return 0;
}