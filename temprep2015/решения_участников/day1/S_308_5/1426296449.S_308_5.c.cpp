#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>

using namespace std;

#define name "C"
#define ll long long

const int N = 5005;

int n;
int a[N];
ll cnt = 0;
bool used[N]; 

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	for(int i = 1;i <= n;++ i)
		scanf("%d", &a[i]);
	for(int l = 1;l < n;++ l){
	    memset(used, false, sizeof used);
		for(int r = l; r < n;++ r){
			used[a[r]] = true;
			int len = 0;
			for(int i = r + 1;i <= n;++ i){
				if(used[a[i]]){
				    cnt += len * (len + 1) / 2;
					len = 0;
				}
				else 
					++ len;
			}			
		    cnt += len * (len + 1) / 2;			
		}	
	}
	cout << cnt << endl;
	return 0;
}