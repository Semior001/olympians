#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>

using namespace std;

#define name "C"

const int N = 5005;

int n;
int a[N];
int cnt = 0;
bool used[N];

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	scanf("%d", &n);
	for(int i = 1;i <= n;++ i)
		scanf("%d", &a[i]);
	for(int l1 = 1;l1 < n;++ l1){
	    memset(used, false, sizeof used);
		for(int r1 = l1; r1 < n;++ r1){
			used[a[r1]] = true;
			for(int l2 = r1 + 1;l2 <= n;++ l2){
			    bool bad = false;
				for(int r2 = l2;r2 <= n;++ r2){
					if(used[a[r2]]){
				    	cnt += r2 - l2;
				    	bad = true;
						break;
					}
				}
				if(!bad)
					cnt += n - l2 + 1;
			}
		}	
	}
	printf("%d\n", cnt);
	return 0;
}