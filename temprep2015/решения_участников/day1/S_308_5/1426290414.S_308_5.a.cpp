#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdio>

using namespace std;

#define name "A"

const int N = 100005;

int n, m;
int l[N], r[N], c[N];
int cnt[N], last[N];

void del(int L, int R){
	for(int i = L;i <= R;++ i)
		if(last[i] == 0)
			-- cnt[i];
}

void put(int L, int R, int value){
	for(int i = L;i <= R;++ i){
	    if(last[i] != 0)
	    	continue;
		if(cnt[i] == 0)
			last[i] = value;
		else 
		    ++ cnt[i];
	}
}

int main(){
	freopen(name".in", "r", stdin);	
	freopen(name".out", "w", stdout);	
	memset(last, 0, sizeof last);
	scanf("%d%d", &n, &m);
	for(int query = 1;query <= m;++ query)
		scanf("%d%d%d", &l[query], &r[query], &c[query]);
	for(int query = m;query >= 1;-- query){
		if(!c[query])
			del(l[query], r[query]);		
		else
			put(l[query], r[query], c[query]);
	}
	for(int i = 1;i <= n;++ i)
		printf("%d ", last[i]);
	return 0;
}