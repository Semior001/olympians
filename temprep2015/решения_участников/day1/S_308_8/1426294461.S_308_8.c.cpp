//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 5050;

int a[maxn], b[maxn];

int used[maxn], id[maxn];

int L[maxn], R[maxn];

int s[maxn];

long long f[maxn];

vector <int> c[maxn];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++)
		f[i] = (i * 1ll * (i + 1)) / 2;
	int r = 0, it = 0, cnt = 0;
	long long ans = 0, cur = 0;
	int pos, Left;
	for (int i = 1; i <= n; i++)
	{
		memset(id, 0, sizeof(id));
		cnt = 0;
		for (int j = i; j <= n; j++)
		{
			if (!id[a[j]])
			{
				id[a[j]] = ++cnt;
				c[cnt].clear();
			}
			b[j] = id[a[j]];
			c[id[a[j]]].pb(j);
		}
		r = 0;
		for (int j = i; j <= n; j++)
		{
			while (r && b[s[r - 1]] > b[j])
				r--;
			if (!r)
				L[j] = i - 1;
			else
				L[j] = s[r - 1];
			s[r++] = j;
		}
		r = 0;
		for (int j = n; j >= i; j--)
		{
			while (r && b[s[r - 1]] > b[j])
				r--;
			if (!r)
				R[j] = n + 1;
			else
				R[j] = s[r - 1];
			s[r++] = j;
		}
//		for (int j = i; j <= n; j++)
//			cout << j << " " << L[j] << " " << R[j] << endl;
//		cout << endl;	
		it++;
		cur = f[n - i + 1];
		for (int j = i; j <= n; j++)
		{
			if (used[b[j]] != it)
			{
				used[b[j]] = it;
				for (int l = 0; l < (int)c[b[j]].size(); l++)
				{                          
					pos = c[b[j]][l];
					if (L[pos] < i || b[L[pos]] != b[pos])
						Left = L[pos];
					cur += f[pos - L[pos] - 1];
					if (R[pos] > n || b[R[pos]] != b[pos])
					{
						cur -= f[R[pos] - Left - 1];
						cur += f[R[pos] - pos - 1];
					}	
				}
			}
			ans += cur;
		}						
	}										
	cout << ans;
	return 0;
}