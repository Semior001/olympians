//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "B"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 555;

vector <int> Coffee[maxn];

vector <int> Cake[maxn];

int Coffees[maxn];

int Cakes[maxn];	

int n, m, k;

int ans;

void rec(int x, int cur = 0)
{
	if (x == n + 1)
	{
		ans = cur;
		return ;
	}
	if (cur + n - x > ans)	
		rec(x + 1, cur);
	for (int i = 0; i < (int)Coffee[x].size(); i++)
	{
		if (Coffees[Coffee[x][i]])
		{
			for (int j = 0; j < (int)Cake[x].size(); j++)
			{
				if (Cakes[Cake[x][j]])
				{
					Cakes[Cake[x][j]]--;
					Coffees[Coffee[x][i]]--;
					rec(x + 1, cur + 1);
					Cakes[Cake[x][j]]++;
					Coffees[Coffee[x][i]]++;
				}
			}
		}
	}
}
	
int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	cin >> n >> m >> k;
	for (int i = 1; i <= m; i++)
		scanf("%d", &Coffees[i]);
	for (int i = 1; i <= k; i++)
		scanf("%d", &Cakes[i]);
	int len, x;
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &len);
		for (int j = 1; j <= len; j++)
		{
			scanf("%d", &x);
			Coffee[i].pb(x);
		}
	}
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &len);
		for (int j = 1; j <= len; j++)
		{
			scanf("%d", &x);
			Cake[i].pb(x);
		}
	}
	rec(1);
	cout << ans;
	return 0;
}