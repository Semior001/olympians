//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "B"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 2020;

struct edge
{
	int a, b, cap, flow;
};

vector <edge> e;

vector <int> g[maxn];

void add_edge(int a, int b, int cap)
{
	edge e1 = {a, b, cap, 0};
	g[a].pb((int)e.size());
	e.pb(e1);
	edge e2 = {b, a, 0, 0};
	g[b].pb((int)e.size());
	e.pb(e2);
}

int fn;	

queue <int> q;

int d[maxn];

int used[maxn];

bool bfs()
{
	memset(d, -1, sizeof(d));
	d[0] = 0;
	q.push(0);
	int v, id, to;
	while (!q.empty())
	{
		v = q.front();
		q.pop();
		for (int i = 0; i < (int)g[v].size(); i++)
		{
			id = g[v][i], to = e[id].b;
			if (e[id].cap > e[id].flow && d[to] == -1)
			{
				d[to] = d[v] + 1;
				q.push(to);
			}
		}
	}
	return (d[fn] != -1);
}					

int ptr[maxn];

int dfs(int v, int flow = inf)
{
	if (!flow || v == fn)
		return flow;
	for (; ptr[v] < (int)g[v].size(); ptr[v]++)
	{
		int id = g[v][ptr[v]], to = e[id].b;
		if (d[to] == d[v] + 1)
		{
			int val = dfs(to, min(flow, e[id].cap - e[id].flow));
			if (val)
			{
				e[id].flow += val;
				e[id ^ 1].flow -= val;
				return val;
			}
		}
	}
	return 0;
}	

int Dinic()
{
	int ans = 0;
	while (bfs())
	{
		memset(ptr, 0, sizeof(ptr));
		while (1)
		{
			int val = dfs(0);
			if (!val)
				break;
			ans += val;
		}
	}
	return ans;
}	

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n, m, k;
	cin >> n >> m >> k;
	fn = n + n + m + k + 1;
	int x;
	for (int i = 1; i <= m; i++)
	{
		scanf("%d", &x);
		add_edge(0, i, x);
	}
	for (int i = 1; i <= k; i++)
	{
		scanf("%d", &x);
		add_edge(n + n + m + i, fn, x);
	}
	int len;
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &len);
		for (int j = 1; j <= len; j++)
		{
			scanf("%d", &x);
			add_edge(x, i + m, 1);
		}
	}
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &len);
		for (int j = 1; j <= len; j++)
		{
			scanf("%d", &x);
			add_edge(i + m + n, x + n + n + m, 1);
		}
	}	
	for (int i = 1; i <= n; i++)
		add_edge(i + m, i + m + n, 1);			
	cout << Dinic();	
	return 0;
}