//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "A"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 2020;

vector <int> a[maxn];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n, m;
	cin >> n >> m;
	int c, l, r;
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &l, &r, &c);
		if (!c)
			for (int j = l; j <= r; j++)
				a[j].pop_back();
		else
			for (int j = l; j <= r; j++)
				a[j].pb(c);
	}
	for (int i = 1; i <= n; i++)
	{
		if (!(int)a[i].size())
			printf("%d ", 0);
	    else
	    	printf("%d ", a[i][(int)a[i].size() - 1]);
	}	
	return 0;
}