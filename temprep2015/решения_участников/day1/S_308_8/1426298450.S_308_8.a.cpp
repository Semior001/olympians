//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "A"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 101010;

const int N = (1 << 17);

struct node
{
	int mx, id, add;
	node()
	{
		mx = add = 0;
	}
}t[2 * N + 5];	

struct Query
{
	int l, r, c;
}q[maxn];

void push(int v, int tl, int tr)
{
	if (!t[v].add)
		return ;
	t[v].mx += t[v].add;
	if (tl != tr)
	{
		t[v + v].add += t[v].add;
		t[v + v + 1].add += t[v].add;
	}
	t[v].add = 0;
}

void upd(int l, int r, int val, int v = 1, int tl = 0, int tr = N - 1)
{
	push(v, tl, tr);
	if (l > r || tl > r || l > tr)
		return ;
	if (l <= tl && tr <= r)
	{
		t[v].add = val;
		push(v, tl, tr);
		return ;
	}
	int mid = (tl + tr) >> 1;
	upd(l, r, val, v + v, tl, mid);
	upd(l, r, val, v + v + 1, mid + 1, tr);
	if (t[v + v].mx >= t[v + v + 1].mx)	
	{
		t[v].mx = t[v + v].mx;
		t[v].id = t[v + v].id;
	}
	else
	{
		t[v].mx = t[v + v + 1].mx;
		t[v].id = t[v + v + 1].id;
	}
}	

pair <int, int> get(int l, int r, int v = 1, int tl = 0, int tr = N - 1)
{
	if (l > tr || tl > r || l > r)
		return mp(-inf, 0);
	if (l <= tl && tr <= r)
		return mp(t[v].mx, t[v].id);
	int mid = (tl + tr) >> 1;
	pair <int, int> p1 = get(l, r, v + v, tl, mid);
	pair <int, int> p2 = get(l, r, v + v + 1, mid + 1, tr);
	if (p1.F >= p2.F)
		return p1;
	else
		return p2;	
}	

int ans[maxn];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= m; i++)
		scanf("%d%d%d", &q[i].l, &q[i].r, &q[i].c);
	for (int i = N; i < N + N; i++)
	{
		t[i].mx = -inf;
		t[i].add = 0;
		t[i].id = i - N;
	}
	for (int i = 1; i <= n; i++)
		t[N + i].mx = 0;
	for (int i = N - 1; i >= 1; i--)
	{
		t[i].add = 0;
		if (t[i + i].mx >= t[i + i + 1].mx)
		{
			t[i].mx = t[i + i].mx;
			t[i].id = t[i + i].id;
		}
		else
		{
			t[i].mx = t[i + i + 1].mx;
			t[i].id = t[i + i + 1].id;
		}
	}
	pair <int, int> cur;
	for (int i = m; i >= 1; i--)
	{
		if (q[i].c)
			upd(q[i].l, q[i].r, 1);
		else
			upd(q[i].l, q[i].r, -1);
		while (1)
		{
			cur = get(1, n);
			if (cur.F <= 0)
				break;
			ans[cur.S] = q[i].c;
			upd(cur.S, cur.S, -inf);
		}
	}
	for (int i = 1; i <= n; i++)
		printf("%d ", ans[i]);					
	return 0;
}