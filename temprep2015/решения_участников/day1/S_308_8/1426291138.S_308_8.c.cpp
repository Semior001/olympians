//Solution by Maminov Daniyar
#include <iostream>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define inf 2000000000
#define INF 2000000000000000000ll
using namespace std;

typedef unsigned long long ull;

const int maxn = 5555;

int a[maxn];

int used[maxn];

int main()
{
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	int it = 0;
	long long ans = 0;
	for (int l = 1; l <= n; l++)
	{
		it++;
		for (int r = l; r <= n; r++)
		{
			used[a[r]] = it;
			int r2 = 0;
			for (int l2 = r + 1; l2 <= n; l2++)
			{
				r2 = max(r2, l2);
				while (r2 <= n && used[a[r2]] != it)
					r2++;
				ans += r2 - l2;
			}
		}
	}
	cout << ans;						
	return 0;
}