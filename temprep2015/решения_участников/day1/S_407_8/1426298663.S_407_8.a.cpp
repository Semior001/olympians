#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#define ll long long
#define mp make_pair
#define pb push_back
#define s second
#define f first
#define fname "A."

using namespace std;

const int maxn = int(1e5) + 2;
int n, m, l, r, x;
// deque <int> add[maxn], t[maxn];
int addl[4*maxn], addr[4*maxn], tleft[4*maxn], tright[4*maxn];
map <int, int> add[4*maxn], t[4*maxn];
                   
void push(int v) {
 	 	for (int i = addl[v]; i <= addr[v]; i++) {
 	 	 	int k = add[v][i];
 	 	 	addl[v]++;
 	 	 	if (k == -1) {
 	 	 		tright[v+v]--;
 	 	 		tright[v+v+1]--;
 	 	 	 	add[v+v][++addr[v+v]] = -1;
 	 	 	 	add[v+v+1][++addr[v+v+1]] = -1;
            }
            else {
 	 	 		t[v+v][++tright[v+v]] = k;
 	 	 		t[v+v+1][++tright[v+v+1]] = k;
 	 	 		add[v+v][++addr[v+v]] = k;
 	 	 		add[v+v+1][++addr[v+v+1]] = k;
 	 	 	}
 	 	}
}

void update(int v, int tl, int tr, int l, int r, int x) {
 	if (r < tl || tr < l) return;
 	if (l <= tl && tr <= r) {
 		if (x == 0) {
 	 		tright[v]--;
 	 		add[v][++addr[v]] = -1;
 	 	}
 	 	else {
 	 		t[v][++tright[v]] = x;
 	 		add[v][++addr[v]] = x;
 	 	}
  	}
 	else {
 	 	push(v);
 	 	int tm = (tl+tr) >> 1;
 	 	update(v+v, tl, tm, l, r, x);
 	 	update(v+v+1, tm+1, tr, l, r, x);
 	}
}
int get(int v, int tl, int tr, int pos) {
 	if (tl == tr) {
 	  	if (tleft[v] > tright[v])  return 0;
 	  	else return t[v][tright[v]]; 
  	}
  	int tm = (tl+tr) >> 1;
  	push(v);
  	if (pos <= tm) 
  		return get(v+v, tl, tm, pos);
 	else
 		return get(v+v+1, tm+1, tr, pos);	
}
        


int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d", &n, &m);
/*	if (n <= 1) {
		for (int i = 1; i <= m; i++) {
		 	scanf("%d%d%d", &l, &r, &x);
		 	if (x == 0) {
		 	 	for (int j = l; j <= r; j++)
		 	 		t[j].pop_back();
		 	}
			else {
		 	    for (int j = l; j <= r; j++)
		 	   		t[j].push_back(x);
		 	}    
		}
		for (int i = 1; i <= n; i++) {
			if (t[i].empty())
				printf("%d ", 0);
			else
				printf("%d ", t[i].back());
	 	}
	}   */
//	else {
	 	for (int i = 1; i <= m; i++) {
	 	 	scanf("%d%d%d", &l, &r, &x);
	 	 	update(1, 1, n, l, r, x);
	 	}
	 	for (int i = 1; i <= n; i++) {
	 	 	printf("%d ", get(1, 1, n, i));
	 	}
//	}      
	return 0;
}
