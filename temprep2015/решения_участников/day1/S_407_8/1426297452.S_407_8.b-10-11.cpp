#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define fname "B."

using namespace std;

short n, m, k, ans, pnum[502][502], cnum[502][502], p[502], c[502], pcnt[502], ccnt[502];;
set <short> clike[502], plike[502];
set <pair <short, short> > cq, pq; 
map <pair <short, short>, short> edge;
map <pair <short, short>, bool> used[502];
                    



int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	for (short i = 1; i <= m; i++) {
		scanf("%d", &c[i]);	
  	}
	for (short i = 1; i <= k; i++) {
		scanf("%d", &p[i]);
  	}
  	for (short i = 1; i <= m; i++)
  	for (short j = 1; j <= k; j++)
  		edge[mp(i, j)] = min(c[i], p[i]);
  	for (short i = 1; i <= n; i++) {
  	 	scanf("%d", &ccnt[i]);
  	 	for (short j = 1; j <= ccnt[i]; j++) 
  	 		scanf("%d", &cnum[i][j]);
  	}
  	for (short i = 1; i <= n; i++) {
  	 	scanf("%d", &pcnt[i]);
  	 	for (short j = 1; j <= pcnt[i]; j++) 
  	 	 	scanf("%d", &pnum[i][j]);
  	}
  	for (short i = 1; i <= n; i++) 
  	 	for (short j = 1; j <= ccnt[i]; j++)
  	 		for (short r = 1; r <= pcnt[i]; r++) {
  	 			edge[mp(cnum[i][j], pnum[i][j])]--;
  	 			used[i][mp(cnum[i][j], pnum[i][j])] = true;
  	 	  	}
  	for (short i = 1; i <= n; i++) {
  		short mx = -11111, ind1 = -1, ind2 = -1;
		for (short j = 1; j <= m; j++)
			for (short z = 1; z <= k; z++) {
			 	if (used[i][mp(j, z)]) {
			 	 	if (edge[mp(j, z)] > mx) {
			 	 	 	mx = edge[mp(j, z)];
			 	 	 	ind1 = j, ind2 = z;
                    }
			 	}
			}	
	 	if (mx != -11111) ans++;
  	}
  	cout << ans;
  	return 0;
}
