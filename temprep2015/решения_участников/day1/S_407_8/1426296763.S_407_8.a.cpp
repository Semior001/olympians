#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <deque>
#define ll long long
#define mp make_pair
#define pb push_back
#define s second
#define f first
#define fname "A."

using namespace std;

const int maxn = int(1e5) + 7;
int n, m, l, r, x;
stack <int> st[maxn];
deque <int> add[4*maxn], t[4*maxn];
               
void push(int v) {
 	 	while (!add[v].empty()) {
 	 	 	int k = add[v].front();
 	 	 	add[v].pop_front();
 	 	 	if (k == -1) {
 	 	 	 	t[v+v].pop_back();
 	 	 	 	t[v+v+1].pop_back();
 	 	 	 	add[v+v].pb(-1);
 	 	 	 	add[v+v+1].pb(-1);
            }
            else {
 	 	 		t[v+v].pb(k);
 	 	 		t[v+v+1].pb(k);
 	 	 		add[v+v].pb(k);
 	 	 		add[v+v+1].pb(k);
 	 	 	}
 	 	}
}

void update(int v, int tl, int tr, int l, int r, int x) {
 	if (l > r) return;
 	if (tl == l && tr == r) {
 		if (x == 0) {
 	 		t[v].pop_back();
 	 		add[v].pb(-1);
 	 	}
 	 	else {
 	 		t[v].pb(x);
 	 		add[v].pb(x);
 	 	}
  	}
 	else {
 	 	push(v);
 	 	int tm = (tl+tr) >> 1;
 	 	update(v+v, tl, tm, l, min(tm, r), x);
 	 	update(v+v+1, tm+1, tr, max(tm+1, l), r, x);
 	}
}
int get(int v, int tl, int tr, int pos) {
 	if (tl == tr) {
 	  	if (t[v].empty())  return 0;
 	  	else return t[v].back(); 
  	}
  	int tm = (tl+tr) >> 1;
  	push(v);
  	if (pos <= tm) 
  		return get(v+v, tl, tm, pos);
 	else
 		return get(v+v+1, tm+1, tr, pos);	
}
        


int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n <= 1) {
		for (int i = 1; i <= m; i++) {
		 	scanf("%d%d%d", &l, &r, &x);
		 	if (x == 0) {
		 	 	for (int j = l; j <= r; j++)
		 	 		st[j].pop();
		 	}
			else {
		 	    for (int j = l; j <= r; j++)
		 	   		st[j].push(x);
		 	}    
		}
		for (int i = 1; i <= n; i++) {
			if (st[i].empty())
				printf("%d ", 0);
			else
				printf("%d ", st[i].top());
	 	}
	}
	else {
	 	for (int i = 1; i <= n; i++) {
	 	 	scanf("%d%d%d", &l, &r, &x);
	 	 	update(1, 1, n, l, r, x);
	 	}
	 	for (int i = 1; i <= n; i++) {
	 	 	printf("%d ", get(1, 1, n, i));
	 	}
	}      
	return 0;
}
