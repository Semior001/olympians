#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define fname "C."

using namespace std;

int a[5002], n;
ll ans;
ll d[5002][5002], c[5002][5002];
int sum[5002];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		sum[i] = sum[i-1] + i;


	for (int i = 1; i <= n; i++) 
		scanf("%d", &a[i]);	
  	if (n <= 500) {
  		for (int len = 1; len <= n; len++) {
  		 	for (int i = 1; i + len - 1 <= n; i++)  {
  		 	 	int last = i + len - 1;
  		 	 	set <int> q;
  		 	 	for (int k = i; k <= last; k++)
  		 	 		q.insert(a[k]);
  		 	 	for (int j = i + len; j <= n; j++) 
  		 	 		if (q.count(a[j])) {
  		 	 		  	ans += sum[j - last - 1];
  		 	 		  	last = j;
  		 	 		}
  				ans += sum[n-last];                                   
  	   		}
  		}
  	}
  	cout << ans;
	return 0;
}