#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#define ll long long
#define mp make_pair
#define s second
#define f first
#define fname "B."

using namespace std;

int n, m, k, ans, pnum[502][502], cnum[502][502], p[502], c[502], pcnt[502], ccnt[502];;
set <int> clike[502], plike[502];
map <pair <int, int>, int> edge, cnt;
map <pair <int, int>, bool> used[502];
                    



int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= m; i++) {
		scanf("%d", &c[i]);	
  	}
	for (int i = 1; i <= k; i++) {
		scanf("%d", &p[i]);
  	}
  	for (int i = 1; i <= m; i++)
  	for (int j = 1; j <= k; j++)
  		edge[mp(i, j)] = cnt[mp(i, j)] = min(c[i], p[i]);
  	for (int i = 1; i <= n; i++) {
  	 	scanf("%d", &ccnt[i]);
  	 	for (int j = 1; j <= ccnt[i]; j++) 
  	 		scanf("%d", &cnum[i][j]);
  	}
  	for (int i = 1; i <= n; i++) {
  	 	scanf("%d", &pcnt[i]);
  	 	for (int j = 1; j <= pcnt[i]; j++) 
  	 	 	scanf("%d", &pnum[i][j]);
  	}
  	for (int i = 1; i <= n; i++) 
  	 	for (int j = 1; j <= ccnt[i]; j++)
  	 		for (int r = 1; r <= pcnt[i]; r++) {
  	 			edge[mp(cnum[i][j], pnum[i][r])]--;
  	 			used[i][mp(cnum[i][j], pnum[i][r])] = true;
  	 	  	}
  	for (int i = 1; i <= n; i++) {
  		int mx = -11111, ind1 = -1, ind2 = -1;
		for (int j = 1; j <= m; j++)
			for (int z = 1; z <= k; z++) {
			 	if (used[i][mp(j, z)]) {
			 	 	if (edge[mp(j, z)] > mx && cnt[mp(j, z)]) {
			 	 	 	mx = edge[mp(j, z)];
			 	 	 	ind1 = j, ind2 = z;
                    }
			 	}
			}	
	 	if (mx != -11111) {
	 		ans++;
	 		cnt[mp(ind1, ind2)]--;
	 		cnt[mp(ind2, ind1)]--;
	  	}
  	}
  	cout << ans;
  	return 0;
}
