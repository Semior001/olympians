#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

pair <int, int> g[MXN];
int k, ans = INF, sz;
string st;

int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> st;
    int len = st.size();
    g[0] = mp(0, 0);
    for(int i = 0; i < len; ++i)
        if(st[i] != st[i + 1])
            g[++sz] = mp((g[sz - 1].s + 1), (i + 1));

    cin >> k;
    if(sz < k) cout << -1, exit(0);
    for(int i = 1; i <= sz; ++i) {
        int j = i + k - 1;
        if(sz < j) break;
        int mn = abs(g[j].f - g[i].s) + 1;
        ans = min(mn, ans);
    }
    cout << ans;
    return 0;
}
