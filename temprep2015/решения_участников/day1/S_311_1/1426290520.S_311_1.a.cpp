#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
#define fn "A"
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

int n, m;
vector <int> a[MXN];

int main() {
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
    cin >> n >> m;
    for(int i = 1; i <= m; ++i) {
        int l, r, c;
        cin >> l >> r >> c;
        if(c) for(int j = l; j <= r; ++j) a[j].pb(c);
        else for(int j = l; j <= r; ++j) a[j].popb();
    }
    for(int i = 1; i <= n; ++i) {
        int sz = a[i].size();
        if(sz) cout << a[i][sz - 1];
        else cout << 0;
        cout << ' ';
    }
    return 0;
}
