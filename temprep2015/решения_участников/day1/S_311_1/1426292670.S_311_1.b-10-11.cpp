#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

pair <int, int> g[MXN];
int k, ans = INF, sz;
string s;

int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> s;
    int len = s.size();
    g[0] = mp(0, 0);
    for(int i = 0; i < len; ++i)
        if(s[i] != s[i + 1])
            g[++sz] = mp((g[sz - 1].s + 1), (i + 1));

    cin >> k;
    k --;
    for(int i = 1; i <= sz; ++i) {
        if(sz < i + k) break;
        int mn = g[i + k].f - g[i].s + 1;
        ans = min(mn, ans);
    }
    if(ans == INF) cout << -1;
    else cout << ans;
    return 0;
}
