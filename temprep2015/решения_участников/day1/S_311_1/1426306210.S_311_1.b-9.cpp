#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>

#define llong long long
#define popb pop_back
#define pb push_back
#define mp make_pair
#define f first
#define s second
using namespace std;

const double EPS = 1e-9;
const int INF = 1e9 + 7;
const int MXN = 1e6 + 1;
const int N = 1e3 + 1;

pair <int, int> g[MXN];
int k, ans = INF, sz;
int u[MXN], e[MXN];
string st;
set <char> check;

int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> st;
    int len = st.size();
    g[0] = mp(0, 0);
    for(int i = 0; i < len; ++i) {
        if(st[i] != st[i + 1])
            g[++sz] = mp((g[sz - 1].s + 1), (i + 1)), e[sz] = (st[i] - 'a' + 1);
        check.insert(st[i]);
    }
    g[++sz] = mp((g[sz - 1].s + 1), INF);
    sz--;
    cin >> k;
    if(k == 1 || k == 2) cout << k, exit(0);
    if(check.size() < k) cout << -1, exit(0);
    int change = k;
    for(int i = 1; i <= sz; ++i) {
        k = change + i - 1;
        if(check.size() < k && sz < k) break;
        for(int j = i; j <= k; ++j) {
            if(u[e[j]]) k = ((k + 1 <= sz) ? k + 1 : sz + 1);
            else u[e[j]] = 1;
        }
        for(int j = 1; j <= 26; ++j) u[e[j]] = 0;
        int mn = abs(g[k].f - g[i].s) + 1;
        ans = min(mn, ans);
    }
    if(ans == INF) cout << -1;
    else cout << ans;
    return 0;
}
