#include<iostream>
#include<algorithm>
#include<fstream>
#include<vector>
using namespace std;
typedef int ll;
pair<ll,ll> v[501],t[501];
ll cof[501],pie[501],n,m,k,c[501],p[501],x[501],y[501],a[501][2001],b[501][2001],w[501],ans;
int main() {
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    cin>>n>>m>>k;
    for(ll i=1;i<=m;i++)
        cin>>c[i];
    for(ll i=1;i<=k;i++)
        cin>>p[i];
    for(ll i=1;i<=n;i++)
    {
        cin>>x[i];
        for(ll j=1;j<=x[i];j++)
            cin>>a[i][j],cof[a[i][j]]++;
        v[i].first=x[i],v[i].second=i;
    }
    for(ll i=1;i<=n;i++)
    {
        cin>>y[i];
        for(ll j=1;j<=y[i];j++)
            cin>>b[i][j],pie[b[i][j]]++;
        t[i].first=x[i],t[i].second=i;
    }
    sort(t+1,t+n+1);
    sort(v+1,v+n+1);
    for(ll i=1;i<=n;i++)
    {
        ll minn=10000,pos=-1;
        for(ll j=1;j<=v[i].first;j++)
            if(c[a[v[i].second][j]] && minn>cof[a[v[i].second][j]])
            {
                minn=cof[a[v[i].second][j]];
                pos=a[v[i].second][j];
            }
        if(pos!=-1)
        {
            w[v[i].second]=1;
            c[pos]--;
            for(ll j=1;j<=v[i].first;j++)
                cof[a[v[i].second][j]]--;
        }
    }
    for(ll i=1;i<=n;i++)
    {
        ll minn=10000,pos=-1;
        for(ll j=1;j<=t[i].first;j++)
            if(p[b[t[i].second][j]] && minn>pie[b[t[i].second][j]])
            {
                minn=pie[b[t[i].second][j]];
                pos=b[t[i].second][j];
            }
        if(pos!=-1)
        {
            p[pos]--;
            for(ll j=1;j<=t[i].first;j++)
                pie[b[t[i].second][j]]--;
        }
        else
            w[t[i].second]=0;
    }
    for(ll i=1;i<=n;i++)
        if(w[i])
            ans++;
    cout<<ans;
}
