#include<cstdio>
#include<algorithm>
#include<fstream>
using namespace std;
typedef int ll;
ll cof[501],pie[501],n,m,k,c[501],p[501],x[501],y[501],a[501][2001],b[501][2001],w[501],ans;
int main() {
    //freopen("B.in","r",stdin);
    //freopen("B.out","w",stdout);
    scanf("%d%d%d",&n,&m,&k);
    for(ll i=1;i<=m;i++)
        scanf("%d",&c[i]);
    for(ll i=1;i<=k;i++)
        scanf("%d",&p[i]);
    for(ll i=1;i<=n;i++)
    {
        scanf("%d",&x[i]);
        for(ll j=1;j<=x[i];j++)
            scanf("%d",&a[i][j]),cof[a[i][j]]++;
    }
    for(ll i=1;i<=n;i++)
    {
        scanf("%d",&y[i]);
        for(ll j=1;j<=y[i];j++)
            scanf("%d",&b[i][j]),pie[b[i][j]]++;
    }
    for(ll i=1;i<=n;i++)
    {
        ll mincof=10000,poscof=-1;
        for(ll j=1;j<=x[i];j++)
            if(c[a[i][j]] && mincof>cof[a[i][j]])
            {
                mincof=cof[a[i][j]];
                poscof=a[i][j];
            }
        if(poscof>0)
        {
            w[i]=1;
            c[poscof]--;
            cof[poscof]--;
        }
    }
    for(ll i=1;i<=n;i++)
    {
        ll minpie=10000,pospie=-1;
        for(ll j=1;j<=y[i];j++)
            if(p[b[i][j]] && minpie>pie[b[i][j]])
            {
                minpie=pie[b[i][j]];
                pospie=b[i][j];
            }
        if(pospie>0)
        {
            p[pospie]--;
            pie[pospie]--;
        }
        else
            w[i]=0;
    }
    for(ll i=1;i<=n;i++)
        if(w[i])
            ans++;
    printf("%d",ans);
}
