#include<cstdio>
#include<fstream>
#include<vector>
using namespace std;
typedef long long ll;
ll n,m,l,r,c;
vector<ll> a[100001];
int main() {
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    scanf("%lld%lld",&n,&m);
    for(ll i=1;i<=m;i++)
    {
        scanf("%lld%lld%lld",&l,&r,&c);
        for(ll j=l;j<=r;j++)
            if(c)
                a[j].push_back(c);
            else
                a[j].pop_back();
    }
    for(ll i=1;i<=n;i++)
        if(!a[i].empty())
            printf("%lld ",a[i].back());
        else
            printf("0 ");
}
