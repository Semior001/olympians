#include<iostream>
#include<fstream>
#include<cmath>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<iomanip>
#include<set>
#include<stack>
#include<queue>
using namespace std;
int a[5011],sum[5011];
bool used[5011];
int sz=0;
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout); 
	int n,ans=0;
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i];
		sum[i]=sum[i-1]+i;
	}
	for(int i=1;i<n;i++){
		for(int j=i;j<n;j++){
			sz=0;
			used[a[j]]=1;
			for(int k=j+1;k<=n;k++){
				if(used[a[k]]==1){
					ans+=sum[sz];
					sz=0;
					continue;
				}
				sz++;
			}
			ans+=sum[sz]; 
		}
		for(int j=i;j<=n;j++){
			used[j]=0;
		}
	}            
	cout<<ans;
	return 0;
}