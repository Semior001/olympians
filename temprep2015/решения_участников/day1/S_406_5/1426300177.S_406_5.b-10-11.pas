program B;

const max = 500;

type 	mass = array[1..max] of integer;
	matrix = array[1..max,1..2,1..max] of integer;

var 	n,m,k: integer;
	c, p: mass;
	stud: matrix;
	f: text;

procedure readarr(var a: mass; x: integer);
var i: integer;
begin
        for i:=1 to x do
                read(f, a[i]);
end;

procedure readmx(var a: matrix);
var i, x, j: integer;
begin
        for i:=1 to n do
		begin
			read(f, x);
			for j:=1 to x do
				read(f, a[i,1,j]);
		end;
	for i:=1 to n do
		begin
			read(f, x);
			for j:=1 to x do
				read(f, a[i,2,j]);
		end;
end;



function Counter: integer;
var i, j, fx, count: integer;
begin
	count:=0;	
	for i:=1 to n do
		begin	
			fx:=0;
			for j:=1 to k do
				begin
					if (C[(stud[i,1,j])]>=1) then
						begin
							C[(stud[i,1,j])]:=C[(stud[i,1,j])]-1;
							fx:=fx+1;
							break;
						end;
				end;
			for j:=1 to k do
				begin
					if (P[(stud[i,2,j])]>=1) then
						begin
							P[(stud[i,2,j])]:=P[(stud[i,2,j])]-1;
							fx:=fx+1;
							break;
						end;
				end;
			if fx = 2 then
				count:=count+1;
		end;
	Counter:=count;
end;

begin
	assign(f, 'B.in');
	reset(f);
	readln(f, n, m, k);
	readarr(c,m);
	readarr(p,k);
	readmx(stud);
	close(f);
	
	assign(f,'B.out');
	rewrite(f);
	writeln(f,Counter);
	close(f);
end.
