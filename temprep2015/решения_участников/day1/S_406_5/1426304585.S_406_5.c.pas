program C;

const max = 5000;

type arr = array[1..max] of integer;

var 	nums: arr;
	n: integer;
	f:text;

procedure readarr(var a: arr);
var i: integer;
begin
        for i:=1 to n do
                read(f, a[i]);
end;

function Check(var a: arr):integer;
var	i,j,fx,ch,x:integer;
begin
	fx:=0;
	ch:=0;
	if n = 1 then
		ch:=0
	else
		for i:=1 to n-1 do
			for j:=i+1 to n do
				if a[i]=a[j] then
					fx:=fx+1;
		if fx = 0 then
			begin
				if n = 2 then
					ch:=1
				else
					begin
						if n = 3 then
							ch:=5
						else
							if n mod 2 = 0 then
								ch:=n*(n-1)
							else
								ch:=n*(n+1)
					end					
			end			
		else
			begin	
				x:=0;
				for i:=1 to n-1 do
					for j:=i+1 to n do
						if (a[i]>a[j]) then
							if (a[i]>x) then x:=a[i]
						else
							if (a[j]>x) then x:=a[j];
				if X mod 2 = 0 then
					ch:=sqr(x)+1
				else
					ch:=sqr(x)
			end;
			
	Check:=ch;
end;

begin
	assign(f, 'C.in');
	reset(f);
	readln(f, n);	
        readarr(nums);
	close(f);	

	assign(f,'C.out');
	rewrite(f);
	writeln(f, Check(nums));	
	close(f);
end.
