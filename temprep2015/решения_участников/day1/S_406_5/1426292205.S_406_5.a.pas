program A;

type 	mass = array[1..100000, 1..3] of longint;
	arr = array[1..100000] of longint;

var 	n,m: longint;
	oper: mass;
	f: text;

procedure readarr(var a: mass);
var i: longint;
begin
        for i:=1 to m do
                readln(f, a[i,1], a[i,2], a[i,3]);
end;

procedure Robot(var a:mass);
var 	i, j: longint;
	c: arr;
begin
	for i:=1 to m do
		begin
			if a[i,3] = 0 then
				begin
					if (i-1)<>1 then
						for j:=a[i-1,1] to a[i-1,2] do
							c[j]:=a[i-1,3]
					else
						for j:=a[i,1] to a[i,2] do
							c[j]:=0;
				end
			else
				begin
					for j:=a[i,1] to a[i,2] do
						c[j]:=a[i,3];			
				end;
		end;
	for i:=1 to n do
		write(f,c[i],' ');	
end;

begin
	assign(f, 'array.in');
	reset(f);
	readln(f, n, m);
	if (n>=1) and (m>=1) then
		readarr(oper);
	close(f);

	assign(f,'array.out');
	rewrite(f);
	Robot(oper);
        close(f);
end.
