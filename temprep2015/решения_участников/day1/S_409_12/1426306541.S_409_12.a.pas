program A;
const MaxN = 100000;
type
        TChildUpd = record
                op : integer;
                c : longint;
        end;
        TNode= record
                cnts : array of longint;
                ChildUpd : TChildUpd;
        end;
var
        f1, f2 : Text;
        N, M , L, R, C : longint;
        d : array[1..2*MaxN] of Tnode;
        i : longint;

procedure InsertInto(i, c : longint);
begin
        setlength(d[i].cnts, length(d[i].cnts) + 1);
        d[i].cnts[length(d[i].cnts)-1] := c;
end;

procedure RemoveFrom(i:longint);
begin
        if length(d[i].cnts <> 0) then setlength(d[i].cnts, length(d[i].cnts) -1);
end;
procedure UpdChild(i : longint);
begin
        if d[i].childupd.op = 1 then
        begin
                InsertInto(2*i + 1, d[i].childupd.c);
                InsertInto(2*i + 2, d[i].childupd.c);
        end;
        if d[i].childupd.op = 0 then
        begin
                RemoveFrom(2*i + 1);
                RemoveFrom(2*i + 2);
        end;
        d[2*i + 1].childupd.op := d[i].childupd.op;
        d[2*i + 2].childupd.op := d[i].childupd.op;
        d[2*i + 1].childupd.c := d[i].childupd.c;
        d[2*i + 2].childupd.c := d[i].childupd.c;
        d[i].childupd.op := -1;
        d[i].childupd.c := -1;
end;

procedure Update(i, l, r, a, b, c, op : longint);
var m : longint;
begin
        if (l >= a) and (r <= b) then
        begin
                if op = 0 then  RemoveFrom(i) else InsertInto(i, c);
                d[i].childupd.op := op;
                d[i].childupd.c := c;
                UpdChild(i);
                exit;
        end;
        if (r <= a) or (l >= b) then exit;
        m := (l + r) div 2;

end;

function GetValue(i : integer):longint;
begin
        UpdChild(i);
        GetValue := d[i].cnts[length(d[i].cnts)-1];
end;

begin
        assign(f1, 'A.in');
        reset(f1);
        assign(f2, 'A.out');
        rewrite(f2);
        read(f1, N, M);
        for i := 1 to M do
        begin
                read(f1, L, R, C);
                if C = 0 then Update(0, 0, N, L, R, 0, 0)
                        else Update(0,0, N, L, R, C, 1);
        end;
        for i := 1 to N do
                write(f2, GetValue(i));
        close(f2);
end.