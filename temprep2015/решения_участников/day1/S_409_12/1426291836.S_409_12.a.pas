program A;
var
        f1, f2 : Text;
        N, M, L, R, C : longint;
        d: array[1..100000] of array of longint;
begin
        assign(f1, 'A.in');
        reset(f1);
        assign(f2, 'A.out');
        rewrite(f2);
        read(f1, N, M);
        for i := 1 to M do
        begin
                read(f1, L, R, C);
                for i := L to R do
                        if C = 0 then setlength(d[i], length(d[i]) - 1)
                        else begin
                                setlength(d[i], length(d[i]) + 1) ;
                                d[i, length(d[i])-1] := c;
                        end;
        end;
        for i := 1 to N do
                write(f2, d[i, length(d[i]) - 1]);
        close(f2);
end.