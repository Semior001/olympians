program A;
uses math;
var
        f1, f2 : Text;
        N, M, L, R, C, i, j : longint;
        d: array[1..100000] of array of longint;
begin
        assign(f1, 'A.in');
        reset(f1);
        assign(f2, 'A.out');
        rewrite(f2);
        read(f1, N, M);
        for i := 1 to M do
        begin
                read(f1, L, R, C);
                for j := L to R do
                        if C = 0 then setlength(d[j], max(length(d[j]) - 1, 0))
                        else begin
                                setlength(d[j], length(d[j]) + 1);
                                d[j, length(d[j])-1] := c;
                        end;
        end;
        for i := 1 to N do
                if (length(d[i]) <> 0) then write(f2, d[i, length(d[i]) - 1], ' ') else write(f2, 0, ' ');
        close(f2);
end.