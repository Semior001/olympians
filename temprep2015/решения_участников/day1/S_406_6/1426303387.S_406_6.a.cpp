#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "A"
using namespace std;
const int N = 100500;
int n, m, l, r, c;
vector <int> v[N];
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m;i ++)
	{
		cin >> l >> r >> c;
		if (c)
		{
			for (int j = l; j <= r; j ++)
			{
				v[j].pb(c);
			}
		}
		if (!c)
		{
			for (int j = l; j <= r; j ++)
			{
				if (v[j].size())
				{
					v[j].pop_back();
				}
			}
		}
	}
	for (int i = 1; i <= n;i ++)
	{
		if (v[i].size())
		{
			cout << v[i][v[i].size() - 1] << " ";
		}
		else
		{
			cout << 0 << " ";
		}
	}
	return 0;
}