#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "B"
using namespace std;
const int N = 1000;
typedef unsigned int ui;
int n, m, k, c[N], p[N], x, y, ans, mini[3], pos[3], need[4][N], maxi, cc[N], pp[N];
struct az
{
	int x, y, kol, a[N], b[N];
}a[N];
bool cmp(az a, az b)
{
	if (a.kol == b.kol && a.y == b.y)
	{
		return a.x < b.x;
	}
	if (a.kol == b.kol)
	{
		return a.y < b.y;
	}                        
	return a.kol < b.kol;	
}
bool cmp2(az a, az b)
{
	if (a.kol == b.kol && a.x == b.x)
	{
		return a.y < b.y;
	}
	if (a.kol == b.kol)
	{
		return a.x < b.x;
	}                        
	return a.kol < b.kol;	
}

int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m >> k;
	for (int i = 1; i <= m; i ++)
	{
		cin >> c[i];
		cc[i] = c[i];
	}
	for (int i = 1; i <= k; i ++)
	{
		cin >> p[i];
		pp[i] = p[i];
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i].x;
		a[i].kol = a[i].x;
		for (int j = 1; j <= a[i].x; j ++)
		{
			cin >> a[i].a[j];
			need[1][a[i].a[j]] ++;
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i].y;
		a[i].kol += a[i].x;
		for (int j = 1; j <= a[i].y; j ++)
		{
			cin >> a[i].b[j];
			need[1][a[i].b[j]] ++;
		}
	}
	sort(a + 1, a + n + 1, &cmp);
	for (int i = 1; i <= n; i ++)
	{
		mini[1] = mini[2] = 2147483627;
		pos[1] = pos[2] = 0;
		for (int j = 1; j <= a[i].x; j ++)
		{
			if (c[a[i].a[j]] > need[1][a[i].a[j]])
			{
				mini[1] = c[a[i].a[j]];
				pos[1] = j;    
				break;
			}
			if (c[a[i].a[j]] && mini[1] > need[1][a[i].a[j]])
			{
				mini[1] = c[a[i].a[j]];
				pos[1] = j;
			}
		}
		for (int j = 1; j <= a[i].y ; j ++)
		{
			if (p[a[i].b[j]] > need[1][a[i].b[j]])
			{
				mini[1] = p[a[i].b[j]];
				pos[1] = j;
				break;
			}
			if (p[a[i].b[j]] && mini[2] > need[2][a[i].b[j]])
			{
				mini[2] = p[a[i].b[j]];
				pos[2] = j;
			}
		}
		if (!pos[1] || !pos[2])
		{
			continue;
		}
		++ ans;
		--c [pos[1]];
		--p [pos[2]];
	}
	cout << ans;
	return 0;
}