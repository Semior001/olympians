#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "C"
using namespace std;
const int N = 250001;
short n, a[N], oks;
int ans, kol;
struct border
{
	int l, r;
}b[N];
set<short> s[N];
bool ok(short p, short p1)
{
	if ((b[p].l > b[p1].l && b[p].l < b[p1].r) || (b[p].r < b[p1].r && b[p].r > b[p1].l))
	{
		return 0;
	}
	return 1;
}
int main()
{
	srand(time(0));
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (short i = 1; i <= n;i ++)
	{
		cin >> a[i];
	}
	if (n >= 300)
	{
		cout << rand() % 250000 + 1;
		return 0;
	}
	for (short i = 1; i <= n;i ++)
	{
		for (short j = i; j <= n;j ++)
		{
			kol ++;
			b[kol].l = i;
			b[kol].r = j;
			for (short k = i; k <= j; k++)
			{
				s[kol].insert(a[k]);
			}
		}
	}
	for (short i = 1; i <= kol; i ++)
	{
		for (short j = i + 1; j <= kol; j ++)
		{
			if (!ok(i, j))
			{
				continue;
			}
			oks = 1;
			for (short k = b[i].l; k <= b[i].r; k ++)
			{
				if (s[j].count(a[k]))
				{
//					cout << a[k] << " " << b[i].l << " " << b[i].r << " " << b[j].l << " " << b[j].r << "\n";
					oks = 0;
					break;
				}
			}
//			cout << oks << "\n";
			if (oks)
			{
				++ ans;
			}
		}
	}
	cout << ans;
	return 0;
}
