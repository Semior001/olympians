#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "C"
using namespace std;
const int N = 15000;
int n, a[N], kol, ans;
struct border
{
	int l, r;
}b[N];
set<int> s[N];
bool ok(int p, int p1)
{
	if (b[p].l > b[p1].l || b[p].r < b[p1].r)
	{
		return 0;
	}
	return 1;
}
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n;i ++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i; j <= n;j ++)
		{
			kol ++;
			b[kol].l = i;
			b[kol].r = j;
			for (int k = i; k <= j; k++)
			{
				s[kol].insert(a[k]);
			}
		}
	}
	for (int i = 1; i <= kol; i ++)
	{
		for (int j = i + 1; j <= kol; j ++)
		{
			if (!ok(i, j))
			{
				continue;
			}
			for (int k = b[i].l; k <= b[i].r; k ++)
			{
				if (s[j].count(a[k]))
				{
					continue;
				}
				++ ans;
				break;
			}
		}
	}
	cout << ans;
	return 0;
}
