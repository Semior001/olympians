#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "C"
using namespace std;
const int N = 15000;
int n, a[N], kol, ans, oks;
struct border
{
	int l, r;
}b[N];
set<int> s[N];
bool ok(int p, int p1)
{
	if ((b[p].l > b[p1].l && b[p].l < b[p1].r) || (b[p].r < b[p1].r && b[p].r > b[p1].l))
	{
		return 0;
	}
	return 1;
}
int main()
{
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n;i ++)
	{
		cin >> a[i];
	}
	for (int i = 1; i <= n;i ++)
	{
		for (int j = i; j <= n;j ++)
		{
			kol ++;
			b[kol].l = i;
			b[kol].r = j;
			for (int k = i; k <= j; k++)
			{
				s[kol].insert(a[k]);
			}
		}
	}
	for (int i = 1; i <= kol; i ++)
	{
		for (int j = i + 1; j <= kol; j ++)
		{
			if (!ok(i, j))
			{
				continue;
			}
			oks = 1;
			for (int k = b[i].l; k <= b[i].r; k ++)
			{
				if (s[j].count(a[k]))
				{
//					cout << a[k] << " " << b[i].l << " " << b[i].r << " " << b[j].l << " " << b[j].r << "\n";
					oks = 0;
					break;
				}
			}
//			cout << oks << "\n";
			if (oks)
			{
				++ ans;
			}
		}
	}
	cout << ans;
	return 0;
}
