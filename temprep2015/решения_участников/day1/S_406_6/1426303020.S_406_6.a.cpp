#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "A"
using namespace std;
const int N = 1000001;
int n, m, l[N], r[N], c[N];
int main()
{
//	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	scanf("%d%d", &n, &m);
//	cin >> n >> m;
	assert(n * m <= 30000000);
	for (int i = 1; i <= m; i ++)
	{
		scanf("%d%d%d", &l[i], &r[i], &c[i]);
//		cin >> l[i] >> r[i] >> c[i];
	}
	for (int i = 1; i <= n; i ++)
	{
		stack <int> s;
		s.push(0);
		for (int j = 1; j <= m; j ++)
		{
			if (i >= l[j] && i <= r[j])
			{
				if (c[j])
				{
					s.push(c[j]);
				}
				else
				{
					s.pop();
				}
			}
		}
		printf("%d ", s.top());
//		cout << s.top() << " ";
	}
	return 0;
}