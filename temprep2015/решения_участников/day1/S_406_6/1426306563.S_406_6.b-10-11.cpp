#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <cassert>
#include <stdio.h>
#include <map>
#include <ctime>
#include <iomanip>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <cstdio>
#include <string>
#include <cstring>
#define ll long long
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define fname "B"
#define MS0(a) memset(a, 0, sizeof(a))
using namespace std;
const int N = 1000;
typedef unsigned int ui;
int n, m, k, c[N], p[N], x, y, ans, mini[3], pos[3], need[4][N], maxi, ccc[N], ppp[N], f, s, pp[N], cc[N];
struct az
{
	int x, y, kol, a[N], b[N];
}a[N];
bool cmp(az a, az b)
{
	if (a.kol == b.kol & a.x == b.x)
	{
		return a.y < b.y;
	}
	if (a.kol == b.kol)
	{
		return a.x < b.x;
	}                        
	return a.kol < b.kol;	
}
int main()
{
	srand(time(0));
	ios_base::sync_with_stdio(0);cin.tie(NULL);
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m >> k;
	for (int i = 1; i <= m; i ++)
	{
		cin >> c[i];
		ccc[i] = c[i];
	}
	for (int i = 1; i <= k; i ++)
	{
		cin >> p[i];
		ppp[i] = p[i];
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i].x;
		a[i].kol = a[i].x;
		for (int j = 1; j <= a[i].x; j ++)
		{
			cin >> a[i].a[j];
			need[1][a[i].a[j]] ++;
		}
	}
	for (int i = 1; i <= n; i ++)
	{
		cin >> a[i].y;
		a[i].kol += a[i].x;
		for (int j = 1; j <= a[i].y; j ++)
		{
			cin >> a[i].b[j];
			need[2][a[i].b[j]] ++;
		}
	}
	sort(a + 1, a + n + 1, &cmp);
	for (int i = 1; i <= n; i ++)
	{
		mini[1] = mini[2] = 2147483627;
		pos[1] = pos[2] = 0;
		for (int j = 1; j <= a[i].x; j ++)
		{
			if (ccc[a[i].a[j]] && mini[1] > need[1][a[i].a[j]])
			{
				mini[1] = ccc[a[i].a[j]];
				pos[1] = a[i].a[j];
			}
		}
		for (int j = 1; j <= a[i].y ; j ++)
		{
			if (ppp[a[i].b[j]] && mini[2] > need[2][a[i].b[j]])
			{
				mini[2] = ppp[a[i].b[j]];
				pos[2] = a[i].b[j];
			}
		}
		if (!pos[1] || !pos[2])
		{
			continue;
		}
		++ ans;
		-- need[1][pos[1]];
		-- need[2][pos[2]];
		--ccc [pos[1]];
		--ppp [pos[2]];
	}
	maxi = max(maxi, ans);
	ans = 0;
	for (int i = 1; i <= n; i ++)
	{
		ans = 0;
		for (int j = 1; j <= n; j ++)
		{
			f = rand() % a[i].x + 1;
			s = rand() % a[i].y + 1;
			f = a[i].a[f];
			s = a[i].b[s];
			if (cc[f] < c[f] && pp[s] < p[s])
			{
				++ ans;
				++ cc[f];
				++ pp[s];
			}
		}
		maxi = max(maxi, ans);
	}
	cout << maxi;
	return 0;
}