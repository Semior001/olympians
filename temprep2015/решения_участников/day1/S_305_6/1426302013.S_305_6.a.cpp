#include <iostream>
#include <cstdio>
#include <vector>
#include <stack>
#include <algorithm>
#include <deque>
using namespace std;
int main()
{
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    ios_base::sync_with_stdio();
    unsigned int n,m;
    cin>>n>>m;
    vector <unsigned int> s[n];
    for(int i=0;i<m;i++)
    {
        int l,r,c;
        cin>>l>>r>>c;
        if(c!=0)
        {
            for(int j=l-1;j<r;j++)
            {
                s[j].push_back(c);
            }
        }
        else
        {
            for(int j=l-1;j<r;j++)
            {
                s[j].pop_back();
            }
        }
    }
    for(int i=0;i<n;i++)
    {
        if(s[i].empty())cout<<0<<" ";
        else cout<<s[i].back()<<" ";
    }
}
