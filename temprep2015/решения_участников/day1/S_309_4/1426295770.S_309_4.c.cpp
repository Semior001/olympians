#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
using namespace std;

const int N = 5005;

int n, pr[N], a[N], last[N];

ll res;

bool used[N];

int main ()
{
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1;i <= n;i ++)
	{
		scanf ("%d", &a[i]);
		pr[i] = last[a[i]];
		last[a[i]] = i;
	}                  
	for (int i = 1;i <= n;i ++)
	{
		for (int j = i;j <= n;j ++)
			used[a[j]] = 0;
		for (int j = i;j <= n;j ++)
		{
			used[a[j]] = 1;
			for (int i1 = i + 1;i1 <= n;i1 ++)
				for (int j1 = i1;j1 <= n;j1 ++)
					if (used[a[j1]])
						break;
					else
						res ++;
		}
	}
	printf ("%lld", res);
	return 0;	
}