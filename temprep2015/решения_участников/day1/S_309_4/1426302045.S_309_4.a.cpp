#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
using namespace std;

const int N = 1e5 + 5;

int n, m, l, r, c;
vector < int > t[N * 4];
int fl[2][N * 4]; 

void push (int v, int tl, int tr)
{
	if (tl > tr)
		return;
//	cout << "pushing " << v << " " << tl << " " << tr << " " << fl[1][v] << " " << fl[0][v] << endl;
   	if (tl != tr)
   	{
   		fl[0][v + v] += fl[0][v];
   		fl[0][v + v + 1] += fl[0][v];
   		if (fl[1][v])
   		{
//   			cout << "t[v].back () = " << t[v].size () << endl;
   			t[v + v].pb (t[v].back ());
   			t[v + v + 1].pb (t[v].back ());
   			fl[1][v + v] = fl[1][v + v + 1] = 1;
   		}	
   	}
	for (int i = 1;i <= fl[0][v];i ++)
		t[v].pop_back ();
	fl[1][v] = 0;
	fl[0][v] = 0;
}

void update (int l, int r, int c, int v = 1, int tl = 1, int tr = n)
{
	push (v, tl, tr);
	if (tl > tr || tl > r || tr < l)
		return;
//	cout << "upd " << l << " " << r << " " << c << " " << tl << " " << tr << endl;	
	if (l <= tl && tr <= r)
	{
		if (c)
		{
			fl[1][v] ++;
			t[v].pb (c);
		}
		else
			fl[0][v] ++;
		push (v, tl, tr);
		return;
	}
	int tm = (tl + tr) >> 1;
	update (l, r, c, v + v, tl, tm);
	update (l, r, c, v + v + 1, tm + 1, tr);
}

vector < int > res;

void out (int v = 1, int tl = 1, int tr = n)
{
	push (v, tl, tr);
	if (tl > tr)
		return;
//   	cout << "v = " << v << " " << tl << " " << tr << endl;
	if (tl == tr)
	{
		if (t[v].size ())
			printf ("%d ", t[v].back ());
		else
			printf ("%d ", 0);
		return;
	}
	int tm = (tl + tr) >> 1;
	out (v + v, tl, tm);
	out (v + v + 1, tm + 1, tr);
}

int main ()
{
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	scanf ("%d%d", &n, &m);
	for (int i = 1;i <= m;i ++)
	{
		scanf ("%d%d%d", &l, &r, &c);
		update (l, r, c);
	}
//	puts ("--------------------------");
	out ();
	return 0;          
}