#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
using namespace std;

const int N = 5005;

int n, pr[N], a[N], last[N];

ll res, cur;

bool used[N];

int main ()
{
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1;i <= n;i ++)
	{
		scanf ("%d", &a[i]);
		pr[i] = last[a[i]];
		last[a[i]] = i;
	}                  
	for (int i = 1;i <= n;i ++)
	{
		for (int j = i;j <= n;j ++)
			used[a[j]] = 0;
		for (int j = i;j <= n;j ++)
		{
			used[a[j]] = 1;
			cur = 0;
			for (int i1 = j + 1;i1 <= n;i1 ++)
			{
				if (used[a[i1]])
				{
					res = res + (cur * (cur + 1)) / 2;
					cur = 0;
				}
				else
					cur ++;
			}
			res += (cur * (cur + 1)) / 2;
		}
	}
	printf ("%lld", res);
	return 0;	
}