#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <ctime>
#define ll long long
#define pb push_back
using namespace std;

int n, k, cur, mn = 1e9 + 7;

string s;

vector < int > p[30], tmp;

int main ()
{
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;
	for (int i = 0;i < s.size ();i ++)
	{
		int x = s[i] - 'a';
		p[x].pb (i);
	}
	for (int i = 0;i < s.size ();i ++)
	{
		tmp.clear ();
		for (int j = 0;j < 26;j ++)
		{
			if (p[j].size () == 0 || p[j].back () < i)
				continue;
		   	int pos = lower_bound (p[j].begin (), p[j].end (), i) - p[j].begin ();
		   	tmp.pb (p[j][pos]);
		}
		if (tmp.size () >= k)
		{
			sort (tmp.begin (), tmp.end ());
			int last = tmp[k - 1];
			mn = min (mn, last - i + 1);
		}
	}
	if (mn == 1e9 + 7)
		puts ("-1");
	else
		printf ("%d", mn);
	return 0;
}
