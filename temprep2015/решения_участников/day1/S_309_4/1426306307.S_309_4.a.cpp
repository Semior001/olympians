#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#define ll long long
#define pb push_back 
using namespace std;
const int N = 1e5 + 5;
int n, m, l, r, c;
vector < int > t[N];
int main ()
{
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1;i <= m;i ++)
	{
		cin >> l >> r >> c;
		if (c > 0)
			for (int j = l;j <= r;j ++)
				t[j].pb (c);
		else
			for (int j = l;j <= r;j ++)
				if (t[j].size ())
					t[j].pop_back ();
	}
	for (int i = 1;i <= n;i ++)
		if (t[i].size ())
			cout << t[i].back () << " ";
		else	
			cout << 0 << " ";
	return 0;	
}