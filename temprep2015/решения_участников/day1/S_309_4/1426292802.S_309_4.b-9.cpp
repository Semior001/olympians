#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stack>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <ctime>
#define ll long long
#define pb push_back 
using namespace std;

int n, k, cur, mn = 1e9 + 7;

string s;

bool used[40];

int main ()
{
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;
	for (int i = 0;i < s.size ();i ++)
	{   	
		memset (used, 0, sizeof used);
		cur = 0;
//		cout << "i = " << i << endl;
		for (int j = i;j < s.size ();j ++)
		{
			int tmp = s[j] - 'a';
			if (!used[tmp])
			{
				cur ++;
				used[tmp] = 1;
			}
//			cout << "j = " << j << " " << cur << endl;
			if (cur >= k)
				mn = min (mn, j - i + 1);
		}
	}
	if (mn == 1e9 + 7)
		puts ("-1");
	else
		printf ("%d", mn);
	return 0;	
}