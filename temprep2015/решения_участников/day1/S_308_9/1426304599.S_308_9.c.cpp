#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

const int MaxN = 5009;

vector <int> now(MaxN, -1);
static int a[MaxN];

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);

    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    long long ans = 0;
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (a[i] != a[j]) {
                int next = j;
                while (next > i && a[next] != a[i]) {
                    now[a[next]] = next;
                    next--;
                }
                next++;
                int q = i;
                while (q < j && a[q] != a[j]) {
                    next = max(next, q + 1);
                    if (now[a[q]] != -1) next = max(next, now[a[q]]);
                    //cout << "$ " << i << " " << j << " " << q << " " << next << " " << (q - i + 1) * (j - next + 1) << endl;
                    ans += (j - next + 1);
                    q++;
                }
            }
        }
        for (int j = 1; j <= n; j++) now[j] = -1;
    }
    cout << ans;

    return 0;
}
