#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

const int MaxN = 300009;

static int d[MaxN];
static int forRec[MaxN];
vector <int> query;
vector <pair <int, pair <int, int> > > q;

void recalc(int i, int l, int r) {
    int m = (l + r) / 2;
    d[2 * i + 1] += forRec[i];
    d[2 * i + 2] += forRec[i];
    forRec[2 * i + 1] += forRec[i];
    forRec[2 * i + 2] += forRec[i];
    forRec[i] = 0;
}

void update(int i, int l, int r, int a, int b, int c) {
    //cout << "now " << l << " " << r << endl;
    if (l >= b || r <= a) return;
    if (l >= a && r <= b) {
        d[i] += c;
        forRec[i] += c;
        return;
    }
    int m = (l + r) / 2;
    recalc(i, l, r);
    update(2 * i + 1, l, m, a, b, c);
    update(2 * i + 2, m, r, a, b, c);
    d[i] = min(d[2 * i + 1], d[2 * i + 2]);
}

/*int get(int i, int l, int r, int a, int b) {
    if (l >= b || r <= a) return 0;
    if (l >= a && r <= b) return d[i];
    int m = (l + r) / 2;
    recalc(i, l, r);
    return min(get(2 * i + 1, l, m, a, b), get(2 * i + 2, m, r, a, b));
}*/

void print(int i, int l, int r) {
   if (l + 1 == r) cout << d[i] << " ";
   else {
        int m = (l + r) / 2;
        recalc(i, l, r);
        print(2 * i + 1, l ,m);
        print(2 * i + 2, m ,r);
   }
}

int go(int i, int l, int r) {
    if (l + 1 == r) return l;
    else {
        int m = (l + r) / 2;
        recalc(i, l, r);
        if (d[2 * i + 2] < 0) return go(2 * i + 2, m, r);
        else return go(2 * i + 1, l, m);
    }
}

int main() {
    //freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);

    int n, m;
    cin >> n >> m;
    for (int i = 0; i < m; i++) {
        //cout << "Hello" << endl;
        int l, r, c;
        cin >> l >> r >> c;
        l--; r--;
        query.push_back(c);
        if (c == 0) {
            q.push_back(make_pair(l, make_pair(1, i)));
            q.push_back(make_pair(r + 1, make_pair(-1, i)));
        }
        else {
            q.push_back(make_pair(l, make_pair(-1, i)));
            q.push_back(make_pair(r + 1, make_pair(1, i)));
        }
    }
    sort(q.begin(), q.end());
    /*for (int i = 0; i < q.size(); i++) {
        cout << "^ " << q[i].first << " " << q[i].second.first << " " << q[i].second.second << endl;
    }*/
    //cout << "Hello!" << endl;
    int now = 0;
    for (int i = 0; i < n; i++) {
        while(now < q.size() && q[now].first == i) {
            //cout << "EEEEE " << q[now].first << " " << q[now].second.second << " " << q[now].second.first << endl;
            update(0, 0, m, 0, q[now].second.second + 1, q[now].second.first);
            now++;
        }
        //cout << "\n& ";
        //print(0, 0, m);
        //cout << endl;
        if (d[0] >= 0) cout << 0 << " ";
        else cout << query[go(0, 0, m)] << " ";
    }

    return 0;
}
