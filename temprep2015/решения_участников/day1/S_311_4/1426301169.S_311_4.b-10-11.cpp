#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "B."

typedef long long ll;
typedef unsigned long long ull;

const int N = 523;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n, m, k;
int cnt, x;
int cp[3 * N][3 * N], f[3 * N][3 * N];
int ans;
bool u[3 * N];
int pr[3 * N];

bool dfs(int v, int tg, int p = -1) {
	if (u[v])
		return 0;
	u[v] = 1;
	if (v == tg)
		return 1;
	for (int to = 1; to <= n + m + k + 2; to++)
		if (cp[v][to] > f[v][to] && dfs(to, tg, v)) {
		    pr[to] = v;
			return 1;
		}
	return 0;
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= m; i++) {
		scanf("%d", &x);
		cp[1][i + 1] = x;
	}
	for (int i = 1; i <= k; i++) {
		scanf("%d", &x);
		cp[m + n + i + 1][m + n + k + 2] = x;
	}
	for (int i = 1; i <= n; i++) {
		scanf("%d", &cnt);
		while(cnt--) {
			scanf("%d", &x);
			cp[x + 1][m + i + 1] = 1;
		}
	}
	for (int i = 1; i <= n; i++) {
		scanf("%d", &cnt);
		while(cnt--) {
			scanf("%d", &x);
			cp[m + i + 1][m + n + x + 1] = 1;
		}
	}
	while(dfs(1, m + n + k + 2)) {
		int mini = inf;
		for (int i = m + n + k + 1; i != 1; i = pr[i])
			mini = min(mini, cp[pr[i]][i] - f[pr[i]][i]);
		for (int i = m + n + k + 1; i != 1; i = pr[i])
			f[pr[i]][i] += mini,
			f[i][pr[i]] -= mini;
		ans += mini;
		memset(u, 0, sizeof u);	          	
		memset(pr, -1, sizeof pr);
	}
	printf("%d", ans);
	return 0;
}
