#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "C."
#define sz(a) (int)((a).size())

typedef long long ll;
typedef unsigned long long ull;

const int N = 5123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
vector < int > wh[N];
int st[N];
set < int > cnt;
ll ans;

int get(int cnt) {
	return 1ll * cnt * (cnt + 1) / 2;
}

int bin(vector < int > &a, int x) {
	int l = 0, r = sz(a) - 1, m = -1, ans = inf;
	while(l <= r) {
		m = (l + r) / 2;
		if (a[m] > x) {
			ans = m;
			r = m - 1;
		}
		else
			l = m + 1;
	}
	return ans;
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]),
		wh[a[i]].pb(i);
	for (int i = 1; i <= n; i++) {
		for (int i2 = i; i2 < n; i2++) {
			cnt.insert(a[i2]);
			st[0] = 2;
			st[1] = 0, st[2] = n + 1;
			for (auto j : cnt)
				for (int k = bin(wh[j], i2); k < sz(wh[j]); k++)
					st[++st[0]] = wh[j][k];
			sort(st + 1, st + st[0] + 1);
			for (int j = 1; j + 1 <= st[0]; j++)
				ans += get(st[j + 1] - st[j] - 1);
		}
		cnt.clear();
	}
	cout << ans;
	return 0;
}