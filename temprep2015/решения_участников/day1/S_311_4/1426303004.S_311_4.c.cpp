#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "C."

typedef long long ll;
typedef unsigned long long ull;

const int N = 5123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n;
int a[N];
bool cnt[N];
ll ans;

int get(int x, int y, int l, int r) {
//  cout << x << ' ' << y << ' ' << l << ' ' << r << endl;
	if (l > r)
		return 0;
	int cnt = r - l + 1;
	return 1ll * cnt * (cnt + 1) / 2;
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for (int i = 1; i <= n; i++) {
		for (int i2 = i; i2 < n; i2++) {
			cnt[a[i2]]++;
			int last = i2 + 1;
			for (int j = i2 + 1; j <= n; j++) {
				if (cnt[a[j]]) {
					if (last != -1)
						ans += get(i, i2, last, j - 1);
					last = j + 1;
				}
			}
			ans += get(i, i2, last, n);
		}
	    memset(cnt, 0, sizeof cnt);
	}
	cout << ans;
	return 0;
}