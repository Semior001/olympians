#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "B."

typedef long long ll;
typedef unsigned long long ull;

const int N = 523;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n, m, k;
int C[N], P[N];
int cnt, x;
vector < int > c[N], p[N];
int ans;

void rec(int v = 1, int cnt = 0) {
	if (v == n + 1) {
		ans = max(ans, cnt);
		return;
	}
	for (auto i : c[v])
		for (auto j : p[v]) {
			if (C[i] > 0 && P[j] > 0) {
				C[i]--, P[j]--;
				rec(v + 1, cnt + 1);
				C[i]++, P[j]++;
			}
		}
	rec(v + 1, cnt);
}

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= m; i++)
		scanf("%d", &C[i]);
	for (int i = 1; i <= k; i++)
		scanf("%d", &P[i]);
	for (int i = 1; i <= n; i++) {
		scanf("%d", &cnt);
		while(cnt--) {
			scanf("%d", &x);
			c[i].pb(x);
		}
	}
	for (int i = 1; i <= n; i++) {
		scanf("%d", &cnt);
		while(cnt--) {
			scanf("%d", &x);
			p[i].pb(x);
		}
	}
	rec();
	printf("%d", ans);
	return 0;
}
