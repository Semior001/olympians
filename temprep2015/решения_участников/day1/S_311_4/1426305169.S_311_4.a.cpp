#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "A."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

struct num {
	int x, id, ct;
	int tp;
	num() {}
	num(int x, int id, int ct, int tp) : x(x), id(id), ct(ct), tp(tp) {}
};

bool cmp(num a, num b) {
	if (a.x < b.x)
		return 1;
	else if (a.x > b.x)
		return 0;			
	if (a.tp < b.tp)
		return 1;
	else if (a.tp > b.tp)
		return 0;
	if (a.tp == 2) {
	    if (a.ct < b.ct)
			return 1;
		return 0;
	}
	if (a.id < b.id)
		return 1;
	return 0;
}

int n, m;
vector < num > st;
int ans[N];
int l, r, c;
set < pair < int, int > > now;
int s[N];

int main() {
	freopen(fname"in", "r", stdin);
	freopen(fname"out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		st.pb(num(i, i, -1, 1));
	for (int i = 1; i <= m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		st.pb(num(l, i, c, 0));
		st.pb(num(r, i, c, 2));
	}
	sort(st.begin(), st.end(), &cmp);
	for (auto i : st) {
//	    cout << i.x << " and " << i.tp << " ct " << i.ct << endl;
		if (i.tp == 0)
			now.insert(mp(i.id, i.ct));
		else if (i.tp == 1) {
			if (now.empty())
				continue;
			s[0] = 0;
			for (auto j : now) {
				if (j.s == 0)
					s[0]--;
				else
					s[++s[0]] = j.s;
			}
			ans[i.id] = s[s[0]];
		}
		else
			now.erase(mp(i.id, i.ct));
//		cout << "now is\n";
//		for (auto j : now)
//			cout << j.f << ' ' << j.s << endl;
	}
	for (int i = 1; i <= n; i++)
		printf("%d ", ans[i]);	
	return 0;
}
