#include <bits/stdc++.h>

#define pb push_back
#define pp pop_back
#define mp make_pair
#define f first
#define s second
#define fname "A."

typedef long long ll;
typedef unsigned long long ull;

const int N = (int)1e5 + 123;
const int inf = (int)1e9 + 123;
const double eps = 1e-6;

using namespace std;

int n, m;
vector < int > ans[N];
int l, r, c;

int main() {
	freopen(fname"in", "r", stdin);
	freopen("A.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++) {
		scanf("%d%d%d", &l, &r, &c);
		for (int j = l; j <= r; j++) {
			if (!c)
				ans[j].pp();
			else
				ans[j].pb(c);
		}
	}
	for (int i = 1; i <= n; i++)
		printf("%d ", (ans[i].empty() ? 0 : ans[i].back()));	
	return 0;
}
