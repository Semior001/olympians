#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "B"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int n, m, k;
int x[MaxN], y[MaxN];
int c[MaxN], p[MaxN];
int a[505][505];
int b[505][505];
vector <int> fake_c[MaxN];
vector <int> fake_p[MaxN];
int used_c[MaxN];
int used_p[MaxN];
int timer;

bool ok (int mask) {
	timer++;
	// memset (fake_c, 0, sizeof fake_c);
	// memset (fake_p, 0, sizeof fake_p);

	for (int i = 1; i <= m; ++i)
		fake_c[i].clear();

	for (int i = 1; i <= k; ++i)
		fake_p[i].clear();

	for (int i = 0; i < n; ++i) {
		if (!(mask & (1 << i)))
			continue;
		int cur = i + 1;
		for (int j = 1; j <= x[cur]; ++j) {
			fake_c[a[cur][j]].push_back (i + 1);
			/* int &fake = fake_c[a[cur][j]]; fake++;
			if (fake > c[a[cur][j]])
				return false; */
		}
		for (int j = 1; j <= y[cur]; ++j) {
			fake_p[b[cur][j]].push_back (i + 1);
			/* int &fake = fake_c[a[cur][j]]; fake++;
			if (fake > c[a[cur][j]])
				return false; */
		}
		/* for (int j = 1; j <= y[cur]; ++j) {
			int &fake = fake_p[b[cur][j]]; fake++;
			if (fake > c[b[cur][j]])
				return false;
		} */
	}

	for (int i = 1; i <= m; ++i) {
		int cnt = 0;
		for (int j = 0; j < fake_c[i].size(); ++j)
			cnt += used_c[fake_c[i][j]] != timer;
		if (cnt <= c[i]) {
			for (int j = 0; j < fake_c[i].size(); ++j)
				used_c[fake_c[i][j]] = timer;
		} else
			return false;
		/* if (fake_c[i].size() < c[i]) {
			for (int j = 0; j < fake_c[i].size(); ++j)
				used[fake_c[i][j]] = timer;
		} else {
			if ()
		} */
	}

	for (int i = 1; i <= k; ++i) {
		int cnt = 0;
		for (int j = 0; j < fake_p[i].size(); ++j)
			cnt += used_p[fake_p[i][j]] != timer;
		if (cnt <= p[i]) {
			for (int j = 0; j < fake_p[i].size(); ++j)
				used_p[fake_p[i][j]] = timer;
		} else {
			return false;
		}
		/* if (fake_c[i].size() < c[i]) {
			for (int j = 0; j < fake_c[i].size(); ++j)
				used[fake_c[i][j]] = timer;
		} else {
			if ()
		} */
	}

	return true;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d%d%d", &n, &m, &k);
	
	for (int i = 1; i <= m; ++i)
		scanf ("%d", c + i);

	for (int i = 1; i <= k; ++i)
		scanf ("%d", p + i);

	for (int i = 1; i <= n; ++i) {
		scanf ("%d", x + i);
		for (int j = 1; j <= x[i]; ++j)
			scanf ("%d", &a[i][j]);
	}
	
	// cerr << "OK\n";
	
	for (int i = 1; i <= n; ++i) {
		scanf ("%d", y + i);
		for (int j = 1; j <= y[i]; ++j)
			scanf ("%d", &b[i][j]);
	}

	int res = 0;
	
	for (int mask = 0; mask < (1 << n); ++mask) {
		// cerr << mask << endl;
		int cur = __builtin_popcount (mask);
		if (ok (mask)) {
			res = max (res, cur);
			// cerr << mask << endl;
		}
	}

	cout << res;
	
	return 0;
}
