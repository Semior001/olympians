#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "B"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int n, m, k;
int x[MaxN], y[MaxN];
int c[MaxN], p[MaxN];
int a[505][505];
int b[505][505];
vector <int> fake_c[MaxN];
vector <int> fake_p[MaxN];
int used_c[MaxN];
int used_p[MaxN];
int timer, mask;

bool ok (int pos) {
	if (pos == n)
		return true;
	if (!(mask & (1 << pos))) {
		// cerr << pos << " ";
		return ok (pos + 1);
	}
	bool res = false;
	int cur = pos + 1;
	for (int i = 1; i <= x[cur]; ++i) {
		if (c[a[cur][i]] == 0)
			continue;
		c[a[cur][i]]--;
		for (int j = 1; j <= y[cur]; ++j) {
			if (p[b[cur][j]] == 0)
				continue;
			p[b[cur][j]]--;
			res |= ok (pos + 1);
			p[b[cur][j]]++;
		}
		c[a[cur][i]]++;
	}
	return res;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	scanf ("%d%d%d", &n, &m, &k);
	
	for (int i = 1; i <= m; ++i)
		scanf ("%d", c + i);

	for (int i = 1; i <= k; ++i)
		scanf ("%d", p + i);

	for (int i = 1; i <= n; ++i) {
		scanf ("%d", x + i);
		for (int j = 1; j <= x[i]; ++j)
			scanf ("%d", &a[i][j]);
	}
	
	// cerr << "OK\n";
	
	for (int i = 1; i <= n; ++i) {
		scanf ("%d", y + i);
		for (int j = 1; j <= y[i]; ++j)
			scanf ("%d", &b[i][j]);
	}

	int res = 0;
	
	for (mask = 0; mask < (1 << n); ++mask) {
		// cerr << mask << endl;
		int cur = __builtin_popcount (mask);
		if (res < cur && ok (0)) {
			res = max (res, cur);
			// cerr << mask << endl;
		}
	}

	cout << res;
	
	return 0;
}
