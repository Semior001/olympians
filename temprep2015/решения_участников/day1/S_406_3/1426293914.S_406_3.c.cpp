#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "C"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], res;

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 1; i <= n; ++i) {
		scanf ("%d", a + i);
	}

	for (int l = 1; l <= n; ++l) {
		set <int> S;
		for (int r = l; r <= n; ++r) {
			S.insert (a[r]);
			for (int l2 = r + 1; l2 <= n; ++l2) {
				for (int r2 = l2; r2 <= n && !S.count (a[r2]); r2++)
					res++;
			}
		}
	}

	cout << res;
	
	return 0;
}
