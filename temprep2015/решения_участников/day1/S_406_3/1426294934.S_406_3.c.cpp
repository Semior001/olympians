#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define next _nxt
#define prev _prv

#define fname "C"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], res;
int nxt[MaxN], prev[MaxN];

int get (int k) {
	return (k * (k + 1)) / 2;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 1; i <= n; ++i) {
		scanf ("%d", a + i);
	}

	for (int i = n; i >= 1; --i) {
		if (!prev[a[i]])
			prev[a[i]] = n + 1;
		nxt[i] = prev[a[i]];
		prev[a[i]] = i;
	}

	/* for (int i = 1; i <= n; ++i) {
		cout << nxt[i] << " ";
	} */

	for (int l = 1; l <= n; ++l) {
		int next = n + 1;
		for (int r = l; r <= n; ++r) {
			// cout << l << " " << r << endl;
			next = min (next, nxt[r]);
			if (r + 1 >= next)
				break;
			res += get (next - (r + 1));
		}
	}

	cout << res;
	
	return 0;
}
