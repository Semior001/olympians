#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define next _nxt
#define prev _prv

#define fname "C"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

int a[MaxN], res;
int nxt[MaxN], prev[MaxN];
int used[MaxN], timer;

int get (int k) {
	return (k * (k + 1)) / 2;
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n; scanf ("%d", &n);

	for (int i = 1; i <= n; ++i) {
		scanf ("%d", a + i);
	}

	for (int i = n; i >= 1; --i) {
		if (!prev[a[i]])
			prev[a[i]] = n + 1;
		nxt[i] = prev[a[i]];
		prev[a[i]] = i;
	}
	
	int res = 0;
	for (int l = 1; l <= n; ++l) {
		timer++;
		int next = n + 1;
		for (int r = l; r <= n; ++r) {
			used[a[r]] = timer;
			if (next == r)
				next = n + 1;
			next = min (next, nxt[r]);
			int l2 = r + 1, r2 = next;
			res += get (r2 - l2);
			for (l2 = next + 1; l2 <= n; ++l2) {
				for (r2 = l2; r2 <= n && used[a[r2]] != timer; ++r2)
					res++;
			}
		}
	}

	cout << res;
	
	return 0;
}
                      