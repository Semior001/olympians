#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "A"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

vector <int> a[MaxN];

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n, q; scanf ("%d%d", &n, &q);
	
	while (q--) {
		int l, r, c; scanf ("%d%d%d", &l, &r, &c);
		if (c) {
			for (int i = l; i <= r; ++i)
				a[i].push_back (c);
		} else {
			for (int i = l; i <= r; ++i)
				a[i].pop_back ();
		}
	}

	for (int i = 1; i <= n; ++i)
		cout << (a[i].empty() ? 0 : a[i].back()) << " ";

	return 0;
}
