#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define all(v) v.begin(), v.end()

#define fname "A"

const int MaxN = (int)(1e5) + 256;
const int MOD = (int)(1e9) + 7;

struct node {
	vector <int> v;
} t[MaxN * 4];

void goadd (vector <int> &a, vector <int> &b) {
	if (a.empty())
		return;
	for (int i = 0; i < a.size(); ++i) {
		if (a[i])
			b.push_back (a[i]);
		else
			b.pop_back();
	}
}

void push (int v) {
	goadd (t[v].v, t[v + v].v);
	goadd (t[v].v, t[v + v + 1].v);
	t[v].v.clear();
}

void add (int l, int r, int c, int v, int tl, int tr) {
	if (l <= tl && tr <= r) {
		t[v].v.push_back (c);
		return;
	}
	if (l > tr || tl > r)
		return;
	push (v);
	int tm = (tl + tr) / 2;
	add (l, r, c, v + v, tl, tm);
	add (l, r, c, v + v + 1, tm + 1, tr);
}

void go (int v, int l, int r) {
	if (l == r) {
		cout << (t[v].v.empty() ? 0 : t[v].v.back()) << " ";
		return;
	}
	push (v);
	int m = (l + r) / 2;
	go (v + v, l, m);
	go (v + v + 1, m + 1, r);
}

int main () {
	freopen (fname".in", "r", stdin);
	freopen (fname".out", "w", stdout);
	
	int n, q; scanf ("%d%d", &n, &q);
	
	while (q--) {
		int l, r, c; scanf ("%d%d%d", &l, &r, &c);
		add (l, r, c, 1, 1, n);
		/* if (c) {
			add (l, r, c, 1, 1, n);
		} else {
			erase (l, r, 1, 1, n);
		} */
	}

	go (1, 1, n);

	return 0;
}
