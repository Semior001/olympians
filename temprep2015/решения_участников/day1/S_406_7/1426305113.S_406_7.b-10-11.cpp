#include <bits/stdc++.h>

using namespace std;

const int N = 21;

int a[N], b[N];
vector <int> g1[N], g2[N];
int cur;
int n, m, k, ans;

inline void rec (const int &lvl) {
	if (lvl == n + 1) {
		ans = max (ans, cur);
		if (ans == n)
			printf ("%d", n);
		exit (0);
	}
	for (auto it : g1[lvl]) {
		for (auto it1 : g2[lvl]) {
			if (a[it] > 0 && b[it1] > 0) {
				--a[it];
				--b[it1];
				++cur;
				rec (lvl + 1);
				++a[it];
				++b[it1];
				--cur;
			}
		}
	}
	rec (lvl + 1);
}

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	scanf ("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= m; ++i)
		scanf ("%d", &a[i]);
	for (int i = 1; i <= k; ++i)
		scanf ("%d", &b[i]);
	
	for (int i = 1; i <= n; ++i) {
		int x, y;
		scanf ("%d", &y);
		for (int j = 1; j <= y; ++j) {
			scanf ("%d", &x);
			g1[i].push_back (x);
		}
	}
	for (int i = 1; i <= n; ++i) {
		int x, y;
		scanf ("%d", &y);
		for (int j = 1; j <= y; ++j) {
			scanf ("%d", &x);
			g2[i].push_back (x);
		}
	}
	rec (1);
	printf ("%d", ans);
	return 0;
}