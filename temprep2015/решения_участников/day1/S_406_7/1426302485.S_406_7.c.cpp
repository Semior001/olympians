#include <bits/stdc++.h>

using namespace std;

const int N = 2001, sz = 1 << 17;

int t[sz << 1];

long long d[N][N], len[N], ans;
int n, a[N];

int main () {
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	scanf ("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf ("%d", &a[i]);
	}
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			d[i][j] = d[i - 1][j];
		}
		++d[i][a[i]];
	}
	for (int i = 1; i <= n; ++i)
		len[i] = (i * (i + 1)) / 2;
	
	for (int l = 1; l <= n; ++l) {
		for (int r = l; r <= n; ++r) {
			int now = 0;
			for (int i = r + 1; i <= n; ++i) {
				if (d[r][a[i]] - d[l - 1][a[i]] == 0) {
					++now;
				}
				else {
					ans += len[now];
					now = 0;
				}
			}
			ans += len[now];
		}
	}
	printf ("%lld", ans);
	return 0;
}