#include <bits/stdc++.h>

using namespace std;

const int N = 1e5 + 1, sz = 1 << 17, inf = 1e9;

int t[sz * 3], ch[sz * 3];
int ans[N];
int l[N], r[N], c[N];
inline void push (const int &v) {
	if (v < sz) {
		ch[v * 2] += ch[v];
		ch[v * 2 + 1] += ch[v];
		t[v] = max (t[v * 2] + ch[v * 2], t[v * 2 + 1] + ch[v * 2 + 1]);
	} else {
		t[v] += ch[v];
	}
	ch[v] = 0;
}

inline void upd (const int &v, const int &Tl, const int &Tr, const int &l, const int &r, const int &delta) {
	if (Tl > r || Tr < l)
		return;
	push (v);
	if (l <= Tl && Tr <= r) {
		ch[v] += delta;
		push (v);
		//cerr << Tl << " " << Tr << "\n";
		return;
	}
	int mid = (Tl + Tr) >> 1;
	upd (v * 2, Tl, mid, l, r, delta);
	upd (v * 2 + 1, mid + 1, Tr, l, r, delta);
	t[v] = max (t[v * 2] + ch[v * 2], t[v * 2 + 1] + ch[v * 2 + 1]);
}

int main () {
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	int n, m;
	scanf ("%d%d", &n, &m);

	for (int i = 1; i <= m; ++i)
		scanf ("%d%d%d", &l[i], &r[i], &c[i]);

	for (int i = m; i; --i) {
		if (!c[i]) {
			upd (1, 0, sz - 1, l[i], r[i], -1);
		}
		else {
			upd (1, 0, sz - 1, l[i], r[i], 1);
			while (t[1] > 0) {
				int now = 1;
				while (now < sz) {
					push (now);
					if (t[now * 2] + ch[now * 2] > t[now * 2 + 1] + ch[now * 2 + 1])
						now = now * 2;
					else
						now = now * 2 + 1;
				}
				//cerr << now - sz << " " << c[i] << " ";				
				ans[now - sz] = c[i];
				upd (1, 0, sz - 1, now - sz, now - sz, -200000);

			}
		}
	}
	for (int i = 1; i <= n; ++i)
		printf ("%d ", ans[i]);
	return 0;
}