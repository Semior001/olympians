#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;
#define fname "B"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int N = 1000500;

struct edge {
	int from, to, flow, cap;
	edge() {}
	edge(int from, int to, int flow, int cap) : from(from), to(to), flow(flow), cap(cap) {}
};

vector <edge> e;

vector <int> a[N], MyCoffee[N], MyPirog[N];

void add(int x, int y, int c) {
	a[x].pb((int)e.size());
	e.pb(edge(x, y, 0, c));
	a[y].pb((int)e.size());
	e.pb(edge(y, x, 0, 0));
}

int n, m, k;

int last;

int pirog[N], coffee[N];
int nxt[N], d[N];

queue <int> q;

bool bfs() {
	for (int i = 0; i <= last; ++ i) {
		d[i] = 0;
	}
	d[0] = 1;
	q.push(0);
	while (!q.empty()) {
		int v = q.front();
		q.pop();
		for (int i = 0; i < a[v].size(); ++ i) {
			int id = a[v][i];
			if (e[id].flow < e[id].cap && !d[e[id].to]) {
				d[e[id].to] = d[v] + 1;
				q.push(e[id].to);
			}
		}
	}
	return (d[last] > 0);
}

int dfs(int v, int flow = INF) {
	if (!flow || v == last) return flow;
	for (; nxt[v] < a[v].size(); ++ nxt[v]) {
		int id = a[v][nxt[v]];
		if (e[id].flow < e[id].cap && d[e[id].to] == d[v] + 1) {
			int mn = dfs(e[id].to, min(flow, e[id].cap - e[id].flow));
			if (mn != 0) {
				e[id].flow += mn;
				e[id ^ 1].flow -= mn;
				return mn;
			}
		}
	}
	return 0;
}           

int MaxFlow() {
	int res = 0;
	while(bfs()) {
		for (int i = 0; i <= last; ++ i) nxt[i] = 0;
		while (int pushed = dfs(0)) {
			res += pushed;
		}
	}
	return res;
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d%d", &n, &m, &k);

	for (int i = 1; i <= m; ++ i) {
		scanf("%d", &coffee[i]);
	}

	for (int i = 1; i <= k; ++ i) {
		scanf("%d", &pirog[i]);
	}

	for (int i = 1; i <= n; ++ i) {
		int kol;
		scanf("%d", &kol);
		for (int j = 1; j <= kol; ++ j) {
			int x;
			scanf("%d", &x);
			MyCoffee[i].pb(x);
		}                    
	}
	for (int i = 1; i <= n; ++ i) {
		int kol;
		scanf("%d", &kol);
		for (int j = 1; j <= kol; ++ j) {
			int x;
			scanf("%d", &x);
			MyPirog[i].pb(x);
		}                    
	}

	for (int i = 1; i <= m; ++ i) {
		add(0, i, coffee[i]);
	}

	for (int i = 1; i <= n; ++ i) {
		for (int j = 0; j < MyCoffee[i].size(); ++ j) {
			int type = MyCoffee[i][j];
			add(type, m + i, 1);
		}
		for (int j = 0; j < MyPirog[i].size(); ++ j) {
			int type = MyPirog[i][j];
			add(m + i, m + n + type, 1);
		}
	}

	last = n + m + k + 1;

	for (int i = 1; i <= k; ++ i) {
		add(m + n + i, last, pirog[i]);
	}

	cout << MaxFlow();

	return 0;
}