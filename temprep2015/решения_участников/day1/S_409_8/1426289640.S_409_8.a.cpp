#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;
#define fname "A"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int N = 100500;

int n, m;

vector <int> a[N];

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &m);

	for (int i = 1; i <= m; ++ i) {
		int type, l, r;
		scanf("%d%d%d", &l, &r, &type);
		if (!type) {                     
			for (int j = l; j <= r; ++ j) a[j].pop_back();
		}
		else {
			for (int j = l; j <= r; ++ j) a[j].push_back(type);
		}
	}

	for (int i = 1; i <= n; ++ i) {
		if (!a[i].size()) printf("0 ");
		else printf("%d ", a[i][a[i].size() - 1]); 
	}

	return 0;
}