#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 7000;

int n, a[N], last[N], nxt[N];

set <pair <int, int> > s;

ll calc(int x) {
	return x * 1ll * (x + 1) / 2;
}

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
	}

	for (int i = n; i > 0; -- i) {
		nxt[i] = last[a[i]];
		last[a[i]] = i;
	}

	ll ans = 0;

	for (int l = 1; l <= n; ++ l) {
		
		for (int i = 1; i <= n; ++ i) {
			last[i] = 0;
		}

		s.clear();

		for (int r = l; r <= n; ++ r) {
			if (!last[a[r]]) {
				if (nxt[r]) s.insert(mp(nxt[r], r));
			}
			else {
				if (nxt[last[a[r]]]) s.erase(mp(nxt[last[a[r]]], last[a[r]]));
				if (nxt[r]) s.insert(mp(nxt[r], r));
			}
			last[a[r]] = r;
			int id = n + 1;
			if (!s.empty()) id = s.begin() -> F;
			--id;
			if (r + 1 <= id) ans += calc(id - r);
		}
	}
	
	cout << ans;

	return 0;
}