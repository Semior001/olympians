#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 7000;

int n, a[N], cnt[N];

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
	}

	ll ans = 0;
	
	for (int l = 1; l <= n; ++ l) {
		for (int r = l; r <= n; ++ r) {
			cnt[a[r]]++;
			int last = r;
			for (int i = r + 1; i <= n; ++ i) {
				if (cnt[a[i]]) {
					ans += (i - last) * 1ll * (i - last - 1) / 2;
					last = i;
				}
			}
			ans += (n - last) * 1ll * (n - last + 1) / 2;
		}
		for (int r = l; r <= n; ++ r) {
			--cnt[a[r]];
		}
	}
	
	cout << ans;

	return 0;
}