#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <cstring>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>
#include <bitset>

using namespace std;
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 2010;

int n, a[N];
ll d[N][N];
int last[N], nxt[N], prv[N];

int cnt[N];

set <pair <int, int> > s;

int p[N][N];

int i, j, k, ii, l, r, len;

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);
	for (i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
		a[i] = rand() % n + 1;
		prv[i] = last[a[i]];
		last[a[i]] = i;
	}

	memset(last, 0, sizeof last);
	for (i = n; i > 0; -- i) {
		nxt[i] = last[a[i]];
		last[a[i]] = i;
	}
	for (l = 1; l <= n; ++ l) {
		for (i = 1; i <= n; ++ i) {
			last[i] = 0;
		}
		s.clear();
		for (r = l; r <= n; ++ r) {
			if (!last[a[r]]) {
				if (nxt[r]) s.insert(mp(nxt[r], r));
			}
			else {
				if (nxt[last[a[r]]]) s.erase(mp(nxt[last[a[r]]], last[a[r]]));
				if (nxt[r]) s.insert(mp(nxt[r], r));
			}
			last[a[r]] = r;
			if (!s.empty()) p[l][r] = s.begin() -> F;
			else p[l][r] = n + 1;
			--p[l][r];
		}
	}

	ll ans = 0;

	int lst = 0;
	
	for (len = n - 1; len > 0; -- len) {
		for (i = 1; i <= n; ++ i) cnt[i] = 0;
		for (i = 1; i + len - 1 <= n; ++ i) {
			j = i + len - 1;
			if (i == 1) {
				for (k = i; k <= j; ++ k) cnt[a[k]]++;
			}
			else {
				--cnt[a[i - 1]];
				++cnt[a[j]];
			}
			       
			d[i][j] = d[i][j + 1];
			if (j < n && cnt[a[j + 1]]) {
				d[i][j] += p[i][j] - j;
			}
			else {
				d[i][j] = 0;
				lst = j;
				for (ii = j + 1; ii <= n; ++ ii) {
					if (cnt[a[ii]]) {
						d[i][j] += (ll)(ii - lst) * (ii - lst - 1);
						lst = ii;
					}
				}
				d[i][j] += (ll)(n - lst) * (n - lst + 1);
			}
			ans += d[i][j];
//			cout << i << " " << j << " -> " << d[i][j] << endl;
		}                                  
	}

	cout << ans / 2 << endl;

	return 0;
}