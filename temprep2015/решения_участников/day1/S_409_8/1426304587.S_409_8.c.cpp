#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>
#include <bitset>

using namespace std;
#define fname "C"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long

const int N = 1010;

bitset <N> t[N][N], now;

int n, a[N];
ll d[N][N];

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d", &n);

	for (int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
		t[i][i][a[i]] = 1;
	}

	for (int i = 1; i <= n; ++ i) {
		for (int j = i + 1; j <= n; ++ j) {
			t[i][j] = t[i][j - 1];
			t[i][j][a[j]] = 1;
		}
	}

	ll ans = 0;
	
	for (int len = n - 1; len > 0; -- len) {
		for (int i = 1; i + len - 1 <= n; ++ i) {
			int j = i + len - 1;
			d[i][j] = d[i][j + 1];
			if (j < n && t[i][j][a[j + 1]] == 1) {
				int l = j + 1, r = n, pos = j;
				while (l <= r) {
					int mid = (l + r) >> 1;
					now = t[i][j];
					now &= t[j + 1][mid];
					if (now == 0) {
						pos = mid;
						l = mid + 1;
					}
					else r = mid - 1;
				}
				d[i][j] += pos - j;
			}
			else {
				d[i][j] = 0;
				int last = j;
				ll added = 0;
				for (int ii = j + 1; ii <= n; ++ ii) {
					if (t[i][j][a[ii]] == 1) {
						d[i][j] += (ii - last) * 1ll * (ii - last - 1) / 2;
						last = ii;
					}
				}
				d[i][j] += (n - last) * 1ll * (n - last + 1) / 2;
			}
			ans += d[i][j];
//			cout << i << " " << j << " -> " << d[i][j] << endl;
		}                                  
	}

	cout << ans << endl;

	return 0;
}