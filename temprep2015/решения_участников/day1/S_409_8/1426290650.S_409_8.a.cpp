#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <cassert>
#include <stack>
#include <queue>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <cmath>

using namespace std;
#define fname "A"
#define F first
#define S second
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ll long long
#define ull unsigned long long
const int N = 100500;

int n, m;

int l[N], r[N], type[N];

struct node {
	int add, mn, id;
	node() {
		mn = INF, id = 0, add = 0;
	}
}t[4 * N];

int nn = 1;

void push(int v, int tl, int tr) {
	t[v].mn += t[v].add;
	if (tl == tr) {
		t[v].add = 0;
		return;
	}
	t[v + v].add += t[v].add;
	t[v + v + 1].add += t[v].add;
	t[v].add = 0;
}

void upd(int l, int r, int x, int v = 1, int tl = 1, int tr = nn) {
	push(v, tl, tr);
	if (l > r || l > tr || tl > r) return;
	if (l <= tl && tr <= r) {
		t[v].add += x;
		push(v, tl, tr);
		return;	
	}
	int mid = (tl + tr) >> 1;
	upd(l, r, x, v + v, tl, mid);
	upd(l, r, x, v + v + 1, mid + 1, tr);
	if (t[v + v].mn <= t[v + v + 1].mn) {
		t[v].mn = t[v + v].mn;
		t[v].id = t[v + v].id;
	}
	else {
		t[v].mn = t[v + v + 1].mn;
		t[v].id = t[v + v + 1].id;
	}
}

pair <int, int> get(int l, int r, int v = 1, int tl = 1, int tr = nn) {
	push(v, tl, tr);
	if (l > r || l > tr || tl > r) return mp(INF, 0);
	if (l <= tl && tr <= r) return mp(t[v].mn, t[v].id);
	int mid = (tl + tr) >> 1;
	pair <int, int> n1 = get(l, r, v + v, tl, mid);
	pair <int, int> n2 = get(l, r, v + v + 1, mid + 1, tr);
	if (n1.F <= n2.F) return n1;
	else return n2;
}

int ans[N];

int main() {
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);

	scanf("%d%d", &n, &m);

	while(nn < n) nn <<= 1;

	for (int i = 1; i <= m; ++ i) {
		scanf("%d%d%d", &l[i], &r[i], &type[i]);
	}

	for (int i = 1; i <= n; ++ i) {
		t[i + nn - 1].mn = 0;
		t[i + nn - 1].id = i;
	}

	for (int i = nn - 1; i > 0; -- i) {
		if (t[i + i].mn <= t[i + i + 1].mn) t[i].mn = t[i + i].mn, t[i].id = t[i + i].id;
		else t[i].mn = t[i + i + 1].mn, t[i].id = t[i + i + 1].id;
	}

	for (int i = m; i > 0; -- i) {
		if (!type[i]) {
			upd(l[i], r[i], 1);
		}
		else {
			while(1) {
				pair <int, int> now = get(l[i], r[i]);
				if (now.F > 0) break;
				ans[now.S] = type[i];
				upd(now.S, now.S, INF);
			}
			upd(l[i], r[i], -1);
		}
	}

	for (int i = 1; i <= n; ++ i) printf("%d ", ans[i]);

	return 0;
}