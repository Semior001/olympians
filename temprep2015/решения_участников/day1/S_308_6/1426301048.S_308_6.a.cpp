#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {

 freopen("A.in", "r", stdin); 
 freopen("A.out", "w", stdout); 

 int m, n;

 int l, r, c;

 scanf("%d %d", &n, &m);

 int **cont = new int*[n];

 for (int i = 0; i < n; i++) {
  cont[i] = new int[m+1];
  cont[i][0] = 0;
 }
 
 for (int i = 0; i < m; i++) {
  scanf("%d %d %d", &l, &r, &c);
  if (c != 0) {
   for (int j = l - 1; j < r; j++) {
    cont[j][cont[j][0]+1] = c;
    cont[j][0]++;
   }
  }
  else {
   for (int j = l - 1; j < r; j++) {
    cont[j][cont[j][0]+1] = c;
    cont[j][0]--;
   }
  }
 }

 for (int i = 0; i < n; i++) {
  printf("%d ", cont[i][cont[i][0]]);
 }

 return 0;
}
