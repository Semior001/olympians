#include<bits/stdc++.h>
#define mp(a,b) make_pair(a,b)
#define ss second
#define ff first
using namespace std;
int n,m,ans[100100];
vector<pair<int,int> > t[500500];
void psh(int v,int tl,int tr,int l,int r,int p,int i){
    if(l>r)return;
    if(tl==l && tr==r){
        t[v].push_back(mp(i,p));
    }
    else{
        int tm=(tl+tr)/2;
        psh(v*2,tl,tm,l,min(tm,r),p,i);
        psh(v*2+1,tm+1,tr,max(l,tm+1),r,p,i);
    }
}
void get(int v,int tl,int tr,vector<pair<int,int> >e){
    for(int i=0;i<t[v].size();i++){
        e.push_back(t[v][i]);
        sort(e.begin(),e.end());
    }
    //cout<<v<<" ";
    if(tl==tr){
        while(!e.empty() && e.back().ss==0){
            e.pop_back();
            e.pop_back();
        }
        if(e.empty())ans[tl]=0;
        else ans[tl]=e.back().ss;
    }
    else {
        int tm=(tl+tr)/2;
        get(v*2,tl,tm,e);
        get(v*2+1,tm+1,tr,e);
    }
}
int main(){
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    vector<pair<int,int> >e;
    cin>>n>>m;
    int l,r,c;
    for(int i=1;i<=m;i++){
        cin>>l>>r>>c;
        psh(1,1,n,l,r,c,i);
    }
    get(1,1,n,e);
    for(int i=1;i<=n;i++){
        cout<<ans[i]<<" ";
    }
    return 0;
}
