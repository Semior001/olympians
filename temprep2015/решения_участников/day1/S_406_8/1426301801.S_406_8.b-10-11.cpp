#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long 
#define ullong unsigned long long 
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e5 + 10;
const double EPS = (double)1e-9;

struct edge {
  int u, v, flow, cap;
  edge() {}
  edge(int u, int v, int flow, int cap):
    u(u), v(v), flow(flow), cap(cap) {} 
};

vector <edge> e;
vector <int> g[MXN];
int ff;
int ptr[MXN];
int ans;
int d[MXN];
queue <int> q;

int n, m, k;

inline void make_edge(int u, int v, int cap){
  g[u].pb(e.size());
  e.pb(edge(u, v, 0, cap));
  g[v].pb(e.size());
  e.pb(edge(v, u, 0, 0));
}

bool bfs(){
  q.push(0);
  fill(d + 1, d + 2 * ff, -1);
  edge tmp;
  while(!q.empty()){
    int v = q.front();
    q.pop();
    for(auto key : g[v]){
      tmp = e[key];
      if(d[tmp.v] == -1 && tmp.cap > tmp.flow){
        d[tmp.v] = d[tmp.u] + 1;
        q.push(tmp.v);
      }      
    }
  }
  return d[ff] != -1;
}


int dfs(int v, int flow){
  //cout << v << "\n";
  if(!flow)
    return 0;
  if(v == ff)
    return flow;
  edge tmp;
  int id;
  for(int &i = ptr[v]; i < g[v].size(); ++i){
    id = g[v][i];
    tmp = e[id];
    //cerr << tmp.u << " " << tmp.v << "\n";
    if(d[v] + 1 == d[tmp.v]){
      int pushed = dfs(tmp.v, min(flow, tmp.cap - tmp.flow));
      if(pushed){
        e[id].flow += pushed;
        e[id ^ 1].flow -= pushed;
      }
      return pushed;  
    }
  }
  return 0;
}

int main(){
  #define fn "B"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);  
  #else
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d%d", &n, &m, &k);
  ff = (2 * n + m + k) + 1;
  for(int i = 1; i <= n; ++i){
    make_edge(0, 2 * i - 1, 1);
    make_edge(0, 2 * i,     1);
  }
  int tmp, tmp1;
  for(int i = 1; i <= m; ++i){
    scanf("%d", &tmp);        
    make_edge(2 * n + i, ff, tmp);
  }
  for(int i = 1; i <= k; ++i){
    scanf("%d", &tmp);
    make_edge(2 * n + m + i, ff, tmp);
  }     
  for(int i = 1; i <= n; ++i){
    scanf("%d", &tmp);
    for(int j = 0; j < tmp; ++j) {
      scanf("%d", &tmp1);
      make_edge(2 * i - 1, 2 * n + tmp1, 1);
    }
  }
  for(int i = 1; i <= n; ++i){
    scanf("%d", &tmp);
    for(int j = 0; j < tmp; ++j) {
      scanf("%d", &tmp1);
      make_edge(2 * i, 2 * n + m + tmp1, 1);
    }
  }
  /*for(int i = 0; i <= ff; ++i){
    cerr << i << ": ";
    for(int j = 0; j < g[i].size(); ++j){
      int to = e[g[i][j]].v;
      cerr << to << " ";
    }
    cerr << "\n";
  }*/
  while(1){
    if(!bfs())
      break;
    fill(ptr, ptr + 2 * ff, 0);
    while(int pushed = dfs(0, INF));    
  }
  for(int i = 1; i <= n; ++i){
    ans += (e[4 * i - 4].flow == 1 && e[4 * i - 2].flow == 1);
  } 
  printf("%d", ans);
  return 0;
}