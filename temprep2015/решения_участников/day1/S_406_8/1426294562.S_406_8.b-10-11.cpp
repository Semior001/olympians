#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long 
#define ullong unsigned long long 
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e5 + 10;
const double EPS = (double)1e-9;

int n, m, k;
int a[MXN], b[MXN];
vector <int> c[MXN], d[MXN];
vector <int> v;
int ans;
bool boo;

bool bit(int id, int mask){
  return (1 << id) & mask;
}

void rec(int x){
  if(x == v.size()){
    bool boo1 = true, boo2 = true;
    for(int i = 1; i <= m; ++i){
      if(a[i] < 0)
        boo1 = false;   
    }
    for(int i = 1; i <= k; ++i){
      if(b[i] < 0)
        boo2 = false;   
    }
    boo |= (boo1 & boo2);
  }
  else {
    for(int i = 0; i < c[v[x]].size(); ++i){
      for(int j = 0; j < d[v[x]].size(); ++j){
        a[c[v[x]][i]]--;
        b[d[v[x]][j]]--;
        rec(x + 1);
        a[c[v[x]][i]]++;
        b[d[v[x]][j]]++;
      }
    }
  }
}

int main(){
  #define fn "B"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d%d", &n, &m, &k);
  for(int i = 1; i <= m; ++i){
    scanf("%d", &a[i]);
  }
  for(int i = 1; i <= k; ++i){
    scanf("%d", &b[i]);
  }
  int x;
  int cnt1, cnt2;
  for(int i = 1; i <= n; ++i){
    scanf("%d", &cnt1);
    for(int j = 0; j < cnt1; ++j){
      scanf("%d", &x);
      c[i].pb(x);
    }
  }
  for(int i = 1; i <= n; ++i){
    scanf("%d", &cnt2);
    for(int j = 0; j < cnt2; ++j){
      scanf("%d", &x);
      d[i].pb(x);
    }
  } 
  for(int mask = 0; mask < (1 << n); ++mask){
    boo = false;
    for(int i = 0; i < n; ++i){
      if(bit(i, mask)){
        v.pb(i + 1);
      }      
    }
    rec(0);
    if(boo)
      ans = max(ans, __builtin_popcount(mask));  
    v.clear();
  }
  printf("%d", ans);
  return 0;
}