#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long 
#define ullong unsigned long long 
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

vector <int> a[MXN];
int n, m;

int main(){
  #define fn "A"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d", &n, &m);            
  for(int i = 0; i < m; ++i){
    int l, r, x;
    scanf("%d%d%d", &l, &r, &x);
    if(!x){
      for(int i = l; i <= r; ++i)
        a[i].pop_back();
    }
    else {
      for(int i = l; i <= r; ++i){
        a[i].pb(x);
      }
    }
  }
  for(int i = 1; i <= n; ++i){
    if(a[i].size())
      printf("%d ", a[i].back());
    else
      printf("0 ");
  }
  return 0;
}