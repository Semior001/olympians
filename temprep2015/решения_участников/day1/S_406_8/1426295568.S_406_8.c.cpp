#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long 
#define ullong unsigned long long 
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e6 + 10;
const double EPS = (double)1e-9;

int n;
int a[MXN];
bool boo[MXN];
int len;
int tmp[MXN];
int tmr;
llong ans;

int main(){
  #define fn "C"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d", &n);
  for(int i = 1; i <= n; ++i){
    scanf("%d", &a[i]);
  }  
  for(int i = 1; i <= n; ++i){
    ++tmr;
    fill(boo + 1, boo + n + 1, true);
    for(int j = i; j >= 1; --j){
      if(tmp[a[j]] != tmr){
        tmp[a[j]] = tmr;
        for(int k = i + 1; k <= n; ++k)
          if(a[k] == a[j])
            boo[k] = false;
      }
      for(int k = i + 1; k <= n + 1; ++k){
        if(boo[k]){
          len++;  
        }
        else {
          ans += (len * 1LL * (len + 1)) / 2LL;
          len = 0;
        }
      }  
    }
  }
  cout << ans;
  return 0;
}