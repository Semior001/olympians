#include <bits/stdc++.h>

#define F first
#define S second
#define llong long long 
#define ullong unsigned long long 
#define mp make_pair
#define pb push_back

using namespace std;

const llong LINF = (llong)1e18;
const int INF = (int)1e9 + 7;
const int MXN = (int)1e3 + 10;
const double EPS = (double)1e-9;

int n, m;
vector <int> t[4 * MXN];
int del[MXN];
bool ntp[MXN];

void push(int v){
  if(ntp[v]){
    for(int i = 0; i < t[v].size(); ++i){
      t[v + v].pb(t[v][i]); 
      t[v + v + 1].pb(t[v][i]);
    }
    ntp[v + v] = ntp[v + v + 1] = true;
    ntp[v] = false;
  }  
}

void upd(int l, int r, int val, int v = 1, int tl = 1, int tr = n){
  //cerr << tl << " " << tr << "\n"; 
  if(l > tr || tl > r)
    return;
  if(l <= tl && tr <= r){
    if(val){
      t[v].pb(val);
      ntp[v] = true;  
    }
    else {
      t[v].pop_back();
    }
    return; 
  }
  push(v);
  int tm = (tl + tr) / 2;
  upd(l, r, val, v + v,     tl,     tm); 
  upd(l, r, val, v + v + 1, tm + 1, tr);
}


int get(int pos, int v = 1, int tl = 1, int tr = n){
  push(v);
  //assert(tl <= tr);
  if(tl == tr)
    return (t[v].size() ? t[v].back() : 0);
  int tm = (tl + tr) / 2;
  if(pos <= tm)
    return get(pos, v + v,     tl,     tm);
  else
    return get(pos, v + v + 1, tm + 1, tr);
}

int main(){
  #define fn "A"
  #ifdef LOCAL
    freopen("input.txt", "r", stdin);
  #else
    freopen(fn".in", "r", stdin);
    freopen(fn".out", "w", stdout);
  #endif
  scanf("%d%d", &n, &m);
  int l, r, x;
  for(int i = 0; i < m; ++i){
    scanf("%d%d%d", &l, &r, &x);
    //if(x)
      upd(l, r, x);  
    /*for(int i = 1; i <= 10; ++i){
      for(auto it : t[i]){
        printf("%d ", it);
      }
      printf("\n");
    }
    cerr<< "WTF";*/
  }
  for(int i = 1; i <= n; ++i){
    printf("%d ", get(i));
  }
  return 0;
}