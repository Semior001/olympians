#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int N = 1e3 + 1;
const int INF = 1e9 + 7;
int n, m;
int sz[N], a[N][N];
vector<int> t[MXN * 4];

void push(int v) {
    if(!t[v].empty()) {
        t[v * 2] = t[v * 2 + 1] = t[v];
        t[v].clear();
    }
}

void update(int v, int tl, int tr, int l, int r, int val) {

    if(l > tr || r < tl) return ;
    if(l <= tl && tr <= r) {
        t[v].pb(val);
        push(v);
        return ;
    }push(v);
    int m = (tl + tr) / 2;
    update(v * 2, tl, m, l, r, val);
    update(v * 2 + 1, m + 1, tr, l, r, val);
}

void del(int v, int tl, int tr, int l, int r) {
    if(l > tr || r < tl) return ;
    if(l <= tl && tr <= r) {
        if(!t[v].empty()) {
            t[v].pop_back();
            push(v);
        }
        return ;
    }
    push(v);
    int m = (tl + tr) / 2;
    del(v * 2, tl, m, l, r);
    del(v * 2 + 1, m + 1, tr, l, r);
}

 int get (int v, int tl, int tr, int it) {
    if(it == tl && tr == it)
        return (t[v].empty() ? 0 : t[v].back());
    push(v);
    int m = (tl + tr) / 2;
    if(it <= m) return get (v * 2, tl, m, it);
    else return get (v * 2 + 1, m + 1, tr, it);
 }

int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
    for(int i = 1; i <= m; i++) {
        int l, r, type;
        scanf("%d%d%d", &l, &r, &type);
        if(type==0){
            del(1, 1, n, l, r);
        }
        else{
            update(1,1,n,l,r,type);
        }
    }
    for(int i =1;i<=n;i++)
        cout<<get(1,1,n,i)<<' ';
    return 0;
}

