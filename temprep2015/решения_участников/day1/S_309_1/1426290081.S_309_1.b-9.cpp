#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int INF = 1e9 + 7;
string st;
int k, n, res = INF;
int dp[MXN][26];

int getsum(int l, int r) {
    int sum = 0;
    for(int i = 0; i < 26; i++) {
        int now = dp[r][i];
        if(l) now -= dp[l - 1][i];
        sum += now;
    }
    return sum;
}

int bs(int start) {
    int lo = start, hi = n;
    while(hi - lo > 1) {
        int m = (lo + hi) / 2;
        if(getsum(start, m) >= k) lo = m;
        else hi = m;
    }
    if(getsum(start, lo) >= k) return lo;
    if(getsum(start, hi) >= k) return hi;
    return -1;
}

int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> st >> k;
    n = st.size();
    for(int i = 0; i < n; i++) {
        if(i)
            for(int lst = 0; lst < 26; lst++)
                dp[i][lst] = dp[i - 1][lst];
        dp[i][st[i] - 'a']++;
    }
    for(int i = 0; i < n; i++) {
        int get = bs(i);
        if(get != -1)
            res = min(res, get - i + 1);
    }
    if(res == INF) {
        cout << -1;
        return 0;
    }
    cout << res;
    return 0;
}
