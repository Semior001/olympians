#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int N = 1e3 + 1;
const int INF = 1e9 + 7;
int n, m;
int sz[N], a[N][N];

int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
    if(n <= 1000 && m <= 1000) {
        for(int i = 1; i <= m; i++) {
            int l, r, type;
            scanf("%d%d%d", &l, &r, &type);
            if(type == 0)
                for(int j = l; j <= r; j++)
                    sz[j]--;
            else
                for(int j = l; j <= r; j++)
                    a[j][++sz[j]] = type;
        }
        for(int i = 1; i <= n; i++) {
            if(sz[i] == 0) printf("0 ");
            else printf("%d ", a[i][sz[i]]);
        }
    }
    return 0;
}

