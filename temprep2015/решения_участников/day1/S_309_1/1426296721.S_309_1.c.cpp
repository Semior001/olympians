#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 5e3 + 100;
const int INF = 1e9 + 7;
int n, cnt;
int a[MXN], dp[MXN][MXN];
bool u[MXN];

int main() {
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);
    scanf("%d", &n);
    for(int i = 1; i <= n; i++)
        scanf("%d", &a[i]);
    if(n <= 50) {
        for(int last = 1; last < n; last++) {
            for(int j = last; j >= 1; j--) {
                u[a[j]] = 1;
                for(int r = last + 1; r <= n; r++) {
                    for(int rl = r; rl >= last + 1; rl--) {
                        if(u[a[rl]]) break;
                        cnt++;
                    }
                }
            }
        }
    }
    cout << cnt;
    return 0;
}
