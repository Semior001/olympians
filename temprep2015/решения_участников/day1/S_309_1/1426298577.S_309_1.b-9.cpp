#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int INF = 1e9 + 7;
string st;
int k, n, res = INF, g;
int dp[MXN][26];

int getsum(int l, int r) {
    int sum = 0;
    for(int i = 0; i < 26; i++)
        if(dp[r][i] - (l ? dp[l - 1][i] : 0) > 0)
            sum++;
    return sum;
}

int bs(int start) {
    int lo = start, hi = n - 1, m, cur;
    while(hi - lo > 1) {
        m = (lo + hi) / 2;
        cur = getsum(start, m);
        if(cur < k) lo = m;
        else hi = m;
    }
    if(getsum(start, lo) == k) return lo - start + 1;
    if(getsum(start, hi) == k) return hi - start + 1;
    return -1;
}

int main() {
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);
    cin >> s;
    scanf("%d", &k);
    n = st.size();
    for(int i = 0; i < n; i++) {
        if(i)
            for(int lst = 0; lst < 26; lst++)
                dp[i][lst] = dp[i - 1][lst];
        dp[i][st[i] - 'a']++;
    }
    for(int i = 0; i < n; i++) {
        g = bs(i);
        if(g != -1)
            res = min(res, g);
    }
    if(res == INF)
        printf("-1");
    else
        printf("%d", res);
    return 0;
}
