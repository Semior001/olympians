#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdlib>
#include <vector>

#define llong long long
#define pb push_back
#define mp make_pair
#define F first
#define S second

using namespace std;

const int MXN = 1e5 + 1;
const int N = 1e3 + 1;
const int INF = 1e9 + 7;
int n, m;
int sz[N], a[N][N];
vector<int> t[MXN * 2];

void push(int v) {
    if(!t[v].empty()) {
        t[v * 2] = t[v * 2 + 1] = t[v];
        t[v].clear();
    }
}

void update(int v, int tl, int tr, int l, int r, int val) {
    if(l > r) return ;
    if(l == tl && tr == r) {
        t[v].pb(val);
        return ;
    }
    push(v);
    int m = (tl + tr) / 2;
    update(v * 2, tl, m, l, min(m, r), val);
    update(v * 2 + 1, m + 1, tr, max(m + 1, l), r, val);
}

void del(int v, int tl, int tr, int l, int r) {
    if(l > r) return ;
    if(l == tl && tr == r && tl == tr) {
        t[v].pop_back();
        return ;
    }
    push(v);
    int m = (tl + tr) / 2;
    del(v * 2, tl, m, l, min(m, r));
    del(v * 2 + 1, m + 1, tr, max(m + 1, l), r);
}

int get (int v, int tl, int tr, int it) {
    if(it == tl && tr == it)
        return (t[v].empty() ? 0 : t[v].back());
    push(v);
    int m = (tl + tr) / 2;
    if(it <= m) return get (v * 2, tl, m, it);
    else return get (v * 2 + 1, m + 1, tr, it);
}

int main() {
    //freopen("A.txt", "r", stdin);
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin >> n >> m;
   /* if(n <= 1000 && m <= 1000) {
        for(int i = 1; i <= m; i++) {
            int l, r, type;
            scanf("%d%d%d", &l, &r, &type);
            if(type == 0)
                for(int j = l; j <= r; j++)
                    sz[j]--;
            else
                for(int j = l; j <= r; j++)
                    a[j][++sz[j]] = type;
        }
        for(int i = 1; i <= n; i++) {
            if(sz[i] == 0) printf("0 ");
            else printf("%d ", a[i][sz[i]]);
        }
    }
    else {*/
        for(int i = 1; i <= m; i++) {
            int l, r, type;
            scanf("%d%d%d", &l, &r, &type);
            if(type==0)
                del(1, 1, n, l, r);
            else
                update(1,1,n,l,r,type);
        }
        for(int i =1;i<=n;i++)
            cout<<get(1,1,n,i)<<' ';
   // }
    return 0;
}

