#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <set>
#include <map>
#include <string>
#include <cstring>

using namespace std;
#define fname "C"

int n, ans, a[5001], sz;
map <int, bool> w;

int main ()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> a[i];
	for (int len = 1; len <= n; ++len)
	{
		for (int l = 1; l <= n - len; ++l)
		{
			int r = l + len - 1;
			for (int j = l; j <= r; ++j)
				w[a[j]] = true;
            sz = 0;
			for (int i = r + 1; i <= n; ++i)
			{
				if (!w[a[i]])
					sz++;
				else
					ans += sz * (sz + 1) / 2, sz = 0;
			}
			ans += sz * (sz + 1) / 2;
			for (int j = l; j <= r; ++j)
				w[a[j]] = false;
		}
	}
	cout << ans;

	return  0;
}
