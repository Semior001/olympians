#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <set>
#include <vector>
#include <string>
#include <cstring>

using namespace std;
#define fname "A"

int n, m, l ,r, c;
vector <int> t[100001];

int main ()
{
	freopen(fname".in", "r", stdin);
	freopen(fname".out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; ++i)
	{
		cin >> l >> r >> c;
		if (c > 0)
			for (int j = l; j <= r; ++j)
				t[j].push_back(c);
		else
			for (int j = l; j <= r; ++j)
				t[j].erase(t[j].end() - 1);
	}
	for (int i = 1; i <= n; ++i)
		if (t[i].size() > 0)
			printf("%d ", t[i][t[i].size() - 1]);
		else printf("0 ");
	return  0;
}
