#include<iostream>
#include<cstdio>
#include<stack>
#include<algorithm>
using namespace std;
int n,m,l,r,c,mx,mn;
stack<int> a[555555];
int main()
{
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    cin>>n>>m;
    for(int k=1;k<=m;k++)
    {
        cin>>l>>r>>c;
        mx=max(l,r);
        mn=min(l,r);
        if(c==0)
        {
            for(int i=mn;i<=mx;i++)
            {
                a[i].pop();
            }
        }
        else
        {
            for(int i=mn;i<=mx;i++)
            {
                a[i].push(c);
            }
        }
    }
    for(int i=1;i<=n;i++)
    {
        if(!a[i].empty())cout<<a[i].top();
        else cout<<"0";
        cout<<" ";
    }
    return 0;
}
