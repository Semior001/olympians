#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int diff,n,a[1111],ans;
int fact(int v)
{
    int fact=1;
    for(int i=2;i<=v;i++)
    {
        fact*=i;
    }
    return fact;
}
int main()
{
    freopen("C.in","r",stdin);
    freopen("C.out","w",stdout);
    while(cin>>n)
    {
        for(int i=1;i<=n;i++)
        {
            cin>>a[i];
        }
        sort(a+1,a+n+1);
        for(int i=1;i<=n;i++)
        {
            if(a[i]!=a[i+1])diff++;
        }
        ans=fact(n*2)/(fact(n)*fact(n+1));
        if(n==1)
        {
            cout<<0<<endl;
            continue;
        }
        else if(diff!=n)
        {
            if(n>3&&(ans-3)-(2*(n-diff))>=0)cout<<(ans-3)-(2*(n-diff));
            else if(ans-3>=0) cout<<(ans-3);
            else if(n==2&&a[1]==a[2])
            {
                cout<<0;
            }
        }
        else cout<<ans;
        cout<<endl;
        diff=0;
    }
    return 0;
}
