#include<iostream>
#include<fstream>
#include<stack>
#include<vector>

using namespace std;

const int maxi=100000;

int n,m,
    L[maxi+10],R[maxi+10],C[maxi+10];
vector< stack<int> > otsek;

void in()
{
    ifstream cin("A.in");
    cin >>n >>m;
    for(int i=1;i<=m;i++)
    {
        cin >>L[i] >>R[i] >>C[i];
    }
}

void solution()
{
    otsek.resize(n+10);
    for(int c=1;c<=m;c++)
    {
        if(C[c]>0)
        {
          for(int i=L[c];i<=R[c];i++)
          {
              otsek[i].push(C[c]);
          }
        }
        else
        {
          for(int i=L[c];i<=R[c];i++)
          {
              otsek[i].pop();
          }
        }
    }
}

void out()
{
    ofstream cout("A.out");
    for(int c=1;c<=n;c++)
    {
        if(otsek[c].size()>0){cout <<otsek[c].top() <<" ";}
        else              {cout <<"0 ";}
    }
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
