#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

const int maks=5000;

int N,
    mas[maks+10],
    ans;

bool provka(int f1,int s1,int f2,int s2)
{
    vector<bool> vec(maks+10,0);
    for(int c=f1;c<s1;c++)
    {
        vec[mas[c]]=true;
    }
    for(int c=f2;c<s2;c++)
    {
        if(vec[mas[c]]){return 0;}
    }
    return 1;
}

void in()
{
    ifstream cin("C.in");
    cin >>N;
    for(int c=0;c<N;c++)
    {
        cin >>mas[c];
    }
}

void solution()
{
    for(int c=0;c<N;c++)
    {
        //cout <<"c=" <<c <<"\n";
        for(int v=c+1;v<=N;v++)
        {
            //cout <<" v=" <<v <<"\n";
            for(int cc=v;cc<N;cc++)
            {
                //cout <<"  cc=" <<cc <<"\n";
                for(int vv=cc+1;vv<=N;vv++)
                {
                    //cout <<c <<" " <<v <<" " <<cc <<" " <<vv <<"\n";
                    ans=ans+provka(c,v,cc,vv);
                }
            }
        }
    }
}

void out()
{
    ofstream cout("C.out");
    cout <<ans <<"\n";
}

int main()
{
    in();
    solution();
    out();
    return 0;
}
