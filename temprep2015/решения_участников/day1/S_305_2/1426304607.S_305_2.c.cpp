#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
int n, a[MAXN], ans, d[MAXN], clr = 1, dp[MAXN];

int get (int x) {
	return (x * (x + 1) / 2);
}

int main () {
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
		dp[a[i]]++;
	}
	for (int x = 1; x < n; ++x) {
		for (int i = x; i < n; ++i) {
			int cnt = 0, kk[5555] = {0};
			for (int j = x; j <= i; ++j) {
				kk[a[j]]++;
			}
			for (int j = x; j <= i; ++j) {
				cnt += dp[a[j]] - kk[a[j]];
			}
			ans += get (n - i - cnt);
			cout << x << " "<< i << "-->" << get (n - i - cnt) << ' ' << cnt << '\n';
		}
		//dp[a[x]]--;
	}
	cout << ans;

	return 0;
}