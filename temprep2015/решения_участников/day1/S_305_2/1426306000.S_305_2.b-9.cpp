#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <set>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
string s;
int d[MAXN], k, mn = 1e9, seen[MAXN];

bool check (int dist) {
	for (int i = 0; i + dist - 1 < s.size(); ++i) {
		set < char > ch;
		for (int j = i; j < i + dist; ++j) {
			ch.insert (s[j]);
		}
		if (ch.size() == k)
			return true;
	}
	return false;
}

int binsearch () {
	int l = 0, r = s.size() + 1;
	while (r - l > 1) {
		int mid = (l + r) >> 1;
		if (check (mid))
			r = mid;
		else
			l = mid;
	}
	if (check (l))
		return l;
	return -1;
}

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;

	cout << binsearch ();

	return 0;
}