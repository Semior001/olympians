#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, m, p[MAXN * 5], t[MAXN * 5];
vector < int > query;

void push (int x, int sz) {
	if (p[x] != -1) {
		if (sz != -1)
			p[x + x] = p[x + x + 1] = p[x];
		t[x] = p[x] * sz;
		p[x] = -1;
	}
}

void upd (int x, int l, int r, int xl, int xr, int val) {
	if (xl > r || xr < l)
		return;
	push (x, xr - xl + 1);
	if (xl >= l && xr <= r) {
		p[x] = val;
		push (x, xr - xl + 1);
		return;
	}
	int mid = (xl + xr) >> 1;
	upd (x + x, l, r, xl, mid, val);
	upd (x + x + 1, l, r, mid + 1, xr, val);
	t[x] = t[x + x] + t[x + x + 1];
}

int get (int x, int pos, int xl, int xr) {
	if (xl > xr)
		return 0;
	push (x, xr - xl + 1);
	if (xl == xr) {
		return t[x];
	}
	int mid = (xl + xr) >> 1;
	if (pos <= mid) {
		return get (x + x, pos, xl, mid);
	}
	else
		return get (x + x + 1, pos, mid + 1, xr);
}

int main () {
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);

	cin >> n >> m;

	while (m--) {
		int L, R, C;
		cin >> L >> R >> C;
		if (!C) {
			query.pop_back();
			upd (1, L, R, 1, n, query.back());
		}
		else {
			upd (1, L, R, 1, n, C);
			query.pb (C);
		}
	}
	for (int i = 1; i <= n; ++i) {
		cout << get (1, i, 1, n) << ' ';
	}

	return 0;
}