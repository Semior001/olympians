#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, m, p[MAXN * 5], t[MAXN * 5];
vector < int > query[MAXN];

int main () {
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);

	cin >> n >> m;

	while (m--) {
		int L, R, C;
		cin >> L >> R >> C;
		if (C == 0) {
			for (int i = L; i <= R; ++i)
				query[i].pop_back ();
				continue;
		}
		for (int i = L; i <= R; ++i)
	   		query[i].pb (C);
	}
	for (int i = 1; i <= n; ++i) {
	    if (query[i].empty())
	    	query[i].pb (0);
		cout << query[i].back() << ' ';
	}

	return 0;
}