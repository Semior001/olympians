#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;

int n, m, p[MAXN * 5], t[MAXN * 5];
vector < int > query;

int main () {
	//freopen ("A.in", "r", stdin);
	//freopen ("A.out", "w", stdout);

	cin >> n >> m;

	query.pb (0);

	while (m--) {
		int L, R, C;
		cin >> L >> R >> C;
		if (!C)
		 query.pop_back ();
	   	else
	   		query.pb (C);
	   	for (int i = L; i <= min (n, R); ++i)
	   		t[i] = query.back();
	}
	for (int i = 1; i <= n; ++i)
		cout << t[i] << ' ';

	return 0;
}