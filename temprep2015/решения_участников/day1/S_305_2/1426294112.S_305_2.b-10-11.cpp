#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <unordered_set>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
string s;
int d[MAXN], k, mn = 1e9;
vector < int > pos[MAXN];

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;

	d[0] = 1;

	for (int i = 1; i < int (s.size()); ++i) {
		d[i] = d[i - 1] + 1;
		if (s[i] == s[i-1])
			d[i]--;
	}
	for (int i = 0; i < s.size(); ++i) {
		for (int j = i; j < s.size(); ++j) {
			unordered_set < char > dl;
			for (int x = i; x <= j; ++x)
				dl.insert (s[x]);
			if (dl == k)
				mn = min (mn, j - i + 1);
		}
	}
	if (mn == 1e9)
		mn = 1;
	cout << mn;

	return 0;
}