#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <set>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
string s;
int d[MAXN], k, mn = 1e9, seen[MAXN];
vector < int > pos[MAXN];

bool check (int dist) {
	for (int i = 0; i + dist - 1 < s.size(); ++i) {
		set < char > ch;
		for (int j = i; j < i + dist; ++j) {
			ch.insert (s[j]);
		}
		//cout << i << ' ' << i + dist - 1 << ' '<<ch.size() << "<--" << '\n';
		if (ch.size() == k)
			return true;
	}
	return false;
}

int binsearch () {
	int l = 1, r = s.size();
	while (r - l > 1) {
		int mid = (l + r) >> 1;
		cout << l << ' '<< r << ' '<<mid << ' '<< check (mid) << '\n';
		if (check (mid))
			r = mid;
		else
			l = mid;
	}
	return r;
}

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;

	d[0] = 1;
	seen[s[0] - 'a'] = 1;
	pos[1].pb (0);
	set < char > bn;
	for (int i = 0; i < int (s.size()); ++i) {
		bn.insert (s[i]);
	}
	if (bn.size() < k)
		cout << -1, exit (0);
	cout << binsearch ();

	return 0;
}