#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>

#define pb push_back
#define all(v) v.begin(), v.end()
#define mp make_pair

using namespace std;

const int MAXN = 1e5 + 256;
string s;
int d[MAXN], k, mn = 1e9;
vector < int > pos[MAXN];

int main () {
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> s >> k;

	d[0] = 1;

	for (int i = 1; i < int (s.size()); ++i) {
		d[i] = d[i - 1] + 1;
		if (s[i] == s[i-1])
			d[i]--;
	}
	for (int i = 0; i < int (s.size()); ++i) {
		pos[d[i]].pb (i);
		if (d[i] - k + 1 <= 0) continue;
		for (int j = pos[d[i] - k + 1].size() - 1; j >= 0; --j) {
			if (pos[d[i] - k + 1][j] <= i) {
				mn = min (mn, i - pos[d[i] - k + 1][j] + 1);
			}
		}
	}
	if (mn == 1e9)
		mn = 1;
	cout << mn;

	return 0;
}