#include <fstream>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");

char s[100000];
int l;
int k;
int used[26];
int lmin=1000000;
int n;
int x;

int main()
{
    fin>>s>>k;
    if(k==1)
    {
        fout<<1;
        return 0;
    }
    for(;s[n]!=0;n++);
    for(int i=0;i<n;i++)
    {
        while(s[i]==s[i+1])i++;
        l=0;
        x=0;
        for(int j=0;j<26;j++)
            used[j]=0;
        for(int j=i;j<n && x<k;j++)
        {
            l++;
            int p=s[j]-97;
            while(used[p]==1)
            {
                j++;
                l++;
                p=s[j]-97;
            }
            used[p]=1;
            x++;
            if(x==k && l<lmin)
            {
                lmin=l;
            }
        }
    }
    if(lmin==1000000)fout<<-1;
    else fout<<lmin;
    fin.close();
    fout.close();
    return 0;
}
