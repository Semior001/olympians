#include <fstream>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");

int n;
int a[5000];
int used[5001];
int x;

int main()
{
    fin>>n;
    for(int i=0;i<n;i++)fin>>a[i];
    for(int p=1;p<n;p++)
    {
        for(int l=1;l<=n-p;l++)
        {
            for(int i=p;i<n;i++)
                used[a[i]]=0;
            for(int k=0;k<p;k++)
                used[a[k]]=1;
            for(int i=p-1;i<n-l;i++)
            {
                used[a[i]]=1;
                int w=0;
                for(int k=1;k<=l && k+i+w<n;k++)
                    if(used[a[k+i+w]]==1)
                    {
                        w+=k;
                        k=1;
                    }
                for(int j=i+l+w;j<n;j++)
                {
                    if(used[a[j]]==0)
                    {
                        fout<<"YES"<<" ";
                        for(int q=i-p+1;q<=i;q++)
                            fout<<a[q];
                        fout<<"+";
                        for(int q=j-l+1;q<=j;q++)
                            fout<<a[q];
                        x++;
                        fout<<endl;
                    }
                    else
                    {
                        fout<<"NO"<<" ";
                        for(int q=i-p+1;q<=i;q++)
                            fout<<a[q];
                        fout<<"+";
                        for(int q=j-l+1;q<=j;q++)
                            fout<<a[q];
                        fout<<endl;
                        for(int k=0;k<=l;k++)
                            if(used[a[j+k]]==1)
                            {
                                j++;
                                k=0;
                            }
                    }
                }
                used[a[i-p+1]]=0;
            }
        }
    }
    fout<<x;
    fin.close();
    fout.close();
    return 0;
}
