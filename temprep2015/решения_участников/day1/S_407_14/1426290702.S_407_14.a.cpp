#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<vector>
#include<stack>
#include<algorithm>

using namespace std;

int N, M, l, r, c;
stack<int> s[100010];
int main()
{
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	cin>>N>>M;
	for(int i = 1; i <= M; i++)
	{
		cin>>l>>r>>c;
		if(c == 0)
			for(int j = l; j <= r; j++)
				s[j].pop();
		else	for(int j = l; j <= r; j++)
				s[j].push(c);	
	}
	for(int i = 1; i <= N; i++)
	{
		if(s[i].empty())
			cout<<"0 ";
		else	cout<<s[i].top()<<" ";
	}	

	return 0;
}