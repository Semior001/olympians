#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))
#define sz(x) (int((x).size()))

#define TASK "C"

using namespace std;

int a[6666];
bool used[6666];
long long ans;

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);
	
	int n;
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> a[i];
   	for (int l1 = 1; l1 < n; ++l1)
   	{
   		memset(used, 0, sizeof used);
   	 	for (int r1 = l1; r1 < n; ++r1)
   	 	{
   	 	 	used[a[r1]] = 1;
   	 		int l2 = r1 + 1;
   	 		while (l2 <= n)
   	 		{
   	 	        if (used[a[l2]])
   	 	        {
   	 	        	l2++;
   	 	        	continue;
   	 	        }
   	 			int r2;
   	 	 		for (r2 = l2; r2 <= n; ++r2)
   	 	 			if (used[a[r2]]) break;
   	 	 		r2--;
   	 	 		ans += (((r2 - l2 + 2) * (r2 - l2 + 1)) / 2);
   	 	 		
   	 	 		l2 = r2 + 2;
   	 		}
   	 	}
   	}
	cout << ans;	
 	return 0;
}