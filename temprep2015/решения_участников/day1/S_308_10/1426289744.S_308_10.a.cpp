#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))
#define sz(x) (int((x).size()))

#define TASK "A"

using namespace std;

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);
	int n, m;	
	cin >> n >> m;
	vector< stack<int> > v(n + 1);
	for (int i = 0; i < m; ++i)
	{
		int l, r, c;
		cin >> l >> r >> c;
		if (c)
			for (int j = l; j <= r; ++j)
		 		v[j].push(c);
		else
			for (int j = l; j <= r; ++j)
				v[j].pop();
	}
	for (int i = 1; i < sz(v); ++i)
	{
		if (sz(v[i]))
			cout << v[i].top() << ' ';
	    else	
	    	cout << 0 << ' ';
 	}
 	return 0;
}