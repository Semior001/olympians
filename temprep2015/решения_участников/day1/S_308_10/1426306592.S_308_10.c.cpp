#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))
#define sz(x) (int((x).size()))

#define TASK "C"

using namespace std;

int a[6666], z[6666];
int used[6666], USED[6666];
long long ans;

int main()
{
	ios_base::sync_with_stdio(false);
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);
	
	int n;
	cin >> n;
	for (int i = 1; i <= n; ++i)
	{
		cin >> a[i];
		used[a[i]]++;
		USED[a[i]]++;
   	}
   	for (int i = n; i >= 1; --i)
   	{
		used[a[i]]--;
		if (!used[a[i]])
			z[i] = z[i + 1] + USED[a[i]];				 		 	
   	}
   	for (int i = 2; i <= n; ++i)
   	{
   	 	ans += ((i)*(i-1)/2) * (z[i]*(z[i]+1)/2);
   	}
	cout << ans;	
 	return 0;
}