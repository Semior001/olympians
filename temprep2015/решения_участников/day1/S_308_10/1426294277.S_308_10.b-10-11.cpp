#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))
#define sz(x) (int((x).size()))

#define TASK "B"

using namespace std;

int n, m, k, c[1555], p[1555], al[1555], ar[1555], bl[1555], br[1555], ans, tc[1555], tp[1555];
vector<int> a[1555], A[1555], b[1555], B[1555];
bool used[1555], Q[1555], W[1555];

bool khun1(int v, int from = 0)
{
	if (Q[v])
		return false;
	if (v == 0 || tc[from] > 0)
		return true;
	for (int i = 0; i < sz(a[v]); ++i)
	 	if (c[a[v][i]] > 0 && khun1(ar[a[v][i]], a[v][i]))
	 	{
	 		al[v] = a[v][i];
	 		ar[a[v][i]] = v;
	 		tc[a[v][i]]--;
	 		return true;
		}
	return false;
}

bool khun2(int v, int from = 0)
{
	if (W[v])
		return false;
	if (v == 0 || tp[from] > 0)
		return true;
	for (int i = 0; i < sz(b[v]); ++i)
	 	if (p[b[v][i]] > 0 && khun2(br[b[v][i]], b[v][i]))
	 	{
	 		bl[v] = b[v][i];
	 		br[b[v][i]] = v;
	 		tp[b[v][i]]--;
	 		return true;
		}
	return false;
}

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout); 	
	cin >> n >> m >> k;
	for (int i = 1; i <= m; ++i)
		cin >> c[i];
	for (int i = 1; i <= k; ++i)
		cin >> p[i];
	for (int i = 1; i <= n; ++i)
	{
		int x, a_i;
	 	cin >> x;
	 	while(x--)
	 	{
	 	 	cin >> a_i;
	 	 	a[i].pb(a_i);
	 	 	A[a_i].pb(i);
	 	}
    }
    for (int i = 1; i <= n; ++i)
	{
		int x, b_i;
	 	cin >> x;
	 	while (x--)
	 	{
	 	 	cin >> b_i;
	 	 	b[i].pb(b_i);
	 		B[b_i].pb(i);
	 	}
    }
    bool has = 1;
    while (has)
    {
    	has = 0;
    	for (int i = 1; i <= n; ++i)
    	{
    		if (Q[i]) continue;
    		for (int i = 1; i <= m; ++i)
    			tc[i] = c[i];
    	 	khun1(i);
	    }
	    for (int i = 1; i <= n; ++i)
	    {
	     	if (!Q[i] && al[i])
	     	{
	     		c[al[i]]--;
	     		has = Q[i] = true;
	     	}
	    }	
    }
    has = 1;
    while (has)
    {
     	has = 0;
    	for (int i = 1; i <= n; ++i)
    	{
    		if (W[i]) continue;
           	for (int i = 1; i <= k; ++i)
           		tp[i] = p[i];
    	 	khun2(i);
	    }
	    for (int i = 1; i <= n; ++i)
	    {
	     	if (!W[i] && bl[i])
	     	{
	     		p[bl[i]]--;
	     		has = W[i] = true;
	     	}
	    }	
    }                           
 	for (int i = 1; i <= n; ++i)
 	{
 		ans += (Q[i] && W[i]) ? 1 : 0;
// 		cout << i << ' ' << al[i] << ' ' << bl[i] << endl;
 	}
 	cout << ans;
 	return 0;
}