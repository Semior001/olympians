#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstdlib>

#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define sqr(x) ((x)*(x))

#define TASK "A"

using namespace std;

struct Treap
{
 	Treap *L, *R;
 	int y, sz;
 	stack<int> val;
 	int com;
 	Treap(const stack<int> &x) : L(NULL), R(NULL), com(0), y((rand()<<15)|rand()), sz(1), val(x) {}
	Treap(){} 	
};

typedef Treap* TPtr;

int size(TPtr T)
{
 	if (!T)
 		return 0;
   	return T->sz;
}

void recalc(TPtr T)
{
 	if (!T)
 		return;
 	T->sz = size(T->L) + size(T->R) + 1;
}

void push(TPtr T)
{
	if (!T)
		return;
	if (T->com == 0)
		return;
	if (T->L)
	{
		T->L->com = T->com;
		if (T->com == -1)
			T->L->val.pop();
	    else
	    	T->L->val.push(T->com);
	}
	if (T->R)
	{
		T->R->com = T->com;
		if (T->com == -1)
			T->R->val.pop();
	    else
	    	T->R->val.push(T->com);
	}
	T->com = 0;
}

TPtr merge(TPtr L, TPtr R)
{
	push(L);
	push(R);
 	if (!L)
 		return R;
   	if (!R)
   		return L;
   	if (L->y > R->y)
   	{
   	 	L->R = merge(L->R, R);
   	 	recalc(L);
   	 	push(L);
   	 	push(R);
   	 	return L;
   	}
   	else
   	{
   	 	R->L = merge(L, R->L);
   	 	recalc(R);
   	 	push(L);
   	 	push(R);
   	 	return R;
   	}
}

void split(TPtr T, int x, TPtr &L, TPtr &R)
{           
 	if (!T)
 	{
 	 	L = R = NULL;
 	 	return;
 	}       
	push(T);
 	int cur = size(T->L) + 1;
 	if (x < cur)
 	{
 	 	split(T->L, x, L, T->L);
 	 	R = T;
 	}
 	else
   	{
 	 	split(T->R, x - cur, T->R, R);
 	 	L = T;
 	}
 	recalc(T);
 	push(L);
 	push(R);
}

void add(TPtr &T, int l, int r, int c)
{
	if (!T)
		return;
	TPtr L = NULL, M = NULL, R = NULL;
	split(T, r, M, R);
	split(M, l - 1, L, M);
	if (M)
	{
		M->com = c;
		M->val.push(c);
	}
	T = merge(L, merge(M, R));
}

void pop(TPtr &T, int l, int r)
{
 	if (!T)
 		return;
 	TPtr L = NULL, M = NULL, R = NULL;
	split(T, r, M, R);
	split(M, l - 1, L, M);
	if (M)
	{
		M->com = -1;
		M->val.pop();
	}
	T = merge(L, merge(M, R));
}

void print(TPtr T)
{
 	if (!T)
 		return;
    push(T);
	print(T->L);
	if (T->val.size() > 0)
	 	cout << T->val.top() << ' ';
 	else
 		cout << 0 << ' ';
 	print(T->R);	
}

int main()
{
	freopen(TASK".in", "rt", stdin);
	freopen(TASK".out", "wt", stdout);
	int n, m;	
	cin >> n >> m;
	TPtr T = NULL;
	stack<int> s;
	s.push(0);
	for (int i = 0; i < n; ++i)
		T = merge(T, new Treap(s));
	for (int i = 0; i < m; ++i)
	{
		int l, r, c;
	 	cin >> l >> r >> c;
	 	if (c)
	 		add(T, l, r, c);
	    else
	    	pop(T, l, r);
 		/*TPtr L = NULL, M = NULL, R = NULL;
		split(T, r, M, R);
		split(M, l - 1, L, M);
		T = merge(M, merge(L, R));	*/    
	}
	print(T);
	return 0;
}