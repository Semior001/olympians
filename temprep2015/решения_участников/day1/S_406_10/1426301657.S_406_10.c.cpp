#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
#include <set>
using namespace std;
int n,a[6000];
int used[6000];
pair<int,int> d[6000];
inline long long f(int x){
  return (x*(x+1))/2;
}
long long ans=0;
int main(){             
  freopen("C.in","r",stdin);
  freopen("C.out","w",stdout);
  scanf("%d",&n);
  for (int i=1;i<=n;i++){
  		scanf("%d",&a[i]);
  	    if (used[a[i]])d[used[a[i]]].first=i;
  	    used[a[i]]=i;
 }
  for(int i=1;i<=n;i++){
  	if (!d[i].first)d[i].first=n+1;
  	if (!used[a[i]]||used[a[i]]==i)d[i].second=d[i].first;
  	else d[i].second=used[a[i]];
  }
  for(int i=1;i<n;i++){
   set<pair<int,int> > s;
   s.insert(make_pair(n+1,n+1));
    for (int j=i;j<n;j++){
       s.insert(d[j]);
       while(s.begin()->first<=j)s.erase(s.begin());
       int last=j;
       for(set<pair<int,int> >::iterator it=s.begin();it!=s.end();it++){
          if ((it->first-1)-last>=0){
          ans+=f((it->first-1)-last);
          }
          last=it->second;
       }
    }
  }
  cout<<ans;
  return 0;
}
