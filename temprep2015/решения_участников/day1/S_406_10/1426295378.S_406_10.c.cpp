#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
#include <set>
using namespace std;
int n,a[6000];
int d[6000],used[6000];
long long f(int x){
  return (x*(x+1))/2;
}
long long ans=0;
int main(){             
  freopen("C.in","r",stdin);
  freopen("C.out","w",stdout);
  scanf("%d",&n);
  for (int i=1;i<=n;i++){
  		scanf("%d",&a[i]);
  	    if (used[a[i]])d[used[a[i]]]=i;
  	    used[a[i]]=i;
 }
  for(int i=1;i<=n;i++)if (!d[i])d[i]=n+1;
  for(int i=1;i<n;i++){
   set<int> s;
    for (int j=i;j<n;j++){
       s.insert(d[j]);
       while(*s.begin()<=j)s.erase(*s.begin());
       ans+=f((*s.begin()-1)-j);
    }
  }
  cout<<ans;
  return 0;
}
