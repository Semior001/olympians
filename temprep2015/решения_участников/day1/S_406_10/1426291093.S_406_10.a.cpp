#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
int n,m;
vector<int> a[150000],t[600000];
void add(int v){
  if (t[v].size()){
    while(t[v].size()&&t[v][t[v].size()-1]==-1){
      if (t[v+v].size())t[v+v].pop_back();
      else t[v+v].push_back(-1);
      if (t[v+v+1].size())t[v+v+1].pop_back();
      else t[v+v+1].push_back(-1);
      t[v].pop_back();
    }
    for (int i=0;i<t[v].size();i++){
      t[v+v].push_back(t[v][i]);
      t[v+v+1].push_back(t[v][i]);
    }
    t[v].resize(0);
  }
}
void upd(int l,int r,int x,int v=1,int tl=1,int tr=n){
  if (tl>tr||tr<l||tl>r)return;
  if (l<=tl&&tr<=r){
      t[v].push_back(x);
  }
  else{
    int mid=(tl+tr)/2;
	add(v);
    upd(l,r,x,v+v,tl,mid);
    upd(l,r,x,v+v+1,mid+1,tr);
  }
}
void del(int l,int r,int v=1,int tl=1,int tr=n){
  if (tl>tr||tr<l||tl>r)return;
  if (l<=tl&&tr<=r){
      if (!t[v].size()||t[v][t[v].size()-1]==-1)t[v].push_back(-1);
      else t[v].pop_back();
  }
  else{
    int mid=(tl+tr)/2;
	add(v);
	del(l,r,v+v,tl,mid);
	del(l,r,v+v+1,mid+1,tr);
  }
}
void get(int x,int v=1,int l=1,int r=n){
  if(l>r||x<l||x>r)return;
  if (l==r){
    if (t[v].size())printf("%d ",t[v][t[v].size()-1]);
    else printf("0 ");
    return;
  }
  else{
    int mid=(l+r)/2;
    add(v);
    get(x,v+v,l,mid);
    get(x,v+v+1,mid+1,r);
  }
}
int main(){             
  freopen("A.in","r",stdin);
  freopen("A.out","w",stdout);
  scanf("%d%d",&n,&m);
  for (int i=1;i<=m;i++){
    int l,r,x;
    scanf("%d%d%d",&l,&r,&x);
    if (x)upd(l,r,x);
    else del(l,r);
  }
  for (int i=1;i<=n;i++){
    get(i);
  }
  return 0;
}
