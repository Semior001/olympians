#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <vector>
#include <map>
using namespace std;
int n,m;
int t[450000];
void pop(int v){
      if (t[v]<=0)t[v]--;
}
void add(int v){
  if (t[v]!=0){
  	if (t[v]>0){
  	  if (t[v+v]<0){
  	    t[v+v]++;
  	  }
  	  else if(t[v+v]==0)t[v+v]=t[v];
  	}
  	else{
  	  if (t[v+v]<=0)t[v+v]--;
  	}
  	t[v]=0;
  }
}
void upd(int l,int r,int x,int v=1,int tl=1,int tr=n){
  if (tl>tr||tr<l||tl>r)return;
  if (l<=tl&&tr<=r){
  	if (t[v]==0)t[v]=x;
  	else{
  	  if (t[v]<0)t[v]++;
  	}
  }
  else{
    int mid=(tl+tr)/2;
	add(v);
    upd(l,r,x,v+v,tl,mid);
    upd(l,r,x,v+v+1,mid+1,tr);
  }
}
void del(int l,int r,int v=1,int tl=1,int tr=n){
  if (tl>tr||tr<l||tl>r)return;
  if (l<=tl&&tr<=r){
      pop(v);
  }
  else{
    int mid=(tl+tr)/2;
	add(v);
	del(l,r,v+v,tl,mid);
	del(l,r,v+v+1,mid+1,tr);
  }
}
void get(int v=1,int l=1,int r=n){
  if(l>r)return;
  if (l==r){
    if (t[v]>0)printf("%d ",t[v]);
    else printf("0 ");
    return;
  }
  else{
    int mid=(l+r)/2;
    add(v);
    get(v+v,l,mid);
    get(v+v+1,mid+1,r);
  }
}
struct node{
  int l,r,x;
}zapros[200000];
int main(){             
  freopen("A.in","r",stdin);
  freopen("A.out","w",stdout);
  scanf("%d%d",&n,&m);
  for (int i=1;i<=m;i++){
    int l,r,x;
    scanf("%d%d%d",&zapros[i].l,&zapros[i].r,&zapros[i].x);
  }
  for (int i=m;i>=1;i--){
    if (zapros[i].x)upd(zapros[i].l,zapros[i].r,zapros[i].x);
    else del(zapros[i].l,zapros[i].r);
    }
  get();
  return 0;
}
