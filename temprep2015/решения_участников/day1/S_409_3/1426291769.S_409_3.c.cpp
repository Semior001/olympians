#include <bits/stdc++.h>
using namespace std;
const int MaxN = 1e4 + 17;
int n, a[MaxN], u[MaxN];
long long Sum, S;
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	cin >> n;
    for (int i = 1; i <= n; ++ i)
        cin >> a[i];
    for (int i = 1; i <= n; ++ i)
    {
        for (int len = 0; len < n; ++ len)
        {
            u[a[i + len]] = 1;
            S = 0;
            for (int j = i + len + 1; j <= n; ++ j)
            {
                if (u[a[j]])
                    S = 0;
                else
                {
                    S ++;
                    Sum += S;
                }
            }
        }
        for (int j = 1; j <= n; ++ j)
            u[a[j]] = 0;
    }
    cout << Sum;
    return 0;
}
