#include <bits/stdc++.h>
using namespace std;
const int MaxN = 4e5 + 17;
int n, m, l, r, c;
vector < int > V[MaxN];
struct Node
{
    int del, add, Td, Tadd, last;
    Node ()
    {
        Td = 0;
        last = 0;
        Tadd = 0;
        del = 0;
        add = 0;
    }
}t[MaxN];
void build (int v, int l, int r)
{
    if (l == r)
        V[v].push_back (0);
    else
    {
        int m = (l + r) >> 1;
        build (v + v, l, m);
        build (v + v + 1, m + 1, r);
        V[v].push_back (0);
    }
}
void Dl (int v)
{
    V[v].pop_back (), t[v].last = V[v][V[v].size () - 1];
    t[v + v].del = t[v].del;
    t[v + v + 1].del = t[v].del;
    t[v].del = 0;
    t[v].Td = 0;
}
void Ad (int v)
{
    V[v].push_back (t[v].add);
    t[v].last = t[v].add;
    t[v + v].add = t[v].add;
    t[v + v + 1].add = t[v].add;
    t[v].add = 0;
    t[v].Tadd = 0;
}
void push (int v)
{
    if (t[v].del && t[v].add)
    {
        if (t[v].Tadd > t[v].Td)
            Dl (v), Ad (v);
        else
            Ad (v), Dl (v);
    }
    if (t[v].add)
        Ad (v);
    if (t[v].del)
        Dl (v);
}
void Upd (int v, int l, int r, int L, int R, int T)
{
    push (v);
    if (l > R || r < L)
        return;
    if (L <= l && r <= R)
    {
        t[v].del = 1;
        t[v].Td = T;
        push(v);
        return;
    }
    int m = (l + r) >> 1;
    Upd (v + v, l, m, L, R, T);
    Upd (v + v + 1, m + 1, r, L, R, T);
}
void UpD (int v, int l, int r, int L, int R, int w, int T)
{
    push (v);
    if (l > R || r < L)
        return;
    if (L <= l && r <= R)
    {
        t[v].add = w;
        t[v].Tadd = T;
        push(v);
        return;
    }
    int m = (l + r) >> 1;
    UpD (v + v, l, m, L, R, w, T);
    UpD (v + v + 1, m + 1, r, L, R, w, T);
}
int Get (int v, int l, int r, int L, int R)
{
    push (v);
    if (l > R || r < L)
        return 0;
    if (L <= l && r <= R)
        return t[v].last;
    int m = (l + r) >> 1;
    return max (Get (v + v, l, m, L, R), Get (v + v + 1, m + 1, r, L, R));
}
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	cin >> n >> m;
	build (1, 1, n);
	for (int i = 1; i <= m; ++ i)
	{
		cin >> l >> r >> c;
		if (!c)
		{
			Upd (1, 1, n, l, r, i);
		}
		else
		{
            UpD (1, 1, n, l, r, c, i);
		}
	}
	for (int i = 1; i <= n; ++ i)
    {
        cout << Get (1, 1, n, i, i) << ' ';
    }
    return 0;
}
