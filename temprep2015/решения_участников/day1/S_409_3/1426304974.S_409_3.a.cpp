#include <bits/stdc++.h>
using namespace std;
const int MaxN = 1e5 + 17;
int n, m, l, r, c;
vector < int > v[MaxN];
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; ++ i)
	{
		cin >> l >> r >> c;
		if (!c)
		{
			for (int j = l; j <= r; ++ j)
				v[j].pop_back ();
		}
		else
		{
			for (int j = l; j <= r; ++ j)
				v[j].push_back (c);
		}
	}
	for (int i = 1; i <= n; ++ i)
    {
        if (v[i].size() > 0)
            cout << v[i][v[i].size() - 1] << ' ';
        else
            cout << 0 << ' ';
    }
    return 0;
}
