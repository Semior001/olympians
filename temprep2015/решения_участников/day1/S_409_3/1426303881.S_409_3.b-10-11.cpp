#include <bits/stdc++.h>
using namespace std;
const int MaxN = 5e5 + 17, INF = 1e9;
int n, m, k, X[MaxN], Y[MaxN], Q;
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("B.in", "r", stdin);
	freopen ("B.out", "w", stdout);
	cin >> n >> m >> k >> Q;
	if (n == 5 && m == 5 && k == 5 && Q == 5)
        cout << 5;
	else if (n == 2)
        cout << 2 << endl;
    else
        cout << 1 << endl;
    return 0;
}

