#include <bits/stdc++.h>
using namespace std;
const int MaxN = 1e4 + 17;
int n, a[MaxN], u[MaxN], Sum;
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("C.in", "r", stdin);
	freopen ("C.out", "w", stdout);
	cin >> n;
    for (int i = 1; i <= n; ++ i)
        cin >> a[i];
    for (int i = 1; i <= n; ++ i)
    {
        for (int len = 0; len < n; ++ len)
        {
            u[a[i + len]] = 1;
            for (int j = i + len + 1; j <= n; ++ j)
            {
                for (int l = 0; l + j <= n; ++ l)
                {
                    if (u[a[j + l]])
                        break;
                    else
                        Sum ++;
                }
            }
        }
        for (int j = 1; j <= n; ++ j)
            u[a[j]] = 0;
    }
    cout << Sum;
    return 0;
}
