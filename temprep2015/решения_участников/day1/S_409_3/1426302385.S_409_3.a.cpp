#include <bits/stdc++.h>
using namespace std;
const int MaxN = 5e5 + 17, INF = 1e9;
int n, m, l, r, c;
vector < int > V[MaxN], add[MaxN];
struct Node
{
    int del, Td, Tadd, last;
    Node ()
    {
        last = 0;
        Td = 0;
        Tadd = 0;
        del = 0;
    }
}t[MaxN];
void build (int v, int l, int r)
{
    if (l == r)
        V[v].push_back (0);
    else
    {
        int m = (l + r) >> 1;
        build (v + v, l, m);
        build (v + v + 1, m + 1, r);
        V[v].push_back (0);
    }
}
void Dl (int v)
{
    for (int i = 1; i <= t[v].del; ++ i)
    {
        V[v].pop_back ();
        t[v].last = V[v][V[v].size () - 1];
        t[v + v].del ++;
        t[v + v + 1].del ++;
    }
    t[v].del = 0;
    t[v].Td = 0;
}
void Ad (int v)
{
    for (int i = 0; i < add[v].size(); ++ i)
    {
        V[v].push_back (add[v][i]);
        t[v].last = add[v][i];
        add[v + v].push_back (add[v][i]);
        add[1 + v + v].push_back (add[v][i]);
        //add[v].pop_back();
    }
    add[v].clear ();
    t[v].Tadd = 0;
}
void push (int v)
{
    if (t[v].del && add[v].size() >= 1)
    {
        if (t[v].Tadd > t[v].Td)
            Dl (v), Ad (v);
        else
            Ad (v), Dl (v);
    }
    else
    {
        if (t[v].del)
            Dl (v);
        if (add[v].size() >= 1)
            Ad (v);
    }
}
void Upd (int v, int l, int r, int L, int R, int T)
{
    push (v);
    if (l > R || r < L)
        return;
    if (L <= l && r <= R)
    {
        t[v].del ++;
        t[v].Td = T;
        push(v);
        return;
    }
    int m = (l + r) >> 1;
    Upd (v + v, l, m, L, R, T);
    Upd (v + v + 1, m + 1, r, L, R, T);
}
void UpD (int v, int l, int r, int L, int R, int w, int T)
{
    push (v);
    if (l > R || r < L)
        return;
    if (L <= l && r <= R)
    {
        add[v].push_back (w);
        t[v].Tadd = T;
        push(v);
        return;
    }
    int m = (l + r) >> 1;
    UpD (v + v, l, m, L, R, w, T);
    UpD (v + v + 1, m + 1, r, L, R, w, T);
}
int Get (int v, int l, int r, int L, int R)
{
    push (v);
    if (l > R || r < L)
        return -1;
    if (L <= l && r <= R)
        return t[v].last;
    int m = (l + r) >> 1;
    return max (Get (v + v, l, m, L, R), Get (v + v + 1, m + 1, r, L, R));
}
int main ()
{
	ios_base :: sync_with_stdio(0);
	freopen ("A.in", "r", stdin);
	freopen ("A.out", "w", stdout);
	cin >> n >> m;
	build (1, 1, n);
	for (int i = 1; i <= m; ++ i)
	{
		cin >> l >> r >> c;
		if (!c)
			Upd (1, 1, n, l, r, i);
		else
		    UpD (1, 1, n, l, r, c, i);
    }
	for (int i = 1; i <= n; ++ i)
    {
        cout << Get (1, 1, n, i, i) << ' ';
    }
    return 0;
}

