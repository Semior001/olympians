#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>

using namespace std;

const int MAXN = 100010;

stack <int> a[MAXN];
int n,m,st=1;

int main()
{
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    cin>>n>>m;
    while (st<n)
            st*=2;
    for (int i=1;i<st;i++)
        a[i].push(0);
    for (int i=1;i<=m;i++)
    {
        int l,r,c;
        cin>>l>>r>>c;
        if (!c)
            for (int j=l;j<=r;j++)
                a[j].pop();
        else
            for (int j=l;j<=r;j++)
                a[j].push(c);
    }
    for (int i=1;i<=n;i++)
        cout<<a[i].top()<<" ";

    return 0;
}
