#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>

using namespace std;

const int MAXN = 100010, N = 21;

int n,m,k1,c[N],k[N],cnt,maxcnt,cntc[N],cntk[N],nc[N][N],nk[N][N];

void rec(int x)
{
    if (x>n)
        return;
    int cofe,keks;
    for (int i=1;i<=cntc[x];i++)
    {
        if (c[nc[x][i]]>0)
        {
            cofe=nc[x][i];
            for (int j=1;j<=cntk[x];j++)
            {
                if (k[nk[x][j]]>0)
                {
                    keks=nk[x][j];
                    cnt++;
                    //cout<<cofe<<" "<<keks<<endl;
                    maxcnt=max(maxcnt,cnt);
                    c[cofe]--;
                    k[keks]--;
                    rec(x+1);
                    c[cofe]++;
                    k[keks]++;
                    cnt--;cnt++;
                cout<<cofe<<" "<<keks<<endl;
                maxcnt=max(maxcnt,cnt);
                c[cofe]--;
                k[keks]--;
                rec(x+1);
                c[cofe]++;
                k[keks]++;
                cnt--;
                }
            }
        }
    }

}

int main()
{
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);

    cin>>n>>m>>k1;

    for (int i=1;i<=m;i++)
        cin>>c[i];
    for (int i=1;i<=k1;i++)
        cin>>k[i];
    for (int i=1;i<=n;i++)
    {
        cin>>cntc[i];
        for (int j=1;j<=cntc[i];j++)
            cin>>nc[i][j];
    }
    for (int i=1;i<=n;i++)
    {
        cin>>cntk[i];
        for (int j=1;j<=cntk[i];j++)
            cin>>nk[i][j];
    }
    rec(1);
    cout<<maxcnt;
    return 0;
}
