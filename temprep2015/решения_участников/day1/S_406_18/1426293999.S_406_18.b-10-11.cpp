#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

const int N = 21;

int n,m,k1,c[N],k[N],cnt,maxcnt,cntc[N],cntk[N],nc[N][N],nk[N][N];

void rec(int x)
{
    if (x>n)
        return;
    for (int i=1;i<=cntc[x];i++)
    {
        if (c[nc[x][i]])
        {
            for (int j=1;j<=cntk[x];j++)
            {
                if (k[nk[x][j]])
                {
                    ++cnt;
                    if (maxcnt<cnt)
                        maxcnt=cnt;
                    --c[nc[x][i]];
                    --k[nk[x][j]];
                    rec(x+1);
                    ++c[nc[x][i]];
                    ++k[nk[x][j]];
                    --cnt;
                }
            }
        }
    }

}

int main()
{
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);

    cin>>n>>m>>k1;

    for (int i=1;i<=m;i++)
        cin>>c[i];
    for (int i=1;i<=k1;i++)
        cin>>k[i];
    for (int i=1;i<=n;i++)
    {
        cin>>cntc[i];
        for (int j=1;j<=cntc[i];j++)
            cin>>nc[i][j];
    }
    for (int i=1;i<=n;i++)
    {
        cin>>cntk[i];
        for (int j=1;j<=cntk[i];j++)
            cin>>nk[i][j];
    }
    rec(1);
    cout<<maxcnt;
    return 0;
}
