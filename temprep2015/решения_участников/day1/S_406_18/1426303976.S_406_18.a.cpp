#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>

using namespace std;

const int MAXN = 262145;

stack <int> a[MAXN];
int n,m,st=1;
bool upd[MAXN];
int cnt;
void push1(int pos)
{
    if (pos*2>st)
        return;
    a[pos*2].push(a[pos].top());
    a[pos*2+1].push(a[pos].top());
    a[pos].pop();
    upd[pos]=false;
    upd[pos*2]=true;
    upd[pos*2+1]=true;
}

void add(int L, int R, int l, int r, int pos, int c)
{
    if (r<L || R<l)
        return;
    if (upd[pos])
        push1(pos);
    if (l<=L && R<=r)
    {
        a[pos].push(c);
        upd[pos]=true;
    }
    else
    {
        int m=(L+R)/2;
        add(L,m,l,r,pos*2,c);
        add(m+1,R,l,r,pos*2+1,c);
    }
}

void del(int L, int R, int l, int r, int pos)
{
    if (r<L || R<l)
        return;
    if (l<=L && R<=r)
        if (!a[pos].empty())
            a[pos].pop();
    if (upd[pos])
            push1(pos);
    if (!(l<=L && R<=r))
    {
        int m=(L+R)/2;
        del(L,m,l,r,pos*2);
        del(m+1,R,l,r,pos*2+1);
    }
}

int find1(int x)
{
    int res=0;
    while (x)
    {
        if (!(a[x].empty()))
            res=a[x].top();
        x/=2;
    }
    return res;
}

int main()
{
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);

    scanf("%d%d", &n, &m);
    while (st<n)
            st*=2;

    for (int i=1;i<=m;++i)
    {
        int l,r,c;
        scanf("%d%d%d", &l, &r, &c);
        c ? add(1,st,l,r,1,c) : del(1,st,l,r,1);
    }
    for (int i=1;i<=n;++i)
        printf("%d ",find1(i+st-1));

    return 0;
}
