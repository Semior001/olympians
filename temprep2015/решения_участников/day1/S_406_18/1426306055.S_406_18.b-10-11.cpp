#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

const int N = 501;

int n,m,k,c[N],p[N],cnt,maxcnt,nc[N][N],np[N][N],cc[N],cp[N];

void rec(int x, int cnt)
{
    if (maxcnt<cnt)
        maxcnt=cnt;
    if (x>n)
        return;
    for (int i=1;i<=cc[x];i++)
    {
        if (c[nc[x][i]])
        {
            for (int j=1;j<=cp[x];j++)
            {
                if (p[np[x][j]])
                {
                    c[nc[x][i]]--;
                    p[np[x][j]]--;
                    rec(x+1,cnt+1);
                    c[nc[x][i]]++;
                    p[np[x][j]]++;
                }
            }
        }
    }
}

int main()
{
    freopen("B.in", "r", stdin);
    freopen("B.out", "w", stdout);

    cin>>n>>m>>k;
    for (int i=1;i<=m;i++)
        cin>>c[i];
    for (int i=1;i<=k;i++)
        cin>>p[i];
    for (int i=1;i<=n;i++)
    {
        cin>>cc[i];
        for (int j=1;j<=cc[i];j++)
            cin>>nc[i][j];
    }
    for (int i=1;i<=n;i++)
    {
        cin>>cp[i];
        for (int j=1;j<=cp[i];j++)
            cin>>np[i][j];
    }
    for (int i=1;i<=n;i++)
        rec(i,0);
    cout<<maxcnt;
    return 0;
}
