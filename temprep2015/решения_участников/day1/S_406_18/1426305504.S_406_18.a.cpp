#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <stack>
using namespace std;

const int MAXN = 262145;

stack <int> a[MAXN];
int n,m,st=1;
bool upd[MAXN];

void push1(int pos)
{
    if (a[pos].size()==0)
        return;
    if (pos>=st && pos<=st*2)
        return;
    a[pos*2].push(a[pos].top());
    a[pos*2+1].push(a[pos].top());
    a[pos].pop();
    upd[pos*2]=upd[pos*2+1]=true;
    upd[pos]=false;
}

void add(int L, int R, int l, int r, int pos, int val)
{
    if (r<L || R<l)
        return;
    if (l<=L && R<=r)
    {
        a[pos].push(val);
        upd[pos]=true;
    }
    if (upd[pos])
        push1(pos);
    if (!(l<=L && R<=r))
    {
        int m=(L+R)/2;
        add(L,m,l,r,pos*2,val);
        add(m+1,R,l,r,pos*2+1,val);
    }
}

void del(int L, int R, int l, int r, int pos)
{
    if (r<L || R<l)
        return;
    if (l<=L && R<=r)
        if (!a[pos].empty())
            a[pos].pop();
    if (upd[pos])
            push1(pos);
    if (!(l<=L && R<=r))
    {
        int m=(L+R)/2;
        del(L,m,l,r,pos*2);
        del(m+1,R,l,r,pos*2+1);
    }
}
int main()
{
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);

    int n,m;
    cin>>n>>m;
    while (st<n)
        st*=2;
    for (int i=1;i<=m;i++)
    {
        int l,r,c;
        cin>>l>>r>>c;
        if (c!=0)
            add(1,st,l,r,1,c);
        else
            del(1,st,l,r,1);
    }
    for (int i=1;i<=n;i++)
    {
        int x=i+st-1,res=0;
        while (x>=1)
        {
            if (a[x].size()!=0)
                res=a[x].top();
            x/=2;
        }
        cout<<res<<" ";
    }
    return 0;
}
