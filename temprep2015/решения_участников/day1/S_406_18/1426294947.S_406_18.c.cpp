#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

const int N = 501;

int n,a[N],cnt;
bool used[N];

int main()
{
    freopen("C.in", "r", stdin);
    freopen("C.out", "w", stdout);

    cin>>n;
    for (int i=1;i<=n;i++)
        cin>>a[i];
    for (int l1=1;l1<n;l1++)
    {
        for (int r1=l1;r1<n;r1++)
        {
            for (int i=l1;i<=r1;i++)
                    used[a[i]]=true;
            for (int l2=r1+1;l2<=n;l2++)
            {
                int r2=l2;
                while (!used[a[r2]] && r2<=n)
                    cnt++,r2++;
            }
            for (int i=l1;i<=r1;i++)
                    used[a[i]]=false;
        }
    }
    cout<<cnt;
    return 0;
}
