#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <utility>
#include <vector>
using namespace std;

vector< vector<int> > arr;

void add(int L, int R, int c)
{
    for(int i=L;i<=R;i++)
    {
        arr[i].push_back(c);
    }
}

void del(int L, int R, int c)
{
    for(int i=L;i<=R;i++)
    {
        arr[i].pop_back();
    }
}
int main()
{
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    int L,R,c,n,m;

    cin>>n>>m;
    for(int i=0;i<m;i++)
        {
            cin>>L>>R>>c;
            if(c==0)
            {
                del(L,R,c);
            }
            else
            {
                add(L,R,c);
            }
        }
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
            {
                cout<<arr[i][j]<<" ";
            }
        cout<<endl;
    }
    return 0;
}
