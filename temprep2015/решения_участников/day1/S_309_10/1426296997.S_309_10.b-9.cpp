#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#define pb push_back
#define mp make_pair
#define len length()
#define f first
#define s second
#define ll long long

using namespace std;

string s;
int k, raz, mn = 100002, a[30];

int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	cin >> s >> k;
	int n = s.len;
	for (int i = 0; i < s.len; i++)
		a[s[i]-'a']++;
	for (int i = 0; i < 26; i++){
		if (a[i] > 0)
			raz++;
	}
	if (raz < k){
		cout << 1 ;
		return 0;
	}
	for (int i = 0; i < n; i++){
		int b[30];
		for (int d = 0; d < 26; d++)
			b[d] = 0;
		for (int j = i, t = 1; j < n; j++, t++){
			b[s[j]-'a']++;
			int an = 0;
			for (int q = 0; q < 26; q++){
				if (b[q] > 0 ) an++;
			}
			if (an == k){
				mn = min(mn, t);
				break;
			}	
		}
	}
	if (mn == s.len){
		cout << 1;
	}	
	else
	cout << mn ;
	return 0;

}