#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#define pb push_back
#define mp make_pair
#define len length()
#define f first
#define s second
#define ll long long

using namespace std;

const int maxn = (int)(1e5+11);

ll a[maxn];

int n, m;
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout); 
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		a[i] = 0;
	for (int i = 0; i < m; i++){
		ll r, l, c;
		cin >> l >> r >> c;
		for (int i = l; i <= r; i++)
			a[i] = c;
	}
	for (int i = 1; i <= n; i++)
		cout << a[i] <<' ';
	return 0;
}
