#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#define pb push_back
#define mp make_pair
#define len length()
#define f first
#define s second
#define ll long long

using namespace std;

const int maxn = (int)(1e5+11);

int n, m;
vector < int > a[maxn];

int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	cin >> n >> m;
	for (int i = 0; i < m; i++){  
		ll r, c, l;
		cin >> l >> r >> c;
		if (c == 0){
			for (int i = l; i <= r; i++){
				a[i].pop_back();		
			}
		}
		if (c != 0){
			for (int i = l; i <= r; i++){
	                	a[i].pb(c);
			}
		}
	}
	for (int i = 1; i<= n; i++){
		if (a[i].size()  != 0)
			cout << a[i][a[i].size()-1] <<' ';
		else	
			cout << 0 <<' ';
	}
	return 0;
}