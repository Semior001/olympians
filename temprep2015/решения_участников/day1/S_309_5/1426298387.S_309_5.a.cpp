#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<map>
#include<set>
#include<cstring>
#include<stack>
using namespace std;
struct NN {
    stack<int> a;
};
int N, M;
NN arr[100100];
int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    int l, r, c;
    scanf("%d %d", &N, &M);
    for (int i = 0; i < N; i++) {
        arr[i].a.push(0);
    }
    for (int i = 0; i < M; i++) {
        scanf("%d %d %d", &l, &r, &c);
        l--;
        r--;
        for(int j = l; j <= r; j++) {
            if (c > 0) {
                arr[j].a.push(c);
            } else {
                arr[j].a.pop();
            }
        }
    }
    for (int i = 0; i < N; i++) {
        printf("%d ", arr[i].a.top());
    }
    printf("\n");
    return 0;
}
