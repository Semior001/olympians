#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<map>
#include<set>
#include<cstring>
#include<stack>
using namespace std;

struct NN {
    stack<int> a;
    bool give;
    bool take;
};
int N, M;
NN tree[600100];
void build_tree(int L, int R, int v) {
    if (L == R) {
        tree[v].a.push(0);
        tree[v].give = false;
        tree[v].give = false;
    } else {
        int mid = (L + R) / 2;
        build_tree(L, mid, 2 * v + 1);
        build_tree(mid + 1, R, 2 * v + 2);
        tree[v].a.push(tree[2 * v + 1].a.top());
        tree[v].give = false;
        tree[v].give = false;
    }
}

void push_s(int L, int R, int v) {
    if (tree[v].give == true && tree[v].take == false) {
        if (L < R) {
            tree[2 * v + 1].a.push(tree[v].a.top());
            tree[2 * v + 2].a.push(tree[v].a.top());
            tree[2 * v + 1].give = true;
            tree[2 * v + 2].give = true;
        }
        tree[v].give = false;
    } else if (tree[v].give == false && tree[v].take == true) {
        if (L < R) {
            tree[2 * v + 1].a.pop();
            tree[2 * v + 2].a.pop();
            tree[2 * v + 1].take = true;
            tree[2 * v + 2].take = true;
        }
        tree[v].take = false;
    }
}

void push_inn(int L, int R, int l, int r, int c, int v) {
    push_s(L, R, v);
    if (L > r || R < l) {
        return;
    }
    if (L >= l && R <= r) {
        if (c > 0) {
            tree[v].give = true;
            tree[v].a.push(c);
        } else {
            tree[v].take = true;
            tree[v].a.pop();
        }
        push_s(L, R, v);
        return;
    }
    int mid = (L + R) / 2;
    push_inn(L, mid, l, r, c, 2 * v + 1);
    push_inn(mid + 1, R, l, r, c, 2 * v + 2);
}

void printt(int L, int R, int v) {
    push_s(L, R, v);
    if (L == R) {
        printf("%d ", tree[v].a.top());
    } else {
        int mid = (L + R) / 2;
        printt(L, mid, 2 * v + 1);
        printt(mid + 1, R, 2 * v + 2);
    }
}

int main() {
    freopen("A.in", "r", stdin);
    freopen("A.out", "w", stdout);
    int l, r, c;
    scanf("%d %d", &N, &M);
    build_tree(0, N, 0);
    for (int i = 0; i < M; i++) {
        scanf("%d %d %d", &l, &r, &c);
        l--;
        r--;
        push_inn(0, N, l, r, c, 0);
    }
    printt(0, N - 1, 0);
    printf("\n");
    return 0;
}
