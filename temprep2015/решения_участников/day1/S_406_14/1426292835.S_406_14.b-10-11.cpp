# include <iostream>
# include <cstdio>
# include <cstdlib>
# include <iomanip>
# include <string>
# include <cstring>
# include <map>
# include <cstdio>
# include <cstdlib>
# include <set>
# include <algorithm>
# include <vector>
# include <valarray>
# include <unordered_set>
# include <bitset>
# include <ctime>
# include <clocale>

# define fi first
# define se second
# define mp make_pair
# define INF ll(1e17)
# define FOI(i,x,n) for(ll i=x;i<=n;i++)
# define FOD(i,x,n) for(ll i=n;i>=x;i--)
# define sz(x) ll(x.size())

using namespace std;

typedef long long ll;

string a;
ll k,ans[555][555],len=INF,b[555555];
unordered_set<char>st;

int main(){
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    cin>>a>>k;

    for(ll i=0;i<a.size();i++){
        b[i+1]=a[i]-96;
    }
    for(ll i=1;i<=ll(a.size());i++){
        for(ll j=i;j<=ll(a.size());j++){
            for(ll k=i;k<=j;k++){
                st.insert(b[k]);
            }
            ans[i][j]=ll(st.size());
            st.clear();
        }
    }
    for(ll i=1;i<=ll(a.size());i++){
        for(ll j=i;j<=ll(a.size());j++){
            if(ans[i][j]==k){
                len=min(len,j-i+1);
            }
        }
    }
    cout<<(len==INF?-1:len);
    return 0;
}
