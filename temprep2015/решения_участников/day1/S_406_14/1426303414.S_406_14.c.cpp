# include <bits/stdc++.h>

# define fi first
# define se second
# define mp make_pair
# define INF ll(1e17)
# define FOI(i,x,n) for(ll i=x;i<=n;i++)
# define FOD(i,x,n) for(ll i=n;i>=x;i--)
# define sz(x) ll(x.size())

using namespace std;

typedef long long ll;

ll n,a[555555],g[555555],ans=0;

int main(){
    freopen("C.in","r",stdin);
    freopen("C.out","w",stdout);
    scanf("%lld",&n);
    for(ll i=1;i<=n;i++){
        scanf("%lld",&a[i]);
    }
    ans=((n-1)*(n))/2;
    for(ll i=2;i<=n;i++){
        for(ll j=1;j<=i;j++){
            g[a[j]]=1;
        }
        for(ll j=i+1;j<=n;j++){
            ll e=0;
            for(ll k=j+1;k<=n;k++){
                if(g[a[k]]==1){
                    e=1;
                }
            }
            if(e==0){
                ans++;
            }
        }
    }
    cout<<(!ans?0:ans+1);
    return 0;
}
