# include <bits/stdc++.h>

# define fi first
# define se second
# define mp make_pair
# define INF ll(1e17)
# define FOI(i,x,n) for(ll i=x;i<=n;i++)
# define FOD(i,x,n) for(ll i=n;i>=x;i--)
# define sz(x) ll(x.size())

using namespace std;

typedef long long ll;


unordered_set<char>st;
string a;
ll k,len=INF;

int main(){
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    cin>>a>>k;
    ll n=a.size();
    for(ll i=0;i<n;i++){
        for(ll j=i;j<n;j++){
            st.insert(a[j]);
            if(st.size()==k){
                len=min(len,j-i+1);
            }

        }
        st.clear();
    }
    cout<<len;
    return 0;
}
