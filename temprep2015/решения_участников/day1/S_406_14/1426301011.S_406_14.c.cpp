# include <bits/stdc++.h>

# define fi first
# define se second
# define mp make_pair
# define INF ll(1e17)
# define FOI(i,x,n) for(ll i=x;i<=n;i++)
# define FOD(i,x,n) for(ll i=n;i>=x;i--)
# define sz(x) ll(x.size())

using namespace std;

typedef long long ll;

ll n,a[555555],ans,res,g[555555];

bool pr(ll i1,ll i2,ll s1,ll s2){
    for(ll i=1;i<=n;i++){
        g[a[i]]=0;
    }
    for(ll i=i1;i<=i2;i++){
        g[a[i]]=1;
    }
    for(ll i=s1;i<=s2;i++){
        if(g[a[i]]==1){
            return false;
        }
    }

    return true;
}

int main(){
    freopen("C.in","r",stdin);
    freopen("C.out","w",stdout);
    scanf("%lld",&n);
    for(ll i=1;i<=n;i++){
        scanf("%lld",&a[i]);
    }
    sort(a+1,a+1+n);
    for(ll i=1;i<=n;i++){
        for(ll j=i;j<=n;j++){
            unordered_set<ll>st;
            for(ll k=j+1;k<=n;k++){
                st.insert(a[k]);
            }
            ll ind=j+1;
            while(st.size()!=n-ind+1){
                st.erase(a[ind]);
                ++ind;
            }
            res+=ind;
        }
    }
    cout<<(res/4-(res%4!=0));
    return 0;
}
