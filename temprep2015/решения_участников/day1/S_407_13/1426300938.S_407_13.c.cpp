#include<bits/stdc++.h>

#define task "C"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 5e3 + 5;
const int inf = 1 << 30;

int a[maxn], nx[maxn], last[maxn], R[maxn], b[maxn], u[maxn];
int n, v, cur;
ll ans;

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		cin >> n;
		for(int i = 1; i <= n; i++) {
			cin >> a[i];
			last[a[i]] = n + 1;
			R[a[i]] = i;
		}
		for(int i = n; i >= 1; i--) {
			nx[i] = last[a[i]];
			last[a[i]] = i;
		}
		for(int l = 1; l <= n; l++)
			for(int r = l; r <= n; r++) {
				memset(u, 0, sizeof u);
				for(int i = l; i <= r; i++) 
					u[a[i]] = 1;
				cur = 0;
				for(int i = r + 1; i <= n; i++)
					if(!u[a[i]]) cur++;
					else {
						ans += 1ll * cur * (cur + 1) / 2;
						cur = 0;
					}	
				ans += 1ll * cur * (cur + 1) / 2;
			}
		cout << ans;
	return 0;
}
