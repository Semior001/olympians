#include<bits/stdc++.h>

#define task "B"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 25;
const int inf = 1 << 30;

int n, m, k, c[maxn], p[maxn], sz_c[maxn], sz_p[maxn], nc[maxn], np[maxn], ans, cnt, v;
vector < int > lc[maxn], lp[maxn]; 

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		scanf("%d%d%d", &n, &m, &k);
		for(int i = 1; i <= m; i++)
			scanf("%d", &c[i]);
		for(int i = 1; i <= k; i++)
			scanf("%d", &p[i]);
		for(int i = 1; i <= n; i++) {
			scanf("%d", &sz_c[i]);
			lc[i].resize(sz_c[i]);
			for(int j = 0; j < sz_c[i]; j++)
				scanf("%d", &lc[i][j]);
		}
		for(int i = 1; i <= n; i++) {
			scanf("%d", &sz_p[i]);
			lp[i].resize(sz_p[i]);
			for(int j = 0; j < sz_p[i]; j++)
				scanf("%d", &lp[i][j]);
		}
		for(int mask = 0; mask < (1 << n); mask++) {
			memset(nc, 0, sizeof nc);
			memset(np, 0, sizeof np);
			cnt = 0;
			for(int i = 0; i < n; i++)  {
				if(mask & (1 << i)) {
					cnt++;
					v = -1;
					for(int j = 0; j < sz_c[i + 1]; j++) {
						if(v == -1 || c[lc[i + 1][j]] > c[lc[i + 1][v]]) {
							v = j;
						}
					}
					if(v != -1)
						nc[lc[i + 1][v]]++;
					v = -1;
					for(int j = 0; j < sz_p[i + 1]; j++) {
						if(v == -1 || p[lp[i + 1][j]] > p[lp[i + 1][v]]) {
							v = j;
						}
					}
					if(v != -1)
						np[lp[i + 1][v]]++;
				}
			}
			bool bad = 0;
			for(int i = 1; i <= m && !bad; i++)
				if(c[i] < nc[i]) 
					bad = 1;
			for(int i = 1; i <= k && !bad; i++)
				if(p[i] < np[i])
					bad = 1;
			if(!bad)  ans = max(ans, cnt);
		}
		printf("%d\n", ans);
	return 0;
}
