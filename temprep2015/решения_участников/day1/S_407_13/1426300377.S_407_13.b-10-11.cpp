#include<bits/stdc++.h>

#define task "B"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 23;
const int inf = 1 << 30;

int n, m, k, x, N, res, ans;
vector < pair < pair < int, int >, int > > v;
int c[maxn], p[maxn], X[maxn], Y[maxn], cof[maxn], pir[maxn], np[maxn], nc[maxn];

void init() {	
	memset(nc, 0, sizeof nc);
	memset(np, 0, sizeof np);
	memset(cof, 0, sizeof cof);
	memset(pir, 0, sizeof pir);
}

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		cin >> n >> m >> k;
		for(int i = 1; i <= m; i++) cin >> c[i];
		for(int i = 1; i <= k; i++) cin >> p[i];
		for(int i = 1; i <= n; i++) {
			cin >> X[i];
			for(int j = 1; j <= X[i]; j++) {
				cin >> x;
				v.pb(mp(mp(x, 1), i));
			}
		}
		for(int i = 1; i <= n; i++) {
			cin >> Y[i];
			for(int j = 1; j <= Y[i]; j++) {
				cin >> x;
				v.pb(mp(mp(x, 2), i));
			}
		}
		N = int(v.sz);
		for(int mask = 0; mask < (1 << N); mask++) {
			init();
			for(int i = 0; i < N; i++)
				if(mask & (1 << i)) {
					if(v[i].f.s == 1) {
						nc[v[i].f.f]++;
						cof[v[i].s] = 1;
					}
					else {
						np[v[i].f.f]++;
						pir[v[i].s] = 1;	
					}
				}
			res = 0;
			for(int i = 1; i <= n; i++)
		   		if(cof[i] && pir[i]) 
		   			res++;
			bool ok = 1;
			for(int i = 1; i <= m && ok; i++)
				if(nc[i] > c[i])
					ok = 0;
			for(int i = 1; i <= k && ok; i++)
				if(np[i] > p[i])
					ok = 0;
		   	if(ok) ans = max(ans, res);
		}
		cout << ans;
	return 0;
}
