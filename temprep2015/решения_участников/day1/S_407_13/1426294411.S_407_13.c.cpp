#include<bits/stdc++.h>

#define task "C"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 5e3 + 5;
const int inf = 1 << 30;

int a[maxn], nx[maxn], last[maxn], R[maxn], b[maxn];
int n, v, cur, ans, res;

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		cin >> n;
		for(int i = 1; i <= n; i++) {
			cin >> a[i];
			last[a[i]] = n + 1;
			R[a[i]] = i;
		}
		for(int i = n; i >= 1; i--) {
			nx[i] = last[a[i]];
			last[a[i]] = i;
		}
		for(int l = 1; l <= n; l++)
			for(int r = l; r <= n; r++) {
				memset(b, 0, sizeof b);
				for(int i = l; i <= r; i++) {
					if(b[R[a[i]]]) continue;
					v = i;
					while(v <= r) v = nx[v];
					while(v <= n) {
						b[v] = 1;
						v = nx[v];
					}
				}
				cur = res = 0;
				for(int i = r + 1; i <= n; i++)
					if(!b[i]) cur++;
					else {
						res += cur * (cur + 1) / 2;
						cur = 0;
					}	
				res += cur * (cur + 1) / 2;
				ans += res;
			}
		cout << ans;
	return 0;
}
