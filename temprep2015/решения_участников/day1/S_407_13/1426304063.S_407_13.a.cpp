#include<bits/stdc++.h>

#define task "A"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 5;
const int inf = 1 << 30;

int n, m, l, r, x;
int t[4 * maxn], back[4 * maxn];
map < int, int > pre[4 * maxn];

void push(int v) {
	if(t[v] == 0) return;
	if(back[v]) {
		t[v] = pre[v][t[v]];
		back[v] = 0;
	}
	pre[v + v] = pre[v + v + 1] = pre[v];
	t[v + v] = t[v + v + 1] = t[v];
	t[v] = 0;
}

void update(int v, int l, int r, int L, int R, int x) {
	if(r < L || R < l) return;
	if(L <= l && r <= R) {
		if(x != 0) {
			pre[v][x] = t[v];
			t[v] = x;
		}
		else {
			back[v] = 1;
			t[v] = pre[v][t[v]];
		}
		return;
	}
	int mid = (l + r) >> 1;
	push(v);
	update(v + v, l, mid, L, R, x);
	update(v + v + 1, mid + 1, r, L, R, x);
}

void walk(int v, int l, int r) {
	if(l == r) {
		cout << t[v] << " ";
		return;
	}
	int mid = (l + r) >> 1;
	push(v);
	walk(v + v, l, mid);
	walk(v + v + 1, mid + 1, r);
}

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		scanf("%d%d", &n, &m);
		for(int i = 1; i <= m; i++) {
			scanf("%d%d%d", &l, &r, &x);
			update(1, 1, n, l, r, x);
		}
		walk(1, 1, n);
	return 0;
}
