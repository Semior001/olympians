#include<bits/stdc++.h>

#define task "A"

#define ll long long
#define mp make_pair
#define pb push_back
#define sz size()
#define f first
#define s second

using namespace std;

const int maxn = 1e5 + 123;
const int inf = 1 << 30;

int n, m, l, r, x;
vector < int > v[maxn];

int main() {
	freopen(task".in", "r", stdin);
	freopen(task".out", "w", stdout);
		scanf("%d%d", &n, &m);
		for(int i = 1; i <= m; i++) {
			scanf("%d%d%d", &l, &r, &x);
			for(int j = l; j <= r; j++)
				if(x == 0)
					v[j].pop_back();
				else
					v[j].pb(x);
		}
		for(int i = 1; i <= n; i++) {
			if(v[i].empty())
				printf("0 ");
			else
				printf("%d ", v[i].back());
		}
	return 0;
}
