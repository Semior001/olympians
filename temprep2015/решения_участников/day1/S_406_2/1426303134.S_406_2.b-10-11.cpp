#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 1e5 + 7;

int n, m, k;
int c[MAXN], p[MAXN];

int tc[MAXN], tp[MAXN];

vector<int> cl[MAXN], pl[MAXN];

int ans;

void rec(int v, int taken = 0) {
  ans = max(ans, taken);
  if (v == n + 1) {
    return;
  }
  else {
    rec(v + 1, taken);
    for (int i = 0; i < cl[v].size(); i++) {
      for (int j = 0; j < pl[v].size(); j++) {
        int x = cl[v][i];
        int y = pl[v][j];
        if (tc[x] < c[x] && tp[y] < p[y]) {
          tc[x]++; tp[y]++;
          rec(v + 1, taken + 1);
          tc[x]--, tp[y]--;
        }
      }
    }
  }
}

int main() {
  freopen("B.in", "r", stdin);
  freopen("B.out", "w", stdout);

  scanf("%d%d%d", &n, &m, &k);
  for (int i = 1; i <= m; i++) {
    scanf("%d", &c[i]);
  }
  for (int i = 1; i <= k; i++) {
    scanf("%d", &p[i]);
  }
  for (int i = 1; i <= n; i++) {
    int sz;
    scanf("%d", &sz);
    while (sz--) {
      int val;
      scanf("%d", &val);
      cl[i].push_back(val);
    }
  }
  for (int i = 1; i <= n; i++) {
    int sz;
    scanf("%d", &sz);
    while (sz--) {
      int val;
      scanf("%d", &val);
      pl[i].push_back(val);
    }
  }
  rec(1);
  cout << ans;
  return 0;
}
