#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n, TIMER;
int a[MAXN], cnt[MAXN], u[MAXN];

vector<int> ids[MAXN];

int calls;
long long csum = 0;

int tmx[MAXN * 4];
int tmn[MAXN * 4];

void upd(int v, int tl, int tr, int pos) {
  if (tl == tr) tmx[v] = tmn[v] = tl;
  else {
    int m = (tl + tr) / 2;
    if (pos <= m) upd(v + v, tl, m, pos);
    else upd(v + v + 1, m + 1, tr, pos);
    tmx[v] = max(tmx[v + v], tmx[v + v + 1]);
    tmn[v] = min(tmn[v + v], tmn[v + v + 1]);
  }
}

int getmx(int v, int tl, int tr, int l, int r) {
  if (tl > r || tr < l) return 0;
  if (l <= tl && tr <= r) return tmx[v];
  int m = (tl + tr) / 2;
  return max(getmx(v + v, tl, m, l, r), getmx(v + v + 1, m + 1, tr, l, r));
}

int getmn(int v, int tl, int tr, int l, int r) {
  if (tl > r || tr < l) return INF;
  if (l <= tl && tr <= r) return tmn[v];
  int m = (tl + tr) / 2;
  return min(getmn(v + v, tl, m, l, r), getmn(v + v + 1, m + 1, tr, l, r));
}

void add(int x) {
  if (cnt[x] != TIMER) {
    ++calls;
    cnt[x] = TIMER;
    int r = getmn(1, 1, n, x + 1, n);
    int l = getmx(1, 1, n, 1, x - 1);
    //cout<<x<<' '<<l<<' '
    csum -= 1LL * (r - l - 1) * (r - l) / 2;
    csum += 1LL * (r - x - 1) * (r - x) / 2;
    csum += 1LL * (x - l - 1) * (x - l) / 2;
    upd(1, 1, n, x);
  }
}

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    ids[a[i]].push_back(i);
  }
  for (int i = 1; i <= n; i++) {
    sort(ids[i].begin(), ids[i].end());
  }

  long long ans = 0;
  for (int pl = 1; pl <= n; pl++) {
    ++TIMER;
    memset(tmx, 0, sizeof tmx);
    for (int i = 1; i <= 4*n; i++)
      tmn[i]=INF;

    upd(1,1,n,pl);
    csum = 1LL * (pl - 1) * (pl) / 2;
    for (int pr = pl; pr <= n; pr++) {
      if (u[a[pr]] != TIMER) {
        u[a[pr]] == TIMER;
        for (int x = 0; x < ids[a[pr]].size(); x++) {
          int cur = ids[a[pr]][x];
          if (cur < pl) add(cur);
          else break;
        }
      }
      ans += csum;
      if (!csum) break;
    }
  }
  cout << ans;
  return 0;
}
