#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 1e5 + 7;

struct query {
  int low, high, color;
};

int n, m;
int ans[MAXN], cnt[MAXN];

query q[MAXN];

int main() {
  freopen("A.in", "r", stdin);
  freopen("A.out", "w", stdout);

  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; i++) {
    scanf("%d%d%d", &q[i].low, &q[i].high, &q[i].color);
  }
  reverse(q + 1, q + m + 1);
  for (int i = 1; i <= m; i++) {
    //stupid
    for (int id = q[i].low; id <= q[i].high; id++) {
      if (!q[i].color) cnt[id]--;
      else {
        cnt[id]++;
        if (cnt[id] == 1 && !ans[id]) {
          ans[id] = q[i].color;
        }
      }
    }
  }
  for (int i = 1; i <= n; i++)
    printf("%d ", ans[i]);

  return 0;
}
