#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n;
int a[MAXN], c[MAXN];

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++)
    scanf("%d", &a[i]);

  int tm = 0,ans=0;
  for(int tl=1;tl<=n;tl++){
    for(int tr=tl;tr<=n;tr++){
      for(int pl=tr+1;pl<=n;pl++){
        for(int pr=pl;pr<=n;pr++){
          ++tm;
          for(int i=tl;i<=tr;i++)
            c[a[i]]=tm;

          bool f=0;
          for(int i=pl;i<=pr;i++){
            if(c[a[i]]==tm){
              f=1;
              break;
            }
          }
          if(!f)
            ++ans;
        }
      }
    }
  }
  cout<<ans;
  return 0;
}
