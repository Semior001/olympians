#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n, TIMER;
int a[MAXN], cnt[MAXN], u[MAXN];

vector<int> ids[MAXN];
set<int> s;

int calls;
long long csum = 0;

void add(int x) {
  if (cnt[x] != TIMER) {
    ++calls;
    cnt[x] = TIMER;
    set<int>::iterator r = s.lower_bound(x);
    set<int>::iterator l = r; --l;
    csum -= 1LL * (*r - *l - 1) * (*r - *l) / 2;
    csum += 1LL * (*r - x - 1) * (*r - x) / 2;
    csum += 1LL * (x - *l - 1) * (x - *l) / 2;
    s.insert(x);
  }
}

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    ids[a[i]].push_back(i);
  }
  for (int i = 1; i <= n; i++) {
    sort(ids[i].begin(), ids[i].end());
  }

  long long ans = 0;
  for (int pl = 1; pl <= n; pl++) {
    ++TIMER;
    s.clear();
    s.insert(0);
    s.insert(pl);
    csum = 1LL * (pl - 1) * (pl) / 2;
    for (int pr = pl; pr <= n; pr++) {
      if (u[a[pr]] != TIMER) {
        u[a[pr]] == TIMER;
        for (int x = 0; x < ids[a[pr]].size(); x++) {
          int cur = ids[a[pr]][x];
          if (cur < pl) add(cur);
          else break;
        }
      }
      ans += csum;
      if (!csum) break;
    }
  }
  cout << ans;
  return 0;
}
