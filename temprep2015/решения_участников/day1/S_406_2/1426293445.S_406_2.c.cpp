#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n;
int a[MAXN], last_in[MAXN], prev_occ[MAXN];

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++)
    scanf("%d", &a[i]);

  for (int i = 1; i <= n; i++) {
    prev_occ[i] = last_in[a[i]];
    last_in[a[i]] = i;
  }
  long long ans = 0;
  for (int tr = 2; tr <= n; tr++) {
    int pl = 1, pr;
    long long csum = 1LL * (tr - 1) * (tr) / 2;
    set<int> s;
    s.insert(0);
    s.insert(tr);
    for (int tl = tr; tl > 1; tl--) {
      int x = prev_occ[tl];
      if (!s.count(tl)) {
        set<int>::iterator l = s.lower_bound(tl); --l;
        set<int>::iterator r = s.upper_bound(tl);
        csum -= 1LL * (*r - *l - 1) * (*r - *l) / 2;
        csum += 1LL * (*r - tr - 1) * (*r - tl) / 2;
        csum += 1LL * (tl - *l - 1) * (tl - *l) / 2;
        s.insert(tl);
      }
      if (!s.count(x)) {
        set<int>::iterator l = s.lower_bound(x); --l;
        set<int>::iterator r = s.upper_bound(x);
        csum -= 1LL * (*r - *l - 1) * (*r - *l) / 2;
        csum += 1LL * (*r - x - 1) * (*r - x) / 2;
        csum += 1LL * (x - *l - 1) * (x - *l) / 2;
        s.insert(x);
      }
      cout<<tl<<' '<<tr<<' '<<csum<<endl;
      ans += csum;
    }
  }
  cout << ans;
  return 0;
}
