#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n;
int a[MAXN], last_in[MAXN], prev_occ[MAXN];

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++)
    scanf("%d", &a[i]);

  for (int i = 1; i <= n; i++) {
    prev_occ[i] = last_in[a[i]];
    last_in[a[i]] = i;
  }
  long long ans = 0;
  for (int tr = 2; tr <= n; tr++) {
    for (int pr = 1; pr < tr; pr++) {
      int pl = 1;
      for (int tl = tr; tl > pr; tl--) {
        int x = prev_occ[tl] + 1;
        if (x - 1 <= pr)
          pl = max(pl, x);

        ans += 1LL * (pr - pl + 1);
      }
    }
  }
  cout << ans;
  return 0;
}
