#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n;
int a[MAXN], u[MAXN];

vector<int> ids[MAXN];
set<int> s;

long long csum = 0;

void add(int x) {
  if (!s.count(x)) {
    set<int>::iterator l = s.lower_bound(x); --l;
    set<int>::iterator r = s.upper_bound(x);
    csum -= 1LL * (*r - *l - 1) * (*r - *l) / 2;
    csum += 1LL * (*r - x - 1) * (*r - x) / 2;
    csum += 1LL * (x - *l - 1) * (x - *l) / 2;
    s.insert(x);
  }
}

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    ids[a[i]].push_back(i);
  }
  for (int i = 1; i <= n; i++) {
    sort(ids[i].begin(), ids[i].end());
  }

  int tm = 0;
  long long ans = 0;
  for (int pl = 1; pl <= n; pl++) {
    s.clear();
    s.insert(0);
    s.insert(pl);
    csum = 1LL * (pl - 1) * (pl) / 2;
    ++tm;
    for (int pr = pl; pr <= n; pr++) {
      if (u[a[pr]] == tm)
        continue;

      u[a[pr]] == tm;
      for (int x = 0; x < ids[a[pr]].size(); x++) {
        int cur = ids[a[pr]][x];
        if (cur < pl) add(cur);
        else break;
      }
      ans += csum;
    }
  }
  cout << ans;
  return 0;
}
