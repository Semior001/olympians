#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 5e3 + 7;

int n;
int a[MAXN], u[MAXN];

int main() {
  freopen("C.in", "r", stdin);
  freopen("C.out", "w", stdout);

  scanf("%d", &n);
  for (int i = 1; i <= n; i++)
    scanf("%d", &a[i]);

  int tm = 0;
  long long ans = 0;
  for (int pl = 1; pl <= n; pl++) {
    for (int pr = pl; pr <= n; pr++) {
      ++tm;
      for (int i = pl; i <= pr; i++)
        u[a[i]] = tm;

      int cur = 0;
      for (int i = 1; i < pl; i++) {
        if (u[a[i]] == tm) cur = 0;
        else ans += ++cur;
      }
    }
  }
  cout << ans;
  return 0;
}
