#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 1e5 + 7;

struct edge {
  int a, b, c, f;
};

edge e[MAXN];
int sz;

vector<int> g[MAXN];

void add_edge2(int u, int v, int cap) {
  e[sz] = {u, v, cap, 0};
  g[u].push_back(sz);
  sz++;

  e[sz] = {v, u, 0, 0};
  g[v].push_back(sz);
  sz++;
}

void add_edge(int u, int v, int cap) {
  //swap(u, v);

  e[sz] = {u, v, cap, 0};
  g[u].push_back(sz);
  sz++;

  e[sz] = {v, u, 0, 0};
  g[v].push_back(sz);
  sz++;

  //add_edge2(v,u,cap);
}

int d[MAXN];
int ptr[MAXN];
int n;

bool bfs() {
  memset(d, -1, sizeof d);

  d[1] = 0;
  queue<int> q;
  q.push(1);
  while (!q.empty()) {
    int v = q.front(); q.pop();
    for (int i = 0; i < g[v].size(); i++) {
      int id = g[v][i];
      int to = e[id].b;
      if (e[id].f < e[id].c && d[to] == -1) {
        d[to] = d[v] + 1;
        q.push(to);
      }
    }
  }
  return d[n] != -1;
}

int dfs(int v, int flow) {
  if (!flow) return 0;
  if (v == n) return flow;
  for (int i = 0; i < g[v].size(); i++) {
    int id = g[v][i];
    int to = e[id].b;
    if (e[id].f < e[id].c && d[v] + 1 == d[to]) {
      int go = min(flow, e[id].c - e[id].f);
      int s = dfs(to, go);

      e[id].f += s;
      e[id ^ 1].f -= s;

      if (s)
        return s;
    }
  }
  return 0;
}

int N, M, K;
int C[MAXN], P[MAXN];

int main() {
  freopen("B.in", "r", stdin);
  freopen("B.out", "w", stdout);

  scanf("%d%d%d", &N, &M, &K);
  for (int i = 1; i <= M; i++) {
    scanf("%d", &C[i]);
    add_edge(i+1, N+M+K+2, C[i]);
  }

  for (int i = 1; i <= K; i++) {
    scanf("%d", &P[i]);
    add_edge(1, i+M+N+1, P[i]);
  }

  for (int i = 1; i <= N; i++) {
    int likes;
    scanf("%d", &likes);
    while (likes--) {
      int wh;
      scanf("%d", &wh);
      add_edge(i + M + 1, wh +1, 1);
    }
  }
  for (int i = 1; i <= N; i++) {
    int likes;
    scanf("%d", &likes);
    while (likes--) {
      int wh;
      scanf("%d", &wh);
      add_edge(wh + 1 + M+N, i + M + 1, 1);
    }
  }

  n = N + M + K + 2;

  int ans=0;
  while (bfs()) {
    memset(ptr,0,sizeof ptr);
    while (int flow=dfs(1,100000))
      ans+=flow;
    //break;
  }
  cout<<ans;
  return 0;
}
