#include <bits/stdc++.h>

using namespace std;

const int INF = (int) 1e9 + 7;
const int MAXN = (int) 1e5 + 7;

struct node {
  int mx, cn;
  node() {
    cn = 1;
  }
};

int d[MAXN];
node t[MAXN];

void push(int v) {
  if (d[v]) {
    d[v + v] += d[v];
    d[v + v + 1] += d[v];
    t[v].mx -= d[v];
    d[v] = 0;
  }
}

void build(int v, int tl, int tr) {
  if (tl == tr) t[v].cn = 1;
  else {
    int m = (tl + tr) / 2;
    build(v + v, tl, m);
    build(v + v + 1, m + 1, tr);
    t[v].cn = t[v + v].cn + t[v + v + 1].cn;
  }
}

void upd(int v, int tl, int tr, int l, int r, int val) {
  push(v);
  if (tl > r || tr < l) return;
  if (l <= tl && tr <= r) {
    d[v] += val;
    push(v);
    return;
  }
  int m = (tl + tr) / 2;
  upd(v + v, tl, m, l, r, val);
  upd(v + v + 1, m + 1, tr, l, r, val);
  t[v].cn = 0;
  t[v].mx = max(t[v + v].mx, t[v + v + 1].mx);
  if (t[v].mx == t[v + v].mx) t[v].cn += t[v + v].cn;
  if (t[v].mx == t[v + v + 1].mx) t[v].cn += t[v + v + 1].cn;
}

int getmax(int v, int tl, int tr, int l, int r) {
  push(v);
  if (tl > r || tr < l) return -INF;
  if (l <= tl && tr <= r) return t[v].mx;
  int m = (tl + tr) / 2;
  return max(getmax(v + v, tl, m, l, r), getmax(v + v + 1, m + 1, tr, l, r));
}

struct query {
  int low, high, color;
};

int n, m;
int ans[MAXN], cnt[MAXN];

query q[MAXN];

void rebuild(int v, int tl, int tr, int C) {
  push(v);
  if (tl == tr) {
    if (t[v].mx == 1) {
      ans[tl] = C;
      t[v].mx = -INF;
      t[v].cn = 1;
    }
  }
  else {
    int m = (tl + tr) / 2;
    rebuild(v + v, tl, m, C);
    rebuild(v + v + 1, m + 1, tr, C);
    t[v].cn = 0;
    t[v].mx = max(t[v + v].mx, t[v + v + 1].mx);
    if (t[v].mx == t[v + v].mx) t[v].cn += t[v + v].cn;
    if (t[v].mx == t[v + v + 1].mx) t[v].cn += t[v + v + 1].cn;
  }
}

int main() {
  freopen("A.in", "r", stdin);
  freopen("A.out", "w", stdout);

  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; i++) {
    scanf("%d%d%d", &q[i].low, &q[i].high, &q[i].color);
  }
  build(1, 1, n);
  reverse(q + 1, q + m + 1);
  for (int i = 1; i <= m; i++) {
    //stupid
    if (!q[i].color) upd(1, 1, n, q[i].low, q[i].high, 1);
    else upd(1, 1, n, q[i].low, q[i].high, -1);

    int cur = getmax(1, 1, n, 1, n);
    if (cur == 1) rebuild(1, 1, n, q[i].color);
  }
  for (int i = 1; i <= n; i++)
    printf("%d ", ans[i]);

  return 0;
}
