#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {
    int n, m;
    vector< vector<int> > a;
    vector<int> b;
    ifstream in("A.in");
    ofstream out("A.out");

    in >> n >> m;

    a.resize(m);
    b.resize(n);
    for(int i = 0; i < m; ++i) {
        a[i].resize(3);
        for(int j = 0; j < 3; ++j)
            in >> a[i][j];
    }

    for(int i = 0; i < m; ++i)
        for(int j = a[i][0]-1; j < a[i][1]; ++j)
            b[j] = a[i][2];

    for(int i = 0; i < n; ++i)
        out << b[i] << ' ';

    return 0;
}
