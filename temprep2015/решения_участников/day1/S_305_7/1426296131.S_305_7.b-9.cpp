#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

using namespace std;

bool check(string a, int n) {
    vector<char> t;
    t.push_back(a[0]);
    for(int i = 1; i < a.length(); ++i)
        if(a[0] != a[i])
            for(int j = 0; j < t.size(); ++j) {
                if(t[j] == a[i]) break;
                if(j == t.size()-1) t.push_back(a[i]);
            }
    return t.size() >= n ? true : false;
}

int main() {
    int k;
    string s;
    ifstream in("B.in");
    ofstream out("B.out");

    in >> s >> k;

    if(k > s.length()) { out << -1; return 0; }

    if(check(s, k)) {
        for(int l = 0; l < s.length()-k; ++l)
            for(int i = 0; i <= s.length()-k-l; ++i) {
                string t;
                for(int j = i; j < i+k+l; ++j)
                    t += s[j];
                if(check(t, k)) { out << t.length(); return 0; }
            }
    }
    else out << -1;

    return 0;
}
