#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

using namespace std;

bool check(string a, int n) {
    int c = 0;
    char al[27] = "qwertyuiopasdfghjklzxcvbnm";
    for(int i = 0; i < a.length(); ++i) {
        for(int j = 0; j < 27; ++j)
            if(al[j] == a[i]) { al[j] = '#'; c++; break; }
        if(c >= n) return true;
    }
    return false;
}

int main() {
    int k;
    string s;
    ifstream in("B.in");
    ofstream out("B.out");

    in >> s >> k;

    if(k > s.length()) { out << -1; return 0; }

    for(int l = 0; l < s.length()-k; ++l)
        for(int i = 0; i <= s.length()-k-l; ++i) {
            string t;
            for(int j = i; j < i+k+l; ++j)
                t += s[j];
            if(check(t, k)) { out << t.length(); return 0; }
        }

    out << -1;

    return 0;
}
