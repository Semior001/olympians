#include <bits/stdc++.h>

using namespace std;

const int N = 100001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    string s;
    set<char>q;
    int l[N],r[N],k,res = INF;
    bool u[N]={false};
    cin>>s>>k;
    for (int i=0;i<s.size();++i) {
        q.insert(s[i]);
        int d = q.size();
        r[d] = i;
        if (!u[d]) {
            u[d] = true;
            l[d] = i;
            if (d>=k) res = min(res,l[d]-r[d-k+1]+1);
        }
    }
    if (res==INF) res = -1;
    cout<<res;
    return 0;
}
