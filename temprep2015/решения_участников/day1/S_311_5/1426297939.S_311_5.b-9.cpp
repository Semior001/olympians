#include <bits/stdc++.h>

using namespace std;

const int N = 100001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    string s;
    set<char>q;
    int sz[N],k,res = INF;
    bool u[N]={false};
    cin>>s>>k;
    for (int i=0;i<s.size();++i) {
        q.insert(s[i]);
        int d = q.size();
        if (!u[d]) {
            u[d] = true;
            sz[d] = i;
            if (d-k+1>0 && sz[d]-sz[d-k+2]+2>0 && sz[d]-sz[d-k+2]+2<res)
                res = sz[d]-sz[d-k+2]+2;
        }
    }
    if (res==INF) res = -1;
    cout<<res;
    return 0;
}
