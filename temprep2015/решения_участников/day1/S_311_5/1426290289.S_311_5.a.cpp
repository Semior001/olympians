#include <bits/stdc++.h>

using namespace std;

const int N = 100001;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    int n,m;
    vector<int>v[N];
    cin>>n>>m;
    for (int i=0;i<m;++i) {
        int l,r,c;
        cin>>l>>r>>c;
        if (c) for (int j=l;j<=r;++j) v[j].push_back(c);
        else for (int j=l;j<=r;++j) v[j].pop_back();
    }
    for (int i=1;i<=n;++i) {
        if (v[i].empty()) cout<<0<<' ';
        else cout<<v[i].back()<<' ';
    }
    return 0;
}
