#include <bits/stdc++.h>

using namespace std;

const int N = 5001, INF = 1e9;

struct edge {
    int l,a[30],res;
    bool u[30];
};

vector<edge>v(N);
int sz,k;

void build() {
    v[0].l = v[0].res = 0;
    for (int i=0;i<30;++i) v[0].a[i] = -1;
    for (int i=0;i<30;++i) v[0].u[i] = false;
    sz = 1;
}

int main() {
    ios_base::sync_with_stdio(0);
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    build();
    vector<int>q;
    q.push_back(0);
    string s;
    int ans = INF;
    cin>>s>>k;
    for (int i=0;i<s.size();++i) {
        int x = s[i] - 'a';
        vector<int>nq;
        for (int j=0;j<q.size();++j) {
            //cout<<q[j]<<' ';
            int p = q[j];
            if (v[p].a[x] == -1) {
                //if (v[p].l == 1 && x == v[p].id) continue;
                v[p].a[x] = sz;
                v[sz].l = v[p].l + 1;
                for (int o=0;o<30;++o) v[sz].a[o] = -1;
                for (int o=0;o<30;++o) v[sz].u[o] = v[p].u[o];
                v[sz].res = v[p].res;
                if (!v[sz].u[x]) {
                    v[sz].u[x] = true;
                    ++v[sz].res;
                }
                //v[sz].d = v[p].d;
                //v[sz].d.insert(s[i]);
                if (v[sz].res==k) ans = min(ans,v[sz].l);
                //cout<<s[i]<<' '<<v[sz].l<<' '<<v[sz].d.size()<<endl;
                nq.push_back(sz++);
            }
            else nq.push_back(v[p].a[x]);
        }
        //cout<<endl;
        q = nq;
        q.push_back(0);
        nq.clear();
    }
    if (ans == INF) ans = -1;
    cout<<ans;
    return 0;
}
