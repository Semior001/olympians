#include <bits/stdc++.h>

using namespace std;

const int N = 100001, INF = 1e9;

int main() {
    ios_base::sync_with_stdio(0);
    freopen("B.in","r",stdin);
    freopen("B.out","w",stdout);
    string s;
    bool u[300]={false};
    int d[N],k,sz=0,res=INF;
    cin>>s>>k;
    for (int i=0;i<s.size();++i) {
        if (!u[s[i]]) {
            u[s[i]] = true;
            d[++sz] = i;
            if (sz>=k) res = min(res,d[sz]-d[sz-k+2]+2);
            //cout<<sz<<' '<<i<<' '<<d[sz-k+2]<<' '<<d[sz]-d[sz-k+2]+2<<endl;
        }
    }
    if (res==INF) res = -1;
    cout<<res;
    return 0;
}
