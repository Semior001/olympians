#include <fstream>
#include <iostream>
#include <vector>
using namespace std;
long n,ops,i,j,tmp,q;
long l,r,c;
int main()
{
    ifstream in;
    ofstream out;
    in.open("A.in");
    out.open("A.out");
    in>>n>>ops;
    vector<vector<long> >allops(ops,vector<long>(3));
    for (i=0;i<ops;i++)
    {
        for (j=0;j<3;j++)
        {
            in>>tmp;
            allops[i][j]=tmp;
        }
    }
    vector<long>cont(n);
    for (i=0;i<n;i++)
    {
        cont[i]=0;
    }
    for (i=0;i<ops;i++)
    {

        l=allops[i][0];
        r=allops[i][1];
        c=allops[i][2];
        for (j=l-1;j<r;j++)
        {
          if (c!=0)
          {
              if (c>cont[j])
              {
              cont[j]=c;
              }
          }
          else { cont[j]--;}
        }
    }
    for (i=0;i<n;i++)
    {
        out<<cont[i]<<" ";
    }
    in.close();
    out.close();
    return 0;
}
