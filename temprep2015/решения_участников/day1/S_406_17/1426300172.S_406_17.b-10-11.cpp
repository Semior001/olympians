#include <fstream>
#include <iostream>
#include <vector>
using namespace std;
int n,cof,pir,cofcnt,pircnt,i,j,q;
int temp;
int client=0;
bool good;
int main()
{
    ifstream in;
    ofstream out;
    in.open("B.in");
    out.open("B.out");
    in>>n>>cof>>pir;
    vector<int> vcof(cof);
    for (i=0;i<cof;i++)
    {
        in>>temp;
        vcof[i]=temp;
    }
    vector<int> vpir(pir);
    for (i=0;i<pir;i++)
    {
        in>>temp;
        vpir[i]=temp;
    }
    vector<vector<int> > studcof(n,vector<int>(cof));
    vector<vector<int> > studpir(n,vector<int>(pir));
    for (i=0;i<n;i++)
    {
    in>>cofcnt;
    for (j=0;j<cofcnt;j++)
    {
       in>>temp;
       temp--;
       for (q=0;q<cof;q++)
       {
           if (temp==q)
           {
               studcof[i][q]=1;
           }
       }
    }
    }
    for (i=0;i<n;i++) {
     in>>pircnt;
    for (j=0;j<pircnt;j++)
    {
       in>>temp;
       temp--;
       for (q=0;q<pir;q++)
       {
           if (temp==q)
           {
               studpir[i][q]=1;
           }
       }
    }
    }
    for (q=0;q<n;q++)
    {
    for (i=0;i<cof;i++)
    {
        for (j=0;j<pir;j++)
        {
            if (studcof[q][i]==1 && studpir[q][j]==1 && vcof[i]!=0 && vpir[j]!=0 )
            {
                client++;
                vcof[i]--;
                vpir[j]--;
            }
        }
    }
    }
    out<<client;
    return 0;
}
