#include <iostream>
#include <cstdio>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <cstdlib>
#include <string>
#include <cmath>
#define m_p make_pair
#define p_b push_back
#define M 100010
using namespace std;

int n, mm, t[M], x, y, z, sz[100010];
int a[1000][1000];
void qsort(int b[], int x, int y)
{
    int l = x, r = y;
    int m = b[(x + y)/2];

    do{
        while(m > b[l])l++;
        while(m < b[r])r--;
        if(l<=r){
            swap(b[l], b[r]);
            l++;
            r--;
        }
    }while(l<=r);
    if(l<=y) qsort(b, l, y);
    if(x<= r)qsort(b, x, r);
}
int main(){
   freopen("A.in", "r", stdin);
   freopen("A.out", "w", stdout);
     scanf("%d %d", &n, &mm);
     while(mm--){
        scanf("%d %d %d", &x, &y, &z);
        if(z){
            for(int i = x; i<=y; ++i){
                a[i][++sz[i]] = z;
            }
        }
        else{
            for(int i = x; i<=y; ++i){
                qsort(a[i], 1, sz[i]);
                sz[i]--;
            }
        }
     }
     for(int i = 1; i<=n; ++i){
        qsort(a[i], 1, sz[i]);
        if(sz[i])
            printf("%d ",a[i][sz[i]]);
        else
            printf("0 ");
     }
    return 0;
}
