#include <iostream>
#include <cstdio>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <cstdlib>
#include <string>
#include <cmath>
#define m_p make_pair
#define p_b push_back
#define M 100010
using namespace std;

int n, mm, t[M], x, y, z;
vector<vector<int> >a;
void qsort(vector<int> &b, int x, int y)
{
    int l = x, r = y;
    int m = b[(x + y)/2];

    do{
        while(m > b[l])l++;
        while(m < b[r])r--;
        if(l<=r){
            swap(b[l], b[r]);
            l++;
            r--;
        }
    }while(l<=r);
    if(l<=y) qsort(b, l, y);
    if(x<= r)qsort(b, x, r);
}
int main(){
    freopen("A.in", "r", stdin);
   freopen("A.out", "w", stdout);
     cin >> n >> mm;
     a.resize(n + 1);

     while(mm--){
        cin >> x >> y >> z;
        if(z){
            for(int i = x; i<=y; ++i){
                a[i].p_b(z);
                qsort(a[i], 0, a[i].size()-1);
            }
        }
        else{
            for(int i = x; i<=y; ++i){
                int q = a[i].size();
                a[i].resize(q - 1);
            }
        }
     }
     for(int i = 1; i<=n; ++i){
        int q = a[i].size();
        if(q)
            cout<<a[i][q - 1]<<" ";
        else
            cout<<0<<" ";
     }
    return 0;
}
