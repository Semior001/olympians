#include <iostream>
#include <cstdio>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <cstdlib>
#include <string>
#include <cmath>
#define m_p make_pair
#define p_b push_back
#define M 100010
using namespace std;

int n, mm, t[M], x, y, z;
vector<vector<int> >a;
void qsort(vector<int> &b, int x, int y)
{
    int l = y - 1, r = y;
    int m = b[(x + y)/2];

    do{
        while(m > b[l])l++;
        while(m < b[r])r--;
        if(l<=r){
            swap(b[l], b[r]);
            l++;
            r--;
        }
    }while(l<=r);
    if(l<=y) qsort(b, l, y);
    if(x<= r)qsort(b, x, r);
}
int main(){
    ifstream fin("A.in");
    ofstream fout("A.out");
     cin >> n >> mm;
     a.resize(n + 1);
     for(int i = 1; i<=n; ++i)
        a[i].p_b(0);
     while(mm--){
        cin >> x >> y >> z;
        if(z){
            for(int i = x; i<=y; ++i){
                a[i].p_b(z);
                qsort(a[i], 0, a[i].size()-1);
            }
        }
        else{
            for(int i = x; i<=y; ++i){
                a[i].resize(a[i].size() - 1);
            }
        }
     }
     for(int i = 1; i<=n; ++i){
            cout<<a[i][a[i].size() - 1]<<" ";

     }
    return 0;
}
