#include<iostream>
#include<stack>
#include<cstdio>
#include <cstring>
#include<cmath>
#include<vector>
using namespace std;
int n,mq,l,r,c,t[4*100001],last,mi[4*100001],coun=0,ans[4*100001],ma;
vector< vector<pair<int,int> > >g(4* 111009);
vector< vector <int> > m(4*111009);
void add(int v,int tl,int tr,int l,int r,int num,int shag)
{
    if (tl>tr || tl>r || tr<l) return ;
    if(tl>=l && tr<=r) {g[v].push_back(make_pair(num,shag));return ;}
    int mid=(tl+tr)/2;
    add(v*2,tl,mid,l,r,num,shag);
    add(v*2+1,mid+1,tr,l,r,num,shag);
}
void minu(int v,int tl,int tr,int l,int r,int shag)
{
    if (tl>tr || tl>r || tr<l) return ;
    if(tl>=l && tr<=r) {m[v].push_back(shag);/*cout<<v<<" "<<t[v]<<" "<<mi[v]<<" "<<last<<endl;*/ return ;}
    int mid=(tl+tr)/2;
    minu(v*2,tl,mid,l,r,shag);
    minu(v*2+1,mid+1,tr,l,r,shag);
}
void get(int v,int l,int r,int index)
{
    for(int i=0;i<g[v].size();i++)
    {
        int to=g[v][i].second;
        if(to>ma)ma=to;
        ans[to]=g[v][i].first;
    }
    for(int i=0;i<m[v].size();i++)
    {
        int to=m[v][i];
        if(to>ma)ma=to;
        ans[to]=0;

    }
    if(l==r){return ;}
    int mid=(l+r)/2;
    if(index<=mid) get(v*2,l,mid,index);
    else            get(v*2+1,mid+1,r,index);

}
int main()
{
    freopen("A.in","r",stdin);
    freopen("A.out","w",stdout);
    memset(t,0,sizeof t);
    memset(mi,0,sizeof mi);
    cin>>n>>mq;
    for(int i=1;i<=mq;i++)
    {
        cin>>l>>r>>c;
        if(c>0)
        {
             add(1,1,n,l,r,c,i);
             last=c;
        }
        else if(c==0)
        {
             minu(1,1,n,l,r,i);
        }
    }
    for(int i=1;i<=n;i++)
    {
        ma=0;
        get(1,1,n,i);
        cout<<ans[ma]<<" ";
    }
    return 0;
}
