#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;
int n, m, k, g, s, a[533][533], b[533][533], c[533], p[533], x[533], y[533], cnt;

int main () {
	freopen ("B.in", "r" , stdin);
	freopen ("B.out", "w", stdout);
	cin >> n >> m >> k;
	for (int i = 1; i <= m; i++) 
		cin >> c[i];
	for (int j = 1; j <= k; j++) 
		cin >> p[j];
	for (int i = 1; i <= n; i++) {
		g++;
		cin >> x[i];
		for (int j = 1; j <= x[i]; j++) {
			cin >> a[g][j];
		}
	}
	for (int i = 1; i <= n; i++) {
		cin >> y[i];
		s++;
		for (int j = 1; j <= y[i]; j++) 
			cin >> b[s][j];
	}
	for (int i = 1; i <= n; i++) {
		bool flag = false;
		for (int j = 1; j <= x[i]; j++) {
			if (c[a[i][j]] > 0) {
				for (int z = 1; z <= y[i]; z++) {
					if (p[b[i][z]] > 0) {
						p[b[i][z]] --;
						c[a[i][j]] --;
						cnt++;
						flag = true;
						break;
					}
				}
			}
			if (flag) 
				break;
		}
	}
	cout << cnt;
	return 0;
}