#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>

using namespace std;

#define pb push_back

int n,m,i,l,r,x,j;
vector<int> a[1001];

int main() {
	freopen("A.in","r", stdin);
	freopen("A.out", "w", stdout);
	cin >> n >> m;
	for (i = 1; i <= m; ++i) {
		cin >> l >> r >> x;
		if (x == 0) {
			for (j = l; j <= r; ++j)
				if (a[j].size() > 0)
					a[j].pop_back();
		}
		else {
			for (j = l; j <= r; ++j)
				a[j].pb(x);
		}
	}
	for (i = 1; i <= n; ++i)
		if (a[i].size() > 0)
			cout << a[i].back() << " ";
		else
			cout << 0 << " ";
}
