#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>

using namespace std;
#define pb push_back

int v[21], len, a[21],b[21], i,j,n,m,k,kol[21], p, mask, mx;
vector<int> x[21], y[21], c[1048577];
bool go(int len) {
    if (len == n+1)
            return true;
	for (int i = 0; i < x[v[len]].size(); ++i)
	for (int j = 0; j < y[v[len]].size(); ++j)
		if (a[x[v[len]][i]] >0 && b[y[v[len]][j]] > 0)
		{
			a[x[v[len]][i]]--;
			b[y[v[len]][j]]--;
			if (!go(len+1)) {
				a[x[v[len]][i]]++;
				b[y[v[len]][j]]++;
			}
			else
			return true;
		}
	return false;
}

bool check(int mask) {
	for (int j = 0; j < c[mask].size(); ++j){
		int to = c[mask][j];
		v[++len] = to;
	}
	for (i = 0; i < x[v[1]].size(); ++i)
	for (j = 0; j < y[v[1]].size(); ++j)
		if (a[x[v[1]][i]] >0 && b[y[v[1]][j]] > 0)
		{
			a[x[v[1]][i]]--;
			b[y[v[1]][j]]--;
			if (!go(2)) {
				a[x[v[1]][i]]++;
				b[y[v[1]][j]]++;
			}
			else
			return true;
		}
	return false;
}

int main() {
	freopen("B.in","r", stdin);
	freopen("B.out", "w", stdout);
	cin >> n >> m >> k;

	for (i = 1; i <= m; ++i)
		cin >> a[i];
	for (j = 1; j <= k; ++j)
		cin >> b[j];

	for (i = 1; i <= n; ++i) {
		cin >> kol[i];
		for (j = 1; j <= kol[i]; ++j) {
			cin >> p;
			x[i].pb(p);
		}
	}
	for (i = 1; i <= n; ++i) {
		cin >> kol[i];
		for (j = 1; j <= kol[i]; ++j) {
			cin >> p;
			y[i].pb(p);
		}
	}

	for (mask = 1; mask < (1<<n); ++mask)
		for (j = 0; j < n; ++j)
			if (mask&(1<<j))
				c[mask].pb(j);
	mx = 0;
	for (mask = 1; mask < (1<<n); ++mask)
		if (mx < c[mask].size() && !check(mask)) {
			mx = c[mask].size();
		}
	cout << mx;

}
