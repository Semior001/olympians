#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <cmath>
using namespace std;

#define pb push_back

int n,m,i,j, ans,k,N,sz,to,l[100001], r[100001], x[100001],kol,block,Rev;
int d[317][131072];
vector <int> b[317];

inline void upd(int pos, int l1, int r1, int x) {
    	while (l1 <= r1) {
        	if (l1%2==1)
			d[pos][l1] += x;
		if (r1%2==0)
			d[pos][r1] += x;
		++l1;
		--r1;
		l1/=2;
		r1/=2;
	}
}

inline int get(int block, int x) {
	int give = 0;
	while (x) {
		give+=d[block][x];
		x>>=1;
	}
	return give;
}

inline int solve(int pos) {
	ans = 0;
	for (k = N; k >= 1; --k)
	if (b[k].size() > 0) {
    	ans += get(k, pos+sz-1);
	if (ans > 0)
	break;
	}
	if (ans <= 0)
		return 0;
	ans = 0;
	for (j = b[k].size()-1; j >= 0; --j) {
		to = b[k][j];
		if (x[to] > 0 && l[to] <= pos && r[to] >= pos)
		kol++;
		else
		if (x[to] == 0 && l[to] <= pos && r[to] >= pos)
		kol--;
		if (kol > 0)
			return x[to];
	}
	return 0;
}

int main() {
	freopen("A.in","r", stdin);
	freopen("A.out", "w", stdout);
	ios_base::sync_with_stdio(false);
	cin >> n >> m;
	N = int(sqrt(m));
	sz = 1;
	while (sz < n)
	sz*=2;
	Rev = N;
	for (i = 1; i <= m; ++i) {
		cin >> l[i] >> r[i] >> x[i];
		block = (i-1)/N+1;
		Rev = max(block, Rev);
		b[block].pb(i);
		if (x[i] > 0)
			upd(block,l[i]+sz-1, r[i]+sz-1, 1);
		else
			upd(block,l[i]+sz-1, r[i]+sz-1, -1);
	}
	N = Rev;
	for (i = 1; i <= n; ++i) {
		cout << solve(i) << " ";
	}

}
