#include<bits/stdc++.h>
using namespace std;
#define task "B"
typedef long long ll;
ll n,m,k,c[2555][2555],f[2555][2555],e[2555],h[2555],n1,x,y,ans;
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>m>>k;
    for(ll i=1;i<=n;i++)
        c[1][(i-1)*3+2]=2,c[(i-1)*3+2][(i-1)*3+3]=c[(i-1)*3+2][(i-1)*3+4]=1;
    n1=3*n+m+k+2;
    for(ll i=1;i<=m;i++)
    {
        scanf("%d",&x);
        c[n*3+i+1][n1]=x;
    }
    for(ll i=1;i<=k;i++)
    {
        scanf("%d",&x);
        c[n*3+m+i+1][n1]=x;
    }
    for(ll i=1;i<=n;i++)
    {
        scanf("%d",&y);
        for(ll j=1;j<=y;j++)
        {
            scanf("%d",&x);
            c[(i-1)*3+3][n*3+x+1]=1;
        }
    }
    for(ll i=1;i<=n;i++)
    {
        scanf("%d",&y);
        for(ll j=1;j<=y;j++)
        {
            scanf("%d",&x);
            c[(i-1)*3+4][n*3+m+x+1]=1;
        }
    }
    h[1]=n1;
    for(ll i=2;i<=n1;i++)
        if(c[1][i]>0)
        {
            f[1][i]=c[1][i];
            f[i][1]=-f[1][i];
            e[i]+=c[1][i];
            e[1]-=c[1][i];
        }
    for(;;)
    {
        ll u=0,v=1;
        for(ll i=2;i<n1;i++)
            if(e[i]>0)
            u=i,i=n1;
        if(!u)
            break;
        for(ll j=1;j<=n1;j++)
            if(c[u][j]-f[u][j]>0&&h[j]<h[v])
            v=j;
        h[u]=h[v]+1;
        ll d=min(e[u],c[u][v]-f[u][v]);
        f[u][v]+=d;
        f[v][u]-=d;
        e[v]+=d;
        e[u]-=d;
    }
    for(ll i=1;i<=n;i++)
        ans+=(f[1][(i-1)*3+2]==2);
    cout<<ans;
}
