#include<bits/stdc++.h>
using namespace std;
#define INF 2e9
#define task "C"
typedef long long ll;
ll n,a[5555],ans,w[5555],last,calc,len,pos,ne[5555],pr[5555],g,h,v[5555][5555],b[5555];
struct item
{
    ll x,y;
    item *l,*r;
}c[5555];
typedef item * pitem;
pitem t,it,l,r;
void split(pitem t, ll x, pitem &l, pitem &r)
{
    if(!t)
    {
        l=r=0;
        return;
    }
    if(t->x>x)
        split(t->l,x,l,t->l),r=t;
    else
        split(t->r,x,t->r,r),l=t;
}
void in(pitem &t, pitem it)
{
    if(!t)
        t=it;
    else if(it->y<t->y)
        split(t,it->x,it->l,it->r),t=it;
    else
        in(t->x>it->x?t->l:t->r,it);
}
ll fin(pitem t, ll x)
{
    if(!t)
        return INF;
    if(t->x<x)
        return fin(t->r,x);
    else
        return min(t->x,fin(t->l,x));
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n;
    srand(time(0));
    for(ll i=1;i<=n;i++)
        scanf("%d",&a[i]),v[a[i]][++b[a[i]]]=i;
    for(ll i=1;i<=n;i++)
    {
        it=&c[i];
        it->x=0;
        it->y=rand();
        it->l=it->r=0;
        in(t,it);
        it=&c[i];
        it->x=i;
        it->y=rand();
        it->l=it->r=0;
        in(t,it);
        ne[0]=i,pr[i]=0;
        calc=i*(i-1)/2;
        for(ll j=i;j<=n;j++)
        {
            if(++w[a[j]]==1)
            {
                for(ll k=1;k<=b[a[j]];k++)
                {
                    pos=v[a[j]][k];
                    if(pos>=i)
                        break;
                    g=fin(t,pos);
                    it=&c[pos];
                    it->x=pos;
                    it->y=rand();
                    it->l=it->r=0;
                    in(t,it);
                    h=pr[g];
                    ne[h]=pos;
                    pr[g]=pos;
                    ne[pos]=g;
                    pr[pos]=h;
                    len=g-h-1;
                    calc-=len*(len+1)/2;
                    len=g-pos-1;
                    calc+=len*(len+1)/2;
                    len=pos-h-1;
                    calc+=len*(len+1)/2;
                }
            }
            ans+=calc;
        }
        for(ll j=i;j<=n;j++)
            w[a[j]]=0;
        t=0;
    }
    cout<<ans;
}
