#include<bits/stdc++.h>
using namespace std;
#define INF 2e9
#define task "A"
typedef long long ll;
ll n,m,l[111111],r[111111],x[111111],t[333333],z[333333],pos[333333],ans[111111],p,res;
void build(ll v, ll l, ll r)
{
    if(l==r)
    {
        pos[v]=l;
        return;
    }
    ll mid=(l+r)/2;
    build(v*2,l,mid);
    build(v*2+1,mid+1,r);
    pos[v]=pos[v*2];
}
void f(ll v, ll l, ll r, ll x, ll y, ll c)
{
    if(x>r||y<l)
        return;
    if(x<=l&&y>=r)
    {
        t[v]+=c;
        z[v]+=c;
        return;
    }
    ll mid=(l+r)/2;
    if(z[v]!=0)
        z[v*2]+=z[v],z[v*2+1]+=z[v],t[v*2]+=z[v],t[v*2+1]+=z[v],z[v]=0;
    f(v*2,l,mid,x,y,c);
    f(v*2+1,mid+1,r,x,y,c);
    if(t[v*2]>=t[v*2+1])
        pos[v]=pos[v*2];
    else
        pos[v]=pos[v*2+1];
    t[v]=max(t[v*2],t[v*2+1]);
}
ll maxx(ll v, ll l, ll r, ll x, ll y)
{
    if(y<l||x>r)
        return -INF;
    if(x<=l&&y>=r)
    {
        if(t[v]>res)
            p=pos[v];
        return res=max(res,t[v]);
    }
    ll mid=(l+r)/2;
    if(z[v]!=0)
        z[v*2]+=z[v],z[v*2+1]+=z[v],t[v*2]+=z[v],t[v*2+1]+=z[v],z[v]=0;
    return max(maxx(v*2,l,mid,x,y),maxx(v*2+1,mid+1,r,x,y));
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>m;
    for(ll i=1;i<=m;i++)
        scanf("%lld%lld%lld",&l[i],&r[i],&x[i]);
    build(1,1,n);
    for(ll i=m;i>=1;i--)
    {
        if(x[i]==0)
            f(1,1,n,l[i],r[i],-1);
        else
        {
            f(1,1,n,l[i],r[i],1);
            res=0;
            while(maxx(1,1,n,l[i],r[i])>0)
            {
                ans[p]=x[i];
                f(1,1,n,p,p,-INF);
                res=0;
            }
        }
    }
    for(ll i=1;i<=n;i++)
        printf("%lld ",ans[i]);
}
