#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define task "C"
typedef long long ll;
ll n,a[5555],ans,w[5555],last,calc,len,pos,ne[5555],pr[5555],g,h,v[5555][5555],b[5555];
set<ll>s;
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n;
    for(ll i=1;i<=n;i++)
        scanf("%d",&a[i]),v[a[i]][++b[a[i]]]=i;
    for(ll i=1;i<=n;i++)
    {
        s.insert(0);
        s.insert(i);
        ne[0]=i,pr[i]=0;
        calc=i*(i-1)/2;
        for(ll j=i;j<=n;j++)
        {
            if(++w[a[j]]==1)
            {
                for(ll k=1;k<=b[a[j]];k++)
                {
                    pos=v[a[j]][k];
                    if(pos>=i)
                        break;
                    set<ll>::iterator r=s.lower_bound(pos);
                    s.insert(pos);
                    g=*r;
                    h=pr[g];
                    ne[h]=pos;
                    pr[g]=pos;
                    ne[pos]=g;
                    pr[pos]=h;
                    len=g-h-1;
                    calc-=len*(len+1)/2;
                    len=g-pos-1;
                    calc+=len*(len+1)/2;
                    len=pos-h-1;
                    calc+=len*(len+1)/2;
                }
            }
            ans+=calc;
        }
        for(ll j=i;j<=n;j++)
            w[a[j]]=0;
        s.clear();
    }
    cout<<ans;
}
