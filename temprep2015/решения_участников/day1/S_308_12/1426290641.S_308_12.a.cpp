#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define task "A"
typedef long long ll;
ll n,m,l,r,x;
vector<ll>t[333333];
void upd(ll v, ll l, ll r, ll x, ll y, ll c)
{
    if(y<l||x>r)
        return;
    if(x<=l&&y>=r)
    {
        t[v].pb(c);
        return;
    }
    for(ll i=0;i<t[v].size();i++)
        t[v*2].pb(t[v][i]),t[v*2+1].pb(t[v][i]);
    t[v].clear();
    ll mid=(l+r)/2;
    upd(v*2,l,mid,x,y,c);
    upd(v*2+1,mid+1,r,x,y,c);
}
void print(ll v, ll l, ll r)
{
    if(l==r)
    {
        if(t[v].size()==0)
        {
            printf("0 ");
        }
        else
        {
            ll ans=0,cnt=0;
            for(ll i=t[v].size()-1;i>=0;i--)
            {
                if(cnt==0&&t[v][i]>0)
                    ans=t[v][i],i=0;
                if(t[v][i]==0)
                    cnt++;
                else
                    cnt--;
            }
            printf("%lld ",ans);
        }
        return;
    }
    ll mid=(l+r)/2;
    for(ll i=0;i<t[v].size();i++)
        t[v*2].pb(t[v][i]),t[v*2+1].pb(t[v][i]);
    t[v].clear();
    print(v*2,l,mid);
    print(v*2+1,mid+1,r);
}
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n>>m;
    for(ll i=1;i<=m;i++)
    {
        scanf("%lld%lld%lld",&l,&r,&x);
        upd(1,1,n,l,r,x);
    }
    print(1,1,n);
}
