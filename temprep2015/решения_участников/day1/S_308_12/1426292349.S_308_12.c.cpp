#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define task "C"
typedef long long ll;
ll n,a[5555],ans,w[5555],last,len,pos;
vector<ll>v[5555];
set<ll>s;
int main()
{
    freopen(task".in","r",stdin);
    freopen(task".out","w",stdout);

    cin>>n;
    for(ll i=1;i<=n;i++)
        scanf("%d",&a[i]),v[a[i]].pb(i);
    for(ll i=1;i<=n;i++)
    {
        s.insert(0);
        s.insert(i);
        ans+=i*(i-1)/2;
        for(ll j=i;j<=n;j++)
        {
            if(++w[a[j]]==1)
            {
                for(ll k=0;k<v[a[j]].size();k++)
                {
                    pos=v[a[j]][k];
                    set<ll>::iterator r=s.lower_bound(pos);
                    set<ll>::iterator l=r;
                    l--;
                    len=*r-*l-1;
                    ans-=len*(len+1)/2;
                    len=*r-pos-1;
                    ans+=len*(len+1)/2;
                    len=pos-*l-1;
                    ans+=len*(len+1)/2;
                    s.insert(pos);
                }
            }
        }
        for(ll j=i;j<=n;j++)
            w[a[j]]=0;
        s.clear();
    }
    cout<<ans;
}
