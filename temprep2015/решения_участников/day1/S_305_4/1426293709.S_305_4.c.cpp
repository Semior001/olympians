#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 5002;
int n, a[N], u[N], lat;
LL ans;
int main() {
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; ++ i) {
		scanf("%d", &a[i]);
	}
	for(int i = 1; i < n; ++ i) {
		int latest = 0;
		memset(u, 0, sizeof u);
		for(int j = 1; j <= i; ++ j) {
			u[a[j]] ++;
		}                    
		for(int cur = 0; cur < i; ++ cur) {
			if(cur) {
				-- u[a[cur]];
			}
			int l = i;
			for(int j = i + 1; j <= n; ++ j) {
				if(u[a[j]]) {                                             
					LL z = j - l - 1ll;
					ans += ((z * (z + 1ll)) >> 1);
					l = j;
				}
			}                                                                                   
			LL z = n - l;
			ans += ((z * (z + 1ll)) >> 1);
		}
	}
	printf("%I64d", ans);
}
