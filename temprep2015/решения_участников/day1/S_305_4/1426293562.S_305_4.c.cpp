#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 5002;
int n, a[N], used[N], u[N], lat;
LL ans;
bool iv[N];
vector<int> all;
int main() {
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	cin >> n;
	for(int i = 1; i <= n; ++ i) {
		cin >> a[i];
	}
	for(int i = 1; i < n; ++ i) {
		int latest = 0;
		if(!iv[a[i]]) {
			iv[a[i]] = 1;
			all.PB(a[i]);
		}
		used[a[i]] ++;
		memset(u, 0, sizeof u);
		for(int j = 1; j <= i; ++ j) {
			u[a[j]] ++;
		}                    
		for(int cur = 0; cur < i; ++ cur) {
			if(cur) {
				-- u[a[cur]];
			}
			for(int check = 1; check <= i; ++ check) {
//				cout << a[check] << " " << u[a[check]] << "-";
			}
//			cout << endl;
			int l = i, r = n + 1;
			for(int j = i + 1; j <= n; ++ j) {
				if(u[a[j]]) {                                             
					LL z = j - l - 1;
//					cout << cur + 1 << " to " << i << " at " << j << " is " << ((z * (z + 1)) >> 1) << endl;
					ans += ((z * (z + 1)) >> 1);
					l = j;
				}
			}                                                                                   
			LL z = n - l;
			ans += ((z * (z + 1)) >> 1);
//			cout << cur + 1 << " to " << i << " at the end is " << ((z * (z + 1)) >> 1) << endl;
		}
	}
	cout << ans;
}
