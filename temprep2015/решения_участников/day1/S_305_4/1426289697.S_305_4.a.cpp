#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 100001;
int n, m, l, r, c;
vector<int> a[N];
int main() {
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	cin >> n >> m;
	m = min(m, 1000);
	while(m --) {
		cin >> l >> r >> c;
		if(c) {
			for(int i = l; i <= r; ++ i) {
				a[i].PB(c);
			}
		}
		else {
			for(int i = l; i <= r; ++ i) {
				a[i].pop_back();
			}
		}
	}
	for(int i = 1; i <= n; ++ i) {
		if(a[i].size()) {
			cout << a[i][a[i].size() - 1] << " ";
		}
		else {
			cout << "0 ";
		}
	}
}