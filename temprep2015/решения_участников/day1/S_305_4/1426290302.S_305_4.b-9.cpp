#include<algorithm>
#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<stack>
#include<deque>
#include<cstring>
#include<ctime>
#include<map>
#include<set>
#define LL long long
#define INF 1000000000
#define BINF 1000000000000000000ll
#define MP make_pair
#define PB push_back
using namespace std;
const int N = 100001;
int k, ans;
string s;
bool used[27];
int main() {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	cin >> s;
	for(int i = 0; i < s.size(); ++ i) {
		if(!used[s[i] - 'a']) {
			ans ++;
		}
		used[s[i] - 'a'] = 1;
	}
	cin >> k;
	if(ans < k) {
		cout << -1;
		return 0;
	}
	if(k < 3) {
		cout << k;
		return 0;
	}
	ans = INF;
	for(int i = 1; i < s.size() - 1; ++ i) {
		if(s[i] != s[i - 1]) {
			memset(used, 0, sizeof used);
			int temp = 2;
			used[s[i - 1] - 'a'] = 1;
			used[s[i] - 'a'] = 1;
			for(int j = i + 1; j < s.size(); ++ j) {
				if(!used[s[j] - 'a']) {
					used[s[j] - 'a'] = 1;
					temp ++;
				}
				if(temp == k) {
					ans = min(ans, j - i + 2);
				}
			}
		}
	}
	cout << ans;
}
