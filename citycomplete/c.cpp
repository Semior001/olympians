#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
using namespace std;

struct compare_length {
    bool operator()(const string &l, const string &r) const {
        return l.size()>r.size();
    }
};

int main() {
	int i, j, n;
	cin >> n;
	vector<string> com(n);
	string name, press;
	map<string, string> com_name;

	for (i=0;i<n;i++) {
		cin >> com[i] >> name;
		com_name[com[i]] = name;
	}
	cin >> press;
	sort(com.begin(), com.end(), compare_length());
	for (i=0;i<press.length();) {
		for (j=0;j<n;j++) {
			if (press.compare(i, com[j].length(), com[j]) == 0) {
				cout << com_name[com[j]] << endl;
				i += com[j].length();
				break;
			}
		}
	}
	return 0;
}
