#include <iostream>
#include <stack>
using namespace std;
const int N = 100005;
int n;
int a[N];
int l[N], r[N];
vector < int > s;
int main(){
	cin >> n;

	for(int i = 1;i <= n;++ i)
		cin >> a[i];

	for(int i = 1;i <= n;++ i){
	    while(!s.empty() && s.top() < a[i])
	    	s.pop();
		l[i] = s.size();
		s.push(a[i]);
	}

	while(!s.empty())
		s.pop();

	for(int i = n;i >= 1;-- i){
	    while(!s.empty() && s.top() < a[i])
	    	s.pop();
		r[i] = s.size();
		s.push(a[i]);
	}

	for(int i = 1;i <= n;++ i)
		cout << l[i] << " " << r[i] << endl;

	return 0;
}
