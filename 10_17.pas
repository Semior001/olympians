uses crt;
var
    t: string;
    words: array[1..50] of string;
    i: integer;
    n: byte; {Количество слов}
{ Перевод текста в стандартный вид }
procedure to_standart;
begin
    while pos('  ',t)>0 do
    begin
        delete(t,pos('  ',t),1);
    end;
end;
{ Разделение текста на массив слов }
procedure wrap_words;
begin
    n:=0;
    while pos(' ',t)>0 do
    begin
        n:=n+1;
        words[n]:=copy(t,1,pos(' ',t));
        delete(t,1,pos(' ',t));
    end;
    n:=n+1;
    words[n]:=copy(t,1,pos('.',t)-1);
end;
{ Задача А }
{procedure ex1;
var
    isNashe: boolean;
    j: integer;
begin
    writeln('Задача А:');
    for i:=1 to n-1 do
    begin
        j:=1;
        isNashe:=true;
        while (j<=Length(words[i]) div 2) and (words[i][j]=words[i][Length(words[i])-j+1]) do
        begin
            if words[i][j]<>words[i][Length(words[i])-j+1] then
                isNashe:=false;
            j:=j+1;
        end;
        if isNashe and (words[i]<>words[n]) then
            write(words[i],' ');
    end;
end;}
{ Задача Б }
procedure ex2;
var
    isNashe: boolean;
    j: integer;
begin
    writeln('Задача 2');
    for i:=1 to n do
    begin
        if pos(words[i][1],copy(words[i],2,Length(words[i])))>0 then
            write(words[i],' ');
    end;
end;
{ Задача С }
procedure ex3;
var
    j: integer;
    isNashe: boolean;
begin
    writeln('Задача 3');
    for i:=1 to n do
    begin
        isNashe:=true;
        for j:=2 to Length(words[i]) do
            if words[i][j]>words[i][j-1] then
                isNashe:=false;
        if isNashe then
            write(words[i],' ');
    end;
end;
{ Основная программа }
begin
    writeln('Text:');
    readln(t);
    to_standart;
    writeln('Отформатированный текст');
    writeln(t);
    wrap_words;
    writeln('Обнаруженные слова');
    for i:=1 to n do
        write(words[i],' ');
    writeln;
    ex2;
    ex3;
end.