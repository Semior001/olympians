#!/usr/bin/python
import os
import sys
import argparse
 
def createParser ():
    parser = argparse.ArgumentParser()
    parser.add_argument ('-t', '--test', default='none')
 
    return parser
 
if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])

os.system("clear")
for i in range(1,41):
    if i<10:
        cmd = './d < indata/0' + str(i) + ' > 0' + str(i) + '.out'
    else:
        cmd = './d < indata/' + str(i) + ' > ' + str(i) + '.out'
    os.system(cmd)
    if i<10:
        my_result = open('0' + str(i) + '.out', 'r')
        result = open('result/0' + str(i) + '.a', 'r')
    else:
        my_result = open(str(i) + '.out', 'r')
        result = open('result/' + str(i) + '.a', 'r')
    a = my_result.read()
    b = result.read()
    if a!=b:
        print(str(i) + " - fail")
    else:
        print(str(i) + " - success")
    my_result.close()
    result.close()
# Чтобы мусор не скапливался в папке   
if(namespace.test!='none'): 
    for i in range(1,41):
        if i<10:
            if(int(namespace.test)==i):
                my_result = open('0' + str(i) + '.out','r')
                result = open('result/0' + str(i) + '.a', 'r')
                a = my_result.read()
                b = result.read()
                print("-----------------------------")
                print(a)
                print(b)
                my_result.close()
                result.close()
            os.system('rm 0' + str(i) + '.out')
        else:
            if(int(namespace.test)==i):
                my_result = open(str(i) + '.out','r')
                result = open('result/' + str(i) + '.a', 'r')
                a = my_result.read()
                b = result.read()
                print("-----------------------------")
                print(a)
                print(b)
                my_result.close()
                result.close()
            os.system('rm ' + str(i) + '.out')
else:
    for i in range(1,41):
        if i<10:
            os.system('rm 0' + str(i) + '.out')
        else:
            os.system('rm ' + str(i) + '.out')