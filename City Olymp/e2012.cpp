#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>
using namespace std;

bool is_prime(int n) {
	for (int i=2;i<=sqrt(n);i++)
		if (n%i==0)
			return false;
	return true;
}

int main(){
	int x;
	cin >> x;
	int i;
	int left,right;
	bool find_prime = false;
	i = x;
	while(!find_prime){
		if(is_prime(i)){
			left = i;
			find_prime = true;
		}
		i--;
	}
	find_prime = false;
	i = x+1;
	while(!find_prime){
		if(is_prime(i)){
			right = i;
			find_prime = true;
		}
		i++;
	}
	if(right-x >= x-left){
		cout << left << endl;
		return 0;
	}else{
		cout << right << endl;
		return 0;
	}
	return 0;
}