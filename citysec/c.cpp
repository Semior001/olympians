#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <ctime>
typedef long long ll;
using namespace std;

int main(){
    unsigned int start_time =  clock(); // начальное время

    ifstream fin("c.in");
    ofstream fout("c.out");
    

    unsigned int search_time = clock() - start_time; // искомое время
    cout << "---------------------" << endl;
    cout<<(double)(search_time)/CLOCKS_PER_SEC<<endl;
    return 0;
}
