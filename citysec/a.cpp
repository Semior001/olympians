#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <ctime>
#include <iomanip>
typedef long long ll;
using namespace std;

int main(){
    unsigned int start_time =  clock(); // начальное время
    ifstream fin("a.in");
    ofstream fout("a.out");
    ll n,i;
    fin >> n;

    ll answer = 9*(9+1)/2;

    ll znak = 0;

    while(n>0){
        znak++;
        n/=10;
    }
    
    for(i = 2; i < znak; i++){
        answer = (answer*(9+i))/(i+1);
    }

    cout << answer << endl;

    unsigned int search_time = clock() - start_time; // искомое время
    cout << "---------------------" << endl;
    cout<<(double)(search_time)/CLOCKS_PER_SEC<<endl;
    return 0;
}
